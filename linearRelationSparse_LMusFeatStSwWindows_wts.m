% Relate Mus feat data to muscle stimulation
clc;
close all;
clear all;

load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSwWindows_RfeatFlip.mat;

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

%%

for z = 1:window_num
    
    %%%%% LEFT %%%%%%%%
    mFeatDataLSt = mallDataLSt{z,1};
    mMusDataLSt = mallDataLSt{z,2};
    
    mFeatDataLSw = mallDataLSw{z,1};
    mMusDataLSw = mallDataLSw{z,2};
    
    % only least square solution using pinv
    wp_LSt = mMusDataLSt*pinv(mFeatDataLSt); % wp is 14X14
    wp_LSw = mMusDataLSw*pinv(mFeatDataLSw); % wp is 14X14
    
    % l1-least square
    wLSt = NaN(size(wp_LSt));
    wLSw = NaN(size(wp_LSw));
    
    ALSt = mFeatDataLSt';
    ALSw = mFeatDataLSw';
    lambdaSt = 0.7; % l1 cost weight
    %lambdaSw = 0.1; % l1 cost weight

    for i = 1:7
        BLSt = mMusDataLSt(i,:)';
        
        %[lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
        %lambdaSt = lambda_max*1e-3
        
        [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt);
        wLSt(i,:) = xLSt';
        
        BLSw = mMusDataLSw(i,:)';
        
        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
        lambdaSw = lambda_max*1e-3;
        
        [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw);
        wLSw(i,:) = xLSw';
    end

    %%%%%%%%%%% Visualize w %%%%%%%%%%%%
    %%% left stance w
%     figure;imagesc(abs(wp_LSt)/(max(max(abs(wp_LSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-leastSq');
%     set(gca(),'YTick',1:7)
%     set(gca(),'YTickLabel',labels_ly);
%     set(gca(),'XTick',1:18)
%     set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

    figure;imagesc(abs(wLSt)/(max(max(abs(wLSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-sparse');
    set(gca(),'YTick',1:7)
    set(gca(),'YTickLabel',labels_ly);
    set(gca(),'XTick',1:18)
    set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

    %%% left swing w
%     figure;imagesc(abs(wp_LSw)/(max(max(abs(wp_LSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-leastSq');
%     set(gca(),'YTick',1:7)
%     set(gca(),'YTickLabel',labels_ly);
%     set(gca(),'XTick',1:18)
%     set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

    figure;imagesc(abs(wLSw)/(max(max(abs(wLSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-sparse');
    set(gca(),'YTick',1:7)
    set(gca(),'YTickLabel',labels_ly);
    set(gca(),'XTick',1:18)
    set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

    keyboard
    
end    