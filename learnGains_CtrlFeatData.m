%% Learn gains for the existing feedback pathways in 2010 NM control

%clc;close all;
%load

%% swing gains

r_mMusDataLSw = NaN(size(mMusDataLSw(1:4,:)));
r_mMusDataRSw = NaN(size(mMusDataRSw(1:4,:)));

%%%% LEFT %%%%%

% HFL
LSw_S_HFL = mMusDataLSw(1,:)';
LSw_Ft_HFL = [ones(1,size(mFeatDataLSw,2));mFeatDataLSw(1,:);mFeatDataLSw(3,:);mFeatDataLSw(6,:)]';

wLSw_HFL = LSw_Ft_HFL\LSw_S_HFL;

r_musDataLSw(1,:) = (wLSw_HFL'*LSw_Ft_HFL');

% GLU
LSw_S_GLU = mMusDataLSw(2,:)';
LSw_Ft_GLU = [ones(1,size(mFeatDataLSw,2));mFeatDataLSw(2,:)]';

wLSw_GLU = LSw_Ft_GLU\LSw_S_GLU;

r_musDataLSw(2,:) = (wLSw_GLU'*LSw_Ft_GLU');

% HAM
LSw_S_HAM = mMusDataLSw(3,:)';
LSw_Ft_HAM = [ones(1,size(mFeatDataLSw,2));mFeatDataLSw(4,:)]';

wLSw_HAM = LSw_Ft_HAM\LSw_S_HAM;

r_musDataLSw(3,:) = (wLSw_HAM'*LSw_Ft_HAM');

% TA
LSw_S_TA = mMusDataLSw(7,:)';
LSw_Ft_TA = [ones(1,size(mFeatDataLSw,2));mFeatDataLSw(5,:)]';

wLSw_TA = LSw_Ft_TA\LSw_S_TA;

r_musDataLSw(4,:) = (wLSw_TA'*LSw_Ft_TA');

%%%% RIGHT %%%%%

% HFL
RSw_S_HFL = mMusDataRSw(1,:)';
RSw_Ft_HFL = [ones(1,size(mFeatDataRSw,2));mFeatDataRSw(1,:);mFeatDataRSw(3,:);mFeatDataRSw(6,:)]';

wRSw_HFL = RSw_Ft_HFL\RSw_S_HFL;

r_musDataRSw(1,:) = (wRSw_HFL'*RSw_Ft_HFL');

% GLU
RSw_S_GLU = mMusDataRSw(2,:)';
RSw_Ft_GLU = [ones(1,size(mFeatDataRSw,2));mFeatDataRSw(2,:)]';

wRSw_GLU = RSw_Ft_GLU\RSw_S_GLU;

r_musDataRSw(2,:) = (wRSw_GLU'*RSw_Ft_GLU');

% HAM
RSw_S_HAM = mMusDataRSw(3,:)';
RSw_Ft_HAM = [ones(1,size(mFeatDataRSw,2));mFeatDataRSw(4,:)]';

wRSw_HAM = RSw_Ft_HAM\RSw_S_HAM;

r_musDataRSw(3,:) = (wRSw_HAM'*RSw_Ft_HAM');

% TA
RSw_S_TA = mMusDataRSw(7,:)';
RSw_Ft_TA = [ones(1,size(mFeatDataRSw,2));mFeatDataRSw(5,:)]';

wRSw_TA = RSw_Ft_TA\RSw_S_TA;

r_musDataRSw(4,:) = (wRSw_TA'*RSw_Ft_TA');

%% stance gains

r_mMusDataLSt = NaN(size(mMusDataLSt));
r_mMusDataRSt = NaN(size(mMusDataRSt));

%%%% LEFT %%%%%

% HFL
LSt_S_HFL = mMusDataLSt(1,:)';
LSt_Ft_HFL = [ones(1,size(mFeatDataLSt,2));(mFeatDataLSt(8,:).*mFeatDataLSt(3,:)./DeltaThRef);(mFeatDataLSt(9,:).*mFeatDataLSt(3,:)./DeltaThRef)]';

wLSt_HFL = LSt_Ft_HFL\LSt_S_HFL;

r_musDataLSt(1,:) = (wLSt_HFL'*LSt_Ft_HFL');

% GLU
LSt_S_GLU = mMusDataLSt(2,:)';
LSt_Ft_GLU = [ones(1,size(mFeatDataLSt,2));mFeatDataLSt(8,:).*mFeatDataLSt(3,:)./DeltaThRef;mFeatDataLSt(9,:).*mFeatDataLSt(3,:)./DeltaThRef]';

wLSt_GLU = LSt_Ft_GLU\LSt_S_GLU;

r_musDataLSt(2,:) = (wLSt_GLU'*LSt_Ft_GLU');

% HAM
LSt_S_HAM = mMusDataLSt(3,:)';
LSt_Ft_HAM = [ones(1,size(mFeatDataLSt,2));mFeatDataLSt(8,:).*mFeatDataLSt(3,:)./DeltaThRef;mFeatDataLSt(9,:).*mFeatDataLSt(3,:)./DeltaThRef]';

wLSt_HAM = LSt_Ft_HAM\LSt_S_HAM;

r_musDataLSt(3,:) = (wLSt_HAM'*LSt_Ft_HAM');

% VAS
LSt_S_VAS = mMusDataLSt(4,:)';
LSt_Ft_VAS = [ones(1,size(mFeatDataLSt,2));mFeatDataLSt(4,:);mFeatDataLSt(1,:)]';

wLSt_VAS = LSt_Ft_VAS\LSt_S_VAS;

r_musDataLSt(4,:) = (wLSt_VAS'*LSt_Ft_VAS');
r_musDataLSt(4,:) = (LSt_Ft_VAS*wLSt_VAS);
%r_musDataLSt_original = (wLSt_VAS_original'*LSt_Ft_VAS');

% GAS
LSt_S_GAS = mMusDataLSt(5,:)';
LSt_Ft_GAS = [ones(1,size(mFeatDataLSt,2));mFeatDataLSt(5,:)]';

wLSt_GAS = LSt_Ft_GAS\LSt_S_GAS;

r_musDataLSt(5,:) = (wLSt_GAS'*LSt_Ft_GAS');

% SOL
LSt_S_SOL = mMusDataLSt(6,:)';
LSt_Ft_SOL = [ones(1,size(mFeatDataLSt,2));mFeatDataLSt(6,:)]';

wLSt_SOL = LSt_Ft_SOL\LSt_S_SOL;

r_musDataLSt(6,:) = (wLSt_SOL'*LSt_Ft_SOL');

% TA
LSt_S_TA = mMusDataLSt(7,:)';
LSt_Ft_TA = [ones(1,size(mFeatDataLSt,2));mFeatDataLSt(7,:);mFeatDataLSt(6,:)]';

wLSt_TA = LSt_Ft_TA\LSt_S_TA;

r_musDataLSt(7,:) = (wLSt_TA'*LSt_Ft_TA');
%r_musDataLSt_originalTA = (wLSt_TA_original'*LSt_Ft_TA');

%%%% RIGHT %%%%%

% HFL
RSt_S_HFL = mMusDataRSt(1,:)';
RSt_Ft_HFL = [ones(1,size(mFeatDataRSt,2));(mFeatDataRSt(8,:).*mFeatDataRSt(3,:)./DeltaThRef);(mFeatDataRSt(9,:).*mFeatDataRSt(3,:)./DeltaThRef)]';

wRSt_HFL = RSt_Ft_HFL\RSt_S_HFL;

r_musDataRSt(1,:) = (wRSt_HFL'*RSt_Ft_HFL');

% GLU
RSt_S_GLU = mMusDataRSt(2,:)';
RSt_Ft_GLU = [ones(1,size(mFeatDataRSt,2));mFeatDataRSt(8,:).*mFeatDataRSt(3,:)./DeltaThRef;mFeatDataRSt(9,:).*mFeatDataRSt(3,:)./DeltaThRef]';

wRSt_GLU = RSt_Ft_GLU\RSt_S_GLU;

r_musDataRSt(2,:) = (wRSt_GLU'*RSt_Ft_GLU');

% HAM
RSt_S_HAM = mMusDataRSt(3,:)';
RSt_Ft_HAM = [ones(1,size(mFeatDataRSt,2));mFeatDataRSt(8,:).*mFeatDataRSt(3,:)./DeltaThRef;mFeatDataRSt(9,:).*mFeatDataRSt(3,:)./DeltaThRef]';

wRSt_HAM = RSt_Ft_HAM\RSt_S_HAM;

r_musDataRSt(3,:) = (wRSt_HAM'*RSt_Ft_HAM');

% VAS
RSt_S_VAS = mMusDataRSt(4,:)';
RSt_Ft_VAS = [ones(1,size(mFeatDataRSt,2));mFeatDataRSt(4,:);mFeatDataRSt(1,:)]';

wRSt_VAS = RSt_Ft_VAS\RSt_S_VAS;

r_musDataRSt(4,:) = (wRSt_VAS'*RSt_Ft_VAS');

% GAS
RSt_S_GAS = mMusDataRSt(5,:)';
RSt_Ft_GAS = [ones(1,size(mFeatDataRSt,2));mFeatDataRSt(5,:)]';

wRSt_GAS = RSt_Ft_GAS\RSt_S_GAS;

r_musDataRSt(5,:) = (wRSt_GAS'*RSt_Ft_GAS');

% SOL
RSt_S_SOL = mMusDataRSt(6,:)';
RSt_Ft_SOL = [ones(1,size(mFeatDataRSt,2));mFeatDataRSt(6,:)]';

wRSt_SOL = RSt_Ft_SOL\RSt_S_SOL;

r_musDataRSt(6,:) = (wRSt_SOL'*RSt_Ft_SOL');

% TA
RSt_S_TA = mMusDataRSt(7,:)';
RSt_Ft_TA = [ones(1,size(mFeatDataRSt,2));mFeatDataRSt(7,:);mFeatDataRSt(6,:)]';

wRSt_TA = RSt_Ft_TA\RSt_S_TA;

r_musDataRSt(7,:) = (wRSt_TA'*RSt_Ft_TA');

%% Reconstruction error plots

figure;
subplot(7,1,1); plot(mMusDataLSt(1,:)');hold on
subplot(7,1,1); plot(r_musDataLSt(1,:)');
title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
legend('TrueData','Reconstructed-Train');

subplot(7,1,2); plot(mMusDataLSt(2,:)');hold on
subplot(7,1,2); plot(r_musDataLSt(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(mMusDataLSt(3,:)');hold on
subplot(7,1,3); plot(r_musDataLSt(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(mMusDataLSt(4,:)');hold on
subplot(7,1,4); plot(r_musDataLSt(4,:)');ylabel('LVAS');
%subplot(7,1,4); plot(r_musDataLSt_original);

subplot(7,1,5); plot(mMusDataLSt(5,:)');hold on
subplot(7,1,5); plot(r_musDataLSt(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(mMusDataLSt(6,:)');hold on
subplot(7,1,6); plot(r_musDataLSt(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(mMusDataLSt(7,:)');hold on
subplot(7,1,7); plot(r_musDataLSt(7,:)');ylabel('LTA');
%subplot(7,1,7); plot(r_musDataLSt_originalTA);


figure;
subplot(7,1,1); plot(mMusDataRSt(1,:)');hold on
subplot(7,1,1); plot(r_musDataRSt(1,:)');
title('Right muscles STANCE reconstructed (using 2-leg Mus Feat)'); ylabel('RHFL');
legend('TrueData','Reconstructed-Train');

subplot(7,1,2); plot(mMusDataRSt(2,:)');hold on
subplot(7,1,2); plot(r_musDataRSt(2,:)');ylabel('RGLU');

subplot(7,1,3); plot(mMusDataRSt(3,:)');hold on
subplot(7,1,3); plot(r_musDataRSt(3,:)');ylabel('RHAM');

subplot(7,1,4); plot(mMusDataRSt(4,:)');hold on
subplot(7,1,4); plot(r_musDataRSt(4,:)');ylabel('RVAS');

subplot(7,1,5); plot(mMusDataRSt(5,:)');hold on
subplot(7,1,5); plot(r_musDataRSt(5,:)');ylabel('RGAS');

subplot(7,1,6); plot(mMusDataRSt(6,:)');hold on
subplot(7,1,6); plot(r_musDataRSt(6,:)');ylabel('RSOL');

subplot(7,1,7); plot(mMusDataRSt(7,:)');hold on
subplot(7,1,7); plot(r_musDataRSt(7,:)');ylabel('RTA');

figure;
subplot(4,1,1); plot(mMusDataLSw(1,:)');hold on
subplot(4,1,1); plot(r_musDataLSw(1,:)');
title('Left muscles SWING reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
legend('TrueData','Reconstructed-Train');

subplot(4,1,2); plot(mMusDataLSw(2,:)');hold on
subplot(4,1,2); plot(r_musDataLSw(2,:)');ylabel('LGLU');

subplot(4,1,3); plot(mMusDataLSw(3,:)');hold on
subplot(4,1,3); plot(r_musDataLSw(3,:)');ylabel('LHAM');

subplot(4,1,4); plot(mMusDataLSw(7,:)');hold on
subplot(4,1,4); plot(r_musDataLSw(4,:)');ylabel('LTA');

figure;
subplot(4,1,1); plot(mMusDataRSw(1,:)');hold on
subplot(4,1,1); plot(r_musDataRSw(1,:)');
title('Right muscles SWING reconstructed (using 2-leg Mus Feat)'); ylabel('RHFL');
legend('TrueData','Reconstructed-Train');

subplot(4,1,2); plot(mMusDataRSw(2,:)');hold on
subplot(4,1,2); plot(r_musDataRSw(2,:)');ylabel('RGLU');

subplot(4,1,3); plot(mMusDataRSw(3,:)');hold on
subplot(4,1,3); plot(r_musDataRSw(3,:)');ylabel('RHAM');

subplot(4,1,4); plot(mMusDataRSw(7,:)');hold on
subplot(4,1,4); plot(r_musDataRSw(4,:)');ylabel('RTA');

