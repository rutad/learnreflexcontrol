clc
clear all;
close all;

load linRel_13ms_NMwalkingRoughGrd_100sSimData_musFeatStSw.mat;
numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;

K = 5; % K-fold validation size
testNum = 4; % number of test samples

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end);%LTO_ind(2:end-1);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end-1);%RTO_ind(3:end);
RswingEnd_ind = RgaitEnd_ind;

%% K-fold intervals

KDatalen = length(LgaitStart_ind) - testNum;
Klen = KDatalen/K;
Kfold_int = NaN(K,Klen);

intStart = 1;
for i=1:K   
    intEnd = intStart +Klen-1;
    Kfold_int(i,:) = linspace(intStart, intEnd,Klen);
    intStart = intEnd+1;
end


%% Test data 

testDataInd = [(KDatalen+1):length(LgaitStart_ind)];

%% Cross validated l1-ls
lambda_min = 1e-4; % l1_ls suggested min lambda
numCV = 10; %100 % number of lambda values tried

% CV error
errLSt = NaN(numMusFeatures/2,numCV,K);
errLSw = NaN(numMusFeatures/2,numCV,K);

% CV params
lambdaLSt = NaN(numMusFeatures/2, numCV, K);
lambdaLSw = NaN(numMusFeatures/2, numCV, K);

for zz = 1:K
    holdDataInd = Kfold_int(zz,:);
    trainDataInd = Kfold_int;
    trainDataInd(zz,:)=[];
    trainDataInd = trainDataInd(:);
    
    LStim = [LStimHFL; LStimGLU; LStimHAM; LStimVAS; LStimGAS; LStimSOL; LStimTA];
    RStim = [RStimHFL; RStimGLU; RStimHAM; RStimVAS; RStimGAS; RStimSOL; RStimTA];   
    LFeat = [LL_HFL; LF_GLU; LL_HAM; LF_HAM; LF_VAS; LF_GAS; LF_SOL;LL_TA];
    RFeat = [RL_HFL; RF_GLU; RL_HAM; RF_HAM; RF_VAS; RF_GAS; RF_SOL;RL_TA];
    
    [FeatDataLSt, FeatDataRSt, FeatDataLSw, FeatDataRSw, MusDataLSt,MusDataRSt, MusDataLSw, MusDataRSw] = ...
    collectGaitDataMusFeat_CVRoughGnd_Train(trainDataInd,holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind);

    [holdFeatDataLSt, holdFeatDataRSt, holdFeatDataLSw, holdFeatDataRSw, holdMusDataLSt,holdMusDataRSt, holdMusDataLSw, holdMusDataRSw] = ...
    collectGaitDataMusFeat_CVRoughGnd_Hold(holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind);

    %%% LEFT %%%
    ALSt = FeatDataLSt;
    ALSw = FeatDataLSw;
    
    for i = 1:numMusFeatures/2
        BLSt = MusDataLSt(:,i);

        [lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);  % l1_ls suggested max lambda
        lambdaLSt(i,:,zz) = linspace(lambda_max*lambda_min, lambda_max, numCV);

        % CV left stance
        for l = 1:numCV
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaLSt(i,l,zz),[],'quiet'); 
            errLSt(i,l,zz) = mean((holdMusDataLSt(:,i)-holdFeatDataLSt*xLSt).^2);
        end   

        BLSw = MusDataLSw(:,i);

        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);  % l1_ls suggested max lambda
        lambdaLSw(i,:,zz) = linspace(lambda_max*lambda_min, lambda_max, numCV);

        % CV left swing
        for l = 1:numCV
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaLSw(i,l,zz),[],'quiet');        
            errLSw(i,l,zz) = mean((holdMusDataLSw(:,i)-holdFeatDataLSw*xLSw).^2);
        end  

    end

end    

%% visualize CV error

for mm =  1:numMusFeatures/2
    str1 = strcat('LSt CV error Mus',num2str(mm)); 
    str2 = strcat('LSw CV error Mus',num2str(mm));
    
    figure(mm); hold on;title(str1); xlabel('lambda index'); ylabel('CV error');
    figure(mm+numMusFeatures/2); hold on;title(str2); xlabel('lambda index'); ylabel('CV error');
    
    for pp = 1:numCV
       err1 = mean(errLSt(mm,pp,:));
       err2 = mean(errLSw(mm,pp,:));
       
       figure(mm);plot(pp,err1,'*');
       figure(mm+numMusFeatures/2);plot(pp,err2,'*');
    end
    
    keyboard
end

