%% run causation experiment script

clc;clear all;close all;

all_fDisturb = [25,50,150];

for do = 1:length(all_fDisturb)
    fDisturb = all_fDisturb(do);
    saveName = strcat('causationExp_meanDisturbWaitLSt_musFeat_F',num2str(fDisturb),'.mat');
    causationDisturbExp_musFeatSt;
    
    save (saveName, 'WStore', 'mean_delWLSt', 'mean_wDistLSt', 'tDisturb','fDisturb')
    
    
end