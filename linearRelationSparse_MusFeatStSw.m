% Relate kinematics data to muscle stimulation
clc;
close all;

load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSw.mat;

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% only least square solution using pinv
wp_St = mMusDataSt*pinv(mFeatDataSt); % wp is 14X14
wp_Sw = mMusDataSw*pinv(mFeatDataSw); % wp is 14X14

% l1-least square
wSt = NaN(size(wp_St));
wSw = NaN(size(wp_Sw));

ASt = mFeatDataSt';
ASw = mFeatDataSw';
lambda = 0.7; % l1 cost weight

for i = 1:14
    BSt = mMusDataSt(i,:)';
    [xSt,~] = l1_ls(ASt,BSt,lambda);

    wSt(i,:) = xSt';
    
    BSw = mMusDataSw(i,:)';
    [xSw,~] = l1_ls(ASw,BSw,lambda);

    wSw(i,:) = xSw';
end

%% Test weight w on a particular sample gait        

t = randi(testNum,1,1); % index of test sample

% stance
testFeatSt = [tLL_HFLDataSt(t,:); tLF_GLUDataSt(t,:); tLL_HAMDataSt(t,:); tLF_HAMDataSt(t,:); tLF_VASDataSt(t,:); tLF_GASDataSt(t,:); tLF_SOLDataSt(t,:); tLL_TADataSt(t,:); tTrunkDataSt(t,:); ...
            tRL_HFLDataSt(t,:); tRF_GLUDataSt(t,:); tRL_HAMDataSt(t,:); tRF_HAMDataSt(t,:); tRF_VASDataSt(t,:); tRF_GASDataSt(t,:); tRF_SOLDataSt(t,:); tRL_TADataSt(t,:); tdTrunkDataSt(t,:)];
       
trueMusSt = [tLHFLDataSt(t,:); tLGLUDataSt(t,:); tLHAMDataSt(t,:); tLVASDataSt(t,:); tLGASDataSt(t,:); tLSOLDataSt(t,:); tLTADataSt(t,:); ...
            tRHFLDataSt(t,:); tRGLUDataSt(t,:); tRHAMDataSt(t,:); tRVASDataSt(t,:); tRGASDataSt(t,:); tRSOLDataSt(t,:); tRTADataSt(t,:)]; 
 
testMusSt = wSt*testFeatSt;        
testMusp_St = wp_St*testFeatSt;

% swing
testFeatSw = [tLL_HFLDataSw(t,:); tLF_GLUDataSw(t,:); tLL_HAMDataSw(t,:); tLF_HAMDataSw(t,:); tLF_VASDataSw(t,:); tLF_GASDataSw(t,:); tLF_SOLDataSw(t,:); tLL_TADataSw(t,:); tTrunkDataSw(t,:); ...
            tRL_HFLDataSw(t,:); tRF_GLUDataSw(t,:); tRL_HAMDataSw(t,:); tRF_HAMDataSw(t,:); tRF_VASDataSw(t,:); tRF_GASDataSw(t,:); tRF_SOLDataSw(t,:); tRL_TADataSw(t,:); tdTrunkDataSw(t,:)];
       
trueMusSw = [tLHFLDataSw(t,:); tLGLUDataSw(t,:); tLHAMDataSw(t,:); tLVASDataSw(t,:); tLGASDataSw(t,:); tLSOLDataSw(t,:); tLTADataSw(t,:); ...
            tRHFLDataSw(t,:); tRGLUDataSw(t,:); tRHAMDataSw(t,:); tRVASDataSw(t,:); tRGASDataSw(t,:); tRSOLDataSw(t,:); tRTADataSw(t,:)]; 
 
testMusSw = wSw*testFeatSw;        
testMusp_Sw = wp_Sw*testFeatSw;

%% visualize stance
cd linRel_Results/

figure;
subplot(7,1,1); plot(testMusSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_St(1,:)');
subplot(7,1,1); plot(trueMusSt(1,:)');
title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_St(2,:)');
subplot(7,1,2); plot(trueMusSt(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_St(3,:)');
subplot(7,1,3); plot(trueMusSt(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_St(4,:)');
subplot(7,1,4); plot(trueMusSt(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_St(5,:)');
subplot(7,1,5); plot(trueMusSt(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_St(6,:)');
subplot(7,1,6); plot(trueMusSt(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_St(7,:)');
subplot(7,1,7); plot(trueMusSt(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_LMusFeatSt.pdf

figure;
subplot(7,1,1); plot(testMusSt(8,:)');hold on
subplot(7,1,1); plot(testMusp_St(8,:)');
subplot(7,1,1); plot(trueMusSt(8,:)');
title('Right muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusSt(9,:)');hold on
subplot(7,1,2); plot(testMusp_St(9,:)');
subplot(7,1,2); plot(trueMusSt(9,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusSt(10,:)');hold on
subplot(7,1,3); plot(testMusp_St(10,:)');
subplot(7,1,3); plot(trueMusSt(10,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusSt(11,:)');hold on
subplot(7,1,4); plot(testMusp_St(11,:)');
subplot(7,1,4); plot(trueMusSt(11,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusSt(12,:)');hold on
subplot(7,1,5); plot(testMusp_St(12,:)');
subplot(7,1,5); plot(trueMusSt(12,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusSt(13,:)');hold on
subplot(7,1,6); plot(testMusp_St(13,:)');
subplot(7,1,6); plot(trueMusSt(13,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusSt(14,:)');hold on
subplot(7,1,7); plot(testMusp_St(14,:)');
subplot(7,1,7); plot(trueMusSt(14,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_RMusFeatSt.pdf

%% visualize swing 

figure;
subplot(7,1,1); plot(testMusSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_Sw(1,:)');
subplot(7,1,1); plot(trueMusSw(1,:)');
title('Left muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_Sw(2,:)');
subplot(7,1,2); plot(trueMusSw(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_Sw(3,:)');
subplot(7,1,3); plot(trueMusSw(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_Sw(4,:)');
subplot(7,1,4); plot(trueMusSw(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_Sw(5,:)');
subplot(7,1,5); plot(trueMusSw(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_Sw(6,:)');
subplot(7,1,6); plot(trueMusSw(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_Sw(7,:)');
subplot(7,1,7); plot(trueMusSw(7,:)');ylabel('LTA');

export_fig -transparent reconstructSparse_LMusFeatSw.pdf

figure;
subplot(7,1,1); plot(testMusSw(8,:)');hold on
subplot(7,1,1); plot(testMusp_Sw(8,:)');
subplot(7,1,1); plot(trueMusSw(8,:)');
title('Right muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusSw(9,:)');hold on
subplot(7,1,2); plot(testMusp_Sw(9,:)');
subplot(7,1,2); plot(trueMusSw(9,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusSw(10,:)');hold on
subplot(7,1,3); plot(testMusp_Sw(10,:)');
subplot(7,1,3); plot(trueMusSw(10,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusSw(11,:)');hold on
subplot(7,1,4); plot(testMusp_Sw(11,:)');
subplot(7,1,4); plot(trueMusSw(11,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusSw(12,:)');hold on
subplot(7,1,5); plot(testMusp_Sw(12,:)');
subplot(7,1,5); plot(trueMusSw(12,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusSw(13,:)');hold on
subplot(7,1,6); plot(testMusp_Sw(13,:)');
subplot(7,1,6); plot(trueMusSw(13,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusSw(14,:)');hold on
subplot(7,1,7); plot(testMusp_Sw(14,:)');
subplot(7,1,7); plot(trueMusSw(14,:)');ylabel('RTA');

export_fig -transparent reconstructSparse_RMusFeatSw.pdf

%% Visualize w

labels_y = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA','RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_x = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

% stance
figure;imagesc(abs(wp_St)/(max(max(abs(wp_St)))));colorbar;title('Leg weights STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);

figure;imagesc(abs(wSt)/(max(max(abs(wSt)))));colorbar;title('Leg weights STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);

% swing
figure;imagesc(abs(wp_Sw)/(max(max(abs(wp_Sw)))));colorbar;title('Leg weights SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);

figure;imagesc(abs(wSw)/(max(max(abs(wSw)))));colorbar;title('Leg weights SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);

cd ..