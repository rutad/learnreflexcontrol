%% 4 segment muscle walker params
% 22nd December 2014


% gravity
g = 9.81;

% ********************* %
% 1. BIPED SEGMENTATION %
% ********************* %


% -------------------------
% 1.1 General Shank Segment
% -------------------------

% Note: CS1 is position of ankle joint.

% shank length
L_S = 0.5; %[m]

% distance CS1 to CG (local center of gravity)
D1G_S = 0.3; %[m]

% distance CS1 to C2 (knee joint)
D12_S = L_S; %[m]

% shank mass
m_S  = 3.5; %[kg]

% shank inertia with respect to axis ankle-CG-knee (scaled from other papers)
In_S = 0.05; %[kg*m^2]



% -------------------------
% 1.2 General Thigh Segment
% -------------------------

% Note: CS1 is position of knee joint.

% total thigh length
L_T = 0.5; %[m]

% distance CS1 to CG (local center of gravity)
D1G_T = 0.3; %[m] 

% distance CS1 to C2 (hip joint)
D12_T = L_T; %[m]

% thigh mass
m_T  = 8.5; %[kg]

% thigh inertia with respect to axis ball-CG-heel (scaled from other papers)
In_T = 0.15; %[kg*m^2]

% ****************************** %
% 2. BIPED MUSCLE-SKELETON-LINKS %
% ****************************** %


% VAStus group attachement
rVAS       =       0.06; % [m]   constant lever contribution 
phimaxVAS  = 165*pi/180; % [rad] angle of maximum lever contribution
phirefVAS  = 125*pi/180; % [rad] reference angle at which MTU length equals 
rhoVAS     =        0.7; %       sum of lopt and lslack 
                       

% GLUtei group attachement
rGLU       =       0.10; % [m]   constant lever contribution 
phirefGLU  = 150*pi/180; % [rad] reference angle at which MTU length equals 
rhoGLU     =        0.5; %       sum of lopt and lslack 
                         
% Hip FLexor group attachement
rHFL       =       0.10; % [m]   constant lever contribution 
phirefHFL  = 180*pi/180; % [rad] reference angle at which MTU length equals 
rhoHFL     =        0.5; %       sum of lopt and lslack 