function [sys,x0] = AnimMan3D(t,x,u, flag,ts); 

%
% AnimNeuroHuman.m: S-function animating the neuromechanical
%                   Biped Model 
%																						                          
%	                              
% H. Geyer 
% (last modified: 10 August 2006) 				                                  
%





% **************** %
% DECLARATION PART %
% **************** %


% Global Variables
% ----------------

global  FigHndl         ... % figure handle
        ViewShiftParams ... % view window 'is in shift' flag
        SphereObjects   ... % 3D sphere objects (contact points, joints)
        ConeObjects     ... % 3D cone objects (bones)
        MuscleObjects   ... % 3D muscle objects
        AviObj          ... % AVI recording object
        zPlates             % ground plate levels 
        
       
        
% Option Flags
% ------------

% recording flag 
RecordAVIflag = 0;

% snapshot saving flag
SnapShotFlag = 0;

% color coding active flag
MuscleColChange = 1;

% show author text
ShowAuthorText = 1;
        

% AVI-Recording
% -------------

% avi properties
AVIname    = 'Man.avi';
AVIcodec   = 'none';%'Cinepak';
AVIquality = 50;
AVIfps     = 30;


% Axes and Figure Options
% -----------------------
        
% view window size 
ViewWin   =   10;%7;%12; %[m]
TolFrac   = 1/50; %[ViewWin]
tShiftTot =  0*1; %[s] total time of view window shift

% figure name (identifier)
FigName = 'NM Walker';


% 3D Sphere Objects
% -----------------

% sphere resolutions
SphereRes = 15;

% contact point radius
rCP  = 0.03; %[m]

% joint radii (ankle, knee, and hip)
rAJ  = 0.04; %[m]
rKJ  = 0.05; %[m]
rHJ  = 0.09; %[m]

% y-shift (arbitrary, since sagittal model displayed with 3D objects)
yShift = rHJ; %[m]

% colors (joints, contact points, and HAT)
JointCol     = [0.8 0.8 1];
ContPointCol = [0   0   1];


% 3D Cone Objects
% ---------------

% cone resolution
ConeRes = 10;

% cone radii (bottom and top)
rFoot     = [rCP rCP]-0.01; %[m]
rShank    = [rAJ-0.02 rAJ-0.02 rAJ-0.02 rAJ-0.02 rAJ-0.01 rAJ]; %[m]
rThigh    = [rKJ-0.01 rKJ-0.02 rKJ-0.02 rKJ-0.01 rKJ rKJ+0.01]; %[m]
%rHAT_Cone = [rHJ-0.02 rHJ-0.02 0.05 0.05 rHJ-0.01 rHJ-0.01 0];  %[m] female
rHAT_Cone = [rHJ-0.02 rHJ-0.02 0.06 0.07 rHJ-0.01 rHJ-0.01 0]; %[m] male

% cone color
ConeCol  = [0.9 0.9 0.8];


% 3D Muscle Objects
% -----------------

% muscle resolution
MuscleRes = 10;

% muscle radii (tendon belly tendon)
rSOL  = [0.01 0.01 0.015 0.015 0.015 0.01 0.01];
rGAS  = rSOL;
rTA   = [0.01 0.01 0.012 0.012 0.012 0.01 0.01];
rQF   = [0.01 0.01 0.025 0.025 0.025 0.025 0.01];
rHFL  = rSOL;
rHAM  = rSOL;
rGLU  = rSOL;
rBFsh = [0.01 0.01];



% Muscle Color Coding
% -------------------

% muscle color
if MuscleColChange
  MuscleCol       = [0.9 0.7 0.7];
  MuscleActiveCol = [0.9 0   0  ];
else
  MuscleCol = [0.9 0 0];
end





% 3D Walkway
% ----------

% total walkway length
WayLength = 100; %[m]

% length of one ground plate
PlateLength = 1;%0.80; %[m]

% plate number
nPlates = ceil(WayLength/PlateLength);

% walkway color
WayCol = [0.95 0.95 1];





% ************ %
% PROGRAM PART %
% ************ %

switch flag,
  
  

  % -------------- %
  % Initialization %
  % -------------- %
  
  case 0,
     
    
    % Initialize Figure
    % -----------------
    
    % initialize animation figure
    Anim_Init(FigName);
    
    % store figure handle for repeated access
    FigHndl = findobj('Type', 'figure',  'Name', FigName);

    % set figure axis range zmin: -0.05
    axis([0-ViewWin/5 0+ViewWin*4/5 -0.5 0.5 -0.05 2]);
    
    axis([8.5-ViewWin 8.5 -0.5 0.5 -0.05 1.8]);
    
    % init view shift parameters:  1. shift flag, 2. shift start time, and
    %                              3. actual shift distance
    ViewShiftParams = [0 0 0];
    
    % switch off the y- and z-axis
    set(gca, 'YColor', [1 1 1], 'ZColor', [1 1 1])
    
    % set x-axis labels
    set(gca, 'XTick', -10:1:100)
       
    %view(-1,0)
    view(25,20)
    
    % include author text
    if ShowAuthorText
      
      AuthorText = text(1, 0, 0, '(H. Geyer, 2oo6)', 'Units', 'Normalized');
      
      set(AuthorText, 'Color', [0.8 0.8 0.8], 'HorizontalAlignment', 'Right', ...
                      'VerticalAlignment', 'Top', 'FontName' , 'Arial', ...
                      'FontSize', 8, 'FontUnits', 'Normalized', 'FontWeight', 'Normal')           
    end
    
    
    % Generate 3D Objects
    % -------------------
    
    % create sphere objects (contact points and joints and HAT top)
    SphereObjects = CreateSphereObjects(SphereRes, yShift, rCP, rAJ, ...
                                        rKJ, rHJ, ContPointCol, JointCol);
    
    % create cone objects (bones)
    ConeObjects = CreateConeObjects(ConeRes, yShift, rFoot, rShank, ...
                                    rThigh, rHAT_Cone, ConeCol);
    
    % create tube objects (muscles)
    MuscleObjects = CreateMuscleObjects(MuscleRes, yShift, rSOL, rGAS, ...
                            rTA, rQF, rHFL, rHAM, rGLU, rBFsh, MuscleCol);
    
    % create walkway
    zPlates = CreateWalkwayObject(WayLength, PlateLength, nPlates, WayCol, rCP);
   
    
        
    % Set Scene Lighting
    % ------------------
                                    
    % lighting goraud (much faster than lghting phong)
    lighting gouraud
  
    % camlights on
    camlight
    
  
    % AVI Recording
    % -------------
    
    if RecordAVIflag
       
      % switch axes off
      axis 'off'
   
      % create AVI object
      AviObj = [];
      AviObj = RecordAVI(flag, AviObj, AVIname, AVIcodec, AVIquality, AVIfps); 
    end
    
    
    % SnapShot Images
    % ---------------
    
    if SnapShotFlag
      
      % switch axes off
      axis 'off'
      
    end
    
    
    % Simulink IO Setup
    % -----------------
    
    % set IO-data: .  .  .  number of Simulink "u(i)" - inputs  .  .
    sys=[0 22 4 30 0 0];
    
    % set initial conditions (ankle joint x an y)
    x0=[zeros(1,22)];

   

  % ------------
  % Modification
  % ------------
  
  case 2, 

    % search root for FigHndl
    if any( get(0,'Children') == FigHndl )
      
      % check handle validity 
      if strcmp(  get( FigHndl,'Name' ), FigName  )
        
        % set actual figure to handle
        set(0, 'CurrentFigure', FigHndl);
        
        % Check if view window is out of sight. If so, shift it
        ViewShiftParams = CheckViewWin( u, t, ViewWin, TolFrac, ...
                                        ViewShiftParams, tShiftTot);
        
        %xlim(u(9)+[-1 1]);
        

        % Switch on all objects
        % ---------------------
        
        if t==0
          set(SphereObjects, 'Visible', 'on')
          set(ConeObjects,   'Visible', 'on')
          set(MuscleObjects, 'Visible', 'on')
          
          % switch inactive BFsH
          set(MuscleObjects(8), 'Visible', 'off')
          
        end
        
        
        % Update 3D-Objects Position and Orientation
        % ------------------------------------------
        
        UpdateSphereObjects(SphereObjects, u, x)
        UpdateConeObjects( ConeObjects, u, x, t);
        UpdateMuscleObjects( MuscleObjects, u, x, t);
        
        if MuscleColChange
          UpdateMuscleColor(MuscleObjects(1:8), u(23:30), MuscleCol, MuscleActiveCol)
        end

        
        % Update Figure
        % -------------
        
        drawnow
        
        
        % AVI Recording
        % -------------
   
        if RecordAVIflag
          AviObj = RecordAVI(flag, AviObj, AVIname, AVIcodec, AVIquality, AVIfps); 
        end
    
        
        % Image Savings
        if SnapShotFlag
          
          % check for image saving event
          if (t>5) & (t<15.5)
              %%% if 1
              %%% !!!! virtualdub??
          
            CurrentImage = getframe(gcf);
            ImgName = ['SnapShots\Shot_' num2str( t*1e3,'%05.0f') '.jpg'];
            imwrite(CurrentImage.cdata, ImgName, 'jpg', 'Quality', 100);
            
          end
          
        end
        
        
      end 
    end

    
    
    % remember all inputs in next call of S-Fct
    sys = [u(1:22)]; 



  % -------------------------
  % Return Values to Simulink
  % -------------------------
  
  case 3,     
  
    % calculate ground levels
    GroundLevels = zPlates( max([0 0 0 0]', floor(x([1 3 11 13])/PlateLength)) +1 );
  
    % return ground levels
    sys = GroundLevels'; 

 
    
  % ---------------------------
  % Calculate Next Calling Time
  % ---------------------------
  
  case 4,

    % calculate next calling time
  	sys = t + ts;


  % -----------------
  % End Of Simulation
  % -----------------
  
  case 9,
    
    
    % AVI Recording
    % -------------

    if RecordAVIflag
      AviObj = RecordAVI(flag, AviObj, AVIname, AVIcodec, AVIquality, AVIfps); 
    end
    
    % clean up
    sys = []; 
    
    

  otherwise
    error(['Unhandled flag = ',num2str(flag)]); % flag error handling


end %switch

end








% ************* %
% FUNCTION PART %
% ************* %



function SphereObjects = CreateSphereObjects(SphereRes, yShift, rCP, ...
                                    rAJ, rKJ, rHJ, ContPointCol, ...
                                    JointCol);

  JointCol     = [0.8 0.8 1];
  ContPointCol = [0   0   1];

  JointCol2     = [0.9 0.9 1];
  ContPointCol2 = [0.7 0.7 1];

  % general sphere
  [x,y,z] = sphere(SphereRes);
  
  % make right and left ball contact points and set their properties
  R_Ball = surf(rCP*x, rCP*y-yShift, rCP*z, 'FaceColor', ContPointCol);
  L_Ball = surf(rCP*x, rCP*y+yShift, rCP*z, 'FaceColor', ContPointCol2);
  
  % make right and left heel contact points and set their properties
  R_Heel = surf(rCP*x, rCP*y-yShift, rCP*z, 'FaceColor', ContPointCol);
  L_Heel = surf(rCP*x, rCP*y+yShift, rCP*z, 'FaceColor', ContPointCol2);
  
  % make right and left ankle joint objects and set their properties
  R_AJ = surf(rAJ*x, rAJ*y-yShift, rAJ*z, 'FaceColor', JointCol);
  L_AJ = surf(rAJ*x, rAJ*y+yShift, rAJ*z, 'FaceColor', JointCol2);
    
  % make right and left knee joint objects and set their properties
  R_KJ = surf(rKJ*x, rKJ*y-yShift, rKJ*z, 'FaceColor', JointCol);
  L_KJ = surf(rKJ*x, rKJ*y+yShift, rKJ*z, 'FaceColor', JointCol2);
  
  % make right and left hip joint objects and set their properties
  R_HJ = surf(rHJ*x, rHJ*y-yShift, rHJ*z, 'FaceColor', JointCol);
  L_HJ = surf(rHJ*x, rHJ*y+yShift, rHJ*z, 'FaceColor', JointCol2);
  
  % change material properties of preceeding objects to shiny
  material shiny
 
  % generate objects vector
  SphereObjects = [R_Ball; R_Heel; R_AJ; R_KJ; R_HJ; ...
                   L_Ball; L_Heel; L_AJ; L_KJ; L_HJ];
  
  % set general properties                 
  set(SphereObjects, 'Visible', 'off', 'EdgeColor', 'none', ...
                     'BackFaceLighting', 'unlit')
                    
end  



function ConeObjects = CreateConeObjects(ConeRes, yShift, rFoot, rShank, ...
                                           rThigh, rHAT_Cone, ConeCol);
    
  ConeCol2 = [1 1 0.99];
  ConeCol  = [0.8 0.8 0.7];
                                           
  % foot cone
  [x,y,z] = cylinder( rFoot, ConeRes);
  R_FootObj = surf( x, y-yShift, z, 'FaceColor', ConeCol);
  L_FootObj = surf( x, y+yShift, z, 'FaceColor', ConeCol2);
  
  % shank cones
  [x,y,z] = cylinder( rShank, ConeRes);
  R_ShankObj = surf( x, y-yShift, z, 'FaceColor', ConeCol);
  L_ShankObj = surf( x, y+yShift, z, 'FaceColor', ConeCol2);
  
  % thigh cones
  [x,y,z] = cylinder( rThigh, ConeRes);
  R_ThighObj = surf( x, y-yShift, z, 'FaceColor', ConeCol);
  L_ThighObj = surf( x, y+yShift, z, 'FaceColor', ConeCol2);
  
  % HAT cone
  [x,y,z] = cylinder( rHAT_Cone, ConeRes);
  HAT_ConeObj = surf( 1.3*x, 2.2*y, z, 'FaceColor', ConeCol);
  
  
  % generate objects vector
  ConeObjects = [R_FootObj R_ShankObj R_ThighObj L_FootObj L_ShankObj ...
                 L_ThighObj HAT_ConeObj];
    
  % set general properties                 
  set(ConeObjects, 'Visible', 'off', 'EdgeColor', 'none', ...
                     'BackFaceLighting', 'unlit')
 
end



% ---------------------
% Create Muscle Objects
% ---------------------

function MuscleObjects = CreateMuscleObjects(MuscleRes, yShift, rSOL, ...
                       rGAS, rTA, rQF, rHFL, rHAM, rGLU, rBFsh, MuscleCol);

  % soleus muscle
  [x,y,z] = cylinder( rSOL, MuscleRes);
  
  % right soleus
  R_SOL_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  
  % gastrocnemius muscle
  [x,y,z] = cylinder( rGAS, MuscleRes);
  
  % right gastrocnemius
  R_GAS_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  
  % tibialis anterior muscle
  [x,y,z] = cylinder( rTA, MuscleRes);
  
  % right tibialis anterior
  R_TA_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  
  % quadrizeps femoris muscle
  [x,y,z] = cylinder( rQF, MuscleRes);
  
  % right quadrizeps femoris
  R_QF_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  
  % hip flexor muscle
  [x,y,z] = cylinder( rHFL, MuscleRes);
  
  % right hip flexor
  R_HFL_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  
  % hamstring muscle
  [x,y,z] = cylinder( rHAM, MuscleRes);
  
  % right hamsting muscle
  R_HAM_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  
  % gluteus muscle
  [x,y,z] = cylinder( rGLU, MuscleRes);
  
  % right gluteus muscle
  R_GLU_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol);
  % bizeps femoris short head muscle
  [x,y,z] = cylinder( rBFsh, MuscleRes);
  
  % right bizeps femoris short head
  R_BFsh_Obj = surf( x, y-yShift, z, 'FaceColor', MuscleCol); 
  
  % generate objects vector
  MuscleObjects = [R_SOL_Obj R_GAS_Obj R_TA_Obj R_QF_Obj R_HFL_Obj ...
                   R_HAM_Obj R_GLU_Obj R_BFsh_Obj];
 
  % set general properties                 
  set(MuscleObjects, 'Visible', 'off', 'EdgeColor', 'none', ...
                     'BackFaceLighting', 'unlit')
 
end

% --------------
% Create Walkway
% --------------

function zPlates = CreateWalkwayObject(WayLength, PlateLength, nPlates, ...
                             WayCol, rCP);
   
  % walkway type
  WayType = 2;

  % select walkway
  switch WayType

    
    % level ground / incline / decline
    % --------------------------------
    case 1

      % walkway percent incline 
      % note: zero for level ground, positive/negative values for in-/decline
      PercentSlope = 0; %[in percent WayLength]

      % plate heights
      zPlates = PercentSlope / WayLength * linspace( 0, WayLength, nPlates); %[m]

      
    % random ground levels
    % --------------------
    case 2

      % plate heights
      % zPlates = [0 (rand(1,nPlates-1)*0.05 - 0.05/2)];
      % zPlates = [0 (rand(1,nPlates-1)*0.01 - 0.01/2)];
      zPlates = [0 (rand(1,nPlates-1)*0.02 - 0.02/2)];



    % custom walkway
    % --------------
    case 3

      % initially, make all plates having zero level
      zPlates = zeros(1, nPlates);
      
      % start straight walk
      zPlates(1:5)  = 0;
      
      % random ground level
      zPlates(6:15)  = (rand(1,10)*0.05 - 0.05/2);

      % then 3% incline
      zPlates(16:25) = zPlates(15) +  0.03*linspace(0, 10, 10);
      
      % then level walk again
      zPlates(26:31) = zPlates(25);
      
      % then 3% decline
      zPlates(32:41) = zPlates(31)- 0.03*linspace(0, 10, 10);
      
      % remainder straigth                           
      
    case 4
      
       % initially, make all plates having zero level
      zPlates = zeros(1, nPlates);
      
     
      zPlates(1:48) = [      0       0      0      0 -0.0112 0.0039 0.0161 0.0221 ...
                       -0.0028 -0.0038 0.0248 0.0057  0.0222 0.0206 0.0206 0.0539 ...
                        0.0873  0.1206 0.1539 0.1873  0.2206 0.2539 0.2873 0.3206 ...
                        0.3206  0.3206 0.3206 0.3206  0.3206 0.3206 0.3206 0.2873 ...
                        0.2539  0.2206 0.1873 0.1539  0.1206 0.0873 0.0539 0.0206 ...
                        0       0      0      0       0      0      0      0];
      

    case 5
      
      % initially, make all plates having zero level
      zPlates = zeros(1, nPlates);
     
      zPlates(1:21) = [     0 0      0       0      0      ...
                            0      0       0      0      ...
                            0      0       0      0.0303 ...
                            0.0606 0.0909 0.1212  0.1212 ...
                            0.0909 0.0606 0.0303  0      ];
      
  end

   
  % initialize vertex and face matrices
  VM = [];
  FM = [];
  
  % loop through plates
  for pIdx = 1:nPlates
    
    % add four plate vertices
    VM = [                                     VM; ...
          (pIdx-1)*PlateLength -0.5 zPlates(pIdx); ...
          (pIdx-1)*PlateLength  0.5 zPlates(pIdx); ...
              pIdx*PlateLength -0.5 zPlates(pIdx); ...
              pIdx*PlateLength  0.5 zPlates(pIdx)];
    
    % add two faces (vertical from previous plate to new plate
    % and horizontal for the new plate)
    LastVertex = 4*pIdx;
    
    if pIdx==1
      
      FM = [LastVertex-3 LastVertex-2 LastVertex LastVertex-1];
      
    else
      
      FM = [                                                 FM; ...
            LastVertex-5 LastVertex-4 LastVertex-2 LastVertex-3; ...
            LastVertex-3 LastVertex-2 LastVertex   LastVertex-1];
      
    end
              
  end
  
  % shift vertices downward
  VM(:,3) = VM(:,3)-rCP;
  
  % create walkway patch
  patch('Vertices', VM, 'Faces', FM, 'FaceColor', WayCol, ...
        'EdgeColor', [0.5 0.5 0.5], 'LineWidth', 1);
      
        

end



% ---------------------
% Update Sphere Objects
% ---------------------

function UpdateSphereObjects( SphereObjects, u, x)
  
  % extract sphere objects
  R_BallObj = SphereObjects(1);
  R_HeelObj = SphereObjects(2);
  R_AJ_Obj  = SphereObjects(3);
  R_KJ_Obj  = SphereObjects(4);
  R_HJ_Obj  = SphereObjects(5);
  L_BallObj = SphereObjects(6);
  L_HeelObj = SphereObjects(7);
  L_AJ_Obj  = SphereObjects(8);
  L_KJ_Obj  = SphereObjects(9);
  L_HJ_Obj  = SphereObjects(10);
  
  % shift spheres to their new position
  set(R_BallObj, 'XData', get(R_BallObj, 'XData') +  u(1)-  x(1), ...
                 'ZData', get(R_BallObj, 'ZData') +  u(2)-  x(2))
  set(R_HeelObj, 'XData', get(R_HeelObj, 'XData') +  u(3)-  x(3), ...
                 'ZData', get(R_HeelObj, 'ZData') +  u(4)-  x(4))
  set(R_AJ_Obj,  'XData',  get(R_AJ_Obj, 'XData') +  u(5)-  x(5), ...
                 'ZData',  get(R_AJ_Obj, 'ZData') +  u(6)-  x(6))
  set(R_KJ_Obj,  'XData',  get(R_KJ_Obj, 'XData') +  u(7)-  x(7), ...
                 'ZData',  get(R_KJ_Obj, 'ZData') +  u(8)-  x(8))
  set(R_HJ_Obj,  'XData',  get(R_HJ_Obj, 'XData') +  u(9)-  x(9), ...
                 'ZData',  get(R_HJ_Obj, 'ZData') +  u(10)- x(10))
  set(L_BallObj, 'XData', get(L_BallObj, 'XData') +  u(11)- x(11), ...
                 'ZData', get(L_BallObj, 'ZData') +  u(12)- x(12))
  set(L_HeelObj, 'XData', get(L_HeelObj, 'XData') +  u(13)- x(13), ...
                 'ZData', get(L_HeelObj, 'ZData') +  u(14)- x(14))
  set(L_AJ_Obj,  'XData',  get(L_AJ_Obj, 'XData') +  u(15)- x(15), ...
                 'ZData',  get(L_AJ_Obj, 'ZData') +  u(16)- x(16))
  set(L_KJ_Obj,  'XData',  get(L_KJ_Obj, 'XData') +  u(17)- x(17), ...
                 'ZData',  get(L_KJ_Obj, 'ZData') +  u(18)- x(18))
  set(L_HJ_Obj,  'XData',  get(L_HJ_Obj, 'XData') +  u(19)- x(19), ...
                 'ZData',  get(L_HJ_Obj, 'ZData') +  u(20)- x(20))
 

end



% -------------------
% Update Cone Objects
% -------------------

function UpdateConeObjects( ConeObjects, u, x, t);


 % extract cone objects
 R_FootObj   = ConeObjects(1);
 R_ShankObj  = ConeObjects(2);
 R_ThighObj  = ConeObjects(3);
 L_FootObj   = ConeObjects(4);
 L_ShankObj  = ConeObjects(5);
 L_ThighObj  = ConeObjects(6);
 HAT_ConeObj = ConeObjects(7);
 
 
 % at the initial time step t=0, scale cone objects to their actual length
 if t==0
   
   % set right and left foot length
   R_FootLength = sqrt( (u(3)-u(1))^2 + (u(4)-u(2))^2 );
   set(R_FootObj, 'ZData', get(R_FootObj, 'ZData') * R_FootLength);
   L_FootLength = sqrt( (u(13)-u(11))^2 + (u(14)-u(12))^2 );
   set(L_FootObj, 'ZData', get(L_FootObj, 'ZData') * L_FootLength);
 
   % set right and left shank length
   R_ShankLength = sqrt( (u(7)-u(5))^2 + (u(8)-u(6))^2 );
   set(R_ShankObj, 'ZData', get(R_ShankObj, 'ZData') * R_ShankLength);
   L_ShankLength = sqrt( (u(17)-u(15))^2 + (u(18)-u(16))^2 );
   set(L_ShankObj, 'ZData', get(L_ShankObj, 'ZData') * L_ShankLength);
   
   % set right and left thigh length
   R_ThighLength = sqrt( (u(9)-u(7))^2 + (u(10)-u(8))^2 );
   set(R_ThighObj, 'ZData', get(R_ThighObj, 'ZData') * R_ThighLength);
   L_ThighLength = sqrt( (u(19)-u(17))^2 + (u(20)-u(18))^2 );
   set(L_ThighObj, 'ZData', get(L_ThighObj, 'ZData') * L_ThighLength);
  
   % set HAT length
   HAT_Length = 2*sqrt( (u(21)-u(19))^2 + (u(22)-u(20))^2 );
   set(HAT_ConeObj, 'ZData', get(HAT_ConeObj, 'ZData') * HAT_Length);
  
 end
 
 % rotate and shift cones to their new angles and positions
 RotTransObj(   R_FootObj,   u(1:2),   u(3:4),   x(1:2),   x(3:4)) 
 RotTransObj(   L_FootObj, u(11:12), u(13:14), x(11:12), x(13:14)) 
 RotTransObj(  R_ShankObj,   u(5:6),   u(7:8),   x(5:6),   x(7:8)) 
 RotTransObj(  L_ShankObj, u(15:16), u(17:18), x(15:16), x(17:18)) 
 RotTransObj(  R_ThighObj,   u(7:8),  u(9:10),   x(7:8),  x(9:10))
 RotTransObj(  L_ThighObj, u(17:18), u(19:20), x(17:18), x(19:20))
 RotTransObj( HAT_ConeObj,  u(9:10), u(21:22),  x(9:10), x(21:22))

end



% ---------------------
% Update Muscle Objects
% ---------------------

function UpdateMuscleObjects( MuscleObjects, u, x, t);
 

 % extract muscle objects
 R_SOL_Obj   = MuscleObjects(1);
 R_GAS_Obj   = MuscleObjects(2);
 R_TA_Obj    = MuscleObjects(3);
 R_QF_Obj    = MuscleObjects(4);
 R_HFL_Obj   = MuscleObjects(5);
 R_HAM_Obj   = MuscleObjects(6);
 R_GLU_Obj   = MuscleObjects(7);
 R_BFsh_Obj  = MuscleObjects(8);
 

 
 % update soleus muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(1:2), u(3:4), x(1:2), x(3:4), 0.22,   0, t);
 [InsXY,  InsXYold ] = UpdateAttachPoint( u(5:6), u(7:8), x(5:6), x(7:8), 0.4, 0.02, t);
 RotTransMuscle(R_SOL_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 
 % update soleus muscle
 [InsXY,  InsXYold ] = UpdateAttachPoint( u(7:8), u(9:10), x(7:8), x(9:10), 0.05, 0.03, t);
 RotTransMuscle(R_GAS_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 

 % update soleus muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(1:2), u(3:4), x(1:2), x(3:4), 0.1,   0, t);
 [InsXY,  InsXYold ] = UpdateAttachPoint( u(5:6), u(7:8), x(5:6), x(7:8), 0.4, -0.02, t);
 RotTransMuscle(R_TA_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 
 % update quadrizeps femoris muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(7:8), u(9:10), x(7:8), x(9:10), 0,   -0.04, t);
 [InsXY,  InsXYold ] = UpdateAttachPoint( u(7:8), u(9:10), x(7:8), x(9:10), 0.35, -0.035, t);
 RotTransMuscle(R_QF_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 
 % update hip flexor muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(7:8), u(9:10), x(7:8), x(9:10), 0.37,  -0.04, t);
 [InsXY,  InsXYold ] = UpdateAttachPoint( u(9:10), u(21:22), x(9:10), x(21:22), 0, -0.08, t);
 RotTransMuscle(R_HFL_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 
 % update hamstring muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(5:6), u(7:8), x(5:6), x(7:8), 0.45, 0.02, t);
 [InsXY,  InsXYold ] = UpdateAttachPoint( u(9:10), u(21:22), x(9:10), x(21:22), 0, 0.08, t);
 RotTransMuscle(R_HAM_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 
 % update gluteus muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(7:8), u(9:10), x(7:8), x(9:10), 0.3, 0.03, t);
 RotTransMuscle(R_GLU_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 
 % update bizeps femoris muscle
 [OrigXY, OrigXYold] = UpdateAttachPoint( u(5:6), u(7:8), x(5:6), x(7:8),  0.45, 0.02, t);
 [InsXY, InsXYold ] = UpdateAttachPoint( u(7:8), u(9:10), x(7:8), x(9:10), 0.2, 0.02, t);
 RotTransMuscle(R_BFsh_Obj, OrigXY, InsXY,  OrigXYold, InsXYold, t) 
 

end



% -------------------
% Update Muscle Color
% -------------------

function UpdateMuscleColor(Muscles, Activations, MuscleCol, MuscleActiveCol)
  
  % browse through muscles
  for Midx=1:length(Muscles)
    
    % compute actual muscle's color    
    ActualCol = MuscleCol + (MuscleActiveCol-MuscleCol)*min(2*Activations(Midx), 1);
    
    % set muscle's color
    if any( get(Muscles(Midx), 'FaceColor')~= ActualCol)
      set(Muscles(Midx), 'FaceColor', ActualCol)
    end
    
  end
end



% ------------------------------
% Update Muscle Attachment Point
% ------------------------------

function [PointXY, PointXYold] = ...
          UpdateAttachPoint( LowXY, TopXY, LowXYold, TopXYold, dNorm, dTang, t)

  % calculate normal and tangential vector 
  NormVec = [TopXY(1)-LowXY(1); TopXY(2)-LowXY(2)];
  NormVec = NormVec / norm(NormVec);
  TangVec = [-NormVec(2); NormVec(1)];

  % calculate old normal and tangential vector 
  if t==0
    NormVecOld = [0; 0];
    TangVecOld = [0; 0];
  else
    NormVecOld = [TopXYold(1)-LowXYold(1); TopXYold(2)-LowXYold(2)];
    NormVecOld = NormVecOld / norm(NormVecOld);
    TangVecOld = [-NormVecOld(2); NormVecOld(1)];
  end

  % calculate new and old attachment point
  PointXY    = LowXY    + dNorm*NormVec    + dTang*TangVec;
  PointXYold = LowXYold + dNorm*NormVecOld + dTang*TangVecOld;
  
end



% ---------------------------
% Rotate and Translate Ojects
% ---------------------------

function RotTransObj( Object, LowXY, TopXY, LowXYold, TopXYold )
    
  % calculate change in rotation angle compared to previous angle
  dalpha =  atan2(   LowXY(1)-   TopXY(1),    TopXY(2)-   LowXY(2)) ...
           -atan2(LowXYold(1)-TopXYold(1), TopXYold(2)-LowXYold(2));

  % get actual x and z data and shift it back to zero
  xAct = get(Object, 'XData')-LowXYold(1);
  zAct = get(Object, 'ZData')-LowXYold(2);

  % rotate and shift x and z data to new angle and position
  xNew = cos(dalpha)*xAct - sin(dalpha)*zAct + LowXY(1);
  zNew = sin(dalpha)*xAct + cos(dalpha)*zAct + LowXY(2);

  % update cone object
  set(Object, 'XData', xNew, 'ZData', zNew);

end



% -----------------------------------
% Rotate, Scale, and Translate Muscle
% -----------------------------------


function RotTransMuscle( Muscle, LowXY, TopXY, LowXYold, TopXYold, t)
    
  % calculate change in rotation angle compared to previous angle
  Alpha    =  atan2(   LowXY(1)-   TopXY(1),    TopXY(2)-   LowXY(2));
  AlphaOld =  atan2(LowXYold(1)-TopXYold(1), TopXYold(2)-LowXYold(2));

  % shift actual x and z data back to zero
  xAct = get(Muscle, 'XData')-LowXYold(1);
  zAct = get(Muscle, 'ZData')-LowXYold(2);

  % rotate x and z data back to original position
  xOrig = cos(-AlphaOld)*xAct - sin(-AlphaOld)*zAct;
  zOrig = sin(-AlphaOld)*xAct + cos(-AlphaOld)*zAct;

  % rescale muscle
  Muscle_Length    = sqrt( (TopXY(1)-LowXY(1))^2 + (TopXY(2)-LowXY(2))^2 );
  if t==0,
    Muscle_LengthOld = 1;
  else
    Muscle_LengthOld = sqrt( (TopXYold(1)-LowXYold(1))^2 + (TopXYold(2)-LowXYold(2))^2 );
  end
  zOrig = zOrig * Muscle_Length / Muscle_LengthOld;
    
  % rotate x and z data to new angle and position
  xNew = cos(Alpha)*xOrig - sin(Alpha)*zOrig + LowXY(1);
  zNew = sin(Alpha)*xOrig + cos(Alpha)*zOrig + LowXY(2);

  % update cone object
  set(Muscle, 'XData', xNew, 'ZData', zNew);

end


% Function that shifts view window and  
% and the light sources if model is out
% of view
% ----------------------------------------
function ViewShiftParams = CheckViewWin( u, t, ViewWin, TolFrac, ...
                                         ViewShiftParams, tShiftTot)

  
  % Check For Shift Initiation
  % --------------------------
  
  if ViewShiftParams(1)==0
  
    % get axis limits
    XLimits = get(gca, 'XLim');

    % get min and max xpos of object
    minX = u(21);
    maxX = u(21);

    % check right border
    if XLimits(2) < ( maxX + ViewWin*TolFrac )

      % initiate shift to the right
      StartPos  = XLimits(1);
      dShiftTot = (minX - ViewWin*TolFrac)  - StartPos; 
      ViewShiftParams = [1 t StartPos dShiftTot];
      
      set(gca, 'XLim', [minX - ViewWin*TolFrac  minX + ViewWin*(1-TolFrac)]);

    % check left border
    elseif XLimits(1) > ( minX - ViewWin*TolFrac )

      % initiate shift to the left
      StartPos  = XLimits(1);
      dShiftTot = StartPos - (minX + ViewWin*TolFrac - ViewWin); 
      ViewShiftParams = [-1 t StartPos dShiftTot];
      set(gca, 'XLim', [maxX - ViewWin*(1-TolFrac)  maxX + ViewWin*TolFrac]);

    end

  end
  
  
  % Shift View Window
  % -----------------
  
  if ViewShiftParams(1)~=0
    
    % get current shift time
    tShift   = t - ViewShiftParams(2);
    
    % check for end of shift phase
    if tShift > tShiftTot
      
      % reset view window shift parameters
      ViewShiftParams(1) = 0;
      
    else
        
      % get shift direction
      ShiftDir = ViewShiftParams(1);

      % get shift distance
      dShiftTot = ViewShiftParams(4);
      
      % get start position
      StartPos  = ViewShiftParams(3); 

      % get new distance to former axis limit
      if tShiftTot == 0
        
        xLimShift = dShiftTot;
        
      else
      
        if tShift <= tShiftTot/2 

          xLimShift = 2*dShiftTot * (tShift/tShiftTot)^2;

        else

          xLimShift = dShiftTot-2*dShiftTot*((tShiftTot-tShift)/tShiftTot)^2;

        end
        
      end

      % shift axis limits
      set(gca, 'XLim', StartPos + ShiftDir*xLimShift+[0 ViewWin]);
 
      
      
    end
    
    
  end
  
   
   
end



% -----------------------------
% Record AVI Movie of Animation
% -----------------------------

function AviObj = RecordAVI(flag, AviObj, AVIname, AVIcodec, AVIquality, AVIfps); 

 
  switch flag

   
    % Initialization
    % --------------
    case 0 
    
      % switch double buffer mode on
      set(gcf, 'DoubleBuffer', 'on')

      % check for existing avi file
      if exist(AVIname, 'file')==2
        
        % delete existing file
        delete(AVIname)
      
      end
        
      % create new AVI object
      AviObj = avifile(AVIname);

      % set AVI object properties
      AviObj.compression = AVIcodec; 
      AviObj.quality     = AVIquality;
      AviObj.fps         = AVIfps;
    
      
    % Modification
    % ------------
    case 2
      
      % capture current figure
      F = getframe( gcf );
          
      % add frame to AVI objec
      AviObj = addframe(AviObj, F);

      
    % Termination
    % -----------
    case 9
      
      % close AVI object
      AviObj = close(AviObj);
     
      
  end
    
end



% ---------------------------
% Initialize Animation Figure
% ---------------------------

function Anim_Init(namestr)



  % -----------------
  % Initialize Figure
  % -----------------

  % check whether figure exists already
  [existFlag, figNumber] = figflag(namestr);

  % if not, initialize figure
  if ~existFlag,

    % define figure element
    figure( ...
         'Tag',                          namestr, ...
         'Name',                         namestr, ...
         'NumberTitle',                    'off', ...
         'BackingStore',                   'off', ...
         'MenuBar',                       'default', ...
         'Color',                        [1 1 1], ...
         'Position',         [20  20  600   275], ...
         'Renderer',                    'OpenGL');

    % define axes element
    axes('Position', [0.01 0.01 0.98 0.98], 'FontSize', 8);

  end %if ~existflag



  % ----------------------------------
  % Reset Figure to Simulation Default
  % ----------------------------------

  % reset axes to default properties
  cla reset;

  % change some properties
  set(gca, 'DrawMode',   'fast', ...
           'Color',     [1 1 1], ...
           'XColor',    [0 0 0], ...
           'YColor',    [0 0 0]);

  axis on;
  axis image;

  hold on;

end
