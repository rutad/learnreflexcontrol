clc
clear all;
close all;

load linRel_13ms_NMwalkingRoughGrd_100sSimData_musFeatStSw.mat;
%load linRel_13ms_NMwalking_GndRoughSlopeFlat_100sSimData_musFeatStSw.mat;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;

testNum = 4; %5 % number of test samples
K = 5; %2 % divide train data into K parts and use 1 as hold data

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end);%LTO_ind(2:end-1);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end-1);%RTO_ind(3:end);
RswingEnd_ind = RgaitEnd_ind;

%% K-fold intervals

KDatalen = length(LgaitStart_ind) - testNum;
Klen = KDatalen/K;
Kfold_int = NaN(K,Klen);

intStart = 1;
for i=1:K   
    intEnd = intStart +Klen-1;
    Kfold_int(i,:) = linspace(intStart, intEnd,Klen);
    intStart = intEnd+1;
end

%% Hold Data
lambda_min = 1e-4; % l1_ls suggested min lambda

MSE_errThSt = 0.001; %0.001 %0.01
MSE_errThSw = 0.001;%0.001 %0.00001  %0.01;

RMSE_errThSt = 0.001; %0.001 %0.01
RMSE_errThSw = 0.001; %0.001 %0.01

lambdaLSt = NaN(numMusFeatures/2,1);
lambdaLSw = NaN(numMusFeatures/2,1);

relerrHoldLSt = NaN(numMusFeatures/2,1);
relerrHoldLSw = NaN(numMusFeatures/2,1);

errHoldLSt = NaN(numMusFeatures/2,1);
errHoldLSw = NaN(numMusFeatures/2,1);

holdInd = 2; % number between 1 to K

holdDataInd = Kfold_int(holdInd,:);
trainDataInd = Kfold_int;
trainDataInd(holdInd,:)=[];
trainDataInd = trainDataInd(:);

LStim = [LStimHFL; LStimGLU; LStimHAM; LStimVAS; LStimGAS; LStimSOL; LStimTA];
RStim = [RStimHFL; RStimGLU; RStimHAM; RStimVAS; RStimGAS; RStimSOL; RStimTA];
LFeat = [LL_HFL; LF_GLU; LL_HAM; LF_HAM; LF_VAS; LF_GAS; LF_SOL;LL_TA];
RFeat = [RL_HFL; RF_GLU; RL_HAM; RF_HAM; RF_VAS; RF_GAS; RF_SOL;RL_TA];

[FeatDataLSt, FeatDataRSt, FeatDataLSw, FeatDataRSw, MusDataLSt,MusDataRSt, MusDataLSw, MusDataRSw,trunkMax,dtrunkMax] = ...
    collectGaitDataMusFeat_CVRoughGnd_Train(trainDataInd,holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind);

[holdFeatDataLSt, holdFeatDataRSt, holdFeatDataLSw, holdFeatDataRSw, holdMusDataLSt,holdMusDataRSt, holdMusDataLSw, holdMusDataRSw] = ...
    collectGaitDataMusFeat_CVRoughGnd_Hold(holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind);

%% Test data 

testDataInd = [(KDatalen+1):length(LgaitStart_ind)];

%% Cross-validated l1_ls

%%% LEFT %%%
ALSt = FeatDataLSt;
ALSw = FeatDataLSw;

% zero mean
ALSt = bsxfun(@minus, ALSt, mean(ALSt));
ALSw = bsxfun(@minus, ALSw, mean(ALSw));

% unit variance
ALSt = ALSt*diag(1./std(ALSt));
ALSw = ALSw*diag(1./std(ALSw));

for i = 1:numMusFeatures/2
    
    multThLSt = lambda_min;
    multThLSw = lambda_min;
    
    BLSt = MusDataLSt(:,i);
    BLSt = bsxfun(@minus, BLSt, mean(BLSt));
    if (std(BLSt)<5e-3)
        tooSmall=1;
    else
        tooSmall=0;
    end
    BLSt = BLSt./std(BLSt);

    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
        lambdaSt_CV(cnt) = lambda_max*multThLSt;
        
        lambdaSt = lambdaSt_CV(cnt);
        
        % find min lambda by CV
        while (lambdaSt<=lambda_max)
             
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt,[],'quiet');        
            errLSt(cnt) = mean((holdMusDataLSt(:,i)-holdFeatDataLSt*xLSt).^2);
            relerrLSt(cnt) = errLSt(cnt)/max(holdMusDataLSt(:,i));
            
            cnt = cnt+1;
            multThLSt = multThLSt*2;
            lambdaSt_CV(cnt) = lambda_max*multThLSt;
            lambdaSt = lambdaSt_CV(cnt);

        end    
        
        %figure;plot(errLSt);hold on
        %plot(relerrLSt,'r');title(strcat('Err Stance Mus',num2str(i)));
        %legend('Abs err', 'Rel err');

        [minError,lambdaFinalInd] = min(errLSt);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaStSparse = lambdaSt_CV(lambdaFinalInd);
        relerrLStSparse = relerrLSt(lambdaFinalInd);
        errLStSparse = errLSt(lambdaFinalInd);
        flag = 2;
        
        while (relerrLStSparse<RMSE_errThSt && errLStSparse<MSE_errThSt && lambdaStSparse<=lambda_max)
            
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaStSparse,[],'quiet');        
            errLStSparse = mean((holdMusDataLSt(:,i)-holdFeatDataLSt*xLSt).^2);
            relerrLStSparse = errLStSparse/max(holdMusDataLSt(:,i));
            
            lambdaStSparse = lambdaStSparse*2;
            flag = 1;
            
        end   
        
        [xLSt,~] = l1_ls(ALSt,BLSt,lambdaStSparse*flag/2,[],'quiet');
        errLStFinal = mean((holdMusDataLSt(:,i)-holdFeatDataLSt*xLSt).^2);  
        relerrLStFinal = errLStFinal/max(holdMusDataLSt(:,i));
        
        errHoldLSt(i) = errLStFinal;
        relerrHoldLSt(i) = relerrLStFinal;
        lambdaLSt(i) = lambdaStSparse*flag/2;
        wLSt(i,:) = xLSt';
   
    else
        wLSt(i,:) = zeros(size(wLSt,2),1);
    end       
    clear lambdaSt_CV errLSt relerrLSt;
    
    BLSw = MusDataLSw(:,i);
    BLSw = bsxfun(@minus, BLSw, mean(BLSw));
    if (std(BLSw)<5e-3)
        tooSmall=1;
    else
        tooSmall=0;
    end
        
    BLSw = BLSw./std(BLSw);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
        lambdaSw_CV(cnt) = lambda_max*multThLSw;
        
        lambdaSw = lambdaSw_CV(cnt);
        
        while (lambdaSw<=lambda_max)
             
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw,[],'quiet');        
            errLSw(cnt) = mean((holdMusDataLSw(:,i)-holdFeatDataLSw*xLSw).^2);
            relerrLSw(cnt) = errLSw(cnt)/max(holdMusDataLSw(:,i));
            
            cnt = cnt+1;
            multThLSw = multThLSw*2;
            lambdaSw_CV(cnt) = lambda_max*multThLSw;
            lambdaSw = lambdaSw_CV(cnt);

        end    
        
        [minError,lambdaFinalInd] = min(errLSw);
        
        %figure;plot(errLSw);hold on
        %plot(relerrLSw,'r');title(strcat('Err Swing Mus',num2str(i)));
        %legend('Abs err', 'Rel err');
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaSwSparse = lambdaSw_CV(lambdaFinalInd);
        relerrLSwSparse = relerrLSw(lambdaFinalInd);
        errLSwSparse = errLSw(lambdaFinalInd);
        flag = 2;
        
        while (relerrLSwSparse<RMSE_errThSw && errLSwSparse<MSE_errThSw && lambdaSwSparse<=lambda_max)
            
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSwSparse,[],'quiet');        
            errLSwSparse =  mean((holdMusDataLSw(:,i)-holdFeatDataLSw*xLSw).^2);
            relerrLSwSparse = errLSwSparse/max(holdMusDataLSw(:,i));
            
            lambdaSwSparse = lambdaSwSparse*2;
            flag = 1;
            
        end   
        
        [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSwSparse*flag/2,[],'quiet');
        errLSwFinal = mean((holdMusDataLSw(:,i)-holdFeatDataLSw*xLSw).^2);
        relerrLSwFinal = errLSwFinal/max(holdMusDataLSw(:,i));
        
        errHoldLSw(i) = errLSwFinal;
        relerrHoldLSw(i) = relerrLSwFinal;
        lambdaLSw(i) = lambdaSwSparse*flag/2;
        wLSw(i,:) = xLSw';
   
    else
        wLSw(i,:) = zeros(size(wLSw,2),1);
    end       
    clear lambdaSw_CV errLSw relerrLSw;
    
end


%% Test weight w on a particular sample gait left leg     

tst = randi(testNum,1,1); % index of test sample
strideInd = testDataInd(tst);

tLSt = LgaitStart_ind(strideInd):LswingStart_ind(strideInd);
tLSw = LswingStart_ind(strideInd):LgaitEnd_ind(strideInd);

% left stance
testFeatLSt = [LL_HFL.Data(tLSt,:) LF_GLU.Data(tLSt,:) LL_HAM.Data(tLSt,:) LF_HAM.Data(tLSt,:) LF_VAS.Data(tLSt,:) LF_GAS.Data(tLSt,:) LF_SOL.Data(tLSt,:) LL_TA.Data(tLSt,:) Torso.Data(tLSt,1)./trunkMax ...
            RL_HFL.Data(tLSt,:) RF_GLU.Data(tLSt,:) RL_HAM.Data(tLSt,:) RF_HAM.Data(tLSt,:) RF_VAS.Data(tLSt,:) RF_GAS.Data(tLSt,:) RF_SOL.Data(tLSt,:) RL_TA.Data(tLSt,:) Torso.Data(tLSt,2)./dtrunkMax];
       
trueMusLSt = [LStimHFL.Data(tLSt,:) LStimGLU.Data(tLSt,:) LStimHAM.Data(tLSt,:) LStimVAS.Data(tLSt,:) LStimGAS.Data(tLSt,:) LStimSOL.Data(tLSt,:) LStimTA.Data(tLSt,:)]; 
 
testMusLSt = wLSt*testFeatLSt';        

% left swing
testFeatLSw = [LL_HFL.Data(tLSw,:) LF_GLU.Data(tLSw,:) LL_HAM.Data(tLSw,:) LF_HAM.Data(tLSw,:) LF_VAS.Data(tLSw,:) LF_GAS.Data(tLSw,:) LF_SOL.Data(tLSw,:) LL_TA.Data(tLSw,:) Torso.Data(tLSw,1)./trunkMax ...
            RL_HFL.Data(tLSw,:) RF_GLU.Data(tLSw,:) RL_HAM.Data(tLSw,:) RF_HAM.Data(tLSw,:) RF_VAS.Data(tLSw,:) RF_GAS.Data(tLSw,:) RF_SOL.Data(tLSw,:) RL_TA.Data(tLSw,:) Torso.Data(tLSw,2)./dtrunkMax];
       
trueMusLSw = [LStimHFL.Data(tLSw,:) LStimGLU.Data(tLSw,:) LStimHAM.Data(tLSw,:) LStimVAS.Data(tLSw,:) LStimGAS.Data(tLSw,:) LStimSOL.Data(tLSw,:) LStimTA.Data(tLSw,:)];
            
 
testMusLSw = wLSw*testFeatLSw';        

%% test error 

errTestLSt = NaN(size(testMusLSt,1),1);
errTestLSw = NaN(size(testMusLSw,1),1);

relerrTestLSt = NaN(size(testMusLSt,1),1);
relerrTestLSw = NaN(size(testMusLSw,1),1);

for m = 1:size(testMusLSt,1)
    
    errTestLSt(m) = mean((testMusLSt(m,:)-trueMusLSt(:,m)').^2);
    errTestLSw(m) = mean((testMusLSw(m,:)-trueMusLSw(:,m)').^2);

    relerrTestLSt(m) = mean((testMusLSt(m,:)-trueMusLSt(:,m)').^2)/max(trueMusLSt(:,m));
    relerrTestLSw(m) = mean((testMusLSw(m,:)-trueMusLSw(:,m)').^2)/max(trueMusLSw(:,m));

end


%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

%% left stance w

figure;imagesc(abs(wLSt)./repmat(max(abs(wLSt),[],2), 1, size(wLSt,2)));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
%saveas(gcf,'reconstructLStSparse_MusNormFeatStSwRghGnd_CVWhitenData_2.pdf')

%%% left swing w
figure;imagesc(abs(wLSw)./repmat(max(abs(wLSw),[],2), 1, size(wLSw,2)));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
%saveas(gcf,'reconstructLSwSparse_MusNormFeatStSwRghGnd_CVWhitenData_2.pdf')

%% Reconstruction plots

figure;
subplot(7,1,1); plot(testMusLSt(1,:)');hold on
subplot(7,1,1); plot(trueMusLSt(:,1)');title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
legend('Reconstructed-sparse','TrueData');

subplot(7,1,2); plot(testMusLSt(2,:)');hold on
subplot(7,1,2); plot(trueMusLSt(:,2)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSt(3,:)');hold on
subplot(7,1,3); plot(trueMusLSt(:,3)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSt(4,:)');hold on
subplot(7,1,4); plot(trueMusLSt(:,4)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSt(5,:)');hold on
subplot(7,1,5); plot(trueMusLSt(:,5)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSt(6,:)');hold on
subplot(7,1,6); plot(trueMusLSt(:,6)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSt(7,:)');hold on
subplot(7,1,7); plot(trueMusLSt(:,7)');ylabel('LTA');
%saveas(gcf,'reconstructLStSparse_MusNormFeatStSwRghGnd_CVWhitenData_2.pdf')

figure;
subplot(7,1,1); plot(testMusLSw(1,:)');hold on
subplot(7,1,1); plot(trueMusLSw(:,1)');
title('Left muscles SWING reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
legend('Reconstructed-sparse','TrueData');

subplot(7,1,2); plot(testMusLSw(2,:)');hold on
subplot(7,1,2); plot(trueMusLSw(:,2)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSw(3,:)');hold on
subplot(7,1,3); plot(trueMusLSw(:,3)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSw(4,:)');hold on
subplot(7,1,4); plot(trueMusLSw(:,4)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSw(5,:)');hold on
subplot(7,1,5); plot(trueMusLSw(:,5)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSw(6,:)');hold on
subplot(7,1,6); plot(trueMusLSw(:,6)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSw(7,:)');hold on
subplot(7,1,7); plot(trueMusLSw(:,7)');ylabel('LTA');
%saveas(gcf,'reconstructLSwSparse_MusNormFeatStSwRghGnd_CVWhitenData_2.pdf')


%% bar visualization

figure;
subplot(7,1,1); bar(wLSt(1,:)./max(abs(wLSt(1,:))));hold on
title('Norm weights LEFT STANCE'); ylabel('LHFL');

subplot(7,1,2); bar(wLSt(2,:)./max(abs(wLSt(2,:))));ylabel('LGLU');

subplot(7,1,3); bar(wLSt(3,:)./max(abs(wLSt(3,:))));ylabel('LHAM');

subplot(7,1,4); bar(wLSt(4,:)./max(abs(wLSt(4,:))));ylabel('LVAS');

subplot(7,1,5); bar(wLSt(5,:)./max(abs(wLSt(5,:))));ylabel('LGAS');

subplot(7,1,6); bar(wLSt(6,:)./max(abs(wLSt(6,:))));ylabel('LSOL');

subplot(7,1,7); bar(wLSt(7,:)./max(abs(wLSt(7,:))));ylabel('LTA');
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%saveas(gcf,'barGrph_normWSparseLSt_MusNormFeatStSwRghGnd_CVWhiten_2.pdf')

figure;
subplot(7,1,1); bar(wLSw(1,:)./max(abs(wLSw(1,:))));hold on
title('Norm weights LEFT SWING'); ylabel('LHFL');

subplot(7,1,2); bar(wLSw(2,:)./max(abs(wLSw(2,:))));ylabel('LGLU');

subplot(7,1,3); bar(wLSw(3,:)./max(abs(wLSw(3,:))));ylabel('LHAM');

subplot(7,1,4); bar(wLSw(4,:)./max(abs(wLSw(4,:))));ylabel('LVAS');

subplot(7,1,5); bar(wLSw(5,:)./max(abs(wLSw(5,:))));ylabel('LGAS');

subplot(7,1,6); bar(wLSw(6,:)./max(abs(wLSw(6,:))));ylabel('LSOL');

subplot(7,1,7); bar(wLSw(7,:)./max(abs(wLSw(7,:))));ylabel('LTA');
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%saveas(gcf,'barGrph_normWSparseLSw_MusNormFeatStSwRghGnd_CVWhiten_2.pdf')

%% R-squared and adjusted R-squared plots: Left Swing

figure;
R2_LSw = rsquared(trueMusLSw',testMusLSw);
AR2_LSw = adjusted_rsquared(trueMusLSw',testMusLSw, wLSw);

nPlot = length(find(R2_LSw>0));
plotCnt = 1;
R2_all = [];
AR2_all = [];

for i = 1:length(R2_LSw)
    if (R2_LSw(i))>0

        ww = wLSw(i,:);
        [wwsort,wwind] = sort(ww,2,'descend');
        wwind = wwind( find(abs(wwsort)>0.01));     
        flag_w = zeros(size(wLSw(i,:),1),size(wLSw(i,:),2));

        for j = 1:length(wwind)
           flag_w(wwind(j))=1;
           temp_w = wLSw(i,:).*flag_w;
           R2_all(j) = rsquared(trueMusLSw(:,i)',temp_w*testFeatLSw');
           AR2_all(j) = adjusted_rsquared(trueMusLSw(:,i)',temp_w*testFeatLSw', temp_w);
        end
        wCnt = [linspace(1,length(wwind),length(wwind)), size(wLSw(i,:),2)];
        R2_all = [R2_all R2_LSw(i)];
        AR2_all = [AR2_all AR2_LSw(i)];
        
        subplot(nPlot,2,plotCnt);plot(wCnt,R2_all,'*-');axis([1,18,0,1]);
        title('Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
        
        subplot(nPlot,2,plotCnt+1);plot(wCnt,AR2_all,'*-');axis([1,18,0,1]);
        title('Adj. Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
        
        clear R2_all AR2_all;
        plotCnt = plotCnt+2;
    end      
end

%% R-squared and adjusted R-squared plots: Left Stance

figure;
R2_LSt = rsquared(trueMusLSt',testMusLSt);
AR2_LSt = adjusted_rsquared(trueMusLSt',testMusLSt, wLSt);

nPlot = length(find(R2_LSt>0));
plotCnt = 1;
R2_all = [];
AR2_all = [];

for i = 1:length(R2_LSt)
    if (R2_LSt(i))>0

        ww = wLSt(i,:);
        [wwsort,wwind] = sort(ww,2,'descend');
        wwind = wwind( find(abs(wwsort)>0.01));     
        flag_w = zeros(size(wLSt(i,:),1),size(wLSt(i,:),2));

        for j = 1:length(wwind)
           flag_w(wwind(j))=1;
           temp_w = wLSt(i,:).*flag_w;
           R2_all(j) = rsquared(trueMusLSt(:,i)',temp_w*testFeatLSt');
           AR2_all(j) = adjusted_rsquared(trueMusLSt(:,i)',temp_w*testFeatLSt', temp_w);
        end
        wCnt = [linspace(1,length(wwind),length(wwind)), size(wLSt(i,:),2)];
        R2_all = [R2_all R2_LSt(i)];
        AR2_all = [AR2_all AR2_LSw(i)];
        
        subplot(nPlot,2,plotCnt);plot(wCnt,R2_all,'*-');axis([1,18,0,1]);
        title('Rsquared vs # of weights: Left Stance'); ylabel(labels_ly(i));xlabel('# of w');
        
        subplot(nPlot,2,plotCnt+1);plot(wCnt,AR2_all,'*-');axis([1,18,0,1]);
        title('Adj. Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
        
        clear R2_all AR2_all;
        plotCnt = plotCnt+2;
    end      
end

