%clc;
%clear all;

numMusLFFeatures = 16; 
numFeat = numMusLFFeatures + 2; %(+2 trunk Kin features)
numMusData = 14;

countSt = 0; % running count of test data
testCountSt = 1; % running index of test data

countSw = 0; % running count of test data
testCountSw = 1; % running index of test data

testNum = 9; % number of test samples

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end-1);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end);
RswingEnd_ind = RgaitEnd_ind;

% resampling length left
resampleLSt_p = max(LswingStart_ind-LgaitStart_ind)+1;
resampleLSw_p = max(LswingEnd_ind-LswingStart_ind)+1;

LdataLen = length(LgaitStart_ind);

% resample length right
resampleRSt_p = max(RswingStart_ind-RgaitStart_ind)+1;
resampleRSw_p = max(RswingEnd_ind-RswingStart_ind)+1;

RdataLen = length(RgaitStart_ind);

%% divide data into windows

testDataInd = randi(LdataLen,testNum,1);

while (length(unique(testDataInd))<length(testDataInd))
    testDataInd = randi(LdataLen,testNum,1);
end

window_len = 15; % num of gait cycles
window_num = (LdataLen - testNum)/window_len; % total # of groups of gait cycles

allDataLSt =cell(window_num,numFeat+numMusData/2);
allDataRSt =cell(window_num,numFeat+numMusData/2);
allDataLSw =cell(window_num,numFeat+numMusData/2);
allDataRSw =cell(window_num,numFeat+numMusData/2);

mallDataLSt =cell(window_num,2);
mallDataRSt =cell(window_num,2);
mallDataLSw =cell(window_num,2);
mallDataRSw =cell(window_num,2);

%% Test data

 % test data left stance
 tLL_HFLDataLSt = NaN(testNum,resampleLSt_p); 
 tLF_GLUDataLSt = NaN(testNum,resampleLSt_p); 
 tLL_HAMDataLSt = NaN(testNum,resampleLSt_p); 
 tLF_HAMDataLSt = NaN(testNum,resampleLSt_p); 
 tLF_VASDataLSt = NaN(testNum,resampleLSt_p); 
 tLF_GASDataLSt = NaN(testNum,resampleLSt_p); 
 tLF_SOLDataLSt = NaN(testNum,resampleLSt_p); 
 tLL_TADataLSt = NaN(testNum,resampleLSt_p); 
 
 tTrunkDataLSt = NaN(testNum,resampleLSt_p); 
 
 tRL_HFLDataLSt = NaN(testNum,resampleLSt_p); 
 tRF_GLUDataLSt = NaN(testNum,resampleLSt_p); 
 tRL_HAMDataLSt = NaN(testNum,resampleLSt_p); 
 tRF_HAMDataLSt = NaN(testNum,resampleLSt_p); 
 tRF_VASDataLSt = NaN(testNum,resampleLSt_p); 
 tRF_GASDataLSt = NaN(testNum,resampleLSt_p); 
 tRF_SOLDataLSt = NaN(testNum,resampleLSt_p); 
 tRL_TADataLSt = NaN(testNum,resampleLSt_p); 
 
 tdTrunkDataLSt = NaN(testNum,resampleLSt_p); 
 
 tLHFLDataLSt = NaN(testNum,resampleLSt_p); 
 tLGLUDataLSt = NaN(testNum,resampleLSt_p); 
 tLHAMDataLSt = NaN(testNum,resampleLSt_p); 
 tLVASDataLSt = NaN(testNum,resampleLSt_p); 
 tLGASDataLSt = NaN(testNum,resampleLSt_p); 
 tLSOLDataLSt = NaN(testNum,resampleLSt_p); 
 tLTADataLSt = NaN(testNum,resampleLSt_p); 
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 % test data right stance
 tLL_HFLDataRSt = NaN(testNum,resampleRSt_p); 
 tLF_GLUDataRSt = NaN(testNum,resampleRSt_p);
 tLL_HAMDataRSt = NaN(testNum,resampleRSt_p);
 tLF_HAMDataRSt = NaN(testNum,resampleRSt_p);
 tLF_VASDataRSt = NaN(testNum,resampleRSt_p);
 tLF_GASDataRSt = NaN(testNum,resampleRSt_p);
 tLF_SOLDataRSt = NaN(testNum,resampleRSt_p);
 tLL_TADataRSt = NaN(testNum,resampleRSt_p);
 
 tTrunkDataRSt = NaN(testNum,resampleRSt_p);
 
 tRL_HFLDataRSt = NaN(testNum,resampleRSt_p);
 tRF_GLUDataRSt = NaN(testNum,resampleRSt_p);
 tRL_HAMDataRSt = NaN(testNum,resampleRSt_p);
 tRF_HAMDataRSt = NaN(testNum,resampleRSt_p);
 tRF_VASDataRSt = NaN(testNum,resampleRSt_p);
 tRF_GASDataRSt = NaN(testNum,resampleRSt_p);
 tRF_SOLDataRSt = NaN(testNum,resampleRSt_p);
 tRL_TADataRSt = NaN(testNum,resampleRSt_p);
 
 tdTrunkDataRSt = NaN(testNum,resampleRSt_p);
 
 tRHFLDataRSt = NaN(testNum,resampleRSt_p);
 tRGLUDataRSt = NaN(testNum,resampleRSt_p);
 tRHAMDataRSt = NaN(testNum,resampleRSt_p);
 tRVASDataRSt = NaN(testNum,resampleRSt_p);
 tRGASDataRSt = NaN(testNum,resampleRSt_p);
 tRSOLDataRSt = NaN(testNum,resampleRSt_p);
 tRTADataRSt = NaN(testNum,resampleRSt_p);

 % test data left swing
 tLL_HFLDataLSw = NaN(testNum,resampleLSw_p); 
 tLF_GLUDataLSw = NaN(testNum,resampleLSw_p); 
 tLL_HAMDataLSw = NaN(testNum,resampleLSw_p); 
 tLF_HAMDataLSw = NaN(testNum,resampleLSw_p); 
 tLF_VASDataLSw = NaN(testNum,resampleLSw_p); 
 tLF_GASDataLSw = NaN(testNum,resampleLSw_p); 
 tLF_SOLDataLSw = NaN(testNum,resampleLSw_p); 
 tLL_TADataLSw = NaN(testNum,resampleLSw_p); 
 
 tTrunkDataLSw = NaN(testNum,resampleLSw_p); 
 
 tRL_HFLDataLSw = NaN(testNum,resampleLSw_p); 
 tRF_GLUDataLSw = NaN(testNum,resampleLSw_p); 
 tRL_HAMDataLSw = NaN(testNum,resampleLSw_p); 
 tRF_HAMDataLSw = NaN(testNum,resampleLSw_p); 
 tRF_VASDataLSw = NaN(testNum,resampleLSw_p); 
 tRF_GASDataLSw = NaN(testNum,resampleLSw_p); 
 tRF_SOLDataLSw = NaN(testNum,resampleLSw_p); 
 tRL_TADataLSw = NaN(testNum,resampleLSw_p); 
 
 tdTrunkDataLSw = NaN(testNum,resampleLSw_p); 
 
 tLHFLDataLSw = NaN(testNum,resampleLSw_p); 
 tLGLUDataLSw = NaN(testNum,resampleLSw_p); 
 tLHAMDataLSw = NaN(testNum,resampleLSw_p); 
 tLVASDataLSw = NaN(testNum,resampleLSw_p); 
 tLGASDataLSw = NaN(testNum,resampleLSw_p); 
 tLSOLDataLSw = NaN(testNum,resampleLSw_p); 
 tLTADataLSw = NaN(testNum,resampleLSw_p); 
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 % test data right swing
 tLL_HFLDataRSw = NaN(testNum,resampleRSw_p); 
 tLF_GLUDataRSw = NaN(testNum,resampleRSw_p);
 tLL_HAMDataRSw = NaN(testNum,resampleRSw_p);
 tLF_HAMDataRSw = NaN(testNum,resampleRSw_p);
 tLF_VASDataRSw = NaN(testNum,resampleRSw_p);
 tLF_GASDataRSw = NaN(testNum,resampleRSw_p);
 tLF_SOLDataRSw = NaN(testNum,resampleRSw_p);
 tLL_TADataRSw = NaN(testNum,resampleRSw_p);
 
 tTrunkDataRSw = NaN(testNum,resampleRSw_p);
 
 tRL_HFLDataRSw = NaN(testNum,resampleRSw_p);
 tRF_GLUDataRSw = NaN(testNum,resampleRSw_p);
 tRL_HAMDataRSw = NaN(testNum,resampleRSw_p);
 tRF_HAMDataRSw = NaN(testNum,resampleRSw_p);
 tRF_VASDataRSw = NaN(testNum,resampleRSw_p);
 tRF_GASDataRSw = NaN(testNum,resampleRSw_p);
 tRF_SOLDataRSw = NaN(testNum,resampleRSw_p);
 tRL_TADataRSw = NaN(testNum,resampleRSw_p);
 
 tdTrunkDataRSw = NaN(testNum,resampleRSw_p);
 
 tRHFLDataRSw = NaN(testNum,resampleRSw_p);
 tRGLUDataRSw = NaN(testNum,resampleRSw_p);
 tRHAMDataRSw = NaN(testNum,resampleRSw_p);
 tRVASDataRSw = NaN(testNum,resampleRSw_p);
 tRGASDataRSw = NaN(testNum,resampleRSw_p);
 tRSOLDataRSw = NaN(testNum,resampleRSw_p);
 tRTADataRSw = NaN(testNum,resampleRSw_p);
 
%% Store kinematics and muscle data, its mean and test data from gait cycles (Stance)
for j = 1:window_num
    % Preallocate memory for left stance muscle feat data

    LL_HFLDataLSt = NaN(window_len,resampleLSt_p);
    LF_GLUDataLSt = NaN(window_len,resampleLSt_p);
    LL_HAMDataLSt = NaN(window_len,resampleLSt_p);
    LF_HAMDataLSt = NaN(window_len,resampleLSt_p);
    LF_VASDataLSt = NaN(window_len,resampleLSt_p);
    LF_GASDataLSt = NaN(window_len,resampleLSt_p);
    LF_SOLDataLSt = NaN(window_len,resampleLSt_p);
    LL_TADataLSt = NaN(window_len,resampleLSt_p);

    RL_HFLDataLSt = NaN(window_len,resampleLSt_p);
    RF_GLUDataLSt = NaN(window_len,resampleLSt_p);
    RL_HAMDataLSt = NaN(window_len,resampleLSt_p);
    RF_HAMDataLSt = NaN(window_len,resampleLSt_p);
    RF_VASDataLSt = NaN(window_len,resampleLSt_p);
    RF_GASDataLSt = NaN(window_len,resampleLSt_p);
    RF_SOLDataLSt = NaN(window_len,resampleLSt_p);
    RL_TADataLSt = NaN(window_len,resampleLSt_p);

    TrunkDataLSt = NaN(window_len,resampleLSt_p);
    dTrunkDataLSt = NaN(window_len,resampleLSt_p);

    % Preallocate memory for left stance muscle data

    LHFLDataLSt = NaN(window_len,resampleLSt_p);
    LGLUDataLSt = NaN(window_len,resampleLSt_p);
    LHAMDataLSt = NaN(window_len,resampleLSt_p);
    LVASDataLSt = NaN(window_len,resampleLSt_p);
    LGASDataLSt = NaN(window_len,resampleLSt_p);
    LSOLDataLSt = NaN(window_len,resampleLSt_p);
    LTADataLSt = NaN(window_len,resampleLSt_p);
            
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Preallocate memory for right stance muscle feat data

    LL_HFLDataRSt = NaN(window_len,resampleRSt_p);
    LF_GLUDataRSt = NaN(window_len,resampleRSt_p);
    LL_HAMDataRSt = NaN(window_len,resampleRSt_p);
    LF_HAMDataRSt = NaN(window_len,resampleRSt_p);
    LF_VASDataRSt = NaN(window_len,resampleRSt_p);
    LF_GASDataRSt = NaN(window_len,resampleRSt_p);
    LF_SOLDataRSt = NaN(window_len,resampleRSt_p);
    LL_TADataRSt = NaN(window_len,resampleRSt_p);

    RL_HFLDataRSt = NaN(window_len,resampleRSt_p);
    RF_GLUDataRSt = NaN(window_len,resampleRSt_p);
    RL_HAMDataRSt = NaN(window_len,resampleRSt_p);
    RF_HAMDataRSt = NaN(window_len,resampleRSt_p);
    RF_VASDataRSt = NaN(window_len,resampleRSt_p);
    RF_GASDataRSt = NaN(window_len,resampleRSt_p);
    RF_SOLDataRSt = NaN(window_len,resampleRSt_p);
    RL_TADataRSt = NaN(window_len,resampleRSt_p);

    TrunkDataRSt = NaN(window_len,resampleRSt_p);
    dTrunkDataRSt = NaN(window_len,resampleRSt_p);

    % Preallocate memory for right muscle stance data

    RHFLDataRSt = NaN(window_len,resampleRSt_p);
    RGLUDataRSt = NaN(window_len,resampleRSt_p);
    RHAMDataRSt = NaN(window_len,resampleRSt_p);
    RVASDataRSt = NaN(window_len,resampleRSt_p);
    RGASDataRSt = NaN(window_len,resampleRSt_p);
    RSOLDataRSt = NaN(window_len,resampleRSt_p);
    RTADataRSt = NaN(window_len,resampleRSt_p);

    winLenInd = 15;
    gait_ind = (j-1)*window_len+1+countSt;
    num_testGaits = find(testDataInd>=gait_ind & testDataInd<=window_len+gait_ind-1);
    countSt = countSt + length(num_testGaits);
    i = gait_ind;
    
    disp(gait_ind)
    disp(window_len+gait_ind+length(num_testGaits)-1)
    disp(length(num_testGaits))
    
    while (winLenInd>0)
    %for i=gait_ind:window_len+gait_ind+length(num_testGaits)-1
        
        LLHFL_LSt = LL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LLHFL_LStR = resample(LLHFL_LSt,resampleLSt_p,size(LLHFL_LSt,1)); % resample to max gait cycle length
        
        LFGLU_LSt = LF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LFGLU_LStR = resample(LFGLU_LSt,resampleLSt_p,size(LFGLU_LSt,1));
        
        LLHAM_LSt = LL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LLHAM_LStR = resample(LLHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LFHAM_LSt = LF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LFHAM_LStR = resample(LFHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LFVAS_LSt = LF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LFVAS_LStR = resample(LFVAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LFGAS_LSt = LF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LFGAS_LStR = resample(LFGAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LFSOL_LSt = LF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LFSOL_LStR = resample(LFSOL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LLTA_LSt = LL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LLTA_LStR = resample(LLTA_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RLHFL_LSt = RL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); 
        RLHFL_LStR = resample(RLHFL_LSt,resampleLSt_p,size(LLHAM_LSt,1)); % resample to max gait cycle length
        
        RFGLU_LSt = RF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); 
        RFGLU_LStR = resample(RFGLU_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RLHAM_LSt = RL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        RLHAM_LStR = resample(RLHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RFHAM_LSt = RF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        RFHAM_LStR = resample(RFHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RFVAS_LSt = RF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        RFVAS_LStR = resample(RFVAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RFGAS_LSt = RF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        RFGAS_LStR = resample(RFGAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RFSOL_LSt = RF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        RFSOL_LStR = resample(RFSOL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        RLTA_LSt = RL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        RLTA_LStR = resample(RLTA_LSt,resampleLSt_p,size(LLHAM_LSt,1));
              
        Trunk_LSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),1);
        Trunk_LStR = resample(Trunk_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        dTrunk_LSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),2);
        dTrunk_LStR = resample(dTrunk_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        %%%%%
        
        LHFL_LSt = LStimHFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LHFL_LStR = resample(LHFL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LGLU_LSt = LStimGLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LGLU_LStR = resample(LGLU_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LHAM_LSt = LStimHAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LHAM_LStR = resample(LHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LVAS_LSt = LStimVAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LVAS_LStR = resample(LVAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LGAS_LSt = LStimGAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LGAS_LStR = resample(LGAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LSOL_LSt = LStimSOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LSOL_LStR = resample(LSOL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        LTA_LSt = LStimTA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
        LTA_LStR = resample(LTA_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        LLHFL_RSt = LL_HFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:); 
        LLHFL_RStR = resample(LLHFL_RSt,resampleRSt_p,size(LLHFL_RSt,1)); % resample to max gait cycle length
        
        LFGLU_RSt = LF_GLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:); 
        LFGLU_RStR = resample(LFGLU_RSt,resampleRSt_p,size(LFGLU_RSt,1));
        
        LLHAM_RSt = LL_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        LLHAM_RStR = resample(LLHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        LFHAM_RSt = LF_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        LFHAM_RStR = resample(LFHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        LFVAS_RSt = LF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        LFVAS_RStR = resample(LFVAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        LFGAS_RSt = LF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        LFGAS_RStR = resample(LFGAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        LFSOL_RSt = LF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        LFSOL_RStR = resample(LFSOL_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        LLTA_RSt = LL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        LLTA_RStR = resample(LLTA_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RLHFL_RSt = RL_HFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:); 
        RLHFL_RStR = resample(RLHFL_RSt,resampleRSt_p,size(LLHAM_RSt,1)); % resample to max gait cycle length
        
        RFGLU_RSt = RF_GLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:); 
        RFGLU_RStR = resample(RFGLU_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RLHAM_RSt = RL_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RLHAM_RStR = resample(RLHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RFHAM_RSt = RF_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RFHAM_RStR = resample(RFHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RFVAS_RSt = RF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RFVAS_RStR = resample(RFVAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RFGAS_RSt = RF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RFGAS_RStR = resample(RFGAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RFSOL_RSt = RF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RFSOL_RStR = resample(RFSOL_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RLTA_RSt = RL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RLTA_RStR = resample(RLTA_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        Trunk_RSt = Torso.Data(RgaitStart_ind(i):RswingStart_ind(i),1);
        Trunk_RStR = resample(Trunk_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        dTrunk_RSt = Torso.Data(RgaitStart_ind(i):RswingStart_ind(i),2);
        dTrunk_RStR = resample(dTrunk_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        %%%%
        
        RHF_RSt = RStimHFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RHF_RStR = resample(RHF_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RGLU_RSt = RStimGLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RGLU_RStR = resample(RGLU_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RHAM_RSt = RStimHAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RHAM_RStR = resample(RHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RVAS_RSt = RStimVAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RVAS_RStR = resample(RVAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RGAS_RSt = RStimGAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RGAS_RStR = resample(RGAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RSOL_RSt = RStimSOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RSOL_RStR = resample(RSOL_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        RTA_RSt = RStimTA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
        RTA_RStR = resample(RTA_RSt,resampleRSt_p,size(LLHAM_RSt,1));
        
        if (isempty(find(testDataInd ==i))) % find if this gait cycle is a test sample

            LL_HFLDataLSt(winLenInd,:) = LLHFL_LStR;
            LF_GLUDataLSt(winLenInd,:) = LFGLU_LStR;
            LL_HAMDataLSt(winLenInd,:) = LLHAM_LStR;
            LF_HAMDataLSt(winLenInd,:) = LFHAM_LStR;
            LF_VASDataLSt(winLenInd,:) = LFVAS_LStR;
            LF_GASDataLSt(winLenInd,:) = LFGAS_LStR;
            LF_SOLDataLSt(winLenInd,:) = LFSOL_LStR;
            LL_TADataLSt(winLenInd,:) = LLTA_LStR;

            TrunkDataLSt(winLenInd,:) = Trunk_LStR;

            RL_HFLDataLSt(winLenInd,:) = RLHFL_LStR;
            RF_GLUDataLSt(winLenInd,:) = RFGLU_LStR;
            RL_HAMDataLSt(winLenInd,:) = RLHAM_LStR;
            RF_HAMDataLSt(winLenInd,:) = RFHAM_LStR;
            RF_VASDataLSt(winLenInd,:) = RFVAS_LStR;
            RF_GASDataLSt(winLenInd,:) = RFGAS_LStR;
            RF_SOLDataLSt(winLenInd,:) = RFSOL_LStR;
            RL_TADataLSt(winLenInd,:) = RLTA_LStR;

            dTrunkDataLSt(winLenInd,:) = dTrunk_LStR;

            LHFLDataLSt(winLenInd,:) = LHFL_LStR;
            LGLUDataLSt(winLenInd,:) = LGLU_LStR;
            LHAMDataLSt(winLenInd,:) = LHAM_LStR;
            LVASDataLSt(winLenInd,:) = LVAS_LStR;
            LGASDataLSt(winLenInd,:) = LGAS_LStR;
            LSOLDataLSt(winLenInd,:) = LSOL_LStR;
            LTADataLSt(winLenInd,:) = LTA_LStR;
            
            %%%%%            
            LL_HFLDataRSt(winLenInd,:) = LLHFL_RStR;
            LF_GLUDataRSt(winLenInd,:) = LFGLU_RStR;
            LL_HAMDataRSt(winLenInd,:) = LLHAM_RStR;
            LF_HAMDataRSt(winLenInd,:) = LFHAM_RStR;
            LF_VASDataRSt(winLenInd,:) = LFVAS_RStR;
            LF_GASDataRSt(winLenInd,:) = LFGAS_RStR;
            LF_SOLDataRSt(winLenInd,:) = LFSOL_RStR;
            LL_TADataRSt(winLenInd,:) = LLTA_RStR;

            TrunkDataRSt(winLenInd,:) = Trunk_RStR;

            RL_HFLDataRSt(winLenInd,:) = RLHFL_RStR;
            RF_GLUDataRSt(winLenInd,:) = RFGLU_RStR;
            RL_HAMDataRSt(winLenInd,:) = RLHAM_RStR;
            RF_HAMDataRSt(winLenInd,:) = RFHAM_RStR;
            RF_VASDataRSt(winLenInd,:) = RFVAS_RStR;
            RF_GASDataRSt(winLenInd,:) = RFGAS_RStR;
            RF_SOLDataRSt(winLenInd,:) = RFSOL_RStR;
            RL_TADataRSt(winLenInd,:) = RLTA_RStR;

            dTrunkDataRSt(winLenInd,:) = dTrunk_RStR;

            RHFLDataRSt(winLenInd,:) = RHF_RStR;
            RGLUDataRSt(winLenInd,:) = RGLU_RStR;
            RHAMDataRSt(winLenInd,:) = RHAM_RStR;
            RVASDataRSt(winLenInd,:) = RVAS_RStR;
            RGASDataRSt(winLenInd,:) = RGAS_RStR;
            RSOLDataRSt(winLenInd,:) = RSOL_RStR;
            RTADataRSt(winLenInd,:) = RTA_RStR;
            
            winLenInd = winLenInd + 1;
        else

            disp(testCountSt)
            % test data left stance            
            tLL_HFLDataLSt(testCountSt,:) = LLHFL_LStR;
            tLF_GLUDataLSt(testCountSt,:) = LFGLU_LStR;
            tLL_HAMDataLSt(testCountSt,:) = LLHAM_LStR;
            tLF_HAMDataLSt(testCountSt,:) = LFHAM_LStR;
            tLF_VASDataLSt(testCountSt,:) = LFVAS_LStR;
            tLF_GASDataLSt(testCountSt,:) = LFGAS_LStR;
            tLF_SOLDataLSt(testCountSt,:) = LFSOL_LStR;
            tLL_TADataLSt(testCountSt,:) = LLTA_LStR;

            tTrunkDataLSt(testCountSt,:) = Trunk_LStR;

            tRL_HFLDataLSt(testCountSt,:) = RLHFL_LStR;
            tRF_GLUDataLSt(testCountSt,:) = RFGLU_LStR;
            tRL_HAMDataLSt(testCountSt,:) = RLHAM_LStR;
            tRF_HAMDataLSt(testCountSt,:) = RFHAM_LStR;
            tRF_VASDataLSt(testCountSt,:) = RFVAS_LStR;
            tRF_GASDataLSt(testCountSt,:) = RFGAS_LStR;
            tRF_SOLDataLSt(testCountSt,:) = RFSOL_LStR;
            tRL_TADataLSt(testCountSt,:) = RLTA_LStR;

            tdTrunkDataLSt(testCountSt,:) = dTrunk_LStR;

            tLHFLDataLSt(testCountSt,:) = LHFL_LStR;
            tLGLUDataLSt(testCountSt,:) = LGLU_LStR;
            tLHAMDataLSt(testCountSt,:) = LHAM_LStR;
            tLVASDataLSt(testCountSt,:) = LVAS_LStR;
            tLGASDataLSt(testCountSt,:) = LGAS_LStR;
            tLSOLDataLSt(testCountSt,:) = LSOL_LStR;
            tLTADataLSt(testCountSt,:) = LTA_LStR;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % test data right stance
            
            tLL_HFLDataRSt(testCountSt,:) = LLHFL_RStR;
            tLF_GLUDataRSt(testCountSt,:) = LFGLU_RStR;
            tLL_HAMDataRSt(testCountSt,:) = LLHAM_RStR;
            tLF_HAMDataRSt(testCountSt,:) = LFHAM_RStR;
            tLF_VASDataRSt(testCountSt,:) = LFVAS_RStR;
            tLF_GASDataRSt(testCountSt,:) = LFGAS_RStR;
            tLF_SOLDataRSt(testCountSt,:) = LFSOL_RStR;
            tLL_TADataRSt(testCountSt,:) = LLTA_RStR;

            tTrunkDataRSt(testCountSt,:) = Trunk_RStR;

            tRL_HFLDataRSt(testCountSt,:) = RLHFL_RStR;
            tRF_GLUDataRSt(testCountSt,:) = RFGLU_RStR;
            tRL_HAMDataRSt(testCountSt,:) = RLHAM_RStR;
            tRF_HAMDataRSt(testCountSt,:) = RFHAM_RStR;
            tRF_VASDataRSt(testCountSt,:) = RFVAS_RStR;
            tRF_GASDataRSt(testCountSt,:) = RFGAS_RStR;
            tRF_SOLDataRSt(testCountSt,:) = RFSOL_RStR;
            tRL_TADataRSt(testCountSt,:) = RLTA_RStR;

            tdTrunkDataRSt(testCountSt,:) = dTrunk_RStR;

            tRHFLDataRSt(testCountSt,:) = RHF_RStR;
            tRGLUDataRSt(testCountSt,:) = RGLU_RStR;
            tRHAMDataRSt(testCountSt,:) = RHAM_RStR;
            tRVASDataRSt(testCountSt,:) = RVAS_RStR;
            tRGASDataRSt(testCountSt,:) = RGAS_RStR;
            tRSOLDataRSt(testCountSt,:) = RSOL_RStR;
            tRTADataRSt(testCountSt,:) = RTA_RStR;

            testCountSt = testCountSt+1;
            
        end    

    end
    
    allDataLSt(j,:)={LL_HFLDataLSt, LF_GLUDataLSt, LL_HAMDataLSt, LF_HAMDataLSt, LF_VASDataLSt, LF_GASDataLSt, LF_SOLDataLSt, LL_TADataLSt, TrunkDataLSt, ...
            RL_HFLDataLSt, RF_GLUDataLSt, RL_HAMDataLSt, RF_HAMDataLSt, RF_VASDataLSt, RF_GASDataLSt, RF_SOLDataLSt, RL_TADataLSt, dTrunkDataLSt,...
            LHFLDataLSt, LGLUDataLSt, LHAMDataLSt, LVASDataLSt, LGASDataLSt, LSOLDataLSt, LTADataLSt};
    
    allDataRSt(j,:)={ RL_HFLDataRSt, RF_GLUDataRSt, RL_HAMDataRSt, RF_HAMDataRSt, RF_VASDataRSt, RF_GASDataRSt, RF_SOLDataRSt, RL_TADataRSt, TrunkDataRSt, ...
            LL_HFLDataRSt, LF_GLUDataRSt, LL_HAMDataRSt, LF_HAMDataRSt, LF_VASDataRSt, LF_GASDataRSt, LF_SOLDataRSt, LL_TADataRSt, dTrunkDataRSt,...
            RHFLDataRSt, RGLUDataRSt, RHAMDataRSt, RVASDataRSt, RGASDataRSt, RSOLDataRSt, RTADataRSt};
        
    %%%%
    
    %%% Mean data (left stance)
    
    % mean kinematics over all gait cycles (only training)
    mLL_HFLDataLSt = mean(LL_HFLDataLSt);
    mLF_GLUDataLSt = mean(LF_GLUDataLSt);
    mLL_HAMDataLSt = mean(LL_HAMDataLSt);
    mLF_HAMDataLSt = mean(LF_HAMDataLSt);
    mLF_VASDataLSt = mean(LF_VASDataLSt);
    mLF_GASDataLSt = mean(LF_GASDataLSt);
    mLF_SOLDataLSt = mean(LF_SOLDataLSt);
    mLL_TADataLSt = mean(LL_TADataLSt);
    
    mTrunkDataLSt = mean(TrunkDataLSt);
    
    mRL_HFLDataLSt = mean(RL_HFLDataLSt);
    mRF_GLUDataLSt = mean(RF_GLUDataLSt);
    mRL_HAMDataLSt = mean(RL_HAMDataLSt);
    mRF_HAMDataLSt = mean(RF_HAMDataLSt);
    mRF_VASDataLSt = mean(RF_VASDataLSt);
    mRF_GASDataLSt = mean(RF_GASDataLSt);
    mRF_SOLDataLSt = mean(RF_SOLDataLSt);
    mRL_TADataLSt = mean(RL_TADataLSt);
    
    mdTrunkDataLSt = mean(dTrunkDataLSt);
     
    % collect mean muscle features over gaits (only training)
    mFeatDataLSt = [mLL_HFLDataLSt; mLF_GLUDataLSt; mLL_HAMDataLSt; mLF_HAMDataLSt; mLF_VASDataLSt; mLF_GASDataLSt; mLF_SOLDataLSt; mLL_TADataLSt; mTrunkDataLSt; ...
        mRL_HFLDataLSt; mRF_GLUDataLSt; mRL_HAMDataLSt; mRF_HAMDataLSt; mRF_VASDataLSt; mRF_GASDataLSt; mRF_SOLDataLSt; mRL_TADataLSt; mdTrunkDataLSt];
    
    % mean muscle stimulations over all gait cycles (only training)
    mLHFLDataLSt = mean(LHFLDataLSt);
    mLGLUDataLSt = mean(LGLUDataLSt);
    mLHAMDataLSt = mean(LHAMDataLSt);
    mLVASDataLSt = mean(LVASDataLSt);
    mLGASDataLSt = mean(LGASDataLSt);
    mLSOLDataLSt = mean(LSOLDataLSt);
    mLTADataLSt = mean(LTADataLSt);
    
    % collect mean muscles over gaits (only training)
    mMusDataLSt = [mLHFLDataLSt; mLGLUDataLSt; mLHAMDataLSt; mLVASDataLSt; mLGASDataLSt; mLSOLDataLSt; mLTADataLSt];
    
    mallDataLSt(j,:) = {mFeatDataLSt,mMusDataLSt};
    
    %%% Mean data (right stance)
    
    % mean kinematics over all gait cycles (only training)
    mLL_HFLDataRSt = mean(LL_HFLDataRSt);
    mLF_GLUDataRSt = mean(LF_GLUDataRSt);
    mLL_HAMDataRSt = mean(LL_HAMDataRSt);
    mLF_HAMDataRSt = mean(LF_HAMDataRSt);
    mLF_VASDataRSt = mean(LF_VASDataRSt);
    mLF_GASDataRSt = mean(LF_GASDataRSt);
    mLF_SOLDataRSt = mean(LF_SOLDataRSt);
    mLL_TADataRSt = mean(LL_TADataRSt);
    
    mTrunkDataRSt = mean(TrunkDataRSt);
    
    mRL_HFLDataRSt = mean(RL_HFLDataRSt);
    mRF_GLUDataRSt = mean(RF_GLUDataRSt);
    mRL_HAMDataRSt = mean(RL_HAMDataRSt);
    mRF_HAMDataRSt = mean(RF_HAMDataRSt);
    mRF_VASDataRSt = mean(RF_VASDataRSt);
    mRF_GASDataRSt = mean(RF_GASDataRSt);
    mRF_SOLDataRSt = mean(RF_SOLDataRSt);
    mRL_TADataRSt = mean(RL_TADataRSt);
    
    mdTrunkDataRSt = mean(dTrunkDataRSt);
    
    
    % collect mean muscle features over gaits (only training)
    mFeatDataRSt = [mRL_HFLDataRSt; mRF_GLUDataRSt; mRL_HAMDataRSt; mRF_HAMDataRSt; mRF_VASDataRSt; mRF_GASDataRSt; mRF_SOLDataRSt; mRL_TADataRSt; mTrunkDataRSt;...
        mLL_HFLDataRSt; mLF_GLUDataRSt; mLL_HAMDataRSt; mLF_HAMDataRSt; mLF_VASDataRSt; mLF_GASDataRSt; mLF_SOLDataRSt; mLL_TADataRSt; mdTrunkDataRSt];
    
    
    mRHFLDataRSt = mean(RHFLDataRSt);
    mRGLUDataRSt = mean(RGLUDataRSt);
    mRHAMDataRSt = mean(RHAMDataRSt);
    mRVASDataRSt = mean(RVASDataRSt);
    mRGASDataRSt = mean(RGASDataRSt);
    mRSOLDataRSt = mean(RSOLDataRSt);
    mRTADataRSt = mean(RTADataRSt);
    
    mMusDataRSt =  [mRHFLDataRSt; mRGLUDataRSt; mRHAMDataRSt; mRVASDataRSt; mRGASDataRSt; mRSOLDataRSt; mRTADataRSt];

    mallDataRSt(j,:) = {mFeatDataRSt,mMusDataRSt};
end

%% Store kinematics and muscle data, its mean and test data from gait cycles (Swing)

% for j = 1:window_num
%     % Preallocate memory for left stance muscle feat data
% 
%     LL_HFLDataLSw = NaN(window_len,resampleLSw_p);
%     LF_GLUDataLSw = NaN(window_len,resampleLSw_p);
%     LL_HAMDataLSw = NaN(window_len,resampleLSw_p);
%     LF_HAMDataLSw = NaN(window_len,resampleLSw_p);
%     LF_VASDataLSw = NaN(window_len,resampleLSw_p);
%     LF_GASDataLSw = NaN(window_len,resampleLSw_p);
%     LF_SOLDataLSw = NaN(window_len,resampleLSw_p);
%     LL_TADataLSw = NaN(window_len,resampleLSw_p);
% 
%     RL_HFLDataLSw = NaN(window_len,resampleLSw_p);
%     RF_GLUDataLSw = NaN(window_len,resampleLSw_p);
%     RL_HAMDataLSw = NaN(window_len,resampleLSw_p);
%     RF_HAMDataLSw = NaN(window_len,resampleLSw_p);
%     RF_VASDataLSw = NaN(window_len,resampleLSw_p);
%     RF_GASDataLSw = NaN(window_len,resampleLSw_p);
%     RF_SOLDataLSw = NaN(window_len,resampleLSw_p);
%     RL_TADataLSw = NaN(window_len,resampleLSw_p);
% 
%     TrunkDataLSw = NaN(window_len,resampleLSw_p);
%     dTrunkDataLSw = NaN(window_len,resampleLSw_p);
% 
%     % Preallocate memory for left stance muscle data
% 
%     LHFLDataLSw = NaN(window_len,resampleLSw_p);
%     LGLUDataLSw = NaN(window_len,resampleLSw_p);
%     LHAMDataLSw = NaN(window_len,resampleLSw_p);
%     LVASDataLSw = NaN(window_len,resampleLSw_p);
%     LGASDataLSw = NaN(window_len,resampleLSw_p);
%     LSOLDataLSw = NaN(window_len,resampleLSw_p);
%     LTADataLSw = NaN(window_len,resampleLSw_p);
%             
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%     % Preallocate memory for right stance muscle feat data
% 
%     LL_HFLDataRSw = NaN(window_len,resampleRSw_p);
%     LF_GLUDataRSw = NaN(window_len,resampleRSw_p);
%     LL_HAMDataRSw = NaN(window_len,resampleRSw_p);
%     LF_HAMDataRSw = NaN(window_len,resampleRSw_p);
%     LF_VASDataRSw = NaN(window_len,resampleRSw_p);
%     LF_GASDataRSw = NaN(window_len,resampleRSw_p);
%     LF_SOLDataRSw = NaN(window_len,resampleRSw_p);
%     LL_TADataRSw = NaN(window_len,resampleRSw_p);
% 
%     RL_HFLDataRSw = NaN(window_len,resampleRSw_p);
%     RF_GLUDataRSw = NaN(window_len,resampleRSw_p);
%     RL_HAMDataRSw = NaN(window_len,resampleRSw_p);
%     RF_HAMDataRSw = NaN(window_len,resampleRSw_p);
%     RF_VASDataRSw = NaN(window_len,resampleRSw_p);
%     RF_GASDataRSw = NaN(window_len,resampleRSw_p);
%     RF_SOLDataRSw = NaN(window_len,resampleRSw_p);
%     RL_TADataRSw = NaN(window_len,resampleRSw_p);
% 
%     TrunkDataRSw = NaN(window_len,resampleRSw_p);
%     dTrunkDataRSw = NaN(window_len,resampleRSw_p);
% 
%     % Preallocate memory for right muscle stance data
% 
%     RHFLDataRSw = NaN(window_len,resampleRSw_p);
%     RGLUDataRSw = NaN(window_len,resampleRSw_p);
%     RHAMDataRSw = NaN(window_len,resampleRSw_p);
%     RVASDataRSw = NaN(window_len,resampleRSw_p);
%     RGASDataRSw = NaN(window_len,resampleRSw_p);
%     RSOLDataRSw = NaN(window_len,resampleRSw_p);
%     RTADataRSw = NaN(window_len,resampleRSw_p);
% 
%     winLenInd = 1;
%     gait_ind = (j-1)*window_len+1+countSw;
%     num_testGaits = find(testDataInd>=gait_ind & testDataInd<=window_len+gait_ind-1);
%     countSw = countSw + length(num_testGaits);
% 
%     for i=gait_ind:window_len+gait_ind+length(num_testGaits)-1
%         
%         LLHFL_LSw = LL_HFL.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LLHFL_LSwR = resample(LLHFL_LSw,resampleLSw_p,size(LLHFL_LSw,1)); % resample to max gait cycle length
%         
%         LFGLU_LSw = LF_GLU.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LFGLU_LSwR = resample(LFGLU_LSw,resampleLSw_p,size(LFGLU_LSw,1));
%         
%         LLHAM_LSw = LL_HAM.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LLHAM_LSwR = resample(LLHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LFHAM_LSw = LF_HAM.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LFHAM_LSwR = resample(LFHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LFVAS_LSw = LF_VAS.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LFVAS_LSwR = resample(LFVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LFGAS_LSw = LF_GAS.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LFGAS_LSwR = resample(LFGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LFSOL_LSw = LF_SOL.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LFSOL_LSwR = resample(LFSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LLTA_LSw = LL_TA.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LLTA_LSwR = resample(LLTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RLHFL_LSw = RL_HFL.Data(LswingStart_ind(i):LswingEnd_ind(i),:); 
%         RLHFL_LSwR = resample(RLHFL_LSw,resampleLSw_p,size(LLHAM_LSw,1)); % resample to max gait cycle length
%         
%         RFGLU_LSw = RF_GLU.Data(LswingStart_ind(i):LswingEnd_ind(i),:); 
%         RFGLU_LSwR = resample(RFGLU_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RLHAM_LSw = RL_HAM.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         RLHAM_LSwR = resample(RLHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RFHAM_LSw = RF_HAM.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         RFHAM_LSwR = resample(RFHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RFVAS_LSw = RF_VAS.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         RFVAS_LSwR = resample(RFVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RFGAS_LSw = RF_GAS.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         RFGAS_LSwR = resample(RFGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RFSOL_LSw = RF_SOL.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         RFSOL_LSwR = resample(RFSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         RLTA_LSw = RL_TA.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         RLTA_LSwR = resample(RLTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%               
%         Trunk_LSw = Torso.Data(LswingStart_ind(i):LswingEnd_ind(i),1);
%         Trunk_LSwR = resample(Trunk_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         dTrunk_LSw = Torso.Data(LswingStart_ind(i):LswingEnd_ind(i),2);
%         dTrunk_LSwR = resample(dTrunk_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         %%%%%
%                
%         LHFL_LSw = LStimHFL.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LHFL_LSwR = resample(LHFL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LGLU_LSw = LStimGLU.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LGLU_LSwR = resample(LGLU_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LHAM_LSw = LStimHAM.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LHAM_LSwR = resample(LHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LVAS_LSw = LStimVAS.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LVAS_LSwR = resample(LVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LGAS_LSw = LStimGAS.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LGAS_LSwR = resample(LGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LSOL_LSw = LStimSOL.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LSOL_LSwR = resample(LSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         LTA_LSw = LStimTA.Data(LswingStart_ind(i):LswingEnd_ind(i),:);
%         LTA_LSwR = resample(LTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
%         
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         
%         LLHFL_RSw = LL_HFL.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LLHFL_RSwR = resample(LLHFL_RSw,resampleRSw_p,size(LLHFL_RSw,1)); % resample to max gait cycle length
%         
%         LFGLU_RSw = LF_GLU.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LFGLU_RSwR = resample(LFGLU_RSw,resampleRSw_p,size(LFGLU_RSw,1));
%         
%         LLHAM_RSw = LL_HAM.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LLHAM_RSwR = resample(LLHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         LFHAM_RSw = LF_HAM.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LFHAM_RSwR = resample(LFHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         LFVAS_RSw = LF_VAS.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LFVAS_RSwR = resample(LFVAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         LFGAS_RSw = LF_GAS.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LFGAS_RSwR = resample(LFGAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         LFSOL_RSw = LF_SOL.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LFSOL_RSwR = resample(LFSOL_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         LLTA_RSw = LL_TA.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         LLTA_RSwR = resample(LLTA_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RLHFL_RSw = RL_HFL.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RLHFL_RSwR = resample(RLHFL_RSw,resampleRSw_p,size(LLHAM_RSw,1)); % resample to max gait cycle length
%         
%         RFGLU_RSw = RF_GLU.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RFGLU_RSwR = resample(RFGLU_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RLHAM_RSw = RL_HAM.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RLHAM_RSwR = resample(RLHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RFHAM_RSw = RF_HAM.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RFHAM_RSwR = resample(RFHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RFVAS_RSw = RF_VAS.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RFVAS_RSwR = resample(RFVAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RFGAS_RSw = RF_GAS.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RFGAS_RSwR = resample(RFGAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RFSOL_RSw = RF_SOL.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RFSOL_RSwR = resample(RFSOL_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RLTA_RSw = RL_TA.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RLTA_RSwR = resample(RLTA_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         Trunk_RSw = Torso.Data(RswingStart_ind(i):RswingEnd_ind(i),1);
%         Trunk_RSwR = resample(Trunk_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         dTrunk_RSw = Torso.Data(RswingStart_ind(i):RswingEnd_ind(i),2);
%         dTrunk_RSwR = resample(dTrunk_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         %%%%
%         
%         RHF_RSw = RStimHFL.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RHF_RSwR = resample(RHF_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RGLU_RSw = RStimGLU.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RGLU_RSwR = resample(RGLU_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RHAM_RSw = RStimHAM.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RHAM_RSwR = resample(RHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RVAS_RSw = RStimVAS.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RVAS_RSwR = resample(RVAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RGAS_RSw = RStimGAS.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RGAS_RSwR = resample(RGAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RSOL_RSw = RStimSOL.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RSOL_RSwR = resample(RSOL_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         RTA_RSw = RStimTA.Data(RswingStart_ind(i):RswingEnd_ind(i),:);
%         RTA_RSwR = resample(RTA_RSw,resampleRSw_p,size(LLHAM_RSw,1));
%         
%         if (isempty(find(testDataInd ==i))) % find if this gait cycle is a test sample
% 
%             LL_HFLDataLSw(winLenInd,:) = LLHFL_LSwR;
%             LF_GLUDataLSw(winLenInd,:) = LFGLU_LSwR;
%             LL_HAMDataLSw(winLenInd,:) = LLHAM_LSwR;
%             LF_HAMDataLSw(winLenInd,:) = LFHAM_LSwR;
%             LF_VASDataLSw(winLenInd,:) = LFVAS_LSwR;
%             LF_GASDataLSw(winLenInd,:) = LFGAS_LSwR;
%             LF_SOLDataLSw(winLenInd,:) = LFSOL_LSwR;
%             LL_TADataLSw(winLenInd,:) = LLTA_LSwR;
% 
%             TrunkDataLSw(winLenInd,:) = Trunk_LSwR;
% 
%             RL_HFLDataLSw(winLenInd,:) = RLHFL_LSwR;
%             RF_GLUDataLSw(winLenInd,:) = RFGLU_LSwR;
%             RL_HAMDataLSw(winLenInd,:) = RLHAM_LSwR;
%             RF_HAMDataLSw(winLenInd,:) = RFHAM_LSwR;
%             RF_VASDataLSw(winLenInd,:) = RFVAS_LSwR;
%             RF_GASDataLSw(winLenInd,:) = RFGAS_LSwR;
%             RF_SOLDataLSw(winLenInd,:) = RFSOL_LSwR;
%             RL_TADataLSw(winLenInd,:) = RLTA_LSwR;
% 
%             dTrunkDataLSw(winLenInd,:) = dTrunk_LSwR;
% 
%             LHFLDataLSw(winLenInd,:) = LHFL_LSwR;
%             LGLUDataLSw(winLenInd,:) = LGLU_LSwR;
%             LHAMDataLSw(winLenInd,:) = LHAM_LSwR;
%             LVASDataLSw(winLenInd,:) = LVAS_LSwR;
%             LGASDataLSw(winLenInd,:) = LGAS_LSwR;
%             LSOLDataLSw(winLenInd,:) = LSOL_LSwR;
%             LTADataLSw(winLenInd,:) = LTA_LSwR;
%             
%             %%%%%            
%             LL_HFLDataRSw(winLenInd,:) = LLHFL_RSwR;
%             LF_GLUDataRSw(winLenInd,:) = LFGLU_RSwR;
%             LL_HAMDataRSw(winLenInd,:) = LLHAM_RSwR;
%             LF_HAMDataRSw(winLenInd,:) = LFHAM_RSwR;
%             LF_VASDataRSw(winLenInd,:) = LFVAS_RSwR;
%             LF_GASDataRSw(winLenInd,:) = LFGAS_RSwR;
%             LF_SOLDataRSw(winLenInd,:) = LFSOL_RSwR;
%             LL_TADataRSw(winLenInd,:) = LLTA_RSwR;
% 
%             TrunkDataRSw(winLenInd,:) = Trunk_RSwR;
% 
%             RL_HFLDataRSw(winLenInd,:) = RLHFL_RSwR;
%             RF_GLUDataRSw(winLenInd,:) = RFGLU_RSwR;
%             RL_HAMDataRSw(winLenInd,:) = RLHAM_RSwR;
%             RF_HAMDataRSw(winLenInd,:) = RFHAM_RSwR;
%             RF_VASDataRSw(winLenInd,:) = RFVAS_RSwR;
%             RF_GASDataRSw(winLenInd,:) = RFGAS_RSwR;
%             RF_SOLDataRSw(winLenInd,:) = RFSOL_RSwR;
%             RL_TADataRSw(winLenInd,:) = RLTA_RSwR;
% 
%             dTrunkDataRSw(winLenInd,:) = dTrunk_RSwR;
% 
%             RHFLDataRSw(winLenInd,:) = RHF_RSwR;
%             RGLUDataRSw(winLenInd,:) = RGLU_RSwR;
%             RHAMDataRSw(winLenInd,:) = RHAM_RSwR;
%             RVASDataRSw(winLenInd,:) = RVAS_RSwR;
%             RGASDataRSw(winLenInd,:) = RGAS_RSwR;
%             RSOLDataRSw(winLenInd,:) = RSOL_RSwR;
%             RTADataRSw(winLenInd,:) = RTA_RSwR;
%             
%             winLenInd = winLenInd + 1;
%         else
%
%             % test data left stance             
%             tLL_HFLDataLSw(testCountSw,:) = LLHFL_LSwR;
%             tLF_GLUDataLSw(testCountSw,:) = LFGLU_LSwR;
%             tLL_HAMDataLSw(testCountSw,:) = LLHAM_LSwR;
%             tLF_HAMDataLSw(testCountSw,:) = LFHAM_LSwR;
%             tLF_VASDataLSw(testCountSw,:) = LFVAS_LSwR;
%             tLF_GASDataLSw(testCountSw,:) = LFGAS_LSwR;
%             tLF_SOLDataLSw(testCountSw,:) = LFSOL_LSwR;
%             tLL_TADataLSw(testCountSw,:) = LLTA_LSwR;
% 
%             tTrunkDataLSw(testCountSw,:) = Trunk_LSwR;
% 
%             tRL_HFLDataLSw(testCountSw,:) = RLHFL_LSwR;
%             tRF_GLUDataLSw(testCountSw,:) = RFGLU_LSwR;
%             tRL_HAMDataLSw(testCountSw,:) = RLHAM_LSwR;
%             tRF_HAMDataLSw(testCountSw,:) = RFHAM_LSwR;
%             tRF_VASDataLSw(testCountSw,:) = RFVAS_LSwR;
%             tRF_GASDataLSw(testCountSw,:) = RFGAS_LSwR;
%             tRF_SOLDataLSw(testCountSw,:) = RFSOL_LSwR;
%             tRL_TADataLSw(testCountSw,:) = RLTA_LSwR;
% 
%             tdTrunkDataLSw(testCountSw,:) = dTrunk_LSwR;
% 
%             tLHFLDataLSw(testCountSw,:) = LHFL_LSwR;
%             tLGLUDataLSw(testCountSw,:) = LGLU_LSwR;
%             tLHAMDataLSw(testCountSw,:) = LHAM_LSwR;
%             tLVASDataLSw(testCountSw,:) = LVAS_LSwR;
%             tLGASDataLSw(testCountSw,:) = LGAS_LSwR;
%             tLSOLDataLSw(testCountSw,:) = LSOL_LSwR;
%             tLTADataLSw(testCountSw,:) = LTA_LSwR;
% 
%             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%             % test data right stance
%             
%             tLL_HFLDataRSw(testCountSw,:) = LLHFL_RSwR;
%             tLF_GLUDataRSw(testCountSw,:) = LFGLU_RSwR;
%             tLL_HAMDataRSw(testCountSw,:) = LLHAM_RSwR;
%             tLF_HAMDataRSw(testCountSw,:) = LFHAM_RSwR;
%             tLF_VASDataRSw(testCountSw,:) = LFVAS_RSwR;
%             tLF_GASDataRSw(testCountSw,:) = LFGAS_RSwR;
%             tLF_SOLDataRSw(testCountSw,:) = LFSOL_RSwR;
%             tLL_TADataRSw(testCountSw,:) = LLTA_RSwR;
% 
%             tTrunkDataRSw(testCountSw,:) = Trunk_RSwR;
% 
%             tRL_HFLDataRSw(testCountSw,:) = RLHFL_RSwR;
%             tRF_GLUDataRSw(testCountSw,:) = RFGLU_RSwR;
%             tRL_HAMDataRSw(testCountSw,:) = RLHAM_RSwR;
%             tRF_HAMDataRSw(testCountSw,:) = RFHAM_RSwR;
%             tRF_VASDataRSw(testCountSw,:) = RFVAS_RSwR;
%             tRF_GASDataRSw(testCountSw,:) = RFGAS_RSwR;
%             tRF_SOLDataRSw(testCountSw,:) = RFSOL_RSwR;
%             tRL_TADataRSw(testCountSw,:) = RLTA_RSwR;
% 
%             tdTrunkDataRSw(testCountSw,:) = dTrunk_RSwR;
% 
%             tRHFLDataRSw(testCountSw,:) = RHF_RSwR;
%             tRGLUDataRSw(testCountSw,:) = RGLU_RSwR;
%             tRHAMDataRSw(testCountSw,:) = RHAM_RSwR;
%             tRVASDataRSw(testCountSw,:) = RVAS_RSwR;
%             tRGASDataRSw(testCountSw,:) = RGAS_RSwR;
%             tRSOLDataRSw(testCountSw,:) = RSOL_RSwR;
%             tRTADataRSw(testCountSw,:) = RTA_RSwR;
% 
%             testCountSw = testCountSw+1;
%             
%         end    
% 
%     end
%     
%     allDataLSw(j,:)={LL_HFLDataLSw, LF_GLUDataLSw, LL_HAMDataLSw, LF_HAMDataLSw, LF_VASDataLSw, LF_GASDataLSw, LF_SOLDataLSw, LL_TADataLSw, TrunkDataLSw, ...
%             RL_HFLDataLSw, RF_GLUDataLSw, RL_HAMDataLSw, RF_HAMDataLSw, RF_VASDataLSw, RF_GASDataLSw, RF_SOLDataLSw, RL_TADataLSw, dTrunkDataLSw,...
%             LHFLDataLSw, LGLUDataLSw, LHAMDataLSw, LVASDataLSw, LGASDataLSw, LSOLDataLSw, LTADataLSw};
%     
%     allDataRSw(j,:)={ RL_HFLDataRSw, RF_GLUDataRSw, RL_HAMDataRSw, RF_HAMDataRSw, RF_VASDataRSw, RF_GASDataRSw, RF_SOLDataRSw, RL_TADataRSw, TrunkDataRSw, ...
%             LL_HFLDataRSw, LF_GLUDataRSw, LL_HAMDataRSw, LF_HAMDataRSw, LF_VASDataRSw, LF_GASDataRSw, LF_SOLDataRSw, LL_TADataRSw, dTrunkDataRSw,...
%             RHFLDataRSw, RGLUDataRSw, RHAMDataRSw, RVASDataRSw, RGASDataRSw, RSOLDataRSw, RTADataRSw};
%         
%     %%%%
%     
%     %%% Mean data (left stance)
%     
%     % mean kinematics over all gait cycles (only training)
%     mLL_HFLDataLSw = mean(LL_HFLDataLSw);
%     mLF_GLUDataLSw = mean(LF_GLUDataLSw);
%     mLL_HAMDataLSw = mean(LL_HAMDataLSw);
%     mLF_HAMDataLSw = mean(LF_HAMDataLSw);
%     mLF_VASDataLSw = mean(LF_VASDataLSw);
%     mLF_GASDataLSw = mean(LF_GASDataLSw);
%     mLF_SOLDataLSw = mean(LF_SOLDataLSw);
%     mLL_TADataLSw = mean(LL_TADataLSw);
%     
%     mTrunkDataLSw = mean(TrunkDataLSw);
%     
%     mRL_HFLDataLSw = mean(RL_HFLDataLSw);
%     mRF_GLUDataLSw = mean(RF_GLUDataLSw);
%     mRL_HAMDataLSw = mean(RL_HAMDataLSw);
%     mRF_HAMDataLSw = mean(RF_HAMDataLSw);
%     mRF_VASDataLSw = mean(RF_VASDataLSw);
%     mRF_GASDataLSw = mean(RF_GASDataLSw);
%     mRF_SOLDataLSw = mean(RF_SOLDataLSw);
%     mRL_TADataLSw = mean(RL_TADataLSw);
%     
%     mdTrunkDataLSw = mean(dTrunkDataLSw);
%      
%     % collect mean muscle features over gaits (only training)
%     mFeatDataLSw = [mLL_HFLDataLSw; mLF_GLUDataLSw; mLL_HAMDataLSw; mLF_HAMDataLSw; mLF_VASDataLSw; mLF_GASDataLSw; mLF_SOLDataLSw; mLL_TADataLSw; mTrunkDataLSw; ...
%         mRL_HFLDataLSw; mRF_GLUDataLSw; mRL_HAMDataLSw; mRF_HAMDataLSw; mRF_VASDataLSw; mRF_GASDataLSw; mRF_SOLDataLSw; mRL_TADataLSw; mdTrunkDataLSw];
%     
%     % mean muscle stimulations over all gait cycles (only training)
%     mLHFLDataLSw = mean(LHFLDataLSw);
%     mLGLUDataLSw = mean(LGLUDataLSw);
%     mLHAMDataLSw = mean(LHAMDataLSw);
%     mLVASDataLSw = mean(LVASDataLSw);
%     mLGASDataLSw = mean(LGASDataLSw);
%     mLSOLDataLSw = mean(LSOLDataLSw);
%     mLTADataLSw = mean(LTADataLSw);
%     
%     % collect mean muscles over gaits (only training)
%     mMusDataLSw = [mLHFLDataLSw; mLGLUDataLSw; mLHAMDataLSw; mLVASDataLSw; mLGASDataLSw; mLSOLDataLSw; mLTADataLSw];
%     
%     mallDataLSw(j,:) = {mFeatDataLSw,mMusDataLSw};
%     
%     %%% Mean data (right stance)
%     
%     % mean kinematics over all gait cycles (only training)
%     mLL_HFLDataRSw = mean(LL_HFLDataRSw);
%     mLF_GLUDataRSw = mean(LF_GLUDataRSw);
%     mLL_HAMDataRSw = mean(LL_HAMDataRSw);
%     mLF_HAMDataRSw = mean(LF_HAMDataRSw);
%     mLF_VASDataRSw = mean(LF_VASDataRSw);
%     mLF_GASDataRSw = mean(LF_GASDataRSw);
%     mLF_SOLDataRSw = mean(LF_SOLDataRSw);
%     mLL_TADataRSw = mean(LL_TADataRSw);
%     
%     mTrunkDataRSw = mean(TrunkDataRSw);
%     
%     mRL_HFLDataRSw = mean(RL_HFLDataRSw);
%     mRF_GLUDataRSw = mean(RF_GLUDataRSw);
%     mRL_HAMDataRSw = mean(RL_HAMDataRSw);
%     mRF_HAMDataRSw = mean(RF_HAMDataRSw);
%     mRF_VASDataRSw = mean(RF_VASDataRSw);
%     mRF_GASDataRSw = mean(RF_GASDataRSw);
%     mRF_SOLDataRSw = mean(RF_SOLDataRSw);
%     mRL_TADataRSw = mean(RL_TADataRSw);
%     
%     mdTrunkDataRSw = mean(dTrunkDataRSw);
%     
%     
%     % collect mean muscle features over gaits (only training)
%     mFeatDataRSw = [mRL_HFLDataRSw; mRF_GLUDataRSw; mRL_HAMDataRSw; mRF_HAMDataRSw; mRF_VASDataRSw; mRF_GASDataRSw; mRF_SOLDataRSw; mRL_TADataRSw; mTrunkDataRSw;...
%         mLL_HFLDataRSw; mLF_GLUDataRSw; mLL_HAMDataRSw; mLF_HAMDataRSw; mLF_VASDataRSw; mLF_GASDataRSw; mLF_SOLDataRSw; mLL_TADataRSw; mdTrunkDataRSw];
%     
%     
%     mRHFLDataRSw = mean(RHFLDataRSw);
%     mRGLUDataRSw = mean(RGLUDataRSw);
%     mRHAMDataRSw = mean(RHAMDataRSw);
%     mRVASDataRSw = mean(RVASDataRSw);
%     mRGASDataRSw = mean(RGASDataRSw);
%     mRSOLDataRSw = mean(RSOLDataRSw);
%     mRTADataRSw = mean(RTADataRSw);
%     
%     mMusDataRSw =  [mRHFLDataRSw; mRGLUDataRSw; mRHAMDataRSw; mRVASDataRSw; mRGASDataRSw; mRSOLDataRSw; mRTADataRSw];
% 
%     mallDataRSw(j,:) = {mFeatDataRSw,mMusDataRSw};
% end


