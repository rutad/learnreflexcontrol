function [mMusDataDistLSt, mFeatDataDistLSt, trunkNorm, tMusDataDistLSt, tFeatDataDistLSt] =  collectGaitDataDistSt_wTestData(fd_dist, sensors, actuators, L_TD, L_TO)

testNum = 2;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end);%LTO_ind(2:end-1);

% if (length(LswingStart_ind)~=length(LgaitStart_ind))
%     extragait=length(LgaitStart_ind)-length(LswingStart_ind);
%     LgaitStart_ind(end-extragait+1:end)=[];
% end

disturbStanceInd = find(diff(fd_dist.Data(:,1))<0);
distWindInd = NaN(length(disturbStanceInd),1);

for i=1:length(disturbStanceInd)

    distWind = find(LgaitStart_ind>disturbStanceInd(i));
    
    if ~isempty(distWind)
        distWindInd(i) = distWind(1);
    else
        distWindInd(i) =[];
    end
    
end

LdataLen = length(distWindInd);

% resampling length left
resampleDistLSt_p = max(LswingStart_ind(distWindInd)-LgaitStart_ind(distWindInd))+1;

% Extract sensor data
LL_HFL =sensors{1};
LF_GLU = sensors{2};
LL_HAM = sensors{3};
LF_HAM = sensors{4};
LF_VAS = sensors{5};
LF_GAS = sensors{6};
LF_SOL = sensors{7};
LL_TA = sensors{8};

RL_HFL =sensors{9};
RF_GLU = sensors{10};
RL_HAM = sensors{11};
RF_HAM = sensors{12};
RF_VAS = sensors{13};
RF_GAS = sensors{14};
RF_SOL = sensors{15};
RL_TA = sensors{16};

Torso = sensors{17};

% Extract actuator data
LStimHFL = actuators{1};
LStimGLU = actuators{2};
LStimHAM = actuators{3};
LStimVAS = actuators{4};
LStimGAS = actuators{5};
LStimSOL = actuators{6};
LStimTA = actuators{7};

%% Store kinematics and muscle data from gait cycles (Left Disturbed Swing)

% Preallocate memory for left swing muscle feat data

LL_HFLDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LF_GLUDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LL_HAMDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LF_HAMDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LF_VASDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LF_GASDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LF_SOLDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LL_TADataDistLSt = NaN(LdataLen,resampleDistLSt_p);

RL_HFLDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RF_GLUDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RL_HAMDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RF_HAMDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RF_VASDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RF_GASDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RF_SOLDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
RL_TADataDistLSt = NaN(LdataLen,resampleDistLSt_p);

TrunkDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
dTrunkDataDistLSt = NaN(LdataLen,resampleDistLSt_p);

% Preallocate memory for left swing muscle data

LHFLDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LGLUDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LHAMDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LVASDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LGASDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LSOLDataDistLSt = NaN(LdataLen,resampleDistLSt_p);
LTADataDistLSt = NaN(LdataLen,resampleDistLSt_p);


for ind=1:LdataLen
    
    i = distWindInd(ind);
    
    LLHFL_DistLSt = LL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angle
    LLHFL_DistLStR = resample(LLHFL_DistLSt,resampleDistLSt_p,size(LLHFL_DistLSt,1)); % resample to max gait cycle length 
    
    LFGLU_DistLSt = LF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angular velocity
    LFGLU_DistLStR = resample(LFGLU_DistLSt,resampleDistLSt_p,size(LFGLU_DistLSt,1));
    
    LLHAM_DistLSt = LL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LLHAM_DistLStR = resample(LLHAM_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LFHAM_DistLSt = LF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFHAM_DistLStR = resample(LFHAM_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LFVAS_DistLSt = LF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFVAS_DistLStR = resample(LFVAS_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LFGAS_DistLSt = LF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFGAS_DistLStR = resample(LFGAS_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LFSOL_DistLSt = LF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFSOL_DistLStR = resample(LFSOL_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
   
    LLTA_DistLSt = LL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LLTA_DistLStR = resample(LLTA_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    RLHFL_DistLSt = RL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angle
    RLHFL_DistLStR = resample(RLHFL_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1)); % resample to max gait cycle length 
    
    RFGLU_DistLSt = RF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angular velocity
    RFGLU_DistLStR = resample(RFGLU_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    RLHAM_DistLSt = RL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RLHAM_DistLStR = resample(RLHAM_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    RFHAM_DistLSt = RF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFHAM_DistLStR = resample(RFHAM_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    RFVAS_DistLSt = RF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFVAS_DistLStR = resample(RFVAS_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    RFGAS_DistLSt = RF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFGAS_DistLStR = resample(RFGAS_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    RFSOL_DistLSt = RF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFSOL_DistLStR = resample(RFSOL_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
   
    RLTA_DistLSt = RL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RLTA_DistLStR = resample(RLTA_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
  
    Trunk_DistLSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),1);
    Trunk_DistLStR = resample(Trunk_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    dTrunk_DistLSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),2);
    dTrunk_DistLStR = resample(dTrunk_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
        
    LHFL_DistLSt = LStimHFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LHFL_DistLStR = resample(LHFL_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LGLU_DistLSt = LStimGLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LGLU_DistLStR = resample(LGLU_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LHAM_DistLSt = LStimHAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LHAM_DistLStR = resample(LHAM_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LVAS_DistLSt = LStimVAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LVAS_DistLStR = resample(LVAS_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LGAS_DistLSt = LStimGAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LGAS_DistLStR = resample(LGAS_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LSOL_DistLSt = LStimSOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LSOL_DistLStR = resample(LSOL_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    LTA_DistLSt = LStimTA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LTA_DistLStR = resample(LTA_DistLSt,resampleDistLSt_p,size(LLHAM_DistLSt,1));
    
    
    LL_HFLDataDistLSt(ind,:) = LLHFL_DistLStR;
    LF_GLUDataDistLSt(ind,:) = LFGLU_DistLStR;
    LL_HAMDataDistLSt(ind,:) = LLHAM_DistLStR;
    LF_HAMDataDistLSt(ind,:) = LFHAM_DistLStR;
    LF_VASDataDistLSt(ind,:) = LFVAS_DistLStR;
    LF_GASDataDistLSt(ind,:) = LFGAS_DistLStR;
    LF_SOLDataDistLSt(ind,:) = LFSOL_DistLStR;
    LL_TADataDistLSt(ind,:) = LLTA_DistLStR;
    
    TrunkDataDistLSt(ind,:) = Trunk_DistLStR;
    
    RL_HFLDataDistLSt(ind,:) = RLHFL_DistLStR;
    RF_GLUDataDistLSt(ind,:) = RFGLU_DistLStR;
    RL_HAMDataDistLSt(ind,:) = RLHAM_DistLStR;
    RF_HAMDataDistLSt(ind,:) = RFHAM_DistLStR;
    RF_VASDataDistLSt(ind,:) = RFVAS_DistLStR;
    RF_GASDataDistLSt(ind,:) = RFGAS_DistLStR;
    RF_SOLDataDistLSt(ind,:) = RFSOL_DistLStR;
    RL_TADataDistLSt(ind,:) = RLTA_DistLStR;
    
    dTrunkDataDistLSt(ind,:) = dTrunk_DistLStR;
    
    LHFLDataDistLSt(ind,:) = LHFL_DistLStR;
    LGLUDataDistLSt(ind,:) = LGLU_DistLStR;
    LHAMDataDistLSt(ind,:) = LHAM_DistLStR;
    LVASDataDistLSt(ind,:) = LVAS_DistLStR;
    LGASDataDistLSt(ind,:) = LGAS_DistLStR;
    LSOLDataDistLSt(ind,:) = LSOL_DistLStR;
    LTADataDistLSt(ind,:) = LTA_DistLStR;
    
end

%% Normalize trunk data
dtrunkMax = max(max(mean(dTrunkDataDistLSt)));
trunkMax = max(max(mean(TrunkDataDistLSt)));

%% Test Data
if LdataLen>testNum
    testDataInd = LdataLen*ones(1,testNum) - (1:testNum);

for i = 1:testNum
    tLL_HFLDataDistLSt = LL_HFLDataDistLSt(testDataInd(i),:);
    tLF_GLUDataDistLSt = LF_GLUDataDistLSt(testDataInd(i),:);
    tLL_HAMDataDistLSt = LL_HAMDataDistLSt(testDataInd(i),:);
    tLF_HAMDataDistLSt = LF_HAMDataDistLSt(testDataInd(i),:);
    tLF_VASDataDistLSt = LF_VASDataDistLSt(testDataInd(i),:);
    tLF_GASDataDistLSt = LF_GASDataDistLSt(testDataInd(i),:);
    tLF_SOLDataDistLSt = LF_SOLDataDistLSt(testDataInd(i),:);
    tLL_TADataDistLSt = LL_TADataDistLSt(testDataInd(i),:);

    tTrunkDataDistLSt = TrunkDataDistLSt(testDataInd(i),:)./trunkMax;

    tRL_HFLDataDistLSt = RL_HFLDataDistLSt(testDataInd(i),:);
    tRF_GLUDataDistLSt = RF_GLUDataDistLSt(testDataInd(i),:);
    tRL_HAMDataDistLSt = RL_HAMDataDistLSt(testDataInd(i),:);
    tRF_HAMDataDistLSt = RF_HAMDataDistLSt(testDataInd(i),:);
    tRF_VASDataDistLSt = RF_VASDataDistLSt(testDataInd(i),:);
    tRF_GASDataDistLSt = RF_GASDataDistLSt(testDataInd(i),:);
    tRF_SOLDataDistLSt = RF_SOLDataDistLSt(testDataInd(i),:);
    tRL_TADataDistLSt = RL_TADataDistLSt(testDataInd(i),:);

    tdTrunkDataDistLSt = dTrunkDataDistLSt(testDataInd(i),:)./dtrunkMax;

    tLHFLDataDistLSt = LHFLDataDistLSt(testDataInd(i),:);
    tLGLUDataDistLSt = LGLUDataDistLSt(testDataInd(i),:);
    tLHAMDataDistLSt = LHAMDataDistLSt(testDataInd(i),:);
    tLVASDataDistLSt = LVASDataDistLSt(testDataInd(i),:);
    tLGASDataDistLSt = LGASDataDistLSt(testDataInd(i),:);
    tLSOLDataDistLSt = LSOLDataDistLSt(testDataInd(i),:);
    tLTADataDistLSt = LTADataDistLSt(testDataInd(i),:);

    % Remove test data from all data

    LL_HFLDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LF_GLUDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LL_HAMDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LF_HAMDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LF_VASDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LF_GASDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LF_SOLDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    LL_TADataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);

    TrunkDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);

    RL_HFLDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RF_GLUDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RL_HAMDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RF_HAMDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RF_VASDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RF_GASDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RF_SOLDataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);
    RL_TADataDistLSt(testDataInd(i),:)= zeros(length(testDataInd(i)),resampleDistLSt_p);

    dTrunkDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);

    LHFLDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);
    LGLUDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);
    LHAMDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);
    LVASDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);
    LGASDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);
    LSOLDataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);
    LTADataDistLSt(testDataInd(i),:) = zeros(length(testDataInd(i)),resampleDistLSt_p);

    % collect test muscle features over gaits (only training)
    tFeatDataDistLSt{i} = [tLL_HFLDataDistLSt; tLF_GLUDataDistLSt; tLL_HAMDataDistLSt; tLF_HAMDataDistLSt; tLF_VASDataDistLSt; tLF_GASDataDistLSt; tLF_SOLDataDistLSt; tLL_TADataDistLSt; tTrunkDataDistLSt; ...
                tRL_HFLDataDistLSt; tRF_GLUDataDistLSt; tRL_HAMDataDistLSt; tRF_HAMDataDistLSt; tRF_VASDataDistLSt; tRF_GASDataDistLSt; tRF_SOLDataDistLSt; tRL_TADataDistLSt; tdTrunkDataDistLSt];

    % collect test muscles over gaits (only training)
    tMusDataDistLSt{i} = [tLHFLDataDistLSt; tLGLUDataDistLSt; tLHAMDataDistLSt; tLVASDataDistLSt; tLGASDataDistLSt; tLSOLDataDistLSt; tLTADataDistLSt];
    
end

else
    tFeatDataDistLSt = [];
    tMusDataDistLSt = [];
end

%% Mean data (left disturbed stance)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataDistLSt = mean(LL_HFLDataDistLSt);
mLF_GLUDataDistLSt = mean(LF_GLUDataDistLSt);
mLL_HAMDataDistLSt = mean(LL_HAMDataDistLSt);
mLF_HAMDataDistLSt = mean(LF_HAMDataDistLSt);
mLF_VASDataDistLSt = mean(LF_VASDataDistLSt);
mLF_GASDataDistLSt = mean(LF_GASDataDistLSt);
mLF_SOLDataDistLSt = mean(LF_SOLDataDistLSt);
mLL_TADataDistLSt = mean(LL_TADataDistLSt);

mTrunkDataDistLSt = mean(TrunkDataDistLSt)./trunkMax;

mRL_HFLDataDistLSt = mean(RL_HFLDataDistLSt);
mRF_GLUDataDistLSt = mean(RF_GLUDataDistLSt);
mRL_HAMDataDistLSt = mean(RL_HAMDataDistLSt);
mRF_HAMDataDistLSt = mean(RF_HAMDataDistLSt);
mRF_VASDataDistLSt = mean(RF_VASDataDistLSt);
mRF_GASDataDistLSt = mean(RF_GASDataDistLSt);
mRF_SOLDataDistLSt = mean(RF_SOLDataDistLSt);
mRL_TADataDistLSt = mean(RL_TADataDistLSt);

mdTrunkDataDistLSt = mean(dTrunkDataDistLSt)./dtrunkMax;


% collect mean muscle features over gaits (only training)
mFeatDataDistLSt = [mLL_HFLDataDistLSt; mLF_GLUDataDistLSt; mLL_HAMDataDistLSt; mLF_HAMDataDistLSt; mLF_VASDataDistLSt; mLF_GASDataDistLSt; mLF_SOLDataDistLSt; mLL_TADataDistLSt; mTrunkDataDistLSt; ...
            mRL_HFLDataDistLSt; mRF_GLUDataDistLSt; mRL_HAMDataDistLSt; mRF_HAMDataDistLSt; mRF_VASDataDistLSt; mRF_GASDataDistLSt; mRF_SOLDataDistLSt; mRL_TADataDistLSt; mdTrunkDataDistLSt];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataDistLSt = mean(LHFLDataDistLSt);
mLGLUDataDistLSt = mean(LGLUDataDistLSt);
mLHAMDataDistLSt = mean(LHAMDataDistLSt);
mLVASDataDistLSt = mean(LVASDataDistLSt);
mLGASDataDistLSt = mean(LGASDataDistLSt);
mLSOLDataDistLSt = mean(LSOLDataDistLSt);
mLTADataDistLSt = mean(LTADataDistLSt); 

% collect mean muscles over gaits (only training)
mMusDataDistLSt = [mLHFLDataDistLSt; mLGLUDataDistLSt; mLHAMDataDistLSt; mLVASDataDistLSt; mLGASDataDistLSt; mLSOLDataDistLSt; mLTADataDistLSt];

trunkNorm = [trunkMax dtrunkMax];
end