% Relate Mus feat data to muscle stimulation
clc;
close all;
clear all;

load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSwWindows_RfeatFlip.mat;

%%

for z = 1:window_num
    
    %%%%% LEFT %%%%%%%%
    mFeatDataLSt = mallDataLSt{z,1};
    mMusDataLSt = mallDataLSt{z,2};
    
    mFeatDataLSw = mallDataLSw{z,1};
    mMusDataLSw = mallDataLSw{z,2};
    
    % only least square solution using pinv
    wp_LSt = mMusDataLSt*pinv(mFeatDataLSt); % wp is 14X14
    wp_LSw = mMusDataLSw*pinv(mFeatDataLSw); % wp is 14X14
    
    % l1-least square
    wLSt = NaN(size(wp_LSt));
    wLSw = NaN(size(wp_LSw));
    
    ALSt = mFeatDataLSt';
    ALSw = mFeatDataLSw';
    lambdaSt = 0.7; % l1 cost weight
    %lambdaSw = 0.1; % l1 cost weight
    
    for i = 1:7
        BLSt = mMusDataLSt(i,:)';
        
        %[lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
        %lambdaSt = lambda_max*1e-3
        
        [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt);
        wLSt(i,:) = xLSt';
        
        BLSw = mMusDataLSw(i,:)';
        
        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
        lambdaSw = lambda_max*1e-3;
        
        [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw);
        wLSw(i,:) = xLSw';
    end
    
    %%%%%%% Test weight w on a particular sample gait left leg %%%%%%

    t = randi(testNum,1,1); % index of test sample
    
    %%% left stance
    testFeatLSt = [tLL_HFLDataLSt(t,:); tLF_GLUDataLSt(t,:); tLL_HAMDataLSt(t,:); tLF_HAMDataLSt(t,:); tLF_VASDataLSt(t,:); tLF_GASDataLSt(t,:); tLF_SOLDataLSt(t,:); tLL_TADataLSt(t,:); tTrunkDataLSt(t,:); ...
        tRL_HFLDataLSt(t,:); tRF_GLUDataLSt(t,:); tRL_HAMDataLSt(t,:); tRF_HAMDataLSt(t,:); tRF_VASDataLSt(t,:); tRF_GASDataLSt(t,:); tRF_SOLDataLSt(t,:); tRL_TADataLSt(t,:); tdTrunkDataLSt(t,:)];
    
    trueMusLSt = [tLHFLDataLSt(t,:); tLGLUDataLSt(t,:); tLHAMDataLSt(t,:); tLVASDataLSt(t,:); tLGASDataLSt(t,:); tLSOLDataLSt(t,:); tLTADataLSt(t,:)];
    
    testMusLSt = wLSt*testFeatLSt;
    testMusp_LSt = wp_LSt*testFeatLSt;
    
    %%% left swing
    testFeatLSw = [tLL_HFLDataLSw(t,:); tLF_GLUDataLSw(t,:); tLL_HAMDataLSw(t,:); tLF_HAMDataLSw(t,:); tLF_VASDataLSw(t,:); tLF_GASDataLSw(t,:); tLF_SOLDataLSw(t,:); tLL_TADataLSw(t,:); tTrunkDataLSw(t,:); ...
        tRL_HFLDataLSw(t,:); tRF_GLUDataLSw(t,:); tRL_HAMDataLSw(t,:); tRF_HAMDataLSw(t,:); tRF_VASDataLSw(t,:); tRF_GASDataLSw(t,:); tRF_SOLDataLSw(t,:); tRL_TADataLSw(t,:); tdTrunkDataLSw(t,:)];
    
    trueMusLSw = [tLHFLDataLSw(t,:); tLGLUDataLSw(t,:); tLHAMDataLSw(t,:); tLVASDataLSw(t,:); tLGASDataLSw(t,:); tLSOLDataLSw(t,:); tLTADataLSw(t,:)];
    
    
    testMusLSw = wLSw*testFeatLSw;
    testMusp_LSw = wp_LSw*testFeatLSw;

    %%%%%%%%%%% Reconstruction plots %%%%%%%%%%%%
    
    %%% left stance
    figure;
    subplot(7,1,1); plot(testMusLSt(1,:)');hold on
    subplot(7,1,1); plot(testMusp_LSt(1,:)');
    subplot(7,1,1); plot(trueMusLSt(1,:)');
    title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
    legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');
    
    subplot(7,1,2); plot(testMusLSt(2,:)');hold on
    subplot(7,1,2); plot(testMusp_LSt(2,:)');
    subplot(7,1,2); plot(trueMusLSt(2,:)');ylabel('LGLU');
    
    subplot(7,1,3); plot(testMusLSt(3,:)');hold on
    subplot(7,1,3); plot(testMusp_LSt(3,:)');
    subplot(7,1,3); plot(trueMusLSt(3,:)');ylabel('LHAM');
    
    subplot(7,1,4); plot(testMusLSt(4,:)');hold on
    subplot(7,1,4); plot(testMusp_LSt(4,:)');
    subplot(7,1,4); plot(trueMusLSt(4,:)');ylabel('LVAS');
    
    subplot(7,1,5); plot(testMusLSt(5,:)');hold on
    subplot(7,1,5); plot(testMusp_LSt(5,:)');
    subplot(7,1,5); plot(trueMusLSt(5,:)');ylabel('LGAS');
    
    subplot(7,1,6); plot(testMusLSt(6,:)');hold on
    subplot(7,1,6); plot(testMusp_LSt(6,:)');
    subplot(7,1,6); plot(trueMusLSt(6,:)');ylabel('LSOL');
    
    subplot(7,1,7); plot(testMusLSt(7,:)');hold on
    subplot(7,1,7); plot(testMusp_LSt(7,:)');
    subplot(7,1,7); plot(trueMusLSt(7,:)');ylabel('LTA');
    
    %%% left swing
    figure;
    subplot(7,1,1); plot(testMusLSw(1,:)');hold on
    subplot(7,1,1); plot(testMusp_LSw(1,:)');
    subplot(7,1,1); plot(trueMusLSw(1,:)');
    title('Left muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
    legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');
    
    subplot(7,1,2); plot(testMusLSw(2,:)');hold on
    subplot(7,1,2); plot(testMusp_LSw(2,:)');
    subplot(7,1,2); plot(trueMusLSw(2,:)');ylabel('LGLU');
    
    subplot(7,1,3); plot(testMusLSw(3,:)');hold on
    subplot(7,1,3); plot(testMusp_LSw(3,:)');
    subplot(7,1,3); plot(trueMusLSw(3,:)');ylabel('LHAM');
    
    subplot(7,1,4); plot(testMusLSw(4,:)');hold on
    subplot(7,1,4); plot(testMusp_LSw(4,:)');
    subplot(7,1,4); plot(trueMusLSw(4,:)');ylabel('LVAS');
    
    subplot(7,1,5); plot(testMusLSw(5,:)');hold on
    subplot(7,1,5); plot(testMusp_LSw(5,:)');
    subplot(7,1,5); plot(trueMusLSw(5,:)');ylabel('LGAS');
    
    subplot(7,1,6); plot(testMusLSw(6,:)');hold on
    subplot(7,1,6); plot(testMusp_LSw(6,:)');
    subplot(7,1,6); plot(trueMusLSw(6,:)');ylabel('LSOL');
    
    subplot(7,1,7); plot(testMusLSw(7,:)');hold on
    subplot(7,1,7); plot(testMusp_LSw(7,:)');
    subplot(7,1,7); plot(trueMusLSw(7,:)');ylabel('LTA');
    
    
    keyboard
    
end    