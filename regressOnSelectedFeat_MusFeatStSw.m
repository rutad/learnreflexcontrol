%% To obtain good weights on features for the control (so as to later optimize them),
%% 1. find features using LASSO (use whitened data of normalized features, also use cross validation)
%% 2. On the features selected by LASSO, do regression again for predicting each muscle activity
%% 3. In future, optimize these regression weights using CMAES

% Ruta Desai
% 30th June, 2015
% rutad@cs.ccmu.edu

clc;
close all;
%clear all;

load linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat;

%%
%%%%%% STEP-1 LASSO for Feature Selection %%%%%%

%% Hold out data

% divide test data into hold data and test data. This is because we don't
% have inidividual strides from train data :(
holdNum = ceil(testNum - testNum/2);
left_testNum = [holdNum+1:testNum];

holdFeatLSt = NaN(size(mFeatDataLSt,1),holdNum*size(tLL_HAMDataLSt,2));
holdMusLSt = NaN(size(mMusDataLSt,1),holdNum*size(tLHFLDataLSt,2));

holdFeatLSw = NaN(size(mFeatDataLSw,1),holdNum*size(tLL_HAMDataLSw,2));
holdMusLSw = NaN(size(mMusDataLSw,1),holdNum*size(tLHFLDataLSw,2));

tStartStL = 1;
tStartSwL = 1;

for t = 1:holdNum; % index of test sample
    
    tEndStL = size(tLL_HAMDataLSt,2)+tStartStL-1;
    tEndSwL = size(tLL_HAMDataLSw,2)+tStartSwL-1;
    
    % left stance
    testFeatStL = [tLL_HFLDataLSt(t,:); tLF_GLUDataLSt(t,:); tLL_HAMDataLSt(t,:); tLF_HAMDataLSt(t,:); tLF_VASDataLSt(t,:); tLF_GASDataLSt(t,:); tLF_SOLDataLSt(t,:); tLL_TADataLSt(t,:); tTrunkDataLSt(t,:); ...
                tRL_HFLDataLSt(t,:); tRF_GLUDataLSt(t,:); tRL_HAMDataLSt(t,:); tRF_HAMDataLSt(t,:); tRF_VASDataLSt(t,:); tRF_GASDataLSt(t,:); tRF_SOLDataLSt(t,:); tRL_TADataLSt(t,:); tdTrunkDataLSt(t,:)];

    testMusStL = [tLHFLDataLSt(t,:); tLGLUDataLSt(t,:); tLHAMDataLSt(t,:); tLVASDataLSt(t,:); tLGASDataLSt(t,:); tLSOLDataLSt(t,:); tLTADataLSt(t,:)]; 
    
    % left swing
    testFeatSwL = [tLL_HFLDataLSw(t,:); tLF_GLUDataLSw(t,:); tLL_HAMDataLSw(t,:); tLF_HAMDataLSw(t,:); tLF_VASDataLSw(t,:); tLF_GASDataLSw(t,:); tLF_SOLDataLSw(t,:); tLL_TADataLSw(t,:); tTrunkDataLSw(t,:); ...
                tRL_HFLDataLSw(t,:); tRF_GLUDataLSw(t,:); tRL_HAMDataLSw(t,:); tRF_HAMDataLSw(t,:); tRF_VASDataLSw(t,:); tRF_GASDataLSw(t,:); tRF_SOLDataLSw(t,:); tRL_TADataLSw(t,:); tdTrunkDataLSw(t,:)];

    testMusSwL = [tLHFLDataLSw(t,:); tLGLUDataLSw(t,:); tLHAMDataLSw(t,:); tLVASDataLSw(t,:); tLGASDataLSw(t,:); tLSOLDataLSw(t,:); tLTADataLSw(t,:)]; 
    
    
    holdFeatLSt(:,tStartStL:tEndStL) = testFeatStL;
    holdMusLSt(:,tStartStL:tEndStL) = testMusStL;
    holdFeatLSw(:,tStartSwL:tEndSwL) = testFeatSwL;
    holdMusLSw(:,tStartSwL:tEndSwL) = testMusSwL;
    
    tStartStL = tEndStL + 1;
    tStartSwL = tEndSwL + 1;
    
end

%%
holdFeatRSt = NaN(size(mFeatDataRSt,1),holdNum*size(tRL_HAMDataRSt,2));
holdMusRSt = NaN(size(mMusDataRSt,1),holdNum*size(tRHFLDataRSt,2));

holdFeatRSw = NaN(size(mFeatDataRSw,1),holdNum*size(tRL_HAMDataRSw,2));
holdMusRSw = NaN(size(mMusDataRSw,1),holdNum*size(tRHFLDataRSw,2));

tStartStR = 1;
tStartSwR = 1;

for t = 1:holdNum; % index of test sample
    
    tEndStR = size(tRL_HAMDataRSt,2)+tStartStR-1;
    tEndSwR = size(tRL_HAMDataRSw,2)+tStartSwR-1;
    
    % right stance
    testFeatStR = [tRL_HFLDataRSt(t,:); tRF_GLUDataRSt(t,:); tRL_HAMDataRSt(t,:); tRF_HAMDataRSt(t,:); tRF_VASDataRSt(t,:); tRF_GASDataRSt(t,:); tRF_SOLDataRSt(t,:); tRL_TADataRSt(t,:); tTrunkDataRSt(t,:);...
        tLL_HFLDataRSt(t,:); tLF_GLUDataRSt(t,:); tLL_HAMDataRSt(t,:); tLF_HAMDataRSt(t,:); tLF_VASDataRSt(t,:); tLF_GASDataRSt(t,:); tLF_SOLDataRSt(t,:); tLL_TADataRSt(t,:); tdTrunkDataRSt(t,:)];
       
    testMusStR = [tRHFLDataRSt(t,:); tRGLUDataRSt(t,:); tRHAMDataRSt(t,:); tRVASDataRSt(t,:); tRGASDataRSt(t,:); tRSOLDataRSt(t,:); tRTADataRSt(t,:)];  
    
    % right swing
    testFeatSwR = [tRL_HFLDataRSw(t,:); tRF_GLUDataRSw(t,:); tRL_HAMDataRSw(t,:); tRF_HAMDataRSw(t,:); tRF_VASDataRSw(t,:); tRF_GASDataRSw(t,:); tRF_SOLDataRSw(t,:); tRL_TADataRSw(t,:); tTrunkDataRSw(t,:);...
        tLL_HFLDataRSw(t,:); tLF_GLUDataRSw(t,:); tLL_HAMDataRSw(t,:); tLF_HAMDataRSw(t,:); tLF_VASDataRSw(t,:); tLF_GASDataRSw(t,:); tLF_SOLDataRSw(t,:); tLL_TADataRSw(t,:); tdTrunkDataRSw(t,:)];
       
    testMusSwR = [tRHFLDataRSw(t,:); tRGLUDataRSw(t,:); tRHAMDataRSw(t,:); tRVASDataRSw(t,:); tRGASDataRSw(t,:); tRSOLDataRSw(t,:); tRTADataRSw(t,:)]; 
    
    
    holdFeatRSt(:,tStartStR:tEndStR) = testFeatStR;
    holdMusRSt(:,tStartStR:tEndStR) = testMusStR;
    holdFeatRSw(:,tStartSwR:tEndSwR) = testFeatSwR;
    holdMusRSw(:,tStartSwR:tEndSwR) = testMusSwR;
    
    tStartStR = tEndStR + 1;
    tStartSwR = tEndSwR + 1;
    
end

%% Sparse weights (feature selection) using l1 regularized least square
% using l1_ls package from stanford group

%%%%% STANCE %%%%%%%%

% l1-least square
wRSt = NaN(size(mMusDataLSt,1),size(holdFeatLSt,1));
wLSt = NaN(size(wRSt));

ALSt = mFeatDataLSt';
ARSt = mFeatDataRSt';

% zero mean
ALSt = bsxfun(@minus, ALSt, mean(ALSt));
ARSt = bsxfun(@minus, ARSt, mean(ARSt));

% unit variance
ALSt = ALSt*diag(1./std(ALSt));
ARSt = ARSt*diag(1./std(ARSt));

err_tol_min = 1e-4;
MSE_errThSt = 0.001; %0.01 %0.001
MSE_errThSw = 0.001; % 0.01 %0.001 %0.00001;

RMSE_errThSt = 0.001; %0.01 %0.001
RMSE_errThSw = 0.001; %0.01 %0.001

lambdaLSt = NaN(size(mMusDataLSt,1),1);
lambdaLSw = NaN(size(mMusDataLSt,1),1);
lambdaRSt = NaN(size(mMusDataLSt,1),1);
lambdaRSw = NaN(size(mMusDataLSt,1),1);

relerrHoldLSt = NaN(size(mMusDataLSt,1),1);
relerrHoldLSw = NaN(size(mMusDataLSt,1),1);
relerrHoldRSt = NaN(size(mMusDataLSt,1),1);
relerrHoldRSw = NaN(size(mMusDataLSt,1),1);

errHoldLSt = NaN(size(mMusDataLSt,1),1);
errHoldLSw = NaN(size(mMusDataLSt,1),1);
errHoldRSt = NaN(size(mMusDataLSt,1),1);
errHoldRSw = NaN(size(mMusDataLSt,1),1);

for i = 1:7
   
    multThLSt = err_tol_min;
    multThRSt = err_tol_min;
    
    BLSt = mMusDataLSt(i,:)';
    BLSt = bsxfun(@minus, BLSt, mean(BLSt));
    if (std(BLSt)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
    BLSt = BLSt./std(BLSt);

    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
        lambdaSt_CV(cnt) = lambda_max*multThLSt;
        
        lambdaSt = lambdaSt_CV(cnt);
        
        % find min lambda by CV
        while (lambdaSt<=lambda_max)
             
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt,[],'quiet');    
            errLSt(cnt) = mean((holdMusLSt(i,:)-xLSt'*holdFeatLSt).^2);
            relerrLSt(cnt) = errLSt(cnt)/max(holdMusLSt(i,:));
            
            cnt = cnt+1;
            multThLSt = multThLSt*2;
            lambdaSt_CV(cnt) = lambda_max*multThLSt;
            lambdaSt = lambdaSt_CV(cnt);

        end     

        [minError,lambdaFinalInd] = min(errLSt);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaStSparse = lambdaSt_CV(lambdaFinalInd);
        relerrLStSparse = relerrLSt(lambdaFinalInd);
        errLStSparse = errLSt(lambdaFinalInd);
        flag = 2;
        
        %keyboard
        
        while (relerrLStSparse<RMSE_errThSt && errLStSparse<MSE_errThSt && lambdaStSparse<=lambda_max)
            
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaStSparse,[],'quiet');        
            errLStSparse = mean((holdMusLSt(i,:)-xLSt'*holdFeatLSt).^2);
            relerrLStSparse = errLStSparse/max(holdMusLSt(i,:));
            
            lambdaStSparse = lambdaStSparse*2;
            flag = 1;
            %keyboard
            
        end   
        
        [xLSt,~] = l1_ls(ALSt,BLSt,lambdaStSparse*flag/2,[],'quiet');
        errLStFinal = mean((holdMusLSt(i,:)-xLSt'*holdFeatLSt).^2);
        relerrLStFinal = errLStFinal/max(holdMusLSt(i,:));
        
        %keyboard
        
        errHoldLSt(i) = errLStFinal;
        relerrHoldLSt(i) = relerrLStFinal;
        lambdaLSt(i) = lambdaStSparse*flag/2;
        wLSt(i,:) = xLSt';
        %keyboard
   
    else
        wLSt(i,:) = zeros(size(wLSt,2),1);
    end       
    clear lambdaSt_CV errLSt relerrLSt;
    
    BRSt = mMusDataRSt(i,:)';      
    BRSt = bsxfun(@minus, BRSt, mean(BRSt));
    if (std(BRSt)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
    BRSt = BRSt./std(BRSt);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ARSt',BRSt);
        lambdaSt_CV(cnt) = lambda_max*multThRSt;
        
        lambdaSt = lambdaSt_CV(cnt);
        
        % find min lambda by CV
        while (lambdaSt<=lambda_max)
             
            [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt,[],'quiet');        
            errRSt(cnt) = mean((holdMusRSt(i,:)-xRSt'*holdFeatRSt).^2);
            relerrRSt(cnt) = errRSt(cnt)/max(holdMusRSt(i,:));
            
            cnt = cnt+1;
            multThRSt = multThRSt*2;
            lambdaSt_CV(cnt) = lambda_max*multThRSt;
            lambdaSt = lambdaSt_CV(cnt);

        end     
                
        [minError,lambdaFinalInd] = min(errRSt);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaStSparse = lambdaSt_CV(lambdaFinalInd);
        relerrRStSparse = relerrRSt(lambdaFinalInd);
        errRStSparse = errRSt(lambdaFinalInd);
        flag = 2;
        
        while (relerrRStSparse<RMSE_errThSt && errRStSparse<MSE_errThSt && lambdaStSparse<=lambda_max)
            
            [xRSt,~] = l1_ls(ARSt,BRSt,lambdaStSparse,[],'quiet');        
            errRStSparse = mean((holdMusRSt(i,:)-xRSt'*holdFeatRSt).^2);
            relerrRStSparse = errRStSparse/max(holdMusRSt(i,:));
            
            lambdaStSparse = lambdaStSparse*2;
            flag = 1;
            
        end   
        
        [xRSt,~] = l1_ls(ARSt,BRSt,lambdaStSparse*flag/2,[],'quiet');
        errRStFinal = mean((holdMusRSt(i,:)-xRSt'*holdFeatRSt).^2);  
        relerrRStFinal = errRStFinal/max(holdMusRSt(i,:));
        
        errHoldRSt(i) = errRStFinal;
        relerrHoldRSt(i) = relerrRStFinal;
        lambdaRSt(i) = lambdaStSparse*flag/2;
        wRSt(i,:) = xRSt';

    else
        wRSt(i,:) = zeros(size(wRSt,2),1);
    end
    clear lambdaSt_CV errRSt relerrRSt;
       
end

%%
%%%%%%%%% SWING %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% l1-least square
wLSw = NaN(size(wLSt));
wRSw = NaN(size(wRSt));

ALSw = mFeatDataLSw';
ARSw = mFeatDataRSw';

% zero mean
ALSw = bsxfun(@minus, ALSw, mean(ALSw));
ARSw = bsxfun(@minus, ARSw, mean(ARSw));

% unit variance
ALSw = ALSw*diag(1./std(ALSw));
ARSw = ARSw*diag(1./std(ARSw));

for i = 1:7
     
    multThLSw = err_tol_min;
    multThRSw = err_tol_min;
    
    BLSw = mMusDataLSw(i,:)';
    BLSw = bsxfun(@minus, BLSw, mean(BLSw));
    if (std(BLSw)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
        
    BLSw = BLSw./std(BLSw);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
        lambdaSw_CV(cnt) = lambda_max*multThLSw;
        
        lambdaSw = lambdaSw_CV(cnt);
        
        while (lambdaSw<=lambda_max)
             
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw,[],'quiet');        
            errLSw(cnt) = mean((holdMusLSw(i,:)-xLSw'*holdFeatLSw).^2);
            relerrLSw(cnt) = errLSw(cnt)/max(holdMusLSw(i,:));
            
            cnt = cnt+1;
            multThLSw = multThLSw*2;
            lambdaSw_CV(cnt) = lambda_max*multThLSw;
            lambdaSw = lambdaSw_CV(cnt);

        end    
        
        [minError,lambdaFinalInd] = min(errLSw);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaSwSparse = lambdaSw_CV(lambdaFinalInd);
        relerrLSwSparse = relerrLSw(lambdaFinalInd);
        errLSwSparse = errLSw(lambdaFinalInd);
        flag = 2;
        
        while (relerrLSwSparse<RMSE_errThSw && errLSwSparse<MSE_errThSw && lambdaSwSparse<=lambda_max)
            
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSwSparse,[],'quiet');        
            errLSwSparse = mean((holdMusLSw(i,:)-xLSw'*holdFeatLSw).^2);
            relerrLSwSparse = errLSwSparse/max(holdMusLSw(i,:));
            
            lambdaSwSparse = lambdaSwSparse*2;
            flag = 1;
            
        end   
        
        [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSwSparse*flag/2,[],'quiet');
        errLSwFinal = mean((holdMusLSw(i,:)-xLSw'*holdFeatLSw).^2);  
        relerrLSwFinal = errLSwFinal/max(holdMusLSw(i,:));
        
        errHoldLSw(i) = errLSwFinal;
        relerrHoldLSw(i) = relerrLSwFinal;
        lambdaLSw(i) = lambdaSwSparse*flag/2;
        wLSw(i,:) = xLSw';
   
    else
        wLSw(i,:) = zeros(size(wLSw,2),1);
    end       
    clear lambdaSw_CV errLSw relerrLSw;
    
    BRSw = mMusDataRSw(i,:)'; 
    BRSw = bsxfun(@minus, BRSw, mean(BRSw));
    if (std(BRSw)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
    BRSw = BRSw./std(BRSw);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ARSw',BRSw);
        lambdaSw_CV(cnt) = lambda_max*multThRSw;
        
        lambdaSw = lambdaSw_CV(cnt);
        
        while (lambdaSw<=lambda_max)
             
            [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw,[],'quiet');        
            errRSw(cnt) = mean((holdMusRSw(i,:)-xRSw'*holdFeatRSw).^2);
            relerrRSw(cnt) = errRSw(cnt)/max(holdMusRSw(i,:));
            
            cnt = cnt+1;
            multThRSw = multThRSw*2;
            lambdaSw_CV(cnt) = lambda_max*multThRSw;
            lambdaSw = lambdaSw_CV(cnt);

        end     
        
        [minError,lambdaFinalInd] = min(errRSw);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaSwSparse = lambdaSw_CV(lambdaFinalInd);
        relerrRSwSparse = relerrRSw(lambdaFinalInd);
        errRSwSparse = errRSw(lambdaFinalInd);
        flag = 2;
        
        while (relerrRSwSparse<RMSE_errThSw && errRSwSparse<MSE_errThSw && lambdaSwSparse<=lambda_max)
            
            [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSwSparse,[],'quiet');        
            errRSwSparse = mean((holdMusRSw(i,:)-xRSw'*holdFeatRSw).^2);
            relerrRSwSparse = errRSwSparse/max(holdMusRSw(i,:));
            
            lambdaSwSparse = lambdaSwSparse*2;
            flag = 1;
            
        end   
        
        [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSwSparse*flag/2,[],'quiet');
        errRSwFinal = mean((holdMusRSw(i,:)-xRSw'*holdFeatRSw).^2);  
        relerrRSwFinal = errRSwFinal/max(holdMusRSw(i,:));
        
        errHoldRSw(i) = errRSwFinal;
        relerrHoldRSw(i) = relerrRSwFinal;
        lambdaRSw(i) = lambdaSwSparse*flag/2;
        wRSw(i,:) = xRSw';

    else
        wRSw(i,:) = zeros(size(wRSw,2),1);
    end
    clear lambdaSw_CV errRSw relerrRSw;
       
end

%%
%%%%% STEP-2 Second stage of selection using R2 measure. 
% Regress again on the selected features for predicting each muscle %%%%%

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
desRsquared = 0.99; %95

%%%%%% SWING %%%%%
selectedFeatIndLSw = cell(7,1);
linRegWtsLSw = cell(7,1);

figure;

nPlot = 7;
plotCnt = 1;

wRegLSw = zeros(size(wLSw,1),size(wLSw,2)+1);

for i = 1:7

    ww = wLSw(i,:);
    [wwsort,wwind] = sort(abs(ww),2,'descend');
    wwind = wwind( find(abs(wwsort)>0.01));

    feat_ind = [];
    R2_all = [];
    AR2_all = [];
    
    j = 1;
    musRsquared = 0;

    while (musRsquared<desRsquared && j<=length(wwind))
        feat_ind=[feat_ind; wwind(j)];
        X = mFeatDataLSw(feat_ind,:)';
        y = mMusDataLSw(i,:)';
        mdl = fitlm(X,y);
        musRsquared=mdl.Rsquared.Ordinary;
        
        R2_all(j) = musRsquared;
        AR2_all(j) = mdl.Rsquared.Adjusted;
        j =j+1;
    end

    if (~isempty(feat_ind))
        selectedFeatIndLSw{i} = [19; feat_ind];  % add the constant feature 1.
        linRegWtsLSw{i} = mdl.Coefficients.Estimate; % the first feature is 1.
        wRegLSw(i,selectedFeatIndLSw{i}) = linRegWtsLSw{i};
    end
    
    subplot(nPlot,2,plotCnt);plot(2:length(R2_all)+1,R2_all,'*-');axis([1,19,0,1]);
    title('Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
    
    subplot(nPlot,2,plotCnt+1);plot(2:length(AR2_all)+1,AR2_all,'*-');axis([1,19,0,1]); 
    title('Adj. Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
    
    plotCnt = plotCnt+2;
    
    clear mdl X y 
   
end

%%
%%%%%% STANCE %%%%%
selectedFeatIndLSt = cell(7,1);
linRegWtsLSt = cell(7,1);

figure;

nPlot = 7;
plotCnt = 1;

wRegLSt = zeros(size(wLSt,1),size(wLSt,2)+1);

for i = 1:7

    ww = wLSt(i,:);
    [wwsort,wwind] = sort(abs(ww),2,'descend');
    wwind = wwind( find(abs(wwsort)>0.01));

    feat_ind = [];
    R2_all = [];
    AR2_all = [];
    
    j = 1;
    musRsquared = 0;

    while (musRsquared<desRsquared && j<=length(wwind))
        feat_ind=[feat_ind; wwind(j)];
        X = mFeatDataLSt(feat_ind,:)';
        y = mMusDataLSt(i,:)';
        mdl = fitlm(X,y);
        musRsquared=mdl.Rsquared.Ordinary;
        
        R2_all(j) = musRsquared;
        AR2_all(j) = mdl.Rsquared.Adjusted;
        j =j+1;
    end

    if (~isempty(feat_ind))
        selectedFeatIndLSt{i} = [19; feat_ind];  % add the constant feature 1.
        linRegWtsLSt{i} = mdl.Coefficients.Estimate; % the first feature is 1.
        wRegLSt(i,selectedFeatIndLSt{i}) = linRegWtsLSt{i};
    end
    
    subplot(nPlot,2,plotCnt);plot(2:length(R2_all)+1,R2_all,'*-');axis([1,19,0,1]);
    title('Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
    
    subplot(nPlot,2,plotCnt+1);plot(2:length(AR2_all)+1,AR2_all,'*-');axis([1,19,0,1]); 
    title('Adj. Rsquared vs # of weights: Left Swing'); ylabel(labels_ly(i));xlabel('# of w');
    
    plotCnt = plotCnt+2;
    
    clear mdl X y 
   
end

 