%% reconstruction for causation experiment data

close all;
clear all;
clc;

load('causExpDistWaitSw_musFeatNormTrk_distSteadyNORMData_F5.mat')

% steady data
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mMusDataLSw');
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mFeatDataLSw');

% Normalize steady data
mMusDataLSw_NORM = mMusDataLSw;
mFeatDataLSw_NORM = mFeatDataLSw;

for z =1:size(mMusDataLSw,1)
    mMusDataLSw_NORM(z,:) = mMusDataLSw_NORM(z,:)./max(mMusDataLSw(z,:));
end

for z =1:size(mFeatDataLSw,1)
    mFeatDataLSw_NORM(z,:) = mFeatDataLSw_NORM(z,:)./max(mFeatDataLSw(z,:));
end


disturb_ijk = size(WStore);
%% 

for i =1:disturb_ijk(1)

    for j = 1:disturb_ijk(2)
        
        for k = 1:disturb_ijk(3)
            [i,j,k]
            
            mMusDataDistLSw = musDistDataStore{i,j,k};
            mFeatDataDistLSw = featDistDataStore{i,j,k};
            
            % resample undisturbed data to match length of disturbed data
            mFeatDataLSw_r=resample(mFeatDataLSw',size(featDistDataStore{i,j,k},2),size(mFeatDataLSw,2))';
            mMusDataLSw_r=resample(mMusDataLSw',size(musDistDataStore{i,j,k},2),size(mMusDataLSw,2))';
            
            % subtracting steady state motion from the disturbed motion
            del_mMusDataLSw = musDistDataStore{i,j,k} - mMusDataLSw_r;
            del_mFeatDataLSw = featDistDataStore{i,j,k} - mFeatDataLSw_r;
            
            
            % Normalize data
            for z=1:size(del_mMusDataLSw,1)
                del_mMusDataLSw(z,:) =  del_mMusDataLSw(z,:)./max(del_mMusDataLSw(z,:));                
                mMusDataDistLSw(z,:) = mMusDataDistLSw(z,:)./max(mMusDataDistLSw(z,:));
            end
            
            for z=1:size(del_mFeatDataLSw,1)
                del_mFeatDataLSw(z,:) =  del_mFeatDataLSw(z,:)./max(del_mFeatDataLSw(z,:));
                mFeatDataDistLSw(z,:) = mFeatDataDistLSw(z,:)./max(mFeatDataDistLSw(z,:));
            end

            
            trueMus = [mMusDataDistLSw mMusDataLSw];
            testMus = [WStore{i,j,k}(:,1:18)*mFeatDataDistLSw WStore{i,j,k}(:,1:18)*mFeatDataLSw];
            
            trueMus2 = [del_mMusDataLSw mMusDataLSw];
            testMus2 = [WStore{i,j,k}(:,19:36)*del_mFeatDataLSw WStore{i,j,k}(:,19:36)*mFeatDataLSw];
            
            figure; %axis([0,length(testMus),0,0.6])
            subplot(7,1,1); plot(testMus(1,:)');hold on
            subplot(7,1,1); plot(trueMus(1,:)');
            title('Left muscles [Dist Steady] SWING reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
            legend('Reconstructed-sparse','TrueData');
            
            subplot(7,1,2); plot(testMus(2,:)');hold on;%axis([0,length(testMus),0,0.6])
            subplot(7,1,2); plot(trueMus(2,:)');ylabel('LGLU');
            
            subplot(7,1,3); plot(testMus(3,:)');hold on;%axis([0,length(testMus),0,0.6])
            subplot(7,1,3); plot(trueMus(3,:)');ylabel('LHAM');
            
            subplot(7,1,4); plot(testMus(4,:)');hold on;%axis([0,length(testMus),0,0.6])
            subplot(7,1,4); plot(trueMus(4,:)');ylabel('LVAS');
            
            subplot(7,1,5); plot(testMus(5,:)');hold on;%axis([0,length(testMus),0,0.6])
            subplot(7,1,5); plot(trueMus(5,:)');ylabel('LGAS');
            
            subplot(7,1,6); plot(testMus(6,:)');hold on;%axis([0,length(testMus),0,0.6])
            subplot(7,1,6); plot(trueMus(6,:)');ylabel('LSOL');
            
            subplot(7,1,7); plot(testMus(7,:)');hold on;%axis([0,length(testMus),0,0.6])
            subplot(7,1,7); plot(trueMus(7,:)');ylabel('LTA');
            
            
            figure; %axis([0,length(testMus),0,0.4])
            subplot(7,1,1); plot(testMus2(1,:)');hold on
            subplot(7,1,1); plot(trueMus2(1,:)');
            title('Left muscles [Del Steady] SWING reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
            legend('Reconstructed-sparse','TrueData');
            
            subplot(7,1,2); plot(testMus2(2,:)');hold on;%axis([0,length(testMus2),0,0.2])
            subplot(7,1,2); plot(trueMus2(2,:)');ylabel('LGLU');
            
            subplot(7,1,3); plot(testMus2(3,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,3); plot(trueMus2(3,:)');ylabel('LHAM');
            
            subplot(7,1,4); plot(testMus2(4,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,4); plot(trueMus2(4,:)');ylabel('LVAS');
            
            subplot(7,1,5); plot(testMus2(5,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,5); plot(trueMus2(5,:)');ylabel('LGAS');
            
            subplot(7,1,6); plot(testMus2(6,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,6); plot(trueMus2(6,:)');ylabel('LSOL');
            
            subplot(7,1,7); plot(testMus2(7,:)');hold on;%axis([0,length(testMus2),0,0.4])
            subplot(7,1,7); plot(trueMus2(7,:)');ylabel('LTA');
            
            % using disturbed left stance w
            labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
            labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
                'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};
            str_disturb = strcat('Fdisturb =', num2str(fDisturb));
            
            figure;imagesc(abs(WStore{i,j,k}(:,1:18))/(max(max(abs(WStore{i,j,k}(:,1:18))))));colorbar;title({'Leg weights DISTURBED LEFT STANCE (2-leg Mus Feat Data)-sparse',str_disturb});
            set(gca(),'YTick',1:7)
            set(gca(),'YTickLabel',labels_ly);
            set(gca(),'XTick',1:18)
            set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
            
            % using delta left stance w (disturbed - steady)
            figure;imagesc(abs(WStore{i,j,k}(:,19:36))/(max(max(abs(WStore{i,j,k}(:,19:36))))));colorbar;title({'Leg weights DEL LEFT STANCE (2-leg Mus Feat Data)-sparse',str_disturb});
            set(gca(),'YTick',1:7)
            set(gca(),'YTickLabel',labels_ly);
            set(gca(),'XTick',1:18)
            set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

                    
            keyboard
                        
            clear mMusDataDistLSw mFeatDataDistLSw mMusDataLSw_r mFeatDataLSw_r
            clear del_mFeatDataLSw del_mMusDataLSw
            clear z
            clear trueMus trueMus2 testMus testMus2
            
        end
    end
end
    

