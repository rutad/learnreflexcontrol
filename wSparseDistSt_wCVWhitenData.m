% Relate kinematics data to muscle stimulation

function [wDistLSt, delw_LSt] =  wSparseDistSt_wCVWhitenData(distData, testData, steadyData)

mFeatDataDistLSt = distData{1};
mMusDataDistLSt = distData{2};

holdFeatDataLSt = testData{1};
holdMusDataLSt = testData{2};

mFeatDataLSt = steadyData{1};
mMusDataLSt = steadyData{2};

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSt_r=resample(mFeatDataLSt',size(mFeatDataDistLSt,2),size(mFeatDataLSt,2))';
mMusDataLSt_r=resample(mMusDataLSt',size(mMusDataDistLSt,2),size(mMusDataLSt,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSt = mMusDataDistLSt - mMusDataLSt_r;
del_mFeatDataLSt = mFeatDataDistLSt - mFeatDataLSt_r;

% resample undisturbed data to match length of test data
hFeatDataLSt_r=resample(mFeatDataLSt',size(holdFeatDataLSt,2),size(mFeatDataLSt,2))';
hMusDataLSt_r=resample(mMusDataLSt',size(holdMusDataLSt,2),size(mMusDataLSt,2))';

% subtracting steady state motion from the disturbed motion
del_hMusDataLSt = holdMusDataLSt - hMusDataLSt_r;
del_hFeatDataLSt = holdFeatDataLSt - hFeatDataLSt_r;

%%%%% DISTURBED DATA and DISTURBED - STEADY DATA %%%%%%%%

% l1-least square
wDistLSt = NaN(size(mMusDataLSt,1),size(mFeatDataLSt,1));
delw_LSt = NaN(size(wDistLSt));

ADistLSt = mFeatDataDistLSt';
del_ALSt = del_mFeatDataLSt';

% zero mean
ADistLSt = bsxfun(@minus, ADistLSt, mean(ADistLSt));
del_ALSt = bsxfun(@minus, del_ALSt, mean(del_ALSt));

% unit variance
ADistLSt = ADistLSt*diag(1./std(ADistLSt));
del_ALSt = del_ALSt*diag(1./std(del_ALSt));

err_tol_min = 1e-4;
MSE_errThSt = 0.001; % 0.01 %0.001 %0.00001;
RMSE_errThSt = 0.001; %0.01 %0.001

lambdaDistLSt = NaN(size(mMusDataLSt,1),1);
lambdaDelLSt = NaN(size(mMusDataLSt,1),1);

relerrHoldDistLSt = NaN(size(mMusDataLSt,1),1);
relerrHoldDelLSt = NaN(size(mMusDataLSt,1),1);

errHoldDistLSt = NaN(size(mMusDataLSt,1),1);
errHoldDelLSt = NaN(size(mMusDataLSt,1),1);

for i = 1:7
    
    
    multThDistLSt = err_tol_min;
    multThDelLSt = err_tol_min;
    
    BDistLSt = mMusDataDistLSt(i,:)';
    BDistLSt = bsxfun(@minus, BDistLSt, mean(BDistLSt));
    if (std(BDistLSt)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
        
    BDistLSt = BDistLSt./std(BDistLSt);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ADistLSt',BDistLSt);
        lambdaSt_CV(cnt) = lambda_max*multThDistLSt;
        
        lambdaSt = lambdaSt_CV(cnt);
        
        while (lambdaSt<=lambda_max)
             
            [xDistLSt,~] = l1_ls(ADistLSt,BDistLSt,lambdaSt,[],'quiet');        
            errDistLSt(cnt) = mean((holdMusDataLSt(i,:)-xDistLSt'*holdFeatDataLSt).^2);
            relerrDistLSt(cnt) = errDistLSt(cnt)/max(holdMusDataLSt(i,:));
            
            cnt = cnt+1;
            multThDistLSt = multThDistLSt*2;
            lambdaSt_CV(cnt) = lambda_max*multThDistLSt;
            lambdaSt = lambdaSt_CV(cnt);

        end    
        
        [minError,lambdaFinalInd] = min(errDistLSt);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaDistLStSparse = lambdaSt_CV(lambdaFinalInd);
        relerrDistLStSparse = relerrDistLSt(lambdaFinalInd);
        errDistLStSparse = errDistLSt(lambdaFinalInd);
        flag = 2;
        
        while (relerrDistLStSparse<RMSE_errThSt && errDistLStSparse<MSE_errThSt && lambdaDistLStSparse<=lambda_max)
            
            [xDistLSt,~] = l1_ls(ADistLSt,BDistLSt,lambdaDistLStSparse,[],'quiet');        
            errDistLStSparse = mean((holdMusDataLSt(i,:)-xDistLSt'*holdFeatDataLSt).^2);
            relerrDistLStSparse = errDistLStSparse/max(holdMusDataLSt(i,:));
            
            lambdaDistLStSparse = lambdaDistLStSparse*2;
            flag = 1;
            
        end   
        
        [xDistLSt,~] = l1_ls(ADistLSt,BDistLSt,lambdaDistLStSparse*flag/2,[],'quiet');
        errDistLStFinal = mean((holdMusDataLSt(i,:)-xDistLSt'*holdFeatDataLSt).^2);  
        relerrDistLStFinal = errDistLStFinal/max(holdMusDataLSt(i,:));
        
        errHoldDistLSt(i) = errDistLStFinal;
        relerrHoldDistLSt(i) = relerrDistLStFinal;
        lambdaDistLSt(i) = lambdaDistLStSparse*flag/2;
        wDistLSt(i,:) = xDistLSt';
   
    else
        wDistLSt(i,:) = zeros(size(wDistLSt,2),1);
    end       
    clear lambdaSt_CV errDistLSt relerrDistLSt;
    
    
    BDelLSt = del_mMusDataLSt(i,:)';
    BDelLSt = bsxfun(@minus, BDelLSt, mean(BDelLSt));
    if (std(BDelLSt)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
    BDelLSt = BDelLSt./std(BDelLSt);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(del_ALSt',BDelLSt);
        lambdaSt_CV(cnt) = lambda_max*multThDelLSt;
        
        lambdaSt = lambdaSt_CV(cnt);
        
        while (lambdaSt<=lambda_max)
             
            [xDelLSt,~] = l1_ls(del_ALSt,BDelLSt,lambdaSt,[],'quiet');        
            errDelLSt(cnt) = mean((del_hMusDataLSt(i,:)-xDelLSt'*del_hFeatDataLSt).^2);
            relerrDelLSt(cnt) = errDelLSt(cnt)/max(del_hMusDataLSt(i,:));
            
            cnt = cnt+1;
            multThDelLSt = multThDelLSt*2;
            lambdaSt_CV(cnt) = lambda_max*multThDelLSt;
            lambdaSt = lambdaSt_CV(cnt);

        end     
        
        [minError,lambdaFinalInd] = min(errDelLSt);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaDistLStSparse = lambdaSt_CV(lambdaFinalInd);
        relerrDelLStSparse = relerrDelLSt(lambdaFinalInd);
        errDelLStSparse = errDelLSt(lambdaFinalInd);
        flag = 2;
        
        while (relerrDelLStSparse<RMSE_errThSt && errDelLStSparse<MSE_errThSt && lambdaDistLStSparse<=lambda_max)
            
            [xDelLSt,~] = l1_ls(del_ALSt,BDelLSt,lambdaDistLStSparse,[],'quiet');        
            errDelLStSparse = mean((del_hMusDataLSt(i,:)-xDelLSt'*del_hFeatDataLSt).^2);
            relerrDelLStSparse = errDelLStSparse/max(del_hMusDataLSt(i,:));
            
            lambdaDistLStSparse = lambdaDistLStSparse*2;
            flag = 1;
            
        end   
        
        [xDelLSt,~] = l1_ls(del_ALSt,BDelLSt,lambdaDistLStSparse*flag/2,[],'quiet');
        errDelLStFinal = mean((del_hMusDataLSt(i,:)-xDelLSt'*del_hFeatDataLSt).^2);  
        relerrDelLStFinal = errDelLStFinal/max(del_hMusDataLSt(i,:));
        
        errHoldDelLSt(i) = errDelLStFinal;
        relerrHoldDelLSt(i) = relerrDelLStFinal;
        lambdaDelLSt(i) = lambdaDistLStSparse*flag/2;
        delw_LSt(i,:) = xDelLSt';

    else
        delw_LSt(i,:) = zeros(size(delw_LSt,2),1);
    end
    clear lambdaSt_CV errDelLSt relerrDelLSt;
  
end

end