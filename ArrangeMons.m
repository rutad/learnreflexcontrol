% 
% ArrangeMons - Arrange joint monitors on second screen to the left 
% 
% H. Geyer
% 27 July 2006
%


% set monitor offset
x_offset = 0;

% get all childs of root
Childs = get(0, 'Children');

% arrange right ankle monitor
MIdx = find(ismember( get(Childs, 'Name'), 'Right Ankle Monitor')==1);
set(Childs(MIdx),'Position', [x_offset+670 25 600 260])

% arrange right knee monitor
MIdx = find(ismember( get(Childs, 'Name'), 'Right Knee Monitor')==1);
set(Childs(MIdx),'Position', [x_offset+670 320 600 320])

% arrange right hip monitor
MIdx = find(ismember( get(Childs, 'Name'), 'Right Hip Monitor')==1);
set(Childs(MIdx),'Position', [x_offset+670 675 600 320])



% arrange left knee monitor
MIdx = find(ismember( get(Childs, 'Name'), 'Left Knee Monitor')==1);
set(Childs(MIdx),'Position', [x_offset+10 320 600 320])

% arrange right hip monitor
MIdx = find(ismember( get(Childs, 'Name'), 'Left Hip Monitor')==1);
set(Childs(MIdx),'Position', [x_offset+10 675 600 320])



% arrange animation figure
MIdx = find(ismember( get(Childs, 'Name'), 'Man Model (Swing Leg Update)')==1);
set(Childs(MIdx),'Position', [x_offset+10 5 600 280])


