function [sys,x0] = AnkleMonitor(t,x,u, flag,ts, LeftRight); 

% OnlineTraces.m:  S-function for showing angle and stimulation traces
%                  for the ankle joint of the foot model.
%																						                          
%																					                             
% Last modified:   26 June 2006                              
%            by:   H. Geyer  				                                  
%





% **************** %
% DECLARATION PART %
% **************** %

% global variables
global  LeftAnkleMonitorHndl RightAnkleMonitorHndl % figure handle

% view window size 
ViewWin = 4;  %[s]

% figure name (identifier)
if LeftRight==0,
  FigName = 'Left Ankle Monitor';
else
  FigName = 'Right Ankle Monitor';
end


% limits
Phi12Width = 50; %[deg] (will be centered around 90 degree

% colors
if LeftRight == 0
  StCol = 0.6*[0 1 0]; 
else
  StCol = [1 0 0]; 
end



% ************ %
% PROGRAM PART %
% ************ %

switch flag,
  
  

  % --------------
  % Initialization
  % --------------
  
  case 0,
     
    
    % Initialize Figure
    % -----------------
    
    % initialize animation figure
    Anim_Init(FigName);
    
    % store figure handle for repeated access
    AnkleMonitorHndl = findobj('Type', 'figure',  'Name', FigName);

    % assign left or right monitor handle
    if LeftRight == 0,
      LeftAnkleMonitorHndl = AnkleMonitorHndl; 
    else
      RightAnkleMonitorHndl = AnkleMonitorHndl; 
    end

    % set figure axis range
    axis([0 0+ViewWin -0.5 5]);
    
    % set y axis labels
    set(gca, 'YTick', -0.5:0.5:5)
    set(gca, 'YTickLabel',[num2str(0-Phi12Width/2); '  0'; ...
             [' ' num2str(0+Phi12Width/2)]; '-10'; ' 10'; ' 30'; '  0'; '0.5'; '  1'; ...
             '  0'; '0.5'; '  1'])
    
    
             
    % Initialize Plot Handle, i.e. create plot dummy
    % ----------------------------------------------
        
    % Annotation: the Simulink inputs u(i) are not present at
    % flag = 0. So the plot dummy must be initiated with
    % all values set to an arbitrary value (zero); i.e., a
    % dummy is created.
       
             
    % Stance Phase handle
    StPhaseHndl =  patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', 0.9*[1 1 1], 'EdgeColor', 'none');
    
    DSupHndl    =  patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', 0.83*[1 1 1], 'EdgeColor', 'none');
    
    % Plot Separation Lines
    SepLine1Hndl = plot(1e6*[-1 1], [0.75 0.75], 'k', 'EraseMode', 'none');
    SepLine2Hndl = plot(1e6*[-1 1], [2.25 2.25], 'k', 'EraseMode', 'none');
    SepLine3Hndl = plot(1e6*[-1 1], [3.75 3.75], 'k', 'EraseMode', 'none');
   
    % right ankle angle
    RPhi12Hndl  = plot(0, 0, '-', 'Color', StCol, 'EraseMode','none', 'LineWidth', 3);
    RM12Hndl    = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    RSolActHndl = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    RTaActHndl  = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    
  
    
    % set IO-data: .  .  .  number of Simulink "u(i)" - inputs  .  .
    sys = [0 5 0 6 0 0];
    
    % set initial conditions (no conditions)
    x0 = [0 0 1.5 3 4.5];
    
   
   
  % ------------
  % Modification
  % ------------
  
  case 2, 

    % assign monitor hndl
    if LeftRight==0,
      AnkleMonitorHndl = LeftAnkleMonitorHndl;
    else
      AnkleMonitorHndl = RightAnkleMonitorHndl;
    end

    % search root for FigHndl
    if any( get(0,'Children') ==  AnkleMonitorHndl )
      
      % check handle validity 
      if strcmp(  get(  AnkleMonitorHndl,'Name' ), FigName  )
        
        % set actual figure to handle
        set(0, 'CurrentFigure',  AnkleMonitorHndl);
        
        
        % Check whether model is in view window 
        % and adjust window if it is not
        % -------------------------------------
        
        % get axis limits
        XLimits = get(gca, 'XLim');
        
        % check right border
        if XLimits(2) < t
          set(gca, 'XLim', [t  t + ViewWin]);
        end
         

        % Refresh Plot
        % ------------
        
        % get plot handles
        PlotHandles = get( gca, 'Children');
        
        
        % assign plot handles (LIFO: last plot is first element)
        RTaActHndl   = PlotHandles(1);
        RSolActHndl  = PlotHandles(2);
        RM12Hndl     = PlotHandles(3);
        RPhi12Hndl   = PlotHandles(4);
        SepLine3Hndl = PlotHandles(5);
        SepLine2Hndl = PlotHandles(6);
        SepLine1Hndl = PlotHandles(7);
        DSupHndl     = PlotHandles(8);
        StPhaseHndl  = PlotHandles(9);
        
        % calculate new ankle values
        RPhi12  = ( 90-u(2)-(0-Phi12Width/2)) / Phi12Width -0.5;
        RM12    = ( u(3)-(-10) ) / (30-(-10)) + 1;
        RSolAct = ( u(4)-0 )  / (1-0) + 2.5;
        RTaAct  = ( u(5)-0 )  / (1-0) + 4;
        
        % limit outputs to stay within assigned borders
        if RPhi12  >0.7,  RPhi12=0.7; elseif  RPhi12<(-1),   RPhi12=-1; end
        if RM12    >2.2,    RM12=2.2; elseif    RM12< 0.8,    RM12=0.8; end
        
        
        % set new plot values     
        if u(1)==1,
          
          % single or double support background
          if u(6)==1,
            set(DSupHndl,    'XData', [x(1) t t x(1) x(1)], 'YData', [-0.5 -0.5 5 5 -0.5]) 
          else
            set(StPhaseHndl, 'XData', [x(1) t t x(1) x(1)], 'YData', [-0.5 -0.5 5 5 -0.5]) 
          end

          % refresh separation lines (uses trick 1e-6*t to force change,
          % and hence, drawing
          set( SepLine1Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
          set( SepLine2Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
          set( SepLine3Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
        end
          
        set(RPhi12Hndl, 'XData', [x(1) t], 'YData', [x(2) RPhi12], 'LineWidth', 1+2*u(1));
        set(RM12Hndl,   'XData', [x(1) t t x(1) x(1)], 'YData', [1.25 1.25 RM12 x(3) 1.25]);
        set(RSolActHndl,'XData', [x(1) t t x(1) x(1)], 'YData', [2.5 2.5 RSolAct x(4) 2.5]);
        set(RTaActHndl, 'XData', [x(1) t t x(1) x(1)], 'YData', [4   4   RTaAct  x(5) 4]);
        
                        
      end 
    end

    % states to return for next call of S-Fct
    sys = [t RPhi12 RM12 RSolAct RTaAct];



  % -------------------------
  % Return Values to Simulink
  % -------------------------
  
  case 3,     
  
    % no values to return
    sys = []; 

 
    
  % ---------------------------
  % Calculate Next Calling Time
  % ---------------------------
  
  case 4,

    % calculate next calling time
  	sys = t + ts;


  % -----------------
  % End Of Simulation
  % -----------------
  
  case 9,
    
    % clean up
    sys = []; 
    
    

  otherwise
    error(['Unhandled flag = ',num2str(flag)]); % flag error handling


end %switch





% ************* %
% FUNCTION PART %
% ************* %

function Anim_Init(namestr)



% -----------------
% Initialize Figure
% -----------------

% check whether figure exists already
[existFlag, figNumber] = figflag(namestr);

% if not, initialize figure
if ~existFlag,
   
  % define figure element
  h0 = figure( ...
       'Tag',                          namestr, ...
       'Name',                         namestr, ...
       'NumberTitle',                    'off', ...
       'BackingStore',                   'off', ...
       'MenuBar',                       'none', ...
			 'Color',                        [1 1 1], ...
       'Position',     [650  20  600   260]);
     
     
     
  % define axes element
  h1 = axes( ...
       'Parent',                            h0, ...
       'Tag',                           'axes', ...    
       'Units',                   'normalized', ...
       'Position',         [0.07 0.07  0.92 0.92], ...
       'FontSize',                          8);
 
end %if ~existflag



% ----------------------------------
% Reset Figure to Simulation Default
% ----------------------------------



% reset axes to default properties
cla reset;

% change some properties
set(gca, 'DrawMode',   'fast', ...
         'Visible',      'on', ...
         'Color',     [1 1 1], ...
         'XColor',    [0 0 0], ...
				 'YColor',    [0 0 0]);

axis on;
hold on;

xlabel('time (s)')
ylabel('\bf90^\circ-\phi_{12}            M_{12}             A_{SOL}               A_{TA}')

