function [sys,x0] = HipMonitor(t,x,u, flag,ts, LeftRight); 

% OnlineTraces.m:  S-function for showing angle and stimulation traces
%                  for the ankle joint of the foot model.
%																						                          
%																					                             
% Note             LeftRight = 0 <=> left  leg
%                            = 1 <=> right leg
%
% Last modified:   26 June 2006                              
%            by:   H. Geyer  				                                  
%






% **************** %
% DECLARATION PART %
% **************** %

% global variables
global  RightHipMonitorHndl LeftHipMonitorHndl  % figure handle

% view window size 
ViewWin = 4;  %[s]

% figure name (identifier)
if LeftRight == 0
  FigName = 'Left Hip Monitor';
else
  FigName = 'Right Hip Monitor';
end


% limits
Phi3HWidth = 60; %[deg] (will be centered around 0 degree

% colors
if LeftRight == 0
  StCol = 0.6*[0 1 0]; 
else
  StCol = [1 0 0]; 
end




% ************ %
% PROGRAM PART %
% ************ %

switch flag,
  
  

  % --------------
  % Initialization
  % --------------
  
  case 0,
     
    
    % Initialize Figure
    % -----------------
    
    % initialize animation figure
    Anim_Init(FigName);
    
    % store figure handle for repeated access
    if LeftRight==0,
      LeftHipMonitorHndl = findobj('Type', 'figure',  'Name', FigName);
    else
      RightHipMonitorHndl = findobj('Type', 'figure',  'Name', FigName);
    end
    
    % set figure axis range 
    axis([0 0+ViewWin -0.5 6.5]);
    
    % set y axis labels
    set(gca, 'YTick', [-0.5:0.5:6.5])
    set(gca, 'YTickLabel',[num2str(-Phi3HWidth/2); '  0'; [' ' num2str(0+Phi3HWidth/2)]; ...
             '-15'; '  0'; ' 15'; '  0'; '0.5'; '  1'; '  0'; '0.5'; '  1'; '  0'; '0.5'; '  1'])
          
             
    % Initialize Plot Handle, i.e. create plot dummy
    % ----------------------------------------------
        
    % Annotation: the Simulink inputs u(i) are not present at
    % flag = 0. So the plot dummy must be initiated with
    % all values set to an arbitrary value (zero); i.e., a
    % dummy is created.
    
    % Stance Phase handle
    StPhaseHndl =  patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', 0.9*[1 1 1], 'EdgeColor', 'none');
    
    DSupHndl    =  patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', 0.8*[1 1 1], 'EdgeColor', 'none');
    
    % Plot Separation Lines
    SepLine1Hndl = plot(1e6*[-1 1], [0.75 0.75], 'k', 'EraseMode', 'none');
    SepLine2Hndl = plot(1e6*[-1 1], [2.25 2.25], 'k', 'EraseMode', 'none');
    SepLine3Hndl = plot(1e6*[-1 1], [3.75 3.75], 'k', 'EraseMode', 'none');
    SepLine4Hndl = plot(1e6*[-1 1], [5.25 5.25], 'k', 'EraseMode', 'none');
   
    
    % right ankle angle
    RPhi3HHndl   = plot(0, 0, '-', 'Color', StCol, 'EraseMode','none', 'LineWidth', 3);
    RM3HHndl     = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    RHAMActHndl  = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    RGLUActHndl  = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    RHFLActHndl  = patch([0 0 0 0 0], [0 0 0 0 0], 'r', 'EraseMode', 'none', ...
                        'FaceColor', StCol, 'EdgeColor', StCol);
    
    
    % set IO-data: .  .  .  number of Simulink "u(i)" - inputs  .  .
    sys = [0 6 0 7 0 0];
    
    % set initial conditions (no conditions)
    x0 = [0 0 1.5 3 4.5 6];
    
   
   
  % ------------
  % Modification
  % ------------
  
  case 2, 

    
    % select left or right leg
    if LeftRight==0
      HipMonitorHndl = LeftHipMonitorHndl;
    else
      HipMonitorHndl = RightHipMonitorHndl;
    end
    
    % search root for FigHndl
    if any( get(0,'Children') ==  HipMonitorHndl )
      
      % check handle validity 
      if strcmp(  get(  HipMonitorHndl,'Name' ), FigName  )
        
        % set actual figure to handle
        set(0, 'CurrentFigure',  HipMonitorHndl);
        
        
        % Check whether model is in view window 
        % and adjust window if it is not
        % -------------------------------------
        
        % get axis limits
        XLimits = get(gca, 'XLim');
        
        % check right border
        if XLimits(2) < t
          set(gca, 'XLim', [t  t + ViewWin]);
        end
         

        % Refresh Plot
        % ------------
        
        % get plot handles
        PlotHandles = get( gca, 'Children');
           
        % assign plot handles (LIFO: last plot is first element)
        RHFLActHndl  = PlotHandles(1);
        RGLUActHndl  = PlotHandles(2);
        RHAMActHndl  = PlotHandles(3);
        RM3HHndl     = PlotHandles(4);
        RPhi3HHndl   = PlotHandles(5);
        SepLine4Hndl = PlotHandles(6);
        SepLine3Hndl = PlotHandles(7);
        SepLine2Hndl = PlotHandles(8);
        SepLine1Hndl = PlotHandles(9);
        DSupHndl     = PlotHandles(10);
        StPhaseHndl  = PlotHandles(11);
        
        % calculate new ankle values
        RPhi3H  = ( 180-u(2)-(0-Phi3HWidth/2)) / Phi3HWidth -0.5;
        RM3H    = ( u(3)-(-10) ) / (10-(-10)) + 1;
        RHAMAct = ( u(4)-0 )  / (1-0) + 2.5;
        RGLUAct = ( u(5)-0 )  / (1-0) + 4;
        RHFLAct = ( u(6)-0 )  / (1-0) + 5.5;
        
        % limit outputs to stay within assigned borders
        if RPhi3H  >0.7,  RPhi3H=0.7; elseif  RPhi3H<(-1),   RPhi3H=-1; end
        if RM3H    >2.2,    RM3H=2.2; elseif    RM3H< 0.8,    RM3H=0.8; end
        
        % set new plot values     
        if u(1)==1,
          
          % single or double support background
          if u(7)==1,
            set(DSupHndl,    'XData', [x(1) t t x(1) x(1)], 'YData', [-0.5 -0.5 6.5 6.5 -0.5]) 
          else
            set(StPhaseHndl, 'XData', [x(1) t t x(1) x(1)], 'YData', [-0.5 -0.5 6.5 6.5 -0.5]) 
          end
          
          % refresh separation lines (uses trick 1e-6*t to force change,
          % and hence, drawing
          set( SepLine1Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
          set( SepLine2Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
          set( SepLine3Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
          set( SepLine4Hndl, 'XData', 1e6*[-1 1]+1e-6*t)
        end

        set(RPhi3HHndl, 'XData', [x(1) t], 'YData', [x(2) RPhi3H], 'LineWidth', 1+2*u(1));
        set(RM3HHndl,   'XData', [x(1) t t x(1) x(1)], 'YData',  [1.5 1.5 RM3H    x(3) 1.5]);
        set(RHAMActHndl,'XData', [x(1) t t x(1) x(1)], 'YData',  [2.5 2.5 RHAMAct x(4) 2.5]);
        set(RGLUActHndl,'XData', [x(1) t t x(1) x(1)], 'YData',  [4   4   RGLUAct x(5) 4  ]);
        set(RHFLActHndl, 'XData', [x(1) t t x(1) x(1)], 'YData', [5.5 5.5 RHFLAct x(6) 5.5]);
        
      end 
    end

    % states to return for next call of S-Fct
    sys = [t RPhi3H RM3H RHAMAct RGLUAct RHFLAct];



  % -------------------------
  % Return Values to Simulink
  % -------------------------
  
  case 3,     
  
    % no values to return
    sys = []; 

 
    
  % ---------------------------
  % Calculate Next Calling Time
  % ---------------------------
  
  case 4,

    % calculate next calling time
  	sys = t + ts;


  % -----------------
  % End Of Simulation
  % -----------------
  
  case 9,
    
    % clean up
    sys = []; 
    
    

  otherwise
    error(['Unhandled flag = ',num2str(flag)]); % flag error handling


end %switch





% ************* %
% FUNCTION PART %
% ************* %

function Anim_Init(namestr)



% -----------------
% Initialize Figure
% -----------------

% check whether figure exists already
[existFlag, figNumber] = figflag(namestr);

% if not, initialize figure
if ~existFlag,
   
  % define figure element
  h0 = figure( ...
       'Tag',                          namestr, ...
       'Name',                         namestr, ...
       'NumberTitle',                    'off', ...
       'BackingStore',                   'off', ...
       'MenuBar',                       'none', ...
			 'Color',                        [1 1 1], ...
       'Position',     [650  20  600   320]);
     
     
     
  % define axes element
  h1 = axes( ...
       'Parent',                            h0, ...
       'Tag',                           'axes', ...    
       'Units',                   'normalized', ...
       'Position',         [0.07 0.07  0.92 0.92], ...
       'FontSize',                          8);
 
end %if ~existflag



% ----------------------------------
% Reset Figure to Simulation Default
% ----------------------------------



% reset axes to default properties
cla reset;

% change some properties
set(gca, 'DrawMode',   'fast', ...
         'Visible',      'on', ...
         'Color',     [1 1 1], ...
         'XColor',    [0 0 0], ...
				 'YColor',    [0 0 0]);

axis on;
hold on;

xlabel('time (s)')
ylabel('\bf180\circ-\phi_{34}           M_{34}               A_{HAM}            A_{GLU}            A_{HFL}')

