% Relate kinematics data to muscle stimulation
clc;
close all;
clear all;

%load linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_roughGndRfeatFlip.mat;
load linRel_13msNMwalking100sTrainTestData_musFeatNORMStSw_GndRoughSlopeFlat_RfeatFlip.mat;

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

%%%%% LEFT %%%%%%%%

% only least square solution using pinv
wp_LSt = MusDataLSt'*pinv(FeatDataLSt'); % wp is 14X14
wp_LSw = MusDataLSw'*pinv(FeatDataLSw'); % wp is 14X14

% l1-least square
wLSt = NaN(size(wp_LSt));
wLSw = NaN(size(wp_LSw));

ALSt = FeatDataLSt;
ALSw = FeatDataLSw;

err_tol_St = 1e-3*5;
err_tol_Sw = 1e-3;

lambdaSt = 0.7; % l1 cost weight
%lambdaSw = 0.1; % l1 cost weight

for i = 1:7
    BLSt = MusDataLSt(:,i);
    
    [lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
    lambdaSt = lambda_max*err_tol_St;
    
    [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt);
    wLSt(i,:) = xLSt';
        
    BLSw = MusDataLSw(:,i);
    
    [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw);
    wLSw(i,:) = xLSw';
end

%%%%%%%%% RIGHT %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% only least square solution using pinv
wp_RSt = MusDataRSt'*pinv(FeatDataRSt'); % wp is 14X14
wp_RSw = MusDataRSw'*pinv(FeatDataRSw'); % wp is 14X14

% l1-least square
wRSt = NaN(size(wp_RSt));
wRSw = NaN(size(wp_RSw));

ARSt = FeatDataRSt;
ARSw = FeatDataRSw;

for i = 1:7
    BRSt = MusDataRSt(:,i);
       
    [lambda_max] = find_lambdamax_l1_ls(ARSt',BRSt);
    lambdaSt = lambda_max*err_tol_St
    
    [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt);
    wRSt(i,:) = xRSt';
        
    BRSw = MusDataRSw(:,i);
    
    [lambda_max] = find_lambdamax_l1_ls(ARSw',BRSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw);
    wRSw(i,:) = xRSw';
end

%% Test weight w on a particular sample gait left leg     

tst = randi(testNum,1,1); % index of test sample
strideInd = testDataInd(tst);

tLSt = LgaitStart_ind(strideInd):LswingStart_ind(strideInd);
tRSt = RgaitStart_ind(strideInd):LswingStart_ind(strideInd);

tLSw = LswingStart_ind(strideInd):LgaitEnd_ind(strideInd);
tRSw = RswingStart_ind(strideInd):RgaitEnd_ind(strideInd);

% left stance
testFeatLSt = [LL_HFLDataLSt(tLSt,:) LF_GLUDataLSt(tLSt,:) LL_HAMDataLSt(tLSt,:) LF_HAMDataLSt(tLSt,:) LF_VASDataLSt(tLSt,:) LF_GASDataLSt(tLSt,:) LF_SOLDataLSt(tLSt,:) LL_TADataLSt(tLSt,:) TrunkDataLSt(tLSt,:)./trunkMax ...
            RL_HFLDataLSt(tLSt,:) RF_GLUDataLSt(tLSt,:) RL_HAMDataLSt(tLSt,:) RF_HAMDataLSt(tLSt,:) RF_VASDataLSt(tLSt,:) RF_GASDataLSt(tLSt,:) RF_SOLDataLSt(tLSt,:) RL_TADataLSt(tLSt,:) dTrunkDataLSt(tLSt,:)./dtrunkMax];
       
trueMusLSt = [LHFLDataLSt(tLSt,:) LGLUDataLSt(tLSt,:) LHAMDataLSt(tLSt,:) LVASDataLSt(tLSt,:) LGASDataLSt(tLSt,:) LSOLDataLSt(tLSt,:) LTADataLSt(tLSt,:)]; 
 
testMusLSt = wLSt*testFeatLSt';        
testMusp_LSt = wp_LSt*testFeatLSt';

% left swing
testFeatLSw = [LL_HFLDataLSw(tLSw,:) LF_GLUDataLSw(tLSw,:) LL_HAMDataLSw(tLSw,:) LF_HAMDataLSw(tLSw,:) LF_VASDataLSw(tLSw,:) LF_GASDataLSw(tLSw,:) LF_SOLDataLSw(tLSw,:) LL_TADataLSw(tLSw,:) TrunkDataLSw(tLSw,:)./trunkMax ...
            RL_HFLDataLSw(tLSw,:) RF_GLUDataLSw(tLSw,:) RL_HAMDataLSw(tLSw,:) RF_HAMDataLSw(tLSw,:) RF_VASDataLSw(tLSw,:) RF_GASDataLSw(tLSw,:) RF_SOLDataLSw(tLSw,:) RL_TADataLSw(tLSw,:) dTrunkDataLSw(tLSw,:)./dtrunkMax];
       
trueMusLSw = [LHFLDataLSw(tLSw,:) LGLUDataLSw(tLSw,:) LHAMDataLSw(tLSw,:) LVASDataLSw(tLSw,:) LGASDataLSw(tLSw,:) LSOLDataLSw(tLSw,:) LTADataLSw(tLSw,:)];
            
 
testMusLSw = wLSw*testFeatLSw';        
testMusp_LSw = wp_LSw*testFeatLSw';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % right stance
testFeatRSt = [RL_HFLDataRSt(tRSt,:) RF_GLUDataRSt(tRSt,:) RL_HAMDataRSt(tRSt,:) RF_HAMDataRSt(tRSt,:) RF_VASDataRSt(tRSt,:) RF_GASDataRSt(tRSt,:) RF_SOLDataRSt(tRSt,:) RL_TADataRSt(tRSt,:) TrunkDataRSt(tRSt,:)./trunkMax...
    LL_HFLDataRSt(tRSt,:) LF_GLUDataRSt(tRSt,:) LL_HAMDataRSt(tRSt,:) LF_HAMDataRSt(tRSt,:) LF_VASDataRSt(tRSt,:) LF_GASDataRSt(tRSt,:) LF_SOLDataRSt(tRSt,:) LL_TADataRSt(tRSt,:) dTrunkDataRSt(tRSt,:)./dtrunkMax];
       
trueMusRSt = [RHFLDataRSt(tRSt,:) RGLUDataRSt(tRSt,:) RHAMDataRSt(tRSt,:) RVASDataRSt(tRSt,:) RGASDataRSt(tRSt,:) RSOLDataRSt(tRSt,:) RTADataRSt(tRSt,:)]; 
 
testMusRSt = wRSt*testFeatRSt';        
testMusp_RSt = wp_RSt*testFeatRSt';

% right swing
testFeatRSw = [RL_HFLDataRSw(tRSw,:) RF_GLUDataRSw(tRSw,:) RL_HAMDataRSw(tRSw,:) RF_HAMDataRSw(tRSw,:) RF_VASDataRSw(tRSw,:) RF_GASDataRSw(tRSw,:) RF_SOLDataRSw(tRSw,:) RL_TADataRSw(tRSw,:) TrunkDataRSw(tRSw,:)./trunkMax...
    LL_HFLDataRSw(tRSw,:) LF_GLUDataRSw(tRSw,:) LL_HAMDataRSw(tRSw,:) LF_HAMDataRSw(tRSw,:) LF_VASDataRSw(tRSw,:) LF_GASDataRSw(tRSw,:) LF_SOLDataRSw(tRSw,:) LL_TADataRSw(tRSw,:) dTrunkDataRSw(tRSw,:)./dtrunkMax];
       
trueMusRSw = [RHFLDataRSw(tRSw,:) RGLUDataRSw(tRSw,:) RHAMDataRSw(tRSw,:) RVASDataRSw(tRSw,:) RGASDataRSw(tRSw,:) RSOLDataRSw(tRSw,:) RTADataRSw(tRSw,:)]; 
            
 
testMusRSw = wRSw*testFeatRSw';        
testMusp_RSw = wp_RSw*testFeatRSw';

%% visualize stance
%cd linRel_Results/

figure;
subplot(7,1,1); plot(testMusLSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_LSt(1,:)');
subplot(7,1,1); plot(trueMusLSt(:,1)');
title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_LSt(2,:)');
subplot(7,1,2); plot(trueMusLSt(:,2)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_LSt(3,:)');
subplot(7,1,3); plot(trueMusLSt(:,3)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_LSt(4,:)');
subplot(7,1,4); plot(trueMusLSt(:,4)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_LSt(5,:)');
subplot(7,1,5); plot(trueMusLSt(:,5)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_LSt(6,:)');
subplot(7,1,6); plot(trueMusLSt(:,6)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_LSt(7,:)');
subplot(7,1,7); plot(trueMusLSt(:,7)');ylabel('LTA');

export_fig -transparent reconstructSparse_MusFeatNormStSwLSt_roughGndSlopeFlat.pdf

figure;
subplot(7,1,1); plot(testMusRSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_RSt(1,:)');
subplot(7,1,1); plot(trueMusRSt(:,1)');
title('Right muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_RSt(2,:)');
subplot(7,1,2); plot(trueMusRSt(:,2)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_RSt(3,:)');
subplot(7,1,3); plot(trueMusRSt(:,3)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_RSt(4,:)');
subplot(7,1,4); plot(trueMusRSt(:,4)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_RSt(5,:)');
subplot(7,1,5); plot(trueMusRSt(:,5)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_RSt(6,:)');
subplot(7,1,6); plot(trueMusRSt(:,6)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_RSt(7,:)');
subplot(7,1,7); plot(trueMusRSt(:,7)');ylabel('RTA');

export_fig -transparent reconstructSparse_MusFeatNormStSwRSt_roughGndSlopeFlat.pdf

%% visualize swing 

figure;
subplot(7,1,1); plot(testMusLSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_LSw(1,:)');
subplot(7,1,1); plot(trueMusLSw(:,1)');
title('Left muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_LSw(2,:)');
subplot(7,1,2); plot(trueMusLSw(:,2)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_LSw(3,:)');
subplot(7,1,3); plot(trueMusLSw(:,3)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_LSw(4,:)');
subplot(7,1,4); plot(trueMusLSw(:,4)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_LSw(5,:)');
subplot(7,1,5); plot(trueMusLSw(:,5)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_LSw(6,:)');
subplot(7,1,6); plot(trueMusLSw(:,6)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_LSw(7,:)');
subplot(7,1,7); plot(trueMusLSw(:,7)');ylabel('LTA');

export_fig -transparent reconstructSparse_MusFeatNormStSwLSw_roughGndSlopeFlat.pdf

figure;
subplot(7,1,1); plot(testMusRSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_RSw(1,:)');
subplot(7,1,1); plot(trueMusRSw(:,1)');
title('Right muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_RSw(2,:)');
subplot(7,1,2); plot(trueMusRSw(:,2)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_RSw(3,:)');
subplot(7,1,3); plot(trueMusRSw(:,3)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_RSw(4,:)');
subplot(7,1,4); plot(trueMusRSw(:,4)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_RSw(5,:)');
subplot(7,1,5); plot(trueMusRSw(:,5)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_RSw(6,:)');
subplot(7,1,6); plot(trueMusRSw(:,6)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_RSw(7,:)');
subplot(7,1,7); plot(trueMusRSw(:,7)');ylabel('RTA');

export_fig -transparent reconstructSparse_MusFeatNormStSwRSw_roughGndSlopeFlat.pdf

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

%% left stance w
figure;imagesc(abs(wp_LSt)/(max(max(abs(wp_LSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
saveas(gcf,'w_MusFeatLSt_roughGndSlopeFlat.pdf')

figure;imagesc(abs(wLSt)/(max(max(abs(wLSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
saveas(gcf,'wSparse_MusFeatLSt_roughGndSlopeFlat.pdf')

%% left swing w
figure;imagesc(abs(wp_LSw)/(max(max(abs(wp_LSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
saveas(gcf,'w_MusFeatLSw_roughGndSlopeFlat.pdf')

figure;imagesc(abs(wLSw)/(max(max(abs(wLSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
saveas(gcf,'wSparse_MusFeatLSw_roughGndSlopeFlat.pdf')

%% Visualize w RIGHT

labels_ry = {'RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_rx = {'RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','Trunk','LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','dTrunk'};

%% right stance w
figure;imagesc(abs(wp_RSt)/(max(max(abs(wp_RSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
saveas(gcf,'w_MusFeatRSt_roughGndSlopeFlat.pdf')

figure;imagesc(abs(wRSt)/(max(max(abs(wRSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
saveas(gcf,'wSparse_MusFeatRSt_roughGndSlopeFlat.pdf')

%% right swing w
figure;imagesc(abs(wp_RSw)/(max(max(abs(wp_RSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
saveas(gcf,'w_MusFeatRSw_roughGndSlopeFlat.pdf')

figure;imagesc(abs(wRSw)/(max(max(abs(wRSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
saveas(gcf,'wSparse_MusFeatRSw_roughGndSlopeFlat.pdf')

%cd ..