%clc;
%clear all;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;
KinInd = 0;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end-1);%LTO_ind(2:end);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end);%RTO_ind(3:end-1);
RswingEnd_ind = RgaitEnd_ind;

% resampling length left
resampleLSt_p = max(LswingStart_ind-LgaitStart_ind)+1;
resampleLSw_p = max(LswingEnd_ind-LswingStart_ind)+1;

LdataLen = length(LgaitStart_ind);

% resample length right
resampleRSt_p = max(RswingStart_ind-RgaitStart_ind)+1;
resampleRSw_p = max(RswingEnd_ind-RswingStart_ind)+1;

RdataLen = length(RgaitStart_ind);

if LdataLen~=RdataLen
    LdataLen = min(LdataLen,RdataLen);
end

%% Store kinematics and muscle data from gait cycles (Stance)

% Preallocate memory for left stance muscle feat data

L_delKneeDataLSt = NaN(LdataLen,resampleLSt_p);
L_FContraDataLSt = NaN(LdataLen,resampleLSt_p);
L_FIpsiDataLSt = NaN(LdataLen,resampleLSt_p);
LF_VASDataLSt = NaN(LdataLen,resampleLSt_p);
LF_GASDataLSt = NaN(LdataLen,resampleLSt_p);
LF_SOLDataLSt = NaN(LdataLen,resampleLSt_p);
LL_TADataLSt = NaN(LdataLen,resampleLSt_p);

L_delThetaDataLSt = NaN(LdataLen,resampleLSt_p);
dThetaDataLSt = NaN(LdataLen,resampleLSt_p);

% Preallocate memory for left stance muscle data

LHFLDataLSt = NaN(LdataLen,resampleLSt_p);
LGLUDataLSt = NaN(LdataLen,resampleLSt_p);
LHAMDataLSt = NaN(LdataLen,resampleLSt_p);
LVASDataLSt = NaN(LdataLen,resampleLSt_p);
LGASDataLSt = NaN(LdataLen,resampleLSt_p);
LSOLDataLSt = NaN(LdataLen,resampleLSt_p);
LTADataLSt = NaN(LdataLen,resampleLSt_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right stance muscle feat data

R_delKneeDataRSt = NaN(LdataLen,resampleRSt_p);
R_FContraDataRSt = NaN(LdataLen,resampleRSt_p);
R_FIpsiDataRSt = NaN(LdataLen,resampleRSt_p);
RF_VASDataRSt = NaN(LdataLen,resampleRSt_p);
RF_GASDataRSt = NaN(LdataLen,resampleRSt_p);
RF_SOLDataRSt = NaN(LdataLen,resampleRSt_p);
RL_TADataRSt = NaN(LdataLen,resampleRSt_p);

R_delThetaDataRSt = NaN(LdataLen,resampleRSt_p);
dThetaDataRSt = NaN(LdataLen,resampleRSt_p);

% Preallocate memory for right muscle stance data

RHFLDataRSt = NaN(LdataLen,resampleRSt_p);
RGLUDataRSt = NaN(LdataLen,resampleRSt_p);
RHAMDataRSt = NaN(LdataLen,resampleRSt_p);
RVASDataRSt = NaN(LdataLen,resampleRSt_p);
RGASDataRSt = NaN(LdataLen,resampleRSt_p);
RSOLDataRSt = NaN(LdataLen,resampleRSt_p);
RTADataRSt = NaN(LdataLen,resampleRSt_p);

for i=1:LdataLen
    
    LdelKnee_LSt = LdelKnee.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angular velocity
    LdelKnee_LStR = resample(LdelKnee_LSt,resampleLSt_p,size(LdelKnee_LSt,1));
    
    LFContra_LSt = LFcontra.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFContra_LStR = resample(LFContra_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LFIpsi_LSt = LFipsi.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFIpsi_LStR = resample(LFIpsi_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LFVAS_LSt = LF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFVAS_LStR = resample(LFVAS_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LFGAS_LSt = LF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFGAS_LStR = resample(LFGAS_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LFSOL_LSt = LF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFSOL_LStR = resample(LFSOL_LSt,resampleLSt_p,size(LFContra_LSt,1));
   
    LLTA_LSt = LL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LLTA_LStR = resample(LLTA_LSt,resampleLSt_p,size(LFContra_LSt,1));  
  
    L_delTheta_LSt = LdelTheta.Data(LgaitStart_ind(i):LswingStart_ind(i),1);
    L_delTheta_LStR = resample(L_delTheta_LSt,resampleLSt_p,size(LFContra_LSt,1));
    dTheta_LSt = dtheta.Data(LgaitStart_ind(i):LswingStart_ind(i),1);
    dTheta_LStR = resample(dTheta_LSt,resampleLSt_p,size(LFContra_LSt,1));
        
    LHFL_LSt = LStimHFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LHFL_LStR = resample(LHFL_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LGLU_LSt = LStimGLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LGLU_LStR = resample(LGLU_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LHAM_LSt = LStimHAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LHAM_LStR = resample(LHAM_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LVAS_LSt = LStimVAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LVAS_LStR = resample(LVAS_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LGAS_LSt = LStimGAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LGAS_LStR = resample(LGAS_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LSOL_LSt = LStimSOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LSOL_LStR = resample(LSOL_LSt,resampleLSt_p,size(LFContra_LSt,1));
    
    LTA_LSt = LStimTA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LTA_LStR = resample(LTA_LSt,resampleLSt_p,size(LFContra_LSt,1));

    L_delKneeDataLSt(i,:) = LdelKnee_LStR;
    L_FContraDataLSt(i,:) = LFContra_LStR;
    L_FIpsiDataLSt(i,:) = LFIpsi_LStR;
    LF_VASDataLSt(i,:) = LFVAS_LStR;
    LF_GASDataLSt(i,:) = LFGAS_LStR;
    LF_SOLDataLSt(i,:) = LFSOL_LStR;
    LL_TADataLSt(i,:) = LLTA_LStR;
    
    L_delThetaDataLSt(i,:) = L_delTheta_LStR;
    dThetaDataLSt(i,:) = dTheta_LStR;
    
    LHFLDataLSt(i,:) = LHFL_LStR;
    LGLUDataLSt(i,:) = LGLU_LStR;
    LHAMDataLSt(i,:) = LHAM_LStR;
    LVASDataLSt(i,:) = LVAS_LStR;
    LGASDataLSt(i,:) = LGAS_LStR;
    LSOLDataLSt(i,:) = LSOL_LStR;
    LTADataLSt(i,:) = LTA_LStR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    RdelKnee_RSt = RdelKnee.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angular velocity
    RdelKnee_RStR = resample(RdelKnee_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RFContra_RSt = RFcontra.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFContra_RStR = resample(RFContra_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RFIpsi_RSt = RFipsi.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFIpsi_RStR = resample(RFIpsi_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RFVAS_RSt = RF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFVAS_RStR = resample(RFVAS_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RFGAS_RSt = RF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFGAS_RStR = resample(RFGAS_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RFSOL_RSt = RF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFSOL_RStR = resample(RFSOL_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
   
    RLTA_RSt = RL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RLTA_RStR = resample(RLTA_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    R_delTheta_RSt = RdelTheta.Data(RgaitStart_ind(i):RswingStart_ind(i),1);
    R_delTheta_RStR = resample(R_delTheta_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    dTheta_RSt = dtheta.Data(RgaitStart_ind(i):RswingStart_ind(i),1);
    dTheta_RStR = resample(dTheta_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RHF_RSt = RStimHFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RHF_RStR = resample(RHF_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RGLU_RSt = RStimGLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RGLU_RStR = resample(RGLU_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RHAM_RSt = RStimHAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RHAM_RStR = resample(RHAM_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RVAS_RSt = RStimVAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RVAS_RStR = resample(RVAS_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RGAS_RSt = RStimGAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RGAS_RStR = resample(RGAS_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RSOL_RSt = RStimSOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RSOL_RStR = resample(RSOL_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
    
    RTA_RSt = RStimTA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RTA_RStR = resample(RTA_RSt,resampleRSt_p,size(RdelKnee_RSt,1));
   
    
    R_delThetaDataRSt(i,:) = R_delTheta_RStR;
    
    R_delKneeDataRSt(i,:) = RdelKnee_RStR;
    R_FContraDataRSt(i,:) = RFContra_RStR;
    R_FIpsiDataRSt(i,:) = RFIpsi_RStR;
    RF_VASDataRSt(i,:) = RFVAS_RStR;
    RF_GASDataRSt(i,:) = RFGAS_RStR;
    RF_SOLDataRSt(i,:) = RFSOL_RStR;
    RL_TADataRSt(i,:) = RLTA_RStR;
    
    dThetaDataRSt(i,:) = dTheta_RStR;
    
    RHFLDataRSt(i,:) = RHF_RStR;
    RGLUDataRSt(i,:) = RGLU_RStR;
    RHAMDataRSt(i,:) = RHAM_RStR;
    RVASDataRSt(i,:) = RVAS_RStR;
    RGASDataRSt(i,:) = RGAS_RStR;
    RSOLDataRSt(i,:) = RSOL_RStR;
    RTADataRSt(i,:) = RTA_RStR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    KinInd = KinInd + 1; 
end

%% Store kinematics and muscle data from gait cycles (Swing)

% Preallocate memory for left swing muscle feat data

LL_HFLDataLSw = NaN(LdataLen,resampleLSw_p);
LF_GLUDataLSw = NaN(LdataLen,resampleLSw_p);
LL_HAMDataLSw = NaN(LdataLen,resampleLSw_p);
LF_HAMDataLSw = NaN(LdataLen,resampleLSw_p);
LL_TADataLSw = NaN(LdataLen,resampleLSw_p);

L_delThetaDataLSw = NaN(LdataLen,resampleLSw_p);

% Preallocate memory for left swing muscle data

LHFLDataLSw = NaN(LdataLen,resampleLSw_p);
LGLUDataLSw = NaN(LdataLen,resampleLSw_p);
LHAMDataLSw = NaN(LdataLen,resampleLSw_p);
LVASDataLSw = NaN(LdataLen,resampleLSw_p);
LGASDataLSw = NaN(LdataLen,resampleLSw_p);
LSOLDataLSw = NaN(LdataLen,resampleLSw_p);
LTADataLSw = NaN(LdataLen,resampleLSw_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right swing muscle feat data

RL_HFLDataRSw = NaN(LdataLen,resampleRSw_p);
RF_GLUDataRSw = NaN(LdataLen,resampleRSw_p);
RL_HAMDataRSw = NaN(LdataLen,resampleRSw_p);
RF_HAMDataRSw = NaN(LdataLen,resampleRSw_p);
RL_TADataRSw = NaN(LdataLen,resampleRSw_p);

R_delThetaDataRSw = NaN(LdataLen,resampleRSw_p);

% Preallocate memory for right muscle swing data

RHFLDataRSw = NaN(LdataLen,resampleRSw_p);
RGLUDataRSw = NaN(LdataLen,resampleRSw_p);
RHAMDataRSw = NaN(LdataLen,resampleRSw_p);
RVASDataRSw = NaN(LdataLen,resampleRSw_p);
RGASDataRSw = NaN(LdataLen,resampleRSw_p);
RSOLDataRSw = NaN(LdataLen,resampleRSw_p);
RTADataRSw = NaN(LdataLen,resampleRSw_p);

for i=1:LdataLen
    
    LLHFL_LSw = LL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angle
    LLHFL_LSwR = resample(LLHFL_LSw,resampleLSw_p,size(LLHFL_LSw,1)); % resample to max gait cycle length 
    
    LFGLU_LSw = LF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angular velocity
    LFGLU_LSwR = resample(LFGLU_LSw,resampleLSw_p,size(LFGLU_LSw,1));
    
    LLHAM_LSw = LL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LLHAM_LSwR = resample(LLHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LFHAM_LSw = LF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFHAM_LSwR = resample(LFHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
   
    LLTA_LSw = LL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LLTA_LSwR = resample(LLTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
        
  
    L_delTheta_LSw = LdelTheta.Data(LswingStart_ind(i):LgaitEnd_ind(i),1);
    L_delTheta_LSwR = resample(L_delTheta_LSw,resampleLSw_p,size(LLHAM_LSw,1));
        
    LHFL_LSw = LStimHFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LHFL_LSwR = resample(LHFL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LGLU_LSw = LStimGLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LGLU_LSwR = resample(LGLU_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LHAM_LSw = LStimHAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LHAM_LSwR = resample(LHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LVAS_LSw = LStimVAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LVAS_LSwR = resample(LVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LGAS_LSw = LStimGAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LGAS_LSwR = resample(LGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LSOL_LSw = LStimSOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LSOL_LSwR = resample(LSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LTA_LSw = LStimTA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LTA_LSwR = resample(LTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    
    LL_HFLDataLSw(i,:) = LLHFL_LSwR;
    LF_GLUDataLSw(i,:) = LFGLU_LSwR;
    LL_HAMDataLSw(i,:) = LLHAM_LSwR;
    LF_HAMDataLSw(i,:) = LFHAM_LSwR;
    LL_TADataLSw(i,:) = LLTA_LSwR;
    
    L_delThetaDataLSw(i,:) = L_delTheta_LSwR;

    LHFLDataLSw(i,:) = LHFL_LSwR;
    LGLUDataLSw(i,:) = LGLU_LSwR;
    LHAMDataLSw(i,:) = LHAM_LSwR;
    LVASDataLSw(i,:) = LVAS_LSwR;
    LGASDataLSw(i,:) = LGAS_LSwR;
    LSOLDataLSw(i,:) = LSOL_LSwR;
    LTADataLSw(i,:) = LTA_LSwR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    RLHFL_RSw = RL_HFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angle
    RLHFL_RSwR = resample(RLHFL_RSw,resampleRSw_p,size(RLHFL_RSw,1)); % resample to max gait cycle length 
    
    RFGLU_RSw = RF_GLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angular velocity
    RFGLU_RSwR = resample(RFGLU_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RLHAM_RSw = RL_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RLHAM_RSwR = resample(RLHAM_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RFHAM_RSw = RF_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RFHAM_RSwR = resample(RFHAM_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RLTA_RSw = RL_TA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RLTA_RSwR = resample(RLTA_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    R_delTheta_RSw = RdelTheta.Data(RswingStart_ind(i):RgaitEnd_ind(i),1);
    R_delTheta_RSwR = resample(R_delTheta_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RHF_RSw = RStimHFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RHF_RSwR = resample(RHF_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RGLU_RSw = RStimGLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RGLU_RSwR = resample(RGLU_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RHAM_RSw = RStimHAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RHAM_RSwR = resample(RHAM_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RVAS_RSw = RStimVAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RVAS_RSwR = resample(RVAS_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RGAS_RSw = RStimGAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RGAS_RSwR = resample(RGAS_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RSOL_RSw = RStimSOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RSOL_RSwR = resample(RSOL_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    RTA_RSw = RStimTA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RTA_RSwR = resample(RTA_RSw,resampleRSw_p,size(RLHFL_RSw,1));
    
    R_delThetaDataRSw(i,:) = R_delTheta_RSwR;
    
    RL_HFLDataRSw(i,:) = RLHFL_RSwR;
    RF_GLUDataRSw(i,:) = RFGLU_RSwR;
    RL_HAMDataRSw(i,:) = RLHAM_RSwR;
    RF_HAMDataRSw(i,:) = RFHAM_RSwR;
    RL_TADataRSw(i,:) = RLTA_RSwR;
    
    RHFLDataRSw(i,:) = RHF_RSwR;
    RGLUDataRSw(i,:) = RGLU_RSwR;
    RHAMDataRSw(i,:) = RHAM_RSwR;
    RVASDataRSw(i,:) = RVAS_RSwR;
    RGASDataRSw(i,:) = RGAS_RSwR;
    RSOLDataRSw(i,:) = RSOL_RSwR;
    RTADataRSw(i,:) = RTA_RSwR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    KinInd = KinInd + 1; 
end

%% Hold out test data (stance)

testNum = 5; % number of test samples
testDataInd = randi(LdataLen,testNum,1);

% test data left stance
tL_delKneeDataLSt = L_delKneeDataLSt(testDataInd,:);
tL_FContraDataLSt = L_FContraDataLSt(testDataInd,:);
tL_FIpsiDataLSt = L_FIpsiDataLSt(testDataInd,:);
tLF_VASDataLSt = LF_VASDataLSt(testDataInd,:);
tLF_GASDataLSt = LF_GASDataLSt(testDataInd,:);
tLF_SOLDataLSt = LF_SOLDataLSt(testDataInd,:);
tLL_TADataLSt = LL_TADataLSt(testDataInd,:);

tL_delThetaDataLSt = L_delThetaDataLSt(testDataInd,:);
tdThetaDataLSt = dThetaDataLSt(testDataInd,:);

tLHFLDataLSt = LHFLDataLSt(testDataInd,:);
tLGLUDataLSt = LGLUDataLSt(testDataInd,:);
tLHAMDataLSt = LHAMDataLSt(testDataInd,:);
tLVASDataLSt = LVASDataLSt(testDataInd,:);
tLGASDataLSt = LGASDataLSt(testDataInd,:);
tLSOLDataLSt = LSOLDataLSt(testDataInd,:);
tLTADataLSt = LTADataLSt(testDataInd,:);

% Remove test data from all left stance data

L_delKneeDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
L_FContraDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
L_FIpsiDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_VASDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_GASDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_SOLDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LL_TADataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);

L_delThetaDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
dThetaDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);

LHFLDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LGLUDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LHAMDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LVASDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LGASDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LSOLDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LTADataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% test data right stance

tR_delThetaDataRSt = R_delThetaDataRSt(testDataInd,:);

tR_delKneeDataRSt = R_delKneeDataRSt(testDataInd,:);
tR_FContraDataRSt = R_FContraDataRSt(testDataInd,:);
tR_FIpsiDataRSt = R_FIpsiDataRSt(testDataInd,:);
tRF_VASDataRSt = RF_VASDataRSt(testDataInd,:);
tRF_GASDataRSt = RF_GASDataRSt(testDataInd,:);
tRF_SOLDataRSt = RF_SOLDataRSt(testDataInd,:);
tRL_TADataRSt = RL_TADataRSt(testDataInd,:);

tdThetaDataRSt = dThetaDataRSt(testDataInd,:);

tRHFLDataRSt = RHFLDataRSt(testDataInd,:);
tRGLUDataRSt = RGLUDataRSt(testDataInd,:);
tRHAMDataRSt = RHAMDataRSt(testDataInd,:);
tRVASDataRSt = RVASDataRSt(testDataInd,:);
tRGASDataRSt = RGASDataRSt(testDataInd,:);
tRSOLDataRSt = RSOLDataRSt(testDataInd,:);
tRTADataRSt = RTADataRSt(testDataInd,:);

% Remove test data from all right stance data

R_delThetaDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);

R_delKneeDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
R_FContraDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
R_FIpsiDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_VASDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_GASDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_SOLDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RL_TADataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);

dThetaDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);

RHFLDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RGLUDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RHAMDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RVASDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RGASDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RSOLDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RTADataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);

%% Hold out test data (swing)

% test data left swing
tLL_HFLDataLSw = LL_HFLDataLSw(testDataInd,:);
tLF_GLUDataLSw = LF_GLUDataLSw(testDataInd,:);
tLL_HAMDataLSw = LL_HAMDataLSw(testDataInd,:);
tLF_HAMDataLSw = LF_HAMDataLSw(testDataInd,:);
tLL_TADataLSw = LL_TADataLSw(testDataInd,:);

tL_delThetaDataLSw = L_delThetaDataLSw(testDataInd,:);

tLHFLDataLSw = LHFLDataLSw(testDataInd,:);
tLGLUDataLSw = LGLUDataLSw(testDataInd,:);
tLHAMDataLSw = LHAMDataLSw(testDataInd,:);
tLVASDataLSw = LVASDataLSw(testDataInd,:);
tLGASDataLSw = LGASDataLSw(testDataInd,:);
tLSOLDataLSw = LSOLDataLSw(testDataInd,:);
tLTADataLSw = LTADataLSw(testDataInd,:);

% Remove test data from all left swing data

LL_HFLDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_GLUDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LL_HAMDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_HAMDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LL_TADataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);

L_delThetaDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);

LHFLDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LGLUDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LHAMDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LVASDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LGASDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LSOLDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LTADataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% test data right swing
tR_delThetaDataRSw = R_delThetaDataRSw(testDataInd,:);

tRL_HFLDataRSw = RL_HFLDataRSw(testDataInd,:);
tRF_GLUDataRSw = RF_GLUDataRSw(testDataInd,:);
tRL_HAMDataRSw = RL_HAMDataRSw(testDataInd,:);
tRF_HAMDataRSw = RF_HAMDataRSw(testDataInd,:);
tRL_TADataRSw = RL_TADataRSw(testDataInd,:);

tRHFLDataRSw = RHFLDataRSw(testDataInd,:);
tRGLUDataRSw = RGLUDataRSw(testDataInd,:);
tRHAMDataRSw = RHAMDataRSw(testDataInd,:);
tRVASDataRSw = RVASDataRSw(testDataInd,:);
tRGASDataRSw = RGASDataRSw(testDataInd,:);
tRSOLDataRSw = RSOLDataRSw(testDataInd,:);
tRTADataRSw = RTADataRSw(testDataInd,:);

% Remove test data from all right swing data

R_delThetaDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);

RL_HFLDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_GLUDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RL_HAMDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_HAMDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RL_TADataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);

RHFLDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RGLUDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RHAMDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RVASDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RGASDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RSOLDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RTADataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
%% Mean data (left stance)

% mean kinematics over all gait cycles (only training)
mL_delKneeDataLSt = mean(L_delKneeDataLSt);
mL_FContraDataLSt = mean(L_FContraDataLSt);
mL_FIpsiDataLSt = mean(L_FIpsiDataLSt);
mLF_VASDataLSt = mean(LF_VASDataLSt);
mLF_GASDataLSt = mean(LF_GASDataLSt);
mLF_SOLDataLSt = mean(LF_SOLDataLSt);
mLL_TADataLSt = mean(LL_TADataLSt);

mL_delThetaDataLSt = mean(L_delThetaDataLSt);
mdThetaDataLSt = mean(dThetaDataLSt);

% collect mean muscle features over gaits (only training)
mFeatDataLSt = [mL_delKneeDataLSt; mL_FContraDataLSt; mL_FIpsiDataLSt; mLF_VASDataLSt; mLF_GASDataLSt; mLF_SOLDataLSt; mLL_TADataLSt; mL_delThetaDataLSt; mdThetaDataLSt];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataLSt = mean(LHFLDataLSt);
mLGLUDataLSt = mean(LGLUDataLSt);
mLHAMDataLSt = mean(LHAMDataLSt);
mLVASDataLSt = mean(LVASDataLSt);
mLGASDataLSt = mean(LGASDataLSt);
mLSOLDataLSt = mean(LSOLDataLSt);
mLTADataLSt = mean(LTADataLSt); 

% collect mean muscles over gaits (only training)
mMusDataLSt = [mLHFLDataLSt; mLGLUDataLSt; mLHAMDataLSt; mLVASDataLSt; mLGASDataLSt; mLSOLDataLSt; mLTADataLSt];

%% Mean data (right stance)

% mean kinematics over all gait cycles (only training)

mR_delThetaDataRSt = mean(R_delThetaDataRSt);

mR_delKneeDataRSt = mean(R_delKneeDataRSt);
mR_FContraDataRSt = mean(R_FContraDataRSt);
mR_FIpsiDataRSt = mean(R_FIpsiDataRSt);
mRF_VASDataRSt = mean(RF_VASDataRSt);
mRF_GASDataRSt = mean(RF_GASDataRSt);
mRF_SOLDataRSt = mean(RF_SOLDataRSt);
mRL_TADataRSt = mean(RL_TADataRSt);

mdThetaDataRSt = mean(dThetaDataRSt);

% collect mean muscle features over gaits (only training)
mFeatDataRSt = [mR_delKneeDataRSt; mR_FContraDataRSt; mR_FIpsiDataRSt; mRF_VASDataRSt; mRF_GASDataRSt; mRF_SOLDataRSt; mRL_TADataRSt; mR_delThetaDataRSt; mdThetaDataRSt];


mRHFLDataRSt = mean(RHFLDataRSt);
mRGLUDataRSt = mean(RGLUDataRSt);
mRHAMDataRSt = mean(RHAMDataRSt);
mRVASDataRSt = mean(RVASDataRSt);
mRGASDataRSt = mean(RGASDataRSt);
mRSOLDataRSt = mean(RSOLDataRSt);
mRTADataRSt = mean(RTADataRSt); 

mMusDataRSt =  [mRHFLDataRSt; mRGLUDataRSt; mRHAMDataRSt; mRVASDataRSt; mRGASDataRSt; mRSOLDataRSt; mRTADataRSt];
           
        
%% Mean data (left swing)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataLSw = mean(LL_HFLDataLSw);
mLF_GLUDataLSw = mean(LF_GLUDataLSw);
mLL_HAMDataLSw = mean(LL_HAMDataLSw);
mLF_HAMDataLSw = mean(LF_HAMDataLSw);
mLL_TADataLSw = mean(LL_TADataLSw);

mL_delThetaDataLSw = mean(L_delThetaDataLSw);

% collect mean muscle features over gaits (only training)
mFeatDataLSw = [mLL_HFLDataLSw; mLF_GLUDataLSw; mLL_HAMDataLSw; mLF_HAMDataLSw; mLL_TADataLSw; mL_delThetaDataLSw];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataLSw = mean(LHFLDataLSw);
mLGLUDataLSw = mean(LGLUDataLSw);
mLHAMDataLSw = mean(LHAMDataLSw);
mLVASDataLSw = mean(LVASDataLSw);
mLGASDataLSw = mean(LGASDataLSw);
mLSOLDataLSw = mean(LSOLDataLSw);
mLTADataLSw = mean(LTADataLSw); 

% collect mean muscles over gaits (only training)
mMusDataLSw = [mLHFLDataLSw; mLGLUDataLSw; mLHAMDataLSw; mLVASDataLSw; mLGASDataLSw; mLSOLDataLSw; mLTADataLSw];

%% Mean data (right swing)

% mean kinematics over all gait cycles (only training)

mR_delThetaDataRSw = mean(R_delThetaDataRSw);

mRL_HFLDataRSw = mean(RL_HFLDataRSw);
mRF_GLUDataRSw = mean(RF_GLUDataRSw);
mRL_HAMDataRSw = mean(RL_HAMDataRSw);
mRF_HAMDataRSw = mean(RF_HAMDataRSw);
mRL_TADataRSw = mean(RL_TADataRSw);

% collect mean muscle features over gaits (only training)
mFeatDataRSw = [mRL_HFLDataRSw; mRF_GLUDataRSw; mRL_HAMDataRSw; mRF_HAMDataRSw; mRL_TADataRSw; mR_delThetaDataRSw];
        
mRHFLDataRSw = mean(RHFLDataRSw);
mRGLUDataRSw = mean(RGLUDataRSw);
mRHAMDataRSw = mean(RHAMDataRSw);
mRVASDataRSw = mean(RVASDataRSw);
mRGASDataRSw = mean(RGASDataRSw);
mRSOLDataRSw = mean(RSOLDataRSw);
mRTADataRSw = mean(RTADataRSw); 

mMusDataRSw =[mRHFLDataRSw; mRGLUDataRSw; mRHAMDataRSw; mRVASDataRSw; mRGASDataRSw; mRSOLDataRSw; mRTADataRSw];