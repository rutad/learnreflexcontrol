% Relate kinematics data to muscle stimulation
clc;
close all;
clear all;

%load linRel_13ms_NMwalking_100sData.mat;
load linRel_13ms_NMwalking_100sTrainTestData.mat;

%% Direct linear relationship
% mMusData = W mKinData (min kinematic data relates to mean muscle stimualtion)

mKinDataL = [mKinData(1:3,:);mKinData(7,:);mKinData(8:10,:);mKinData(14,:)];% Left leg kin data
mKinDataR = [mKinData(4:6,:);mKinData(7,:);mKinData(11:13,:);mKinData(14,:)];% Right leg kin data

wL= mMusData(1:7,:)*pinv(mKinDataL); % left leg weight matrix wL is 7X8
wR= mMusData(8:end,:)*pinv(mKinDataR); % right leg weight matrix wR is 7X8

% Test weight w on a particular sample gait        

t = randi(testNum,1,1); % index of test sample

testKinL = [tLAnkData(t,:); tLKneeData(t,:); tLHipData(t,:); tTrunkData(t,:); ...
            tdLAnkData(t,:); tdLKneeData(t,:); tdLHipData(t,:); tdTrunkData(t,:)];
 
testKinR = [tRAnkData(t,:); tRKneeData(t,:); tRHipData(t,:); tTrunkData(t,:); ...
            tdRAnkData(t,:); tdRKneeData(t,:); tdRHipData(t,:); tdTrunkData(t,:)];
        
trueMus = [tLHFLData(t,:); tLGLUData(t,:); tLHAMData(t,:); tLVASData(t,:); tLGASData(t,:); tLSOLData(t,:); tLTAData(t,:); ...
            tRHFLData(t,:); tRGLUData(t,:); tRHAMData(t,:); tRVASData(t,:); tRGASData(t,:); tRSOLData(t,:); tRTAData(t,:)]; 
        
testMusL = wL*testKinL;
testMusR = wR*testKinR;

% visualize 

figure;
subplot(7,1,1); plot(testMusL(1,:)');hold on
subplot(7,1,1); plot(trueMus(1,:)');
title('Left muscles reconstructed-using same leg Kin data'); ylabel('LHFL');
legend('Reconstructed','TrueData');

subplot(7,1,2); plot(testMusL(2,:)');hold on
subplot(7,1,2); plot(trueMus(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusL(3,:)');hold on
subplot(7,1,3); plot(trueMus(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusL(4,:)');hold on
subplot(7,1,4); plot(trueMus(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusL(5,:)');hold on
subplot(7,1,5); plot(trueMus(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusL(6,:)');hold on
subplot(7,1,6); plot(trueMus(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusL(7,:)');hold on
subplot(7,1,7); plot(trueMus(7,:)');ylabel('LTA');

%export_fig -transparent reconstruct_LMusKinSameLeg.pdf

figure;
subplot(7,1,1); plot(testMusR(1,:)');hold on
subplot(7,1,1); plot(trueMus(8,:)');
title('Right muscles reconstructed-using same leg Kin data'); ylabel('RHFL');
legend('Reconstructed','TrueData');

subplot(7,1,2); plot(testMusR(2,:)');hold on
subplot(7,1,2); plot(trueMus(9,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusR(3,:)');hold on
subplot(7,1,3); plot(trueMus(10,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusR(4,:)');hold on
subplot(7,1,4); plot(trueMus(11,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusR(5,:)');hold on
subplot(7,1,5); plot(trueMus(12,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusR(6,:)');hold on
subplot(7,1,6); plot(trueMus(13,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusR(7,:)');hold on
subplot(7,1,7); plot(trueMus(14,:)');ylabel('RTA');

%export_fig -transparent reconstruct_RMusKinSameLeg.pdf

%% Visualize w

labels_yL = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_yR = {'RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};

labels_xL = {'LAnk','LKnee','LHip','Trunk',...
    'dLAnk','dLKnee','dLHip','dTrunk'};
labels_xR = {'RAnk','RKnee','RHip','Trunk',...
    'dRAnk','dRKnee','dRHip','dTrunk'};

figure;imagesc(abs(wL));colorbar;title('Left leg weights(same leg Kin Data)');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_yL);
set(gca(),'XTick',1:8) 
set(gca(),'XTickLabel',labels_xL, 'XTickLabelRotation', 90);

figure;imagesc(abs(wR));colorbar;title('Right leg weights(same leg Kin Data)');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_yR);
set(gca(),'XTick',1:8) 
set(gca(),'XTickLabel',labels_xR, 'XTickLabelRotation', 90);