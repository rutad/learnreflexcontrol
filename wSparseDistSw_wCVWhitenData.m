% Relate kinematics data to muscle stimulation

function [wDistLSw, delw_LSw] =  wSparseDistSw_wCVWhitenData(distData, testData, steadyData)

mFeatDataDistLSw = distData{1};
mMusDataDistLSw = distData{2};

holdFeatDataLSw = testData{1};
holdMusDataLSw = testData{2};

mFeatDataLSw = steadyData{1};
mMusDataLSw = steadyData{2};

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSw_r=resample(mFeatDataLSw',size(mFeatDataDistLSw,2),size(mFeatDataLSw,2))';
mMusDataLSw_r=resample(mMusDataLSw',size(mMusDataDistLSw,2),size(mMusDataLSw,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSw = mMusDataDistLSw - mMusDataLSw_r;
del_mFeatDataLSw = mFeatDataDistLSw - mFeatDataLSw_r;

% resample undisturbed data to match length of test data
hFeatDataLSw_r=resample(mFeatDataLSw',size(holdFeatDataLSw,2),size(mFeatDataLSw,2))';
hMusDataLSw_r=resample(mMusDataLSw',size(holdMusDataLSw,2),size(mMusDataLSw,2))';

% subtracting steady state motion from the disturbed motion
del_hMusDataLSw = holdMusDataLSw - hMusDataLSw_r;
del_hFeatDataLSw = holdFeatDataLSw - hFeatDataLSw_r;

%%%%% DISTURBED DATA and DISTURBED - STEADY DATA %%%%%%%%

% l1-least square
wDistLSw = NaN(size(mMusDataLSw,1),size(mFeatDataLSw,1));
delw_LSw = NaN(size(wDistLSw));

ADistLSw = mFeatDataDistLSw';
del_ALSw = del_mFeatDataLSw';

% zero mean
ADistLSw = bsxfun(@minus, ADistLSw, mean(ADistLSw));
del_ALSw = bsxfun(@minus, del_ALSw, mean(del_ALSw));

% unit variance
ADistLSw = ADistLSw*diag(1./std(ADistLSw));
del_ALSw = del_ALSw*diag(1./std(del_ALSw));

err_tol_min = 1e-4;
MSE_errThSw = 0.001; % 0.01 %0.001 %0.00001;
RMSE_errThSw = 0.001; %0.01 %0.001

lambdaDistLSw = NaN(size(mMusDataLSw,1),1);
lambdaDelLSw = NaN(size(mMusDataLSw,1),1);

relerrHoldDistLSw = NaN(size(mMusDataLSw,1),1);
relerrHoldDelLSw = NaN(size(mMusDataLSw,1),1);

errHoldDistLSw = NaN(size(mMusDataLSw,1),1);
errHoldDelLSw = NaN(size(mMusDataLSw,1),1);

for i = 1:7
    
    
    multThDistLSw = err_tol_min;
    multThDelLSw = err_tol_min;
    
    BDistLSw = mMusDataDistLSw(i,:)';
    BDistLSw = bsxfun(@minus, BDistLSw, mean(BDistLSw));
    if (std(BDistLSw)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
        
    BDistLSw = BDistLSw./std(BDistLSw);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(ADistLSw',BDistLSw);
        lambdaSw_CV(cnt) = lambda_max*multThDistLSw;
        
        lambdaSw = lambdaSw_CV(cnt);
        
        while (lambdaSw<=lambda_max)
             
            [xDistLSw,~] = l1_ls(ADistLSw,BDistLSw,lambdaSw,[],'quiet');        
            errDistLSw(cnt) = mean((holdMusDataLSw(i,:)-xDistLSw'*holdFeatDataLSw).^2);
            relerrDistLSw(cnt) = errDistLSw(cnt)/max(holdMusDataLSw(i,:));
            
            cnt = cnt+1;
            multThDistLSw = multThDistLSw*2;
            lambdaSw_CV(cnt) = lambda_max*multThDistLSw;
            lambdaSw = lambdaSw_CV(cnt);

        end    
        
        [minError,lambdaFinalInd] = min(errDistLSw);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaDistLSwSparse = lambdaSw_CV(lambdaFinalInd);
        relerrDistLSwSparse = relerrDistLSw(lambdaFinalInd);
        errDistLSwSparse = errDistLSw(lambdaFinalInd);
        flag = 2;
        
        while (relerrDistLSwSparse<RMSE_errThSw && errDistLSwSparse<MSE_errThSw && lambdaDistLSwSparse<=lambda_max)
            
            [xDistLSw,~] = l1_ls(ADistLSw,BDistLSw,lambdaDistLSwSparse,[],'quiet');        
            errDistLSwSparse = mean((holdMusDataLSw(i,:)-xDistLSw'*holdFeatDataLSw).^2);
            relerrDistLSwSparse = errDistLSwSparse/max(holdMusDataLSw(i,:));
            
            lambdaDistLSwSparse = lambdaDistLSwSparse*2;
            flag = 1;
            
        end   
        
        [xDistLSw,~] = l1_ls(ADistLSw,BDistLSw,lambdaDistLSwSparse*flag/2,[],'quiet');
        errDistLSwFinal = mean((holdMusDataLSw(i,:)-xDistLSw'*holdFeatDataLSw).^2);  
        relerrDistLSwFinal = errDistLSwFinal/max(holdMusDataLSw(i,:));
        
        errHoldDistLSw(i) = errDistLSwFinal;
        relerrHoldDistLSw(i) = relerrDistLSwFinal;
        lambdaDistLSw(i) = lambdaDistLSwSparse*flag/2;
        wDistLSw(i,:) = xDistLSw';
   
    else
        wDistLSw(i,:) = zeros(size(wDistLSw,2),1);
    end       
    clear lambdaSw_CV errDistLSw relerrDistLSw;
    
    
    BDelLSw = del_mMusDataLSw(i,:)';
    BDelLSw = bsxfun(@minus, BDelLSw, mean(BDelLSw));
    if (std(BDelLSw)<1e-4)
        tooSmall=1;
    else
        tooSmall=0;
    end
    BDelLSw = BDelLSw./std(BDelLSw);
    
    if (~tooSmall)
        cnt = 1;

        [lambda_max] = find_lambdamax_l1_ls(del_ALSw',BDelLSw);
        lambdaSw_CV(cnt) = lambda_max*multThDelLSw;
        
        lambdaSw = lambdaSw_CV(cnt);
        
        while (lambdaSw<=lambda_max)
             
            [xDelLSw,~] = l1_ls(del_ALSw,BDelLSw,lambdaSw,[],'quiet');        
            errDelLSw(cnt) = mean((del_hMusDataLSw(i,:)-xDelLSw'*del_hFeatDataLSw).^2);
            relerrDelLSw(cnt) = errDelLSw(cnt)/max(del_hMusDataLSw(i,:));
            
            cnt = cnt+1;
            multThDelLSw = multThDelLSw*2;
            lambdaSw_CV(cnt) = lambda_max*multThDelLSw;
            lambdaSw = lambdaSw_CV(cnt);

        end     
        
        [minError,lambdaFinalInd] = min(errDelLSw);
        
        % find final lambda by increasing lambda beyond lambdamin till
        % errTh is crossed
        
        lambdaDistLSwSparse = lambdaSw_CV(lambdaFinalInd);
        relerrDelLSwSparse = relerrDelLSw(lambdaFinalInd);
        errDelLSwSparse = errDelLSw(lambdaFinalInd);
        flag = 2;
        
        while (relerrDelLSwSparse<RMSE_errThSw && errDelLSwSparse<MSE_errThSw && lambdaDistLSwSparse<=lambda_max)
            
            [xDelLSw,~] = l1_ls(del_ALSw,BDelLSw,lambdaDistLSwSparse,[],'quiet');        
            errDelLSwSparse = mean((del_hMusDataLSw(i,:)-xDelLSw'*del_hFeatDataLSw).^2);
            relerrDelLSwSparse = errDelLSwSparse/max(del_hMusDataLSw(i,:));
            
            lambdaDistLSwSparse = lambdaDistLSwSparse*2;
            flag = 1;
            
        end   
        
        [xDelLSw,~] = l1_ls(del_ALSw,BDelLSw,lambdaDistLSwSparse*flag/2,[],'quiet');
        errDelLSwFinal = mean((del_hMusDataLSw(i,:)-xDelLSw'*del_hFeatDataLSw).^2);  
        relerrDelLSwFinal = errDelLSwFinal/max(del_hMusDataLSw(i,:));
        
        errHoldDelLSw(i) = errDelLSwFinal;
        relerrHoldDelLSw(i) = relerrDelLSwFinal;
        lambdaDelLSw(i) = lambdaDistLSwSparse*flag/2;
        delw_LSw(i,:) = xDelLSw';

    else
        delw_LSw(i,:) = zeros(size(delw_LSw,2),1);
    end
    clear lambdaSw_CV errDelLSw relerrDelLSw;
  
end

end