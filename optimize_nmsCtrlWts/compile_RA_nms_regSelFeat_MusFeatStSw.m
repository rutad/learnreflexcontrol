
nms_model_MechInit;
nms_model_ControlInit;

load regressSelFeat_MusFeatStSw.mat
tsim = 20;

model = 'optim_nms_regSelFeatCtrl_musFeatStSwDsCheat';

numSwCtrlParams = length(find(abs(wRegLSw)>0));
numStCtrlParams = length(find(abs(wRegLSt)>0));

SwParams0 =  cell2mat(linRegWtsLSw);
StParams0 =  cell2mat(linRegWtsLSt);

wSw = zeros(size(wRegLSw,1),size(wRegLSw,2));
wSt = zeros(size(wRegLSt,1),size(wRegLSt,2));

SwStrt = 1;
StStrt = 1;

for i = 1:size(wSw,1)

    SwEnd = SwStrt+length(selectedFeatIndLSw{i})-1;
    StEnd = StStrt+length(selectedFeatIndLSt{i})-1;
    
    wSw(i,selectedFeatIndLSw{i})= SwParams0(SwStrt:SwEnd);
    wSt(i,selectedFeatIndLSt{i})= StParams0(StStrt:StEnd);
    
    SwStrt = SwEnd + 1;
    StStrt = StEnd + 1;

end

load_system(model)
rtp = Simulink.BlockDiagram.buildRapidAcceleratorTarget(model);
close_system(model,0);

clear SwStrt StStrt;
clear wSw wSt;