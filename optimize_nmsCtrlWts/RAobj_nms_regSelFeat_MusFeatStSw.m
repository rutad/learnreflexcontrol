function f = RAobj_nms_regSelFeat_MusFeatStSw(params, varargin)

global simFile rtp initialGuess
simFile1 = simFile;

gains_allPop = bsxfun(@times,initialGuess,exp(params));
popSize = size(gains_allPop,2);

useThis = varargin{1}{4};
selectedFeatIndLSw = useThis{1};
selectedFeatIndLSt = useThis{2};
numSwCtrlParams = useThis{3};
wSize = useThis{4};

f       = nan(popSize,1);

for j = 1:popSize
    gains = gains_allPop(:,j);
    
    SwParams = gains(1:numSwCtrlParams);
    StParams = gains(numSwCtrlParams+1:end);

    wSw = zeros(wSize(1),wSize(2));
    wSt = zeros(wSize(1),wSize(2));
    
    SwStrt = 1;
    StStrt = 1;
    
    for i = 1:size(wSw,1)
        
        SwEnd = SwStrt+length(selectedFeatIndLSw{i})-1;
        StEnd = StStrt+length(selectedFeatIndLSt{i})-1;
        
        wSw(i,selectedFeatIndLSw{i})= SwParams(SwStrt:SwEnd);
        wSt(i,selectedFeatIndLSt{i})= StParams(StStrt:StEnd);
        
        SwStrt = SwEnd + 1;
        StStrt = StEnd + 1;
        
    end

    paramStruct = Simulink.BlockDiagram.modifyTunableParameters(rtp, ...
        'wSw',                	wSw, ...
        'wSt',                wSt);
    
    f(j) = feval(simFile1, paramStruct, varargin);
end

end
