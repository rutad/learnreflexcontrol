function f = sim_nms_regSelFeat_MusFeatStSw(paramSets, varargin)

model           = varargin{1}{1}{1};
tsim           = varargin{1}{1}{2};
c_weight        = varargin{1}{1}{3};

c_time 	= c_weight{1};
c_dist    = c_weight{2};
c_fall    = c_weight{3};

simout = sim(model, ...
    'RapidAcceleratorParameterSets',    paramSets,...
    'RapidAcceleratorUpToDateCheck',    'off',...
    'TimeOut',                          tsim*20,...
    'SaveOutput',                       'on');

        
simout_dist    	= get(simout, 'distance_walked');
simout_tend   	= get(simout, 'tend');
simout_fall    	= get(simout, 'fall');

% penalize
f =  c_time*(tsim-simout_tend) + c_fall*simout_fall + c_dist*(1.3*tsim - simout_dist);

