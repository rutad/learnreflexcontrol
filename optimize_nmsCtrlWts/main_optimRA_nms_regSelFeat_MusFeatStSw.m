clear all; clc; close all;
tStart = tic;
warning off all;

% send email
setEmail;

global simFile rtp initialGuess

%%%%%%
s_simulator = '05';
model       = 'optim_nms_regSelFeatCtrl_musFeatStSwDsCheat';
objFile     = 'RAobj_nms_regSelFeat_MusFeatStSw';
simFile     = 'sim_nms_regSelFeat_MusFeatStSw';
saveName = 'nms_regSelFeat_MusFeatStSw';

compile_RA_nms_regSelFeat_MusFeatStSw;    % output: rtp

% ================== %
% optimization costs %
% ================== %

c_time     = 1e1;
c_dist   = 1e1;
c_fall = 1e5;

c_weight = {c_time,c_dist,c_fall};
usefulVars = {selectedFeatIndLSw, selectedFeatIndLSt, numSwCtrlParams, size(wRegLSw)};

try
   
    %%% optim
    initialGuess = [SwParams0; StParams0];
    initParam   = zeros(length(initialGuess),1)';%bestever.x;
    sigma0       = 1e-1;%*ones(length(initialGuess),1)';
    
    opts.maxiter        = 150; 
    opts.PopSize        = 4+floor(3*log(numSwCtrlParams+numStCtrlParams)); %4+floor(3*log(N)) where N= size(initParam)
    opts.Resume         = 0;
    opts.DiagonalOnly   = 0;
    opts.DispModulo     = 1;
    opts.EvalParallel   = 1;
    opts.LogModulo      = 1;
    opts.LogPlot      = 1;
    
    opts.LogFilenamePrefix = [saveName '_'];
    opts.SaveFilename = [saveName '_variablescmaes.mat'];
    
    varargin = {model, tsim, c_weight, usefulVars};

    % matlabpool open
    [xmin, fmin, counteval, stopflag, out, bestever ] = ...
        cmaes( objFile, initParam, sigma0, opts, varargin);
    % matlabpool close
    
    
    java.lang.System.gc();
    
    
    % send email
    sElapsed = toc(tStart);
    dElapsed = floor(sElapsed/(60*60*24));
    sElapsed = sElapsed - dElapsed*60*60*24;
    hElapsed = floor(sElapsed/(60*60));
    sElapsed = sElapsed - hElapsed*60*60;
    mElapsed = floor(sElapsed/60);
    sElapsed = sElapsed - mElapsed*60;
    sendmail('ruta.p.desai@gmail.com', ['Simulator_' s_simulator ' Success'], ...
        ['Simulator_' s_simulator 10 ...
        'Success' 10 ...
        'runTime (day hour:minute:second) :' ...
        '    ' dElapsed ' ' hElapsed ':' mElapsed ':' sElapsed]);

catch exception
    exception.message
    % send email
    sElapsed = toc(tStart);
    dElapsed = floor(sElapsed/(60*60*24));
    sElapsed = sElapsed - dElapsed*60*60*24;
    hElapsed = floor(sElapsed/(60*60));
    sElapsed = sElapsed - hElapsed*60*60;
    mElapsed = floor(sElapsed/60);
    sElapsed = floor(sElapsed - mElapsed*60);
    sendmail('ruta.p.desai@gmail.com', ['Simulator_' s_simulator ' Exception'], ...
        ['Simulator_' s_simulator 10 ...
        'Exception' 10 ...
        'runTime (day hour:minute:second) :' ...
        '    ' num2str(dElapsed) ' ' num2str(hElapsed) ':' ...
        num2str(mElapsed) ':' num2str(sElapsed)]);
end