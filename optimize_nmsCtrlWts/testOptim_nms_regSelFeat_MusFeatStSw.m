
nms_model_MechInit;
nms_model_ControlInit;

load regressSelFeat_MusFeatStSw.mat
load('nms_regSelFeat_MusFeatStSw_variablescmaes2.mat','bestever');

numSwCtrlParams = length(find(abs(wRegLSw)>0));
numStCtrlParams = length(find(abs(wRegLSt)>0));

gains = bestever.x;

SwParams = gains(1:numSwCtrlParams);
StParams = gains(numSwCtrlParams+1:end);

wSw = zeros(size(wRegLSw,1),size(wRegLSw,2));
wSt = zeros(size(wRegLSw,1),size(wRegLSw,2));

SwStrt = 1;
StStrt = 1;


for i = 1:size(wSw,1)

    SwEnd = SwStrt+length(selectedFeatIndLSw{i})-1;
    StEnd = StStrt+length(selectedFeatIndLSt{i})-1;
    
    wSw(i,selectedFeatIndLSw{i})= SwParams(SwStrt:SwEnd);
    wSt(i,selectedFeatIndLSt{i})= StParams(StStrt:StEnd);
    
    SwStrt = SwEnd + 1;
    StStrt = StEnd + 1;

end

wRegLSw = wSw;
wRegLSt = wSt;