%% Disturbance experiments for establishing causation
% Ruta Desai
% rutad@cs.cmu.edu
% 24th April 2015

%% Set up
clc; 
%close all; 
%clear all;

numFeat = 18; %(16 Mus+2 trunk Kin features)
numMus = 7;
count = 0; % count of disturbance scenarios

nms_model_MechInit;
nms_model_ControlInit;
modelName ='nms_saveMusFeatDataALLDisturbWaitSt_PCALinRel';

load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mMusDataLSt');
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mFeatDataLSt');

%% Disturbance times and forces in swing

% 10%,50% and 90% of stance -- early, mid and late stance disturbances
tDisturb = [0.1*600*1e-3;0.5*600*1e-3;0.9*600*1e-3];
%fDisturb = 15; %15; %[N]

motionDir = [-1,1]; %1 -> flex, -1 -> extend
numDisturbJnts = 4;

WStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
musDistDataStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
featDistDataStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
trunkNormStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);

mean_wDistSteadyLSt = zeros(numMus, numFeat);
mean_wDelSteadyLSt = zeros(size(mean_wDistSteadyLSt));

meanTrunkNormMax = [0, 0];
%% Disturb and collect mean stats over disturbed swings

for k = 1:length(motionDir)

    flex_extend = motionDir(k);
    
    for i =1:length(tDisturb)
       tDisturbSt = tDisturb(i);

       for jntInd = 1:numDisturbJnts

           fd = {0, 0, 0, 0};
           fd(jntInd) = {fDisturb};
           [F_disturb_foot, F_disturb_shank, F_disturb_thigh, F_disturb_trunk]= fd{:};
           sim(modelName);

           sensors = {LL_HFL, LF_GLU, LL_HAM, LF_HAM, LF_VAS, LF_GAS, LF_SOL, LL_TA, ...
               RL_HFL, RF_GLU, RL_HAM, RF_HAM, RF_VAS, RF_GAS, RF_SOL, RL_TA, Torso};

           actuators = {LStimHFL,LStimGLU, LStimHAM, LStimVAS, LStimGAS, LStimSOL, LStimTA};

           fd_sig = {fd_lfoot, fd_lshank, fd_lthigh, fd_trunk};
           
           %disp([k,i,jntInd])
           %keyboard

           % obtain average sensor and actuator over disturbed swings
           [mMusDataDistLSt, mFeatDataDistLSt, trunkNormMax] =  collectGaitDataDistSt (fd_sig{jntInd}, ...
                                                    sensors, actuators, L_TD, L_TO);

           % obtain sparse correlations/ weights for each disturbance scenario
            [wdistSteadyLSt, wdelSteadyLSt] =  wSparse_DistSteadySt({mFeatDataDistLSt,mMusDataDistLSt},...
                                {mFeatDataLSt,mMusDataLSt});

            WStore{k,i,jntInd} = [wdistSteadyLSt, wdelSteadyLSt];
            musDistDataStore{k,i,jntInd}= mMusDataDistLSt;
            featDistDataStore{k,i,jntInd}= mFeatDataDistLSt;
            trunkNormStore{k,i,jntInd}=trunkNormMax;
            
            mean_wDistSteadyLSt = mean_wDistSteadyLSt + wdistSteadyLSt;
            mean_wDelSteadyLSt = mean_wDelSteadyLSt + wdelSteadyLSt;
         
            meanTrunkNormMax = meanTrunkNormMax + trunkNormMax;
         
            count = count +1
                     
       end    


    end
    
end

mean_wDistSteadyLSt = mean_wDistSteadyLSt./count;
mean_wDelSteadyLSt = mean_wDelSteadyLSt./count;

meanTrunkNormMax = meanTrunkNormMax/count;

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

% % undisturbed left swing w
% figure;imagesc(abs(wLSt)/(max(max(abs(wLSt)))));colorbar;title('Leg weights UNDISTURBED LEFT SWING (2-leg Mus Feat Data)-sparse');
% set(gca(),'YTick',1:7) 
% set(gca(),'YTickLabel',labels_ly);
% set(gca(),'XTick',1:18) 
% set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);


%% using disturbed left stance w
str_disturb = strcat('Fdisturb =', num2str(fDisturb));
figure;imagesc(abs(mean_wDistSteadyLSt)/(max(max(abs(mean_wDistSteadyLSt)))));colorbar;title({'Leg weights DISTURBED LEFT STANCE (2-leg Mus Feat Data)-sparse',str_disturb});
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% using delta left stance w (disturbed - steady)
figure;imagesc(abs(mean_wDelSteadyLSt)/(max(max(abs(mean_wDelSteadyLSt)))));colorbar;title({'Leg weights DEL LEFT STANCE (2-leg Mus Feat Data)-sparse',str_disturb});
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);


