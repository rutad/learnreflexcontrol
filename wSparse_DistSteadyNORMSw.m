% Relate kinematics data to muscle stimulation

function [wdistSteadyLSw, wdelSteadyLSw] =  wSparse_DistSteadyNORMSw(distData, steadyData)

mFeatDataDistLSw = distData{1};
mMusDataDistLSw = distData{2};

mFeatDataLSw = steadyData{1};
mMusDataLSw = steadyData{2};

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSw_r=resample(mFeatDataLSw',size(mFeatDataDistLSw,2),size(mFeatDataLSw,2))';
mMusDataLSw_r=resample(mMusDataLSw',size(mMusDataDistLSw,2),size(mMusDataLSw,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSw = mMusDataDistLSw - mMusDataLSw_r;
del_mFeatDataLSw = mFeatDataDistLSw - mFeatDataLSw_r;

% Normalize data
for i=1:size(del_mMusDataLSw,1)
   del_mMusDataLSw(i,:) =  del_mMusDataLSw(i,:)./max(del_mMusDataLSw(i,:));
   mMusDataLSw(i,:) = mMusDataLSw(i,:)./max(mMusDataLSw(i,:));
   mMusDataDistLSw(i,:) = mMusDataDistLSw(i,:)./max(mMusDataDistLSw(i,:));
end

for i=1:size(del_mFeatDataLSw,1)
   del_mFeatDataLSw(i,:) =  del_mFeatDataLSw(i,:)./max(del_mFeatDataLSw(i,:));
   mFeatDataLSw(i,:) = mFeatDataLSw(i,:)./max(mFeatDataLSw(i,:));
   mFeatDataDistLSw(i,:) = mFeatDataDistLSw(i,:)./max(mFeatDataDistLSw(i,:));
end

%%%%% [DISTURBED DATA STEADY DATA] and [DISTURBED - STEADY DATA  STEADY DATA] %%%%%%%%

AdelSteadyLSw = [del_mFeatDataLSw mFeatDataLSw]';
AdistSteadyLSw = [mFeatDataDistLSw mFeatDataLSw]';

BdelSteadyLSw = [del_mMusDataLSw mMusDataLSw];
BdistSteadyLSw = [mMusDataDistLSw mMusDataLSw];

% l1-least square
wdistSteadyLSw = NaN(size(mMusDataLSw,1),size(mFeatDataLSw,1));
wdelSteadyLSw = NaN(size(wdistSteadyLSw));

err_tol_Sw = 1e-4*50;

%lambdaSw = 0.1; % l1 cost weight

for i = 1:7
        
    BDistLSw = BdistSteadyLSw(i,:)';
    del_BLSw = BdelSteadyLSw(i,:)';
    
    [lambda_max] = find_lambdamax_l1_ls(AdistSteadyLSw',BDistLSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [lambdaDel_max] = find_lambdamax_l1_ls(AdelSteadyLSw',del_BLSw);
    lambdaDelSw = lambdaDel_max*err_tol_Sw;%*50;
    
    [xDistLSw,~] = l1_ls(AdelSteadyLSw,BDistLSw,lambdaSw,[],'quiet');
    [del_xLSw,~] = l1_ls(AdistSteadyLSw,del_BLSw,lambdaDelSw,[],'quiet');
    
    wdistSteadyLSw(i,:) = xDistLSw';
    wdelSteadyLSw(i,:) = del_xLSw';
    
end

end