% Relate kinematics data to muscle stimulation
clc;
close all;

load linRel_13ms_NMwalking_100sTrainTestData_musFeat.mat;

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% only least square solution using pinv
wp = mMusData*pinv(mFeatData); % wp is 14X14

% l1-least square
w1 = NaN(size(wp));

A = mFeatData';
lambda = 0.7; % l1 cost weight

for i = 1:14
    B = mMusData(i,:)';
    [x,~] = l1_ls(A,B,lambda);

    w1(i,:) = x';
end

%% Test weight w on a particular sample gait        

t = randi(testNum,1,1); % index of test sample

testFeat = [tLL_HFLData(t,:); tLF_GLUData(t,:); tLL_HAMData(t,:); tLF_HAMData(t,:); tLF_VASData(t,:); tLF_GASData(t,:); tLF_SOLData(t,:); tLL_TAData(t,:); tTrunkData(t,:); ...
            tRL_HFLData(t,:); tRF_GLUData(t,:); tRL_HAMData(t,:); tRF_HAMData(t,:); tRF_VASData(t,:); tRF_GASData(t,:); tRF_SOLData(t,:); tRL_TAData(t,:); tdTrunkData(t,:)];
       
trueMus = [tLHFLData(t,:); tLGLUData(t,:); tLHAMData(t,:); tLVASData(t,:); tLGASData(t,:); tLSOLData(t,:); tLTAData(t,:); ...
            tRHFLData(t,:); tRGLUData(t,:); tRHAMData(t,:); tRVASData(t,:); tRGASData(t,:); tRSOLData(t,:); tRTAData(t,:)]; 
 
testMus1 = w1*testFeat;        
testMusp = wp*testFeat;

% visualize 

figure;
subplot(7,1,1); plot(testMus1(1,:)');hold on
subplot(7,1,1); plot(testMusp(1,:)');
subplot(7,1,1); plot(trueMus(1,:)');
title('Left muscles reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMus1(2,:)');hold on
subplot(7,1,2); plot(testMusp(2,:)');
subplot(7,1,2); plot(trueMus(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMus1(3,:)');hold on
subplot(7,1,3); plot(testMusp(3,:)');
subplot(7,1,3); plot(trueMus(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMus1(4,:)');hold on
subplot(7,1,4); plot(testMusp(4,:)');
subplot(7,1,4); plot(trueMus(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMus1(5,:)');hold on
subplot(7,1,5); plot(testMusp(5,:)');
subplot(7,1,5); plot(trueMus(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMus1(6,:)');hold on
subplot(7,1,6); plot(testMusp(6,:)');
subplot(7,1,6); plot(trueMus(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMus1(7,:)');hold on
subplot(7,1,7); plot(testMusp(7,:)');
subplot(7,1,7); plot(trueMus(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_LMusFeat.pdf

figure;
subplot(7,1,1); plot(testMus1(8,:)');hold on
subplot(7,1,1); plot(testMusp(8,:)');
subplot(7,1,1); plot(trueMus(8,:)');
title('Right muscles reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMus1(9,:)');hold on
subplot(7,1,2); plot(testMusp(9,:)');
subplot(7,1,2); plot(trueMus(9,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMus1(10,:)');hold on
subplot(7,1,3); plot(testMusp(10,:)');
subplot(7,1,3); plot(trueMus(10,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMus1(11,:)');hold on
subplot(7,1,4); plot(testMusp(11,:)');
subplot(7,1,4); plot(trueMus(11,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMus1(12,:)');hold on
subplot(7,1,5); plot(testMusp(12,:)');
subplot(7,1,5); plot(trueMus(12,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMus1(13,:)');hold on
subplot(7,1,6); plot(testMusp(13,:)');
subplot(7,1,6); plot(trueMus(13,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMus1(14,:)');hold on
subplot(7,1,7); plot(testMusp(14,:)');
subplot(7,1,7); plot(trueMus(14,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_RMusFeat.pdf

%% Visualize w

labels_y = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA','RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_x = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

figure;imagesc(abs(wp));colorbar;title('Leg weights(2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);

figure;imagesc(abs(w1));colorbar;title('Leg weights(2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);
