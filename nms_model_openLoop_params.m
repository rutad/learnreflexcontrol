 
clc;
clear all;

load openLoop_kinematics.mat

load RStimHFL.mat
load RStimGLU.mat
load RStimHAM.mat
load RStimVAS.mat
load RStimGAS.mat
load RStimTA.mat
load RStimSOL.mat

load LStimHFL.mat
load LStimGLU.mat
load LStimHAM.mat
load LStimVAS.mat
load LStimGAS.mat
load LStimTA.mat
load LStimSOL.mat

nms_model_MechInit; 


% ************************************ %
% 1. General Neural Control Parameters %
% ************************************ %

% feedback delays
LongDelay  = 0.020; % ankle joint muscles [s]
MidDelay   = 0.010; % knee joint muscles [s]
ShortDelay = 0.005; % hip joint muscles [s]




% ****************************** %
% 2. Specific Control Parameters %
% ****************************** %



% -------------------------------
% 2.1 Stance-Leg Feedback Control 
% -------------------------------

% soleus (self, F+)
GainSOL      =   1.2/FmaxSOL; %[1/N]
PreStimSOL   =         0.01; %[]

% soleus on tibialis anterior (F-)
GainSOLTA    =     0.4/FmaxSOL; %[1/N]
PreStimTA    =          0.01; %[]

% tibialis (self, L+, stance & swing)
GainTA       =         1.1;   %[]
LceOffsetTA  =       1-0.5*w; %[loptTA]

% gastrocnemius (self, F+)
GainGAS      =   1.1/FmaxGAS; %[1/N] 
PreStimGAS   =         0.01; %[]

% vasti group (self, F+)
GainVAS      =    1.2/FmaxVAS; %[1/N]
PreStimVAS   =         0.08; %[]


% knee overextension on vasti (Phi-, directional)
GainKneOverExt = 2;%
KneePh23Offset = 170*pi/180;

% swing initiation
K_swing = 0.25;%0.25



% ------------------------------
% 2.1 Swing-leg Feedback Control 
% ------------------------------


% Fly
% ---

% hip flexors (self, L+, swing)
LceOffsetHFL =        0.65; %[loptHFL]
PreStimHFL   =        0.01; %[] 
GainHFL      =         0.5; %[] 

% balance offset shift (Delta Theta at take-off)
GainDeltaTheta = 2 /100*180/pi; %[percent/deg]


% Catch
% -----

% hip flexor from hamstring stretch reflex (L-, swing)
LceOffsetHAM =        0.85; %[loptHAM]
GainHAMHFL   =           4; %[]

% hamstring group (self, F+, swing)
GainHAM      =  0.65/FmaxHAM; %[1/N]
PreStimHAM   =         0.01; %[]

% gluteus group (self, F+, swing)
GainGLU      =  0.5/FmaxGLU; %[1/N]
PreStimGLU   =         0.01; %[]



% -----------------------------------------------
% 2.2 Stance-Leg HAT Reference Posture PD-Control
% -----------------------------------------------

% stance hip joint position gain
PosGainGG   = 1/(30*pi/180); %[1/rad]

% stance hip joint speed gain
SpeedGainGG = 0.2; %[s/rad] 

% stance posture control muscles pre-stimulation
PreStimGG   = 0.05; %[]

% stance reference posture
Theta0      = 6*pi/180; %[rad]





% ******************************************** %
% 3. Initial Conditions and Simulation Control %
% ******************************************** %



% ----------------------
% 3.1 Initial Conditions
% ----------------------

% initial locomotion speed
vx0 = 1.3; %[m/s] 

% left (stance) leg ankle, knee and hip joint angles
% Lphi120  =  85*pi/180; %[rad]
% Lphi230  = 175*pi/180; %[rad]
% Lphi340  = 175*pi/180; %[rad]
% 
% % right (swing) leg ankle, knee and hip joint angles
% Rphi120  =  90*pi/180; %[rad]
% Rphi230  = 175*pi/180; %[rad]
% Rphi340  = 140*pi/180; %[rad]


% left (stance) leg ankle, knee and hip joint angles
Lphi120  = Lphi12dphi12.Data(1); %[rad]
Lphi230  = Lphi23dphi23.Data(1); %[rad]
Lphi340  = Lphi34dphi34.Data(1); %[rad]

Ldphi120  = 0*Lphi12dphi12.Data(2); %[rad]
Ldphi230  = 0*Lphi23dphi23.Data(2); %[rad]
Ldphi340  = 0*Lphi34dphi34.Data(2); %[rad]

% right (swing) leg ankle, knee and hip joint angles
Rphi120  =  Rphi12dphi12.Data(1); %[rad]
Rphi230  =  Rphi23dphi23.Data(1); %[rad]
Rphi340  =  Rphi34dphi34.Data(1); %[rad]

Rdphi120  =  0*Rphi12dphi12.Data(2); %[rad]
Rdphi230  =  0*Rphi23dphi23.Data(2); %[rad]
Rdphi340  =  0*Rphi34dphi34.Data(2); %[rad]


% ----------------------
% 3.2 Simulation Control
% ----------------------


% Animations
% ----------

% integrator max time step
ts_max = 1e-1;

% animation time steps
ts_anim = 0.04;


% Data Logging
% ------------

% data logging flag
DataLogFlag = 0;

% data logging time step
ts_datalog = 1e-3;

% anti-aliasing low-pass filter frequency
fc_log = 1/ts_datalog/3;

