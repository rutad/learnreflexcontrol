% Relate kinematics data to muscle stimulation

function [wdistSteadyLSw, wdelSteadyLSw] =  wSparse_DistSteadyNORMSw_v2(distData, steadyData)

mFeatDataDistLSw = distData{1};
mMusDataDistLSw = distData{2};

mFeatDataLSw = steadyData{1};
mMusDataLSw = steadyData{2};

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSw_r=resample(mFeatDataLSw',size(mFeatDataDistLSw,2),size(mFeatDataLSw,2))';
mMusDataLSw_r=resample(mMusDataLSw',size(mMusDataDistLSw,2),size(mMusDataLSw,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSw = mMusDataDistLSw - mMusDataLSw_r;
del_mFeatDataLSw = mFeatDataDistLSw - mFeatDataLSw_r;

ind_noMus = [];

% Normalize data
for i=1:size(del_mMusDataLSw,1)
   
   % if Mus activity is very low then set all weights to 0 later
   if (max(mMusDataDistLSw(i,:))-min(mMusDataDistLSw(i,:))<0.1)
       ind_noMus = [ind_noMus; i];
   end
       
   del_mMusDataLSw(i,:) =  del_mMusDataLSw(i,:)./max(del_mMusDataLSw(i,:));
   mMusDataDistLSw(i,:) = mMusDataDistLSw(i,:)./max(mMusDataDistLSw(i,:));
   mMusDataLSw(i,:) = mMusDataLSw(i,:)./max(mMusDataLSw(i,:));
end

for i=1:size(del_mFeatDataLSw,1)
   del_mFeatDataLSw(i,:) =  del_mFeatDataLSw(i,:)./max(del_mFeatDataLSw(i,:));
   mFeatDataLSw(i,:) = mFeatDataLSw(i,:)./max(mFeatDataLSw(i,:));
   mFeatDataDistLSw(i,:) = mFeatDataDistLSw(i,:)./max(mFeatDataDistLSw(i,:));
end

%%%%% [DISTURBED DATA STEADY DATA] and [DISTURBED - STEADY DATA  STEADY DATA] %%%%%%%%

AdelSteadyLSw = [del_mFeatDataLSw mFeatDataLSw]';
AdistSteadyLSw = [mFeatDataDistLSw mFeatDataLSw]';

BdelSteadyLSw = [del_mMusDataLSw mMusDataLSw];
BdistSteadyLSw = [mMusDataDistLSw mMusDataLSw];

% l1-least square
wdistSteadyLSw = NaN(size(mMusDataLSw,1),size(mFeatDataLSw,1));
wdelSteadyLSw = NaN(size(wdistSteadyLSw));

err_tol_Sw = 1e-4*50;

%lambdaSw = 0.1; % l1 cost weight

for i = 1:7
    
    if isempty(find(ind_noMus == i))
        
        BDistLSw = BdistSteadyLSw(i,:)';
        del_BLSw = BdelSteadyLSw(i,:)';

        [lambda_max] = find_lambdamax_l1_ls(AdistSteadyLSw',BDistLSw);
        lambdaSw = lambda_max*err_tol_Sw;

        [lambdaDel_max] = find_lambdamax_l1_ls(AdelSteadyLSw',del_BLSw);
        lambdaDelSw = lambdaDel_max*err_tol_Sw;%*50;

        [xDistLSw,status1] = l1_ls(AdelSteadyLSw,BDistLSw,lambdaSw,[],'quiet');
        [del_xLSw,status2] = l1_ls(AdistSteadyLSw,del_BLSw,lambdaDelSw,[],'quiet');

        if (strcmp(status1,'Solved') && strcmp(status2,'Solved'))
            % more sparsity 
            xDistLSw(find(xDistLSw<0.1)) = 0;   
            del_xLSw(find(del_xLSw<0.1)) = 0;

            wdistSteadyLSw(i,:) = xDistLSw';
            wdelSteadyLSw(i,:) = del_xLSw';

        else
            disp('Error! l1_ls failed to solve!');
        end
        
    else
        wdistSteadyLSw(i,:) = zeros(1,size(mFeatDataLSw,1));
        wdelSteadyLSw(i,:) = zeros(1,size(mFeatDataLSw,1));
    end
    
end

end