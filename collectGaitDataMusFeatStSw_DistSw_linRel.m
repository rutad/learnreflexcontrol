%clc;
%clear all;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;
KinInd = 0;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end);%LTO_ind(2:end-1);
LswingEnd_ind = LgaitEnd_ind;

fd_dist = fd_lthigh;
disturbSwingInd = find(diff(fd_dist.Data(:,1))<0);

LdataLen = length(disturbSwingInd);
distWindInd = NaN(LdataLen,1);

for i=1:LdataLen

    distWind = find(LswingEnd_ind>disturbSwingInd(i));
    distWindInd(i) = distWind(1);
    
end

% resampling length left
resampleDistLSw_p = max(LswingEnd_ind(distWindInd)-LswingStart_ind(distWindInd))+1;


%% Store kinematics and muscle data from gait cycles (Left Disturbed Swing)

% Preallocate memory for left swing muscle feat data

LL_HFLDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LF_GLUDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LL_HAMDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LF_HAMDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LF_VASDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LF_GASDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LF_SOLDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LL_TADataDistLSw = NaN(LdataLen,resampleDistLSw_p);

RL_HFLDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RF_GLUDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RL_HAMDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RF_HAMDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RF_VASDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RF_GASDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RF_SOLDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
RL_TADataDistLSw = NaN(LdataLen,resampleDistLSw_p);

TrunkDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
dTrunkDataDistLSw = NaN(LdataLen,resampleDistLSw_p);

% Preallocate memory for left swing muscle data

LHFLDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LGLUDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LHAMDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LVASDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LGASDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LSOLDataDistLSw = NaN(LdataLen,resampleDistLSw_p);
LTADataDistLSw = NaN(LdataLen,resampleDistLSw_p);


for ind=1:LdataLen
    
    i = distWindInd(ind);
    
    LLHFL_DistLSw = LL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angle
    LLHFL_DistLSwR = resample(LLHFL_DistLSw,resampleDistLSw_p,size(LLHFL_DistLSw,1)); % resample to max gait cycle length 
    
    LFGLU_DistLSw = LF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angular velocity
    LFGLU_DistLSwR = resample(LFGLU_DistLSw,resampleDistLSw_p,size(LFGLU_DistLSw,1));
    
    LLHAM_DistLSw = LL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LLHAM_DistLSwR = resample(LLHAM_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LFHAM_DistLSw = LF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFHAM_DistLSwR = resample(LFHAM_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LFVAS_DistLSw = LF_VAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFVAS_DistLSwR = resample(LFVAS_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LFGAS_DistLSw = LF_GAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFGAS_DistLSwR = resample(LFGAS_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LFSOL_DistLSw = LF_SOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFSOL_DistLSwR = resample(LFSOL_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
   
    LLTA_DistLSw = LL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LLTA_DistLSwR = resample(LLTA_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    RLHFL_DistLSw = RL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angle
    RLHFL_DistLSwR = resample(RLHFL_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1)); % resample to max gait cycle length 
    
    RFGLU_DistLSw = RF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angular velocity
    RFGLU_DistLSwR = resample(RFGLU_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    RLHAM_DistLSw = RL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RLHAM_DistLSwR = resample(RLHAM_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    RFHAM_DistLSw = RF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFHAM_DistLSwR = resample(RFHAM_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    RFVAS_DistLSw = RF_VAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFVAS_DistLSwR = resample(RFVAS_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    RFGAS_DistLSw = RF_GAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFGAS_DistLSwR = resample(RFGAS_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    RFSOL_DistLSw = RF_SOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFSOL_DistLSwR = resample(RFSOL_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
   
    RLTA_DistLSw = RL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RLTA_DistLSwR = resample(RLTA_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
  
    Trunk_DistLSw = Torso.Data(LswingStart_ind(i):LgaitEnd_ind(i),1);
    Trunk_DistLSwR = resample(Trunk_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    dTrunk_DistLSw = Torso.Data(LswingStart_ind(i):LgaitEnd_ind(i),2);
    dTrunk_DistLSwR = resample(dTrunk_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
        
    LHFL_DistLSw = LStimHFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LHFL_DistLSwR = resample(LHFL_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LGLU_DistLSw = LStimGLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LGLU_DistLSwR = resample(LGLU_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LHAM_DistLSw = LStimHAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LHAM_DistLSwR = resample(LHAM_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LVAS_DistLSw = LStimVAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LVAS_DistLSwR = resample(LVAS_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LGAS_DistLSw = LStimGAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LGAS_DistLSwR = resample(LGAS_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LSOL_DistLSw = LStimSOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LSOL_DistLSwR = resample(LSOL_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    LTA_DistLSw = LStimTA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LTA_DistLSwR = resample(LTA_DistLSw,resampleDistLSw_p,size(LLHAM_DistLSw,1));
    
    
    LL_HFLDataDistLSw(ind,:) = LLHFL_DistLSwR;
    LF_GLUDataDistLSw(ind,:) = LFGLU_DistLSwR;
    LL_HAMDataDistLSw(ind,:) = LLHAM_DistLSwR;
    LF_HAMDataDistLSw(ind,:) = LFHAM_DistLSwR;
    LF_VASDataDistLSw(ind,:) = LFVAS_DistLSwR;
    LF_GASDataDistLSw(ind,:) = LFGAS_DistLSwR;
    LF_SOLDataDistLSw(ind,:) = LFSOL_DistLSwR;
    LL_TADataDistLSw(ind,:) = LLTA_DistLSwR;
    
    TrunkDataDistLSw(ind,:) = Trunk_DistLSwR;
    
    RL_HFLDataDistLSw(ind,:) = RLHFL_DistLSwR;
    RF_GLUDataDistLSw(ind,:) = RFGLU_DistLSwR;
    RL_HAMDataDistLSw(ind,:) = RLHAM_DistLSwR;
    RF_HAMDataDistLSw(ind,:) = RFHAM_DistLSwR;
    RF_VASDataDistLSw(ind,:) = RFVAS_DistLSwR;
    RF_GASDataDistLSw(ind,:) = RFGAS_DistLSwR;
    RF_SOLDataDistLSw(ind,:) = RFSOL_DistLSwR;
    RL_TADataDistLSw(ind,:) = RLTA_DistLSwR;
    
    dTrunkDataDistLSw(ind,:) = dTrunk_DistLSwR;
    
    LHFLDataDistLSw(ind,:) = LHFL_DistLSwR;
    LGLUDataDistLSw(ind,:) = LGLU_DistLSwR;
    LHAMDataDistLSw(ind,:) = LHAM_DistLSwR;
    LVASDataDistLSw(ind,:) = LVAS_DistLSwR;
    LGASDataDistLSw(ind,:) = LGAS_DistLSwR;
    LSOLDataDistLSw(ind,:) = LSOL_DistLSwR;
    LTADataDistLSw(ind,:) = LTA_DistLSwR;
    
end

%% Mean data (left disturbed swing)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataDistLSw = mean(LL_HFLDataDistLSw);
mLF_GLUDataDistLSw = mean(LF_GLUDataDistLSw);
mLL_HAMDataDistLSw = mean(LL_HAMDataDistLSw);
mLF_HAMDataDistLSw = mean(LF_HAMDataDistLSw);
mLF_VASDataDistLSw = mean(LF_VASDataDistLSw);
mLF_GASDataDistLSw = mean(LF_GASDataDistLSw);
mLF_SOLDataDistLSw = mean(LF_SOLDataDistLSw);
mLL_TADataDistLSw = mean(LL_TADataDistLSw);

mTrunkDataDistLSw = mean(TrunkDataDistLSw);

mRL_HFLDataDistLSw = mean(RL_HFLDataDistLSw);
mRF_GLUDataDistLSw = mean(RF_GLUDataDistLSw);
mRL_HAMDataDistLSw = mean(RL_HAMDataDistLSw);
mRF_HAMDataDistLSw = mean(RF_HAMDataDistLSw);
mRF_VASDataDistLSw = mean(RF_VASDataDistLSw);
mRF_GASDataDistLSw = mean(RF_GASDataDistLSw);
mRF_SOLDataDistLSw = mean(RF_SOLDataDistLSw);
mRL_TADataDistLSw = mean(RL_TADataDistLSw);

mdTrunkDataDistLSw = mean(dTrunkDataDistLSw);


% collect mean muscle features over gaits (only training)
mFeatDataDistLSw = [mLL_HFLDataDistLSw; mLF_GLUDataDistLSw; mLL_HAMDataDistLSw; mLF_HAMDataDistLSw; mLF_VASDataDistLSw; mLF_GASDataDistLSw; mLF_SOLDataDistLSw; mLL_TADataDistLSw; mTrunkDataDistLSw; ...
            mRL_HFLDataDistLSw; mRF_GLUDataDistLSw; mRL_HAMDataDistLSw; mRF_HAMDataDistLSw; mRF_VASDataDistLSw; mRF_GASDataDistLSw; mRF_SOLDataDistLSw; mRL_TADataDistLSw; mdTrunkDataDistLSw];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataDistLSw = mean(LHFLDataDistLSw);
mLGLUDataDistLSw = mean(LGLUDataDistLSw);
mLHAMDataDistLSw = mean(LHAMDataDistLSw);
mLVASDataDistLSw = mean(LVASDataDistLSw);
mLGASDataDistLSw = mean(LGASDataDistLSw);
mLSOLDataDistLSw = mean(LSOLDataDistLSw);
mLTADataDistLSw = mean(LTADataDistLSw); 

% collect mean muscles over gaits (only training)
mMusDataDistLSw = [mLHFLDataDistLSw; mLGLUDataDistLSw; mLHAMDataDistLSw; mLVASDataDistLSw; mLGASDataDistLSw; mLSOLDataDistLSw; mLTADataDistLSw];

