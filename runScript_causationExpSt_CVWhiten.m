%% run causation experiment script

clc;clear all;close all;

all_fDisturb = [5,10,15];

for do = 1:length(all_fDisturb)
    fDisturb = all_fDisturb(do);
    saveName = strcat('causationExp_meanDisturbWaitLSt_musFeatNormTrk_CVWhiten_F',num2str(fDisturb),'.mat');
    causationDisturbExp_musFeatSt_CVWhiten;
    
    save (saveName, 'WStore','musDistDataStore','featDistDataStore', 'trunkNormStore','mean_wDistLSt', ...
        'mean_delWLSt', 'tDisturb','fDisturb','meanTrunkNormMax','tfeatDistDataStore','tmusDistDataStore');
    
    
end