%clc;
%clear all;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.

TD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
TD_ind2 = TD_ind(2:end);

gaitStart_ind = TD_ind2(1:end-1);
gaitEnd_ind = TD_ind2(2:end);

% resampling length
resample_p = max(gaitEnd_ind-gaitStart_ind)+1;
dataLen = length(gaitStart_ind);

%% Store kinematics and muscle data from gait cycles

% Preallocate memory for kinematic data

% KinData = NaN(resample_p*dataLen,numKinFeatures);
% figure;hold on
KinInd = 0;

LL_HFLData = NaN(dataLen,resample_p);
LF_GLUData = NaN(dataLen,resample_p);
LL_HAMData = NaN(dataLen,resample_p);
LF_HAMData = NaN(dataLen,resample_p);
LF_VASData = NaN(dataLen,resample_p);
LF_GASData = NaN(dataLen,resample_p);
LF_SOLData = NaN(dataLen,resample_p);
LL_TAData = NaN(dataLen,resample_p);

RL_HFLData = NaN(dataLen,resample_p);
RF_GLUData = NaN(dataLen,resample_p);
RL_HAMData = NaN(dataLen,resample_p);
RF_HAMData = NaN(dataLen,resample_p);
RF_VASData = NaN(dataLen,resample_p);
RF_GASData = NaN(dataLen,resample_p);
RF_SOLData = NaN(dataLen,resample_p);
RL_TAData = NaN(dataLen,resample_p);

TrunkData = NaN(dataLen,resample_p);
dTrunkData = NaN(dataLen,resample_p);

% Preallocate memory for muscle data

LHFLData = NaN(dataLen,resample_p);
LGLUData = NaN(dataLen,resample_p);
LHAMData = NaN(dataLen,resample_p);
LVASData = NaN(dataLen,resample_p);
LGASData = NaN(dataLen,resample_p);
LSOLData = NaN(dataLen,resample_p);
LTAData = NaN(dataLen,resample_p);

RHFLData = NaN(dataLen,resample_p);
RGLUData = NaN(dataLen,resample_p);
RHAMData = NaN(dataLen,resample_p);
RVASData = NaN(dataLen,resample_p);
RGASData = NaN(dataLen,resample_p);
RSOLData = NaN(dataLen,resample_p);
RTAData = NaN(dataLen,resample_p);

for i=1:dataLen
    
    LLHFL = LL_HFL.Data(gaitStart_ind(i):gaitEnd_ind(i),:); % angle
    LLHFLR = resample(LLHFL,resample_p,size(LLHFL,1)); % resample to max gait cycle length 
    
    LFGLU = LF_GLU.Data(gaitStart_ind(i):gaitEnd_ind(i),:); % angular velocity
    LFGLUR = resample(LFGLU,resample_p,size(LFGLU,1));
    
    LLHAM = LL_HAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LLHAMR = resample(LLHAM,resample_p,size(LLHAM,1));
    
    LFHAM = LF_HAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LFHAMR = resample(LFHAM,resample_p,size(LFHAM,1));
    
    LFVAS = LF_VAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LFVASR = resample(LFVAS,resample_p,size(LFVAS,1));
    
    LFGAS = LF_GAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LFGASR = resample(LFGAS,resample_p,size(LFGAS,1));
    
    LFSOL = LF_SOL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LFSOLR = resample(LFSOL,resample_p,size(LFSOL,1));
   
    LLTA = LL_TA.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LLTAR = resample(LLTA,resample_p,size(LLTA,1));
    
    RLHFL = RL_HFL.Data(gaitStart_ind(i):gaitEnd_ind(i),:); % angle
    RLHFLR = resample(RLHFL,resample_p,size(LLHFL,1)); % resample to max gait cycle length 
    
    RFGLU = RF_GLU.Data(gaitStart_ind(i):gaitEnd_ind(i),:); % angular velocity
    RFGLUR = resample(RFGLU,resample_p,size(LFGLU,1));
    
    RLHAM = RL_HAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RLHAMR = resample(RLHAM,resample_p,size(LLHAM,1));
    
    RFHAM = RF_HAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RFHAMR = resample(RFHAM,resample_p,size(LFHAM,1));
    
    RFVAS = RF_VAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RFVASR = resample(RFVAS,resample_p,size(LFVAS,1));
    
    RFGAS = RF_GAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RFGASR = resample(RFGAS,resample_p,size(LFGAS,1));
    
    RFSOL = RF_SOL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RFSOLR = resample(RFSOL,resample_p,size(LFSOL,1));
   
    RLTA = RL_TA.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RLTAR = resample(RLTA,resample_p,size(LLTA,1));
    
  
    Trunk = Torso.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    TrunkR = resample(Trunk,resample_p,size(Trunk,1));
    dTrunk = Torso.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dTrunkR = resample(dTrunk,resample_p,size(Trunk,1));
    
    %KinData(KinInd*resample_p+1:KinInd*resample_p+resample_p,:) = [LAnkR LKneeR LHipR RAnkR RKneeR RHipR TrunkR];
    
    LHFL = LStimHFL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LHFLR = resample(LHFL,resample_p,size(LHFL,1));
    
    LGLU = LStimGLU.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LGLUR = resample(LGLU,resample_p,size(LHFL,1));
    
    LHAM = LStimHAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LHAMR = resample(LHAM,resample_p,size(LHFL,1));
    
    LVAS = LStimVAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LVASR = resample(LVAS,resample_p,size(LHFL,1));
    
    LGAS = LStimGAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LGASR = resample(LGAS,resample_p,size(LHFL,1));
    
    LSOL = LStimSOL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LSOLR = resample(LSOL,resample_p,size(LHFL,1));
    
    LTA = LStimTA.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LTAR = resample(LTA,resample_p,size(LHFL,1));
    
    RHFL = RStimHFL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RHFLR = resample(RHFL,resample_p,size(LHFL,1));
    
    RGLU = RStimGLU.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RGLUR = resample(RGLU,resample_p,size(LHFL,1));
    
    RHAM = RStimHAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RHAMR = resample(RHAM,resample_p,size(LHFL,1));
    
    RVAS = RStimVAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RVASR = resample(RVAS,resample_p,size(LHFL,1));
    
    RGAS = RStimGAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RGASR = resample(RGAS,resample_p,size(LHFL,1));
    
    RSOL = RStimSOL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RSOLR = resample(RSOL,resample_p,size(LHFL,1));
    
    RTA = RStimTA.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RTAR = resample(RTA,resample_p,size(LHFL,1));
    
    LL_HFLData(i,:) = LLHFLR;
    LF_GLUData(i,:) = LFGLUR;
    LL_HAMData(i,:) = LLHAMR;
    LF_HAMData(i,:) = LFHAMR;
    LF_VASData(i,:) = LFVASR;
    LF_GASData(i,:) = LFGASR;
    LF_SOLData(i,:) = LFSOLR;
    LL_TAData(i,:) = LLTAR;
    
    TrunkData(i,:) = TrunkR;
    
    RL_HFLData(i,:) = RLHFLR;
    RF_GLUData(i,:) = RFGLUR;
    RL_HAMData(i,:) = RLHAMR;
    RF_HAMData(i,:) = RFHAMR;
    RF_VASData(i,:) = RFVASR;
    RF_GASData(i,:) = RFGASR;
    RF_SOLData(i,:) = RFSOLR;
    RL_TAData(i,:) = RLTAR;
    
    dTrunkData(i,:) = dTrunkR;
    
    LHFLData(i,:) = LHFLR;
    LGLUData(i,:) = LGLUR;
    LHAMData(i,:) = LHAMR;
    LVASData(i,:) = LVASR;
    LGASData(i,:) = LGASR;
    LSOLData(i,:) = LSOLR;
    LTAData(i,:) = LTAR;
    
    RHFLData(i,:) = RHFLR;
    RGLUData(i,:) = RGLUR;
    RHAMData(i,:) = RHAMR;
    RVASData(i,:) = RVASR;
    RGASData(i,:) = RGASR;
    RSOLData(i,:) = RSOLR;
    RTAData(i,:) = RTAR;
    
    KinInd = KinInd + 1; 
    
    %plot(LAnkR(:,1));
end


%% Hold out test data

testNum = 5; % number of test samples
testDataInd = randi(dataLen,testNum,1);

% test data
tLL_HFLData = LL_HFLData(testDataInd,:);
tLF_GLUData = LF_GLUData(testDataInd,:);
tLL_HAMData = LL_HAMData(testDataInd,:);
tLF_HAMData = LF_HAMData(testDataInd,:);
tLF_VASData = LF_VASData(testDataInd,:);
tLF_GASData = LF_GASData(testDataInd,:);
tLF_SOLData = LF_SOLData(testDataInd,:);
tLL_TAData = LL_TAData(testDataInd,:);

tTrunkData = TrunkData(testDataInd,:);

tRL_HFLData = RL_HFLData(testDataInd,:);
tRF_GLUData = RF_GLUData(testDataInd,:);
tRL_HAMData = RL_HAMData(testDataInd,:);
tRF_HAMData = RF_HAMData(testDataInd,:);
tRF_VASData = RF_VASData(testDataInd,:);
tRF_GASData = RF_GASData(testDataInd,:);
tRF_SOLData = RF_SOLData(testDataInd,:);
tRL_TAData = RL_TAData(testDataInd,:);

tdTrunkData = dTrunkData(testDataInd,:);

tLHFLData = LHFLData(testDataInd,:);
tLGLUData = LGLUData(testDataInd,:);
tLHAMData = LHAMData(testDataInd,:);
tLVASData = LVASData(testDataInd,:);
tLGASData = LGASData(testDataInd,:);
tLSOLData = LSOLData(testDataInd,:);
tLTAData = LTAData(testDataInd,:);

tRHFLData = RHFLData(testDataInd,:);
tRGLUData = RGLUData(testDataInd,:);
tRHAMData = RHAMData(testDataInd,:);
tRVASData = RVASData(testDataInd,:);
tRGASData = RGASData(testDataInd,:);
tRSOLData = RSOLData(testDataInd,:);
tRTAData = RTAData(testDataInd,:);

% Remove test data from all data

LL_HFLData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LF_GLUData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LL_HAMData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LF_HAMData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LF_VASData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LF_GASData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LF_SOLData(testDataInd,:)= zeros(length(testDataInd),resample_p);
LL_TAData(testDataInd,:)= zeros(length(testDataInd),resample_p);

TrunkData(testDataInd,:) = zeros(length(testDataInd),resample_p);

RL_HFLData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RF_GLUData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RL_HAMData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RF_HAMData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RF_VASData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RF_GASData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RF_SOLData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RL_TAData(testDataInd,:)= zeros(length(testDataInd),resample_p);

dTrunkData(testDataInd,:) = zeros(length(testDataInd),resample_p);

LHFLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LGLUData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LHAMData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LVASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LGASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LSOLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LTAData(testDataInd,:) = zeros(length(testDataInd),resample_p);

RHFLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RGLUData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RHAMData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RVASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RGASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RSOLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RTAData(testDataInd,:) = zeros(length(testDataInd),resample_p);

%% Mean data

% mean kinematics over all gait cycles (only training)
mLL_HFLData = mean(LL_HFLData);
mLF_GLUData = mean(LF_GLUData);
mLL_HAMData = mean(LL_HAMData);
mLF_HAMData = mean(LF_HAMData);
mLF_VASData = mean(LF_VASData);
mLF_GASData = mean(LF_GASData);
mLF_SOLData = mean(LF_SOLData);
mLL_TAData = mean(LL_TAData);

mTrunkData = mean(TrunkData);

mRL_HFLData = mean(RL_HFLData);
mRF_GLUData = mean(RF_GLUData);
mRL_HAMData = mean(RL_HAMData);
mRF_HAMData = mean(RF_HAMData);
mRF_VASData = mean(RF_VASData);
mRF_GASData = mean(RF_GASData);
mRF_SOLData = mean(RF_SOLData);
mRL_TAData = mean(RL_TAData);

mdTrunkData = mean(dTrunkData);


% collect mean muscle features over gaits (only training)
mFeatData = [mLL_HFLData; mLF_GLUData; mLL_HAMData; mLF_HAMData; mLF_VASData; mLF_GASData; mLF_SOLData; mLL_TAData; mTrunkData; ...
            mRL_HFLData; mRF_GLUData; mRL_HAMData; mRF_HAMData; mRF_VASData; mRF_GASData; mRF_SOLData; mRL_TAData; mdTrunkData];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLData = mean(LHFLData);
mLGLUData = mean(LGLUData);
mLHAMData = mean(LHAMData);
mLVASData = mean(LVASData);
mLGASData = mean(LGASData);
mLSOLData = mean(LSOLData);
mLTAData = mean(LTAData); 
mRHFLData = mean(RHFLData);
mRGLUData = mean(RGLUData);
mRHAMData = mean(RHAMData);
mRVASData = mean(RVASData);
mRGASData = mean(RGASData);
mRSOLData = mean(RSOLData);
mRTAData = mean(RTAData); 

% collect mean muscles over gaits (only training)
mMusData = [mLHFLData; mLGLUData; mLHAMData; mLVASData; mLGASData; mLSOLData; mLTAData; ...
            mRHFLData; mRGLUData; mRHAMData; mRVASData; mRGASData; mRSOLData; mRTAData];