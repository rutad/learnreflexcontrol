%% run causation experiment script

clc;clear all;close all;

%all_fDisturb = [25,50,150];
all_fDisturb = [5,10,15];

for do = 1:length(all_fDisturb)
    fDisturb = all_fDisturb(do);
    %saveName = strcat('causExpDistWaitSw_musFeatNormTrk_distSteadyData_F',num2str(fDisturb),'.mat');
    saveName = strcat('causExpDistWaitSw_musFeatNormTrk_distSteadyNORMData_F',num2str(fDisturb),'.mat');
    causationDisturbExp_musFeatSw_distSteadyData;
    
    save (saveName, 'WStore','musDistDataStore','featDistDataStore', 'trunkNormStore','mean_wDelSteadyLSw', ...
        'mean_wDistSteadyLSw', 'tDisturb','fDisturb','meanTrunkNormMax');
    
    
end