clc
clear all;
close all;

load linRel_13ms_NMwalkingRoughGrd_100sSimData_musFeatStSw.mat;
numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;

K = 5; % K-fold validation size
testNum = 4; % number of test samples

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end);%LTO_ind(2:end-1);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end-1);%RTO_ind(3:end);
RswingEnd_ind = RgaitEnd_ind;

%% K-fold intervals

KDatalen = length(LgaitStart_ind) - testNum;
Klen = KDatalen/K;
Kfold_int = NaN(K,Klen);

intStart = 1;
for i=1:K   
    intEnd = intStart +Klen-1;
    Kfold_int(i,:) = linspace(intStart, intEnd,Klen);
    intStart = intEnd+1;
end


%% Test data 

testDataInd = [(KDatalen+1):length(LgaitStart_ind)];

%% Cross validated l1-ls
lambda_min = 1e-4; % l1_ls suggested min lambda

% CV error
errLSt = cell(numMusFeatures/2,K);
errLSw = cell(numMusFeatures/2,K);

% CV params
lambdaLSt = cell(numMusFeatures/2,K);
lambdaLSw = cell(numMusFeatures/2,K);

for zz = 1:K
    holdDataInd = Kfold_int(zz,:);
    trainDataInd = Kfold_int;
    trainDataInd(zz,:)=[];
    trainDataInd = trainDataInd(:);
    
    LStim = [LStimHFL; LStimGLU; LStimHAM; LStimVAS; LStimGAS; LStimSOL; LStimTA];
    RStim = [RStimHFL; RStimGLU; RStimHAM; RStimVAS; RStimGAS; RStimSOL; RStimTA];   
    LFeat = [LL_HFL; LF_GLU; LL_HAM; LF_HAM; LF_VAS; LF_GAS; LF_SOL;LL_TA];
    RFeat = [RL_HFL; RF_GLU; RL_HAM; RF_HAM; RF_VAS; RF_GAS; RF_SOL;RL_TA];
    
    [FeatDataLSt, FeatDataRSt, FeatDataLSw, FeatDataRSw, MusDataLSt,MusDataRSt, MusDataLSw, MusDataRSw,~,~] = ...
    collectGaitDataMusFeat_CVRoughGnd_Train(trainDataInd,holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind);

    [holdFeatDataLSt, holdFeatDataRSt, holdFeatDataLSw, holdFeatDataRSw, holdMusDataLSt,holdMusDataRSt, holdMusDataLSw, holdMusDataRSw] = ...
    collectGaitDataMusFeat_CVRoughGnd_Hold(holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind);

    %%% LEFT %%%
    ALSt = FeatDataLSt;
    ALSw = FeatDataLSw;
    
    % zero mean
    ALSt = bsxfun(@minus, ALSt, mean(ALSt));
    ALSw = bsxfun(@minus, ALSw, mean(ALSw));
    
    % unit variance
    ALSt = ALSt*diag(1./std(ALSt));
    ALSw = ALSw*diag(1./std(ALSw));
    
    for i = 1:numMusFeatures/2
        BLSt = MusDataLSt(:,i);
        
        BLSt = bsxfun(@minus, BLSt, mean(BLSt));
        BLSt = BLSt./std(BLSt);

        [lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);  % l1_ls suggested max lambda
        lambdaLSt{i,zz} = 2.^[0:log2(lambda_max/lambda_min)].*lambda_min;
        errLSt{i,zz} = NaN(length(lambdaLSt{i,zz}),1);

        % CV left stance
        for l = 1:length(lambdaLSt{i,zz})
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaLSt{i,zz}(l),[],'quiet'); 
            errLSt{i,zz}(l) = mean((holdMusDataLSt(:,i)-holdFeatDataLSt*xLSt).^2);
        end   

        BLSw = MusDataLSw(:,i);
        
        BLSw = bsxfun(@minus, BLSw, mean(BLSw));
        BLSw = BLSw./std(BLSw);

        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);  % l1_ls suggested max lambda
        lambdaLSw{i,zz} = 2.^[0:log2(lambda_max/lambda_min)].*lambda_min;
        errLSw{i,zz} = NaN(length(lambdaLSw{i,zz}),1);
        
        % CV left swing
        for l = 1:length(lambdaLSw{i,zz})
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaLSw{i,zz}(l),[],'quiet');        
            errLSw{i,zz}(l) = mean((holdMusDataLSw(:,i)-holdFeatDataLSw*xLSw).^2);
        end  

    end

end    

%% visualize CV error and calculate lambda corresponding to min cv error.

for mm =  1:numMusFeatures/2
  
    str1 = strcat('LSt CV error Mus',num2str(mm)); 
    str2 = strcat('LSw CV error Mus',num2str(mm));
    
    figure(mm); hold on;title(str1); xlabel('lambda index'); ylabel('CV error');
    figure(mm+numMusFeatures/2); hold on;title(str2); xlabel('lambda index'); ylabel('CV error');
    
    %err1 = NaN(size(errLSt{mm,1},1),K);
    %err2 = NaN(size(errLSw{mm,1},1),K);
    
    err1 = cell(K,1);
    err2 = cell(K,1);
    
    for kk = 1:K
        %err1(:,kk) = errLSt{mm,kk};
        %err2(:,kk) = errLSw{mm,kk};
        
        err1{kk} =errLSt{mm,kk};
        err2{kk} = errLSw{mm,kk};
    end

    %err1 = sum(err1,2)./K;
    %err2 = sum(err2,2)./K;
    
    err1 = sum(cell2mat(err1'),2)./K;
    err2 = sum(cell2mat(err2'),2)./K;
    
    %[minError, minInd] = min(err1);
    figure(mm);plot(err1,'*');
    figure(mm+numMusFeatures/2);plot(err2,'*');

end

