% Relate kinematics data to muscle stimulation

function [wDistLSt, delw_LSt] =  wSparseDistSt (distData, steadyData)

mFeatDataDistLSt = distData{1};
mMusDataDistLSt = distData{2};

mFeatDataLSt = steadyData{1};
mMusDataLSt = steadyData{2};

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSt_r=resample(mFeatDataLSt',size(mFeatDataDistLSt,2),size(mFeatDataLSt,2))';
mMusDataLSt_r=resample(mMusDataLSt',size(mMusDataDistLSt,2),size(mMusDataLSt,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSt = mMusDataDistLSt - mMusDataLSt_r;
del_mFeatDataLSt = mFeatDataDistLSt - mFeatDataLSt_r;

%%%%% DISTURBED DATA and DISTURBED - STEADY DATA %%%%%%%%

% l1-least square
wDistLSt = NaN(size(mMusDataLSt,1),size(mFeatDataLSt,1));
delw_LSt = NaN(size(wDistLSt));

ADistLSt = mFeatDataDistLSt';
del_ALSt = del_mFeatDataLSt';

err_tol_St = 1e-3;

%lambdaSw = 0.1; % l1 cost weight

for i = 1:7
        
    BDistLSt = mMusDataDistLSt(i,:)';
    del_BLSt = del_mMusDataLSt(i,:)';
    
    [lambda_max] = find_lambdamax_l1_ls(ADistLSt',BDistLSt);
    lambdaSt = lambda_max*err_tol_St;
    
    [lambdaDel_max] = find_lambdamax_l1_ls(del_ALSt',del_BLSt);
    lambdaDelSt = lambdaDel_max*err_tol_St*50;
    
    [xDistLSt,~] = l1_ls(ADistLSt,BDistLSt,lambdaSt,[],'quiet');
    [del_xLSt,~] = l1_ls(del_ALSt,del_BLSt,lambdaDelSt,[],'quiet');
    
    wDistLSt(i,:) = xDistLSt';
    delw_LSt(i,:) = del_xLSt';
    
end

end