%clc;
%clear all;

numKinFeatures = 14;
numMusFeatures = 14;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.

TD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
TD_ind2 = TD_ind(2:end);

gaitStart_ind = TD_ind2(1:end-1);
gaitEnd_ind = TD_ind2(2:end);

% resampling length
resample_p = max(gaitEnd_ind-gaitStart_ind)+1;
dataLen = length(gaitStart_ind);

%% Store kinematics and muscle data from gait cycles

% Preallocate memory for kinematic data

% KinData = NaN(resample_p*dataLen,numKinFeatures);
% figure;hold on
KinInd = 0;

LAnkData = NaN(dataLen,resample_p);
LKneeData = NaN(dataLen,resample_p);
LHipData = NaN(dataLen,resample_p);
RAnkData = NaN(dataLen,resample_p);
RKneeData = NaN(dataLen,resample_p);
RHipData = NaN(dataLen,resample_p);
TrunkData = NaN(dataLen,resample_p);

dLAnkData = NaN(dataLen,resample_p);
dLKneeData = NaN(dataLen,resample_p);
dLHipData = NaN(dataLen,resample_p);
dRAnkData = NaN(dataLen,resample_p);
dRKneeData = NaN(dataLen,resample_p);
dRHipData = NaN(dataLen,resample_p);
dTrunkData = NaN(dataLen,resample_p);

% Preallocate memory for muscle data

LHFLData = NaN(dataLen,resample_p);
LGLUData = NaN(dataLen,resample_p);
LHAMData = NaN(dataLen,resample_p);
LVASData = NaN(dataLen,resample_p);
LGASData = NaN(dataLen,resample_p);
LSOLData = NaN(dataLen,resample_p);
LTAData = NaN(dataLen,resample_p);

RHFLData = NaN(dataLen,resample_p);
RGLUData = NaN(dataLen,resample_p);
RHAMData = NaN(dataLen,resample_p);
RVASData = NaN(dataLen,resample_p);
RGASData = NaN(dataLen,resample_p);
RSOLData = NaN(dataLen,resample_p);
RTAData = NaN(dataLen,resample_p);

for i=1:dataLen
    
    LAnk = Lphi12dphi12.Data(gaitStart_ind(i):gaitEnd_ind(i),1); % angle
    LAnkR = resample(LAnk,resample_p,size(LAnk,1)); % resample to max gait cycle length 
    dLAnk = Lphi12dphi12.Data(gaitStart_ind(i):gaitEnd_ind(i),2); % angular velocity
    dLAnkR = resample(dLAnk,resample_p,size(LAnk,1));
    
    LKnee = Lphi23dphi23.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    LKneeR = resample(LKnee,resample_p,size(LKnee,1));
    dLKnee = Lphi23dphi23.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dLKneeR = resample(dLKnee,resample_p,size(LKnee,1));
    
    LHip = Lphi34dphi34.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    LHipR = resample(LHip,resample_p,size(LHip,1));
    dLHip = Lphi34dphi34.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dLHipR = resample(dLHip,resample_p,size(LHip,1));
    
    RAnk = Rphi12dphi12.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    RAnkR = resample(RAnk,resample_p,size(RAnk,1));
    dRAnk = Rphi12dphi12.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dRAnkR = resample(RAnk,resample_p,size(RAnk,1));
    
    RKnee = Rphi23dphi23.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    RKneeR = resample(RKnee,resample_p,size(RKnee,1));
    dRKnee = Rphi23dphi23.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dRKneeR = resample(dRKnee,resample_p,size(RKnee,1));
    
    RHip = Rphi34dphi34.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    RHipR = resample(RHip,resample_p,size(RHip,1));
    dRHip = Rphi34dphi34.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dRHipR = resample(dRHip,resample_p,size(RHip,1));
    
    Trunk = Torso.Data(gaitStart_ind(i):gaitEnd_ind(i),1);
    TrunkR = resample(Trunk,resample_p,size(Trunk,1));
    dTrunk = Torso.Data(gaitStart_ind(i):gaitEnd_ind(i),2);
    dTrunkR = resample(dTrunk,resample_p,size(Trunk,1));
    
    %KinData(KinInd*resample_p+1:KinInd*resample_p+resample_p,:) = [LAnkR LKneeR LHipR RAnkR RKneeR RHipR TrunkR];
    
    LHFL = LStimHFL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LHFLR = resample(LHFL,resample_p,size(LHFL,1));
    
    LGLU = LStimGLU.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LGLUR = resample(LGLU,resample_p,size(LHFL,1));
    
    LHAM = LStimHAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LHAMR = resample(LHAM,resample_p,size(LHFL,1));
    
    LVAS = LStimVAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LVASR = resample(LVAS,resample_p,size(LHFL,1));
    
    LGAS = LStimGAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LGASR = resample(LGAS,resample_p,size(LHFL,1));
    
    LSOL = LStimSOL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LSOLR = resample(LSOL,resample_p,size(LHFL,1));
    
    LTA = LStimTA.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    LTAR = resample(LTA,resample_p,size(LHFL,1));
    
    RHFL = RStimHFL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RHFLR = resample(RHFL,resample_p,size(LHFL,1));
    
    RGLU = RStimGLU.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RGLUR = resample(RGLU,resample_p,size(LHFL,1));
    
    RHAM = RStimHAM.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RHAMR = resample(RHAM,resample_p,size(LHFL,1));
    
    RVAS = RStimVAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RVASR = resample(RVAS,resample_p,size(LHFL,1));
    
    RGAS = RStimGAS.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RGASR = resample(RGAS,resample_p,size(LHFL,1));
    
    RSOL = RStimSOL.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RSOLR = resample(RSOL,resample_p,size(LHFL,1));
    
    RTA = RStimTA.Data(gaitStart_ind(i):gaitEnd_ind(i),:);
    RTAR = resample(RTA,resample_p,size(LHFL,1));
    
    LAnkData(i,:) = LAnkR;
    LKneeData(i,:) = LKneeR;
    LHipData(i,:) = LHipR;
    RAnkData(i,:) = RAnkR;
    RKneeData(i,:) = RKneeR;
    RHipData(i,:) = RHipR;
    TrunkData(i,:) = TrunkR;
    
    dLAnkData(i,:) = dLAnkR;
    dLKneeData(i,:) = dLKneeR;
    dLHipData(i,:) = dLHipR;
    dRAnkData(i,:) = dRAnkR;
    dRKneeData(i,:) = dRKneeR;
    dRHipData(i,:) = dRHipR;
    dTrunkData(i,:) = dTrunkR;
    
    LHFLData(i,:) = LHFLR;
    LGLUData(i,:) = LGLUR;
    LHAMData(i,:) = LHAMR;
    LVASData(i,:) = LVASR;
    LGASData(i,:) = LGASR;
    LSOLData(i,:) = LSOLR;
    LTAData(i,:) = LTAR;
    
    RHFLData(i,:) = RHFLR;
    RGLUData(i,:) = RGLUR;
    RHAMData(i,:) = RHAMR;
    RVASData(i,:) = RVASR;
    RGASData(i,:) = RGASR;
    RSOLData(i,:) = RSOLR;
    RTAData(i,:) = RTAR;
    
    KinInd = KinInd + 1; 
    
    %plot(LAnkR(:,1));
end


%% Hold out test data

testNum = 5; % number of test samples
testDataInd = randi(dataLen,testNum,1);

% test data
tLAnkData = LAnkData(testDataInd,:);
tLKneeData = LKneeData(testDataInd,:);
tLHipData = LHipData(testDataInd,:);
tRAnkData = RAnkData(testDataInd,:);
tRKneeData = RKneeData(testDataInd,:);
tRHipData = RHipData(testDataInd,:);
tTrunkData = TrunkData(testDataInd,:);

tdLAnkData = dLAnkData(testDataInd,:);
tdLKneeData = dLKneeData(testDataInd,:);
tdLHipData = dLHipData(testDataInd,:);
tdRAnkData = dRAnkData(testDataInd,:);
tdRKneeData = dRKneeData(testDataInd,:);
tdRHipData = dRHipData(testDataInd,:);
tdTrunkData = dTrunkData(testDataInd,:);

tLHFLData = LHFLData(testDataInd,:);
tLGLUData = LGLUData(testDataInd,:);
tLHAMData = LHAMData(testDataInd,:);
tLVASData = LVASData(testDataInd,:);
tLGASData = LGASData(testDataInd,:);
tLSOLData = LSOLData(testDataInd,:);
tLTAData = LTAData(testDataInd,:);

tRHFLData = RHFLData(testDataInd,:);
tRGLUData = RGLUData(testDataInd,:);
tRHAMData = RHAMData(testDataInd,:);
tRVASData = RVASData(testDataInd,:);
tRGASData = RGASData(testDataInd,:);
tRSOLData = RSOLData(testDataInd,:);
tRTAData = RTAData(testDataInd,:);

% Remove test data from all data

LAnkData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LKneeData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LHipData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RAnkData(testDataInd,:)= zeros(length(testDataInd),resample_p);
RKneeData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RHipData(testDataInd,:) = zeros(length(testDataInd),resample_p);
TrunkData(testDataInd,:) = zeros(length(testDataInd),resample_p);

dLAnkData(testDataInd,:) = zeros(length(testDataInd),resample_p);
dLKneeData(testDataInd,:) = zeros(length(testDataInd),resample_p);
dLHipData(testDataInd,:) = zeros(length(testDataInd),resample_p);
dRAnkData(testDataInd,:) = zeros(length(testDataInd),resample_p);
dRKneeData(testDataInd,:) = zeros(length(testDataInd),resample_p);
dRHipData(testDataInd,:) = zeros(length(testDataInd),resample_p);
dTrunkData(testDataInd,:) = zeros(length(testDataInd),resample_p);

LHFLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LGLUData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LHAMData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LVASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LGASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LSOLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
LTAData(testDataInd,:) = zeros(length(testDataInd),resample_p);

RHFLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RGLUData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RHAMData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RVASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RGASData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RSOLData(testDataInd,:) = zeros(length(testDataInd),resample_p);
RTAData(testDataInd,:) = zeros(length(testDataInd),resample_p);

%% Mean data

% mean kinematics over all gait cycles (only training)
mLAnkData = mean(LAnkData);
mLKneeData = mean(LKneeData);
mLHipData = mean(LHipData);
mRAnkData = mean(RAnkData);
mRKneeData = mean(RKneeData);
mRHipData = mean(RHipData);
mTrunkData = mean(TrunkData);

mdLAnkData = mean(dLAnkData);
mdLKneeData = mean(dLKneeData);
mdLHipData = mean(dLHipData);
mdRAnkData = mean(dRAnkData);
mdRKneeData = mean(dRKneeData);
mdRHipData = mean(dRHipData);
mdTrunkData = mean(dTrunkData);


% collect mean kinematics over gaits (only training)
mKinData = [mLAnkData; mLKneeData; mLHipData; mRAnkData; mRKneeData; mRHipData; mTrunkData; ...
            mdLAnkData; mdLKneeData; mdLHipData; mdRAnkData; mdRKneeData; mdRHipData; mdTrunkData];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLData = mean(LHFLData);
mLGLUData = mean(LGLUData);
mLHAMData = mean(LHAMData);
mLVASData = mean(LVASData);
mLGASData = mean(LGASData);
mLSOLData = mean(LSOLData);
mLTAData = mean(LTAData); 
mRHFLData = mean(RHFLData);
mRGLUData = mean(RGLUData);
mRHAMData = mean(RHAMData);
mRVASData = mean(RVASData);
mRGASData = mean(RGASData);
mRSOLData = mean(RSOLData);
mRTAData = mean(RTAData); 

% collect mean muscles over gaits (only training)
mMusData = [mLHFLData; mLGLUData; mLHAMData; mLVASData; mLGASData; mLSOLData; mLTAData; ...
            mRHFLData; mRGLUData; mRHAMData; mRVASData; mRGASData; mRSOLData; mRTAData];