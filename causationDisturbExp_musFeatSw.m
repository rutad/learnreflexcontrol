%% Disturbance experiments for establishing causation
% Ruta Desai
% rutad@cs.cmu.edu
% 24th April 2015

%% Set up
clc; 
%close all; 
%clear all;

numFeat = 18; %(16 Mus+2 trunk Kin features)
numMus = 7;
count = 0; % count of disturbance scenarios

nms_model_MechInit;
nms_model_ControlInit;
modelName ='nms_saveMusFeatDataALLDisturbWaitSw_PCALinRel';

load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mMusDataLSw');
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mFeatDataLSw');

%% Disturbance times and forces in swing

% 10%,50% and 90% of swing -- early, mid and late swing disturbances
tDisturb = [0.1*350*1e-3;0.5*350*1e-3;0.9*350*1e-3];
%fDisturb = 25; %15; %[N]

motionDir = [-1,1]; %1 -> flex, -1 -> extend
numDisturbJnts = 4;

WStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
mean_wDistLSw = zeros(numMus, numFeat);
mean_delWLSw = zeros(size(mean_wDistLSw));

%% Disturb and collect mean stats over disturbed swings

for k = 1:length(motionDir)

    flex_extend = motionDir(k);
    
    for i =1:length(tDisturb)
       tDisturbSw = tDisturb(i);

       for jntInd = 1:numDisturbJnts

           fd = {0, 0, 0, 0};
           fd(jntInd) = {fDisturb};
           [F_disturb_foot, F_disturb_shank, F_disturb_thigh,F_disturb_trunk]= fd{:};
           sim(modelName);

           sensors = {LL_HFL, LF_GLU, LL_HAM, LF_HAM, LF_VAS, LF_GAS, LF_SOL, LL_TA, ...
               RL_HFL, RF_GLU, RL_HAM, RF_HAM, RF_VAS, RF_GAS, RF_SOL, RL_TA, Torso};

           actuators = {LStimHFL,LStimGLU, LStimHAM, LStimVAS, LStimGAS, LStimSOL, LStimTA};

           fd_sig = {fd_lfoot, fd_lshank, fd_lthigh, fd_trunk};
           
           %disp([k,i,jntInd])
           %keyboard

           % obtain average sensor and actuator over disturbed swings
           [mMusDataDistLSw, mFeatDataDistLSw,~] =  collectGaitDataDistSw (fd_sig{jntInd}, ...
                                                    sensors, actuators, L_TD, L_TO);

           % obtain sparse correlations/ weights for each disturbance scenario
           [wDistLSw, delWLSw] =  wSparseDistSw ({mFeatDataDistLSw,mMusDataDistLSw},...
                                {mFeatDataLSw,mMusDataLSw});

            WStore{k,i,jntInd} = [wDistLSw, delWLSw];
            mean_wDistLSw = mean_wDistLSw + wDistLSw;
            mean_delWLSw = mean_delWLSw + delWLSw;
         
            count = count +1
                     
       end    


    end
    
end


mean_wDistLSw = mean_wDistLSw./count;
mean_delWLSw = mean_delWLSw./count;

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

% % undisturbed left swing w
% figure;imagesc(abs(wLSw)/(max(max(abs(wLSw)))));colorbar;title('Leg weights UNDISTURBED LEFT SWING (2-leg Mus Feat Data)-sparse');
% set(gca(),'YTick',1:7) 
% set(gca(),'YTickLabel',labels_ly);
% set(gca(),'XTick',1:18) 
% set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);


%% disturbed left swing w
str_disturb = strcat('Fdisturb =', num2str(fDisturb));

figure;imagesc(abs(mean_wDistLSw)/(max(max(abs(mean_wDistLSw)))));colorbar;title({'Leg weights DISTURBED LEFT SWING (2-leg Mus Feat Data)-sparse',str_disturb});
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% delta left swing w (disturbed - steady)
figure;imagesc(abs(mean_delWLSw)/(max(max(abs(mean_delWLSw)))));colorbar;title({'Leg weights DEL LEFT SWING (2-leg Mus Feat Data)-sparse',str_disturb});
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);


