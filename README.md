# README #

Matlab experiments for learning reflex (muscle feedback control) laws controlling the muscles, based on the simulated data of muscle activation and joint movements during walking

Dependency: L1_LS package for l1-regularized least squares.

Questions to: rutad@cmu.edu