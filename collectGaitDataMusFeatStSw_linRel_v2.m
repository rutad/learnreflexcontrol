%clc;
%clear all;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;
KinInd = 0;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end-1);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end);
RswingEnd_ind = RgaitEnd_ind;

% resampling length left
resampleLSt_p = max(LswingStart_ind-LgaitStart_ind)+1;
resampleLSw_p = max(LswingEnd_ind-LswingStart_ind)+1;

LdataLen = length(LgaitStart_ind);

% resample length right
resampleRSt_p = max(RswingStart_ind-RgaitStart_ind)+1;
resampleRSw_p = max(RswingEnd_ind-RswingStart_ind)+1;

RdataLen = length(RgaitStart_ind);

%% Store kinematics and muscle data from gait cycles (Stance)

% Preallocate memory for left stance muscle feat data

LL_HFLDataLSt = NaN(LdataLen,resampleLSt_p);
LF_GLUDataLSt = NaN(LdataLen,resampleLSt_p);
LL_HAMDataLSt = NaN(LdataLen,resampleLSt_p);
LF_HAMDataLSt = NaN(LdataLen,resampleLSt_p);
LF_VASDataLSt = NaN(LdataLen,resampleLSt_p);
LF_GASDataLSt = NaN(LdataLen,resampleLSt_p);
LF_SOLDataLSt = NaN(LdataLen,resampleLSt_p);
LL_TADataLSt = NaN(LdataLen,resampleLSt_p);

RL_HFLDataLSt = NaN(LdataLen,resampleLSt_p);
RF_GLUDataLSt = NaN(LdataLen,resampleLSt_p);
RL_HAMDataLSt = NaN(LdataLen,resampleLSt_p);
RF_HAMDataLSt = NaN(LdataLen,resampleLSt_p);
RF_VASDataLSt = NaN(LdataLen,resampleLSt_p);
RF_GASDataLSt = NaN(LdataLen,resampleLSt_p);
RF_SOLDataLSt = NaN(LdataLen,resampleLSt_p);
RL_TADataLSt = NaN(LdataLen,resampleLSt_p);

TrunkDataLSt = NaN(LdataLen,resampleLSt_p);
dTrunkDataLSt = NaN(LdataLen,resampleLSt_p);

% Preallocate memory for left stance muscle data

LHFLDataLSt = NaN(LdataLen,resampleLSt_p);
LGLUDataLSt = NaN(LdataLen,resampleLSt_p);
LHAMDataLSt = NaN(LdataLen,resampleLSt_p);
LVASDataLSt = NaN(LdataLen,resampleLSt_p);
LGASDataLSt = NaN(LdataLen,resampleLSt_p);
LSOLDataLSt = NaN(LdataLen,resampleLSt_p);
LTADataLSt = NaN(LdataLen,resampleLSt_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right stance muscle feat data

LL_HFLDataRSt = NaN(LdataLen,resampleRSt_p);
LF_GLUDataRSt = NaN(LdataLen,resampleRSt_p);
LL_HAMDataRSt = NaN(LdataLen,resampleRSt_p);
LF_HAMDataRSt = NaN(LdataLen,resampleRSt_p);
LF_VASDataRSt = NaN(LdataLen,resampleRSt_p);
LF_GASDataRSt = NaN(LdataLen,resampleRSt_p);
LF_SOLDataRSt = NaN(LdataLen,resampleRSt_p);
LL_TADataRSt = NaN(LdataLen,resampleRSt_p);

RL_HFLDataRSt = NaN(LdataLen,resampleRSt_p);
RF_GLUDataRSt = NaN(LdataLen,resampleRSt_p);
RL_HAMDataRSt = NaN(LdataLen,resampleRSt_p);
RF_HAMDataRSt = NaN(LdataLen,resampleRSt_p);
RF_VASDataRSt = NaN(LdataLen,resampleRSt_p);
RF_GASDataRSt = NaN(LdataLen,resampleRSt_p);
RF_SOLDataRSt = NaN(LdataLen,resampleRSt_p);
RL_TADataRSt = NaN(LdataLen,resampleRSt_p);

TrunkDataRSt = NaN(LdataLen,resampleRSt_p);
dTrunkDataRSt = NaN(LdataLen,resampleRSt_p);

% Preallocate memory for right muscle stance data

RHFLDataRSt = NaN(LdataLen,resampleRSt_p);
RGLUDataRSt = NaN(LdataLen,resampleRSt_p);
RHAMDataRSt = NaN(LdataLen,resampleRSt_p);
RVASDataRSt = NaN(LdataLen,resampleRSt_p);
RGASDataRSt = NaN(LdataLen,resampleRSt_p);
RSOLDataRSt = NaN(LdataLen,resampleRSt_p);
RTADataRSt = NaN(LdataLen,resampleRSt_p);

for i=1:LdataLen
    
    LLHFL_LSt = LL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angle
    LLHFL_LStR = resample(LLHFL_LSt,resampleLSt_p,size(LLHFL_LSt,1)); % resample to max gait cycle length 
    
    LFGLU_LSt = LF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angular velocity
    LFGLU_LStR = resample(LFGLU_LSt,resampleLSt_p,size(LFGLU_LSt,1));
    
    LLHAM_LSt = LL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LLHAM_LStR = resample(LLHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LFHAM_LSt = LF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFHAM_LStR = resample(LFHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LFVAS_LSt = LF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFVAS_LStR = resample(LFVAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LFGAS_LSt = LF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFGAS_LStR = resample(LFGAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LFSOL_LSt = LF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LFSOL_LStR = resample(LFSOL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
   
    LLTA_LSt = LL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LLTA_LStR = resample(LLTA_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    RLHFL_LSt = RL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angle
    RLHFL_LStR = resample(RLHFL_LSt,resampleLSt_p,size(LLHAM_LSt,1)); % resample to max gait cycle length 
    
    RFGLU_LSt = RF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angular velocity
    RFGLUStR = resample(RFGLU_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    RLHAM_LSt = RL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RLHAM_LStR = resample(RLHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    RFHAM_LSt = RF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFHAM_LStR = resample(RFHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    RFVAS_LSt = RF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFVAS_LStR = resample(RFVAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    RFGAS_LSt = RF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFGAS_LStR = resample(RFGAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    RFSOL_LSt = RF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RFSOL_LStR = resample(RFSOL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
   
    RLTA_LSt = RL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    RLTA_LStR = resample(RLTA_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
  
    Trunk_LSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),1);
    Trunk_LStR = resample(Trunk_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    dTrunk_LSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),2);
    dTrunk_LStR = resample(dTrunk_LSt,resampleLSt_p,size(LLHAM_LSt,1));
        
    LHFL_LSt = LStimHFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LHFL_LStR = resample(LHFL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LGLU_LSt = LStimGLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LGLU_LStR = resample(LGLU_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LHAM_LSt = LStimHAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LHAM_LStR = resample(LHAM_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LVAS_LSt = LStimVAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LVAS_LStR = resample(LVAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LGAS_LSt = LStimGAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LGAS_LStR = resample(LGAS_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LSOL_LSt = LStimSOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LSOL_LStR = resample(LSOL_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    LTA_LSt = LStimTA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);
    LTA_LStR = resample(LTA_LSt,resampleLSt_p,size(LLHAM_LSt,1));
    
    
    LL_HFLDataLSt(i,:) = LLHFL_LStR;
    LF_GLUDataLSt(i,:) = LFGLU_LStR;
    LL_HAMDataLSt(i,:) = LLHAM_LStR;
    LF_HAMDataLSt(i,:) = LFHAM_LStR;
    LF_VASDataLSt(i,:) = LFVAS_LStR;
    LF_GASDataLSt(i,:) = LFGAS_LStR;
    LF_SOLDataLSt(i,:) = LFSOL_LStR;
    LL_TADataLSt(i,:) = LLTA_LStR;
    
    TrunkDataLSt(i,:) = Trunk_LStR;
    
    RL_HFLDataLSt(i,:) = RLHFL_LStR;
    RF_GLUDataLSt(i,:) = RFGLUStR;
    RL_HAMDataLSt(i,:) = RLHAM_LStR;
    RF_HAMDataLSt(i,:) = RFHAM_LStR;
    RF_VASDataLSt(i,:) = RFVAS_LStR;
    RF_GASDataLSt(i,:) = RFGAS_LStR;
    RF_SOLDataLSt(i,:) = RFSOL_LStR;
    RL_TADataLSt(i,:) = RLTA_LStR;
    
    dTrunkDataLSt(i,:) = dTrunk_LStR;
    
    LHFLDataLSt(i,:) = LHFL_LStR;
    LGLUDataLSt(i,:) = LGLU_LStR;
    LHAMDataLSt(i,:) = LHAM_LStR;
    LVASDataLSt(i,:) = LVAS_LStR;
    LGASDataLSt(i,:) = LGAS_LStR;
    LSOLDataLSt(i,:) = LSOL_LStR;
    LTADataLSt(i,:) = LTA_LStR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    LLHFL_RSt = LL_HFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angle
    LLHFL_RStR = resample(LLHFL_RSt,resampleRSt_p,size(LLHFL_RSt,1)); % resample to max gait cycle length 
    
    LFGLU_RSt = LF_GLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angular velocity
    LFGLU_RStR = resample(LFGLU_RSt,resampleRSt_p,size(LFGLU_RSt,1));
    
    LLHAM_RSt = LL_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    LLHAM_RStR = resample(LLHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    LFHAM_RSt = LF_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    LFHAM_RStR = resample(LFHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    LFVAS_RSt = LF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    LFVAS_RStR = resample(LFVAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    LFGAS_RSt = LF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    LFGAS_RStR = resample(LFGAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    LFSOL_RSt = LF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    LFSOL_RStR = resample(LFSOL_RSt,resampleRSt_p,size(LLHAM_RSt,1));
   
    LLTA_RSt = LL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    LLTA_RStR = resample(LLTA_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RLHFL_RSt = RL_HFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angle
    RLHFL_RStR = resample(RLHFL_RSt,resampleRSt_p,size(LLHAM_RSt,1)); % resample to max gait cycle length 
    
    RFGLU_RSt = RF_GLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angular velocity
    RFGLUStR = resample(RFGLU_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RLHAM_RSt = RL_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RLHAM_RStR = resample(RLHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RFHAM_RSt = RF_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFHAM_RStR = resample(RFHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RFVAS_RSt = RF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFVAS_RStR = resample(RFVAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RFGAS_RSt = RF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFGAS_RStR = resample(RFGAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RFSOL_RSt = RF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RFSOL_RStR = resample(RFSOL_RSt,resampleRSt_p,size(LLHAM_RSt,1));
   
    RLTA_RSt = RL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RLTA_RStR = resample(RLTA_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
  
    Trunk_RSt = Torso.Data(RgaitStart_ind(i):RswingStart_ind(i),1);
    Trunk_RStR = resample(Trunk_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    dTrunk_RSt = Torso.Data(RgaitStart_ind(i):RswingStart_ind(i),2);
    dTrunk_RStR = resample(dTrunk_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RHF_RSt = RStimHFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RHF_RStR = resample(RHF_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RGLU_RSt = RStimGLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RGLU_RStR = resample(RGLU_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RHAM_RSt = RStimHAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RHAM_RStR = resample(RHAM_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RVAS_RSt = RStimVAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RVAS_RStR = resample(RVAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RGAS_RSt = RStimGAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RGAS_RStR = resample(RGAS_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RSOL_RSt = RStimSOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RSOL_RStR = resample(RSOL_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    RTA_RSt = RStimTA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);
    RTA_RStR = resample(RTA_RSt,resampleRSt_p,size(LLHAM_RSt,1));
    
    LL_HFLDataRSt(i,:) = LLHFL_RStR;
    LF_GLUDataRSt(i,:) = LFGLU_RStR;
    LL_HAMDataRSt(i,:) = LLHAM_RStR;
    LF_HAMDataRSt(i,:) = LFHAM_RStR;
    LF_VASDataRSt(i,:) = LFVAS_RStR;
    LF_GASDataRSt(i,:) = LFGAS_RStR;
    LF_SOLDataRSt(i,:) = LFSOL_RStR;
    LL_TADataRSt(i,:) = LLTA_RStR;
    
    TrunkDataRSt(i,:) = Trunk_RStR;
    
    RL_HFLDataRSt(i,:) = RLHFL_RStR;
    RF_GLUDataRSt(i,:) = RFGLUStR;
    RL_HAMDataRSt(i,:) = RLHAM_RStR;
    RF_HAMDataRSt(i,:) = RFHAM_RStR;
    RF_VASDataRSt(i,:) = RFVAS_RStR;
    RF_GASDataRSt(i,:) = RFGAS_RStR;
    RF_SOLDataRSt(i,:) = RFSOL_RStR;
    RL_TADataRSt(i,:) = RLTA_RStR;
    
    dTrunkDataRSt(i,:) = dTrunk_RStR;
    
    RHFLDataRSt(i,:) = RHF_RStR;
    RGLUDataRSt(i,:) = RGLU_RStR;
    RHAMDataRSt(i,:) = RHAM_RStR;
    RVASDataRSt(i,:) = RVAS_RStR;
    RGASDataRSt(i,:) = RGAS_RStR;
    RSOLDataRSt(i,:) = RSOL_RStR;
    RTADataRSt(i,:) = RTA_RStR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    KinInd = KinInd + 1; 
end

%% Store kinematics and muscle data from gait cycles (Swing)

% Preallocate memory for left swing muscle feat data

LL_HFLDataLSw = NaN(LdataLen,resampleLSw_p);
LF_GLUDataLSw = NaN(LdataLen,resampleLSw_p);
LL_HAMDataLSw = NaN(LdataLen,resampleLSw_p);
LF_HAMDataLSw = NaN(LdataLen,resampleLSw_p);
LF_VASDataLSw = NaN(LdataLen,resampleLSw_p);
LF_GASDataLSw = NaN(LdataLen,resampleLSw_p);
LF_SOLDataLSw = NaN(LdataLen,resampleLSw_p);
LL_TADataLSw = NaN(LdataLen,resampleLSw_p);

RL_HFLDataLSw = NaN(LdataLen,resampleLSw_p);
RF_GLUDataLSw = NaN(LdataLen,resampleLSw_p);
RL_HAMDataLSw = NaN(LdataLen,resampleLSw_p);
RF_HAMDataLSw = NaN(LdataLen,resampleLSw_p);
RF_VASDataLSw = NaN(LdataLen,resampleLSw_p);
RF_GASDataLSw = NaN(LdataLen,resampleLSw_p);
RF_SOLDataLSw = NaN(LdataLen,resampleLSw_p);
RL_TADataLSw = NaN(LdataLen,resampleLSw_p);

TrunkDataLSw = NaN(LdataLen,resampleLSw_p);
dTrunkDataLSw = NaN(LdataLen,resampleLSw_p);

% Preallocate memory for left swing muscle data

LHFLDataLSw = NaN(LdataLen,resampleLSw_p);
LGLUDataLSw = NaN(LdataLen,resampleLSw_p);
LHAMDataLSw = NaN(LdataLen,resampleLSw_p);
LVASDataLSw = NaN(LdataLen,resampleLSw_p);
LGASDataLSw = NaN(LdataLen,resampleLSw_p);
LSOLDataLSw = NaN(LdataLen,resampleLSw_p);
LTADataLSw = NaN(LdataLen,resampleLSw_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right swing muscle feat data

LL_HFLDataRSw = NaN(LdataLen,resampleRSw_p);
LF_GLUDataRSw = NaN(LdataLen,resampleRSw_p);
LL_HAMDataRSw = NaN(LdataLen,resampleRSw_p);
LF_HAMDataRSw = NaN(LdataLen,resampleRSw_p);
LF_VASDataRSw = NaN(LdataLen,resampleRSw_p);
LF_GASDataRSw = NaN(LdataLen,resampleRSw_p);
LF_SOLDataRSw = NaN(LdataLen,resampleRSw_p);
LL_TADataRSw = NaN(LdataLen,resampleRSw_p);

RL_HFLDataRSw = NaN(LdataLen,resampleRSw_p);
RF_GLUDataRSw = NaN(LdataLen,resampleRSw_p);
RL_HAMDataRSw = NaN(LdataLen,resampleRSw_p);
RF_HAMDataRSw = NaN(LdataLen,resampleRSw_p);
RF_VASDataRSw = NaN(LdataLen,resampleRSw_p);
RF_GASDataRSw = NaN(LdataLen,resampleRSw_p);
RF_SOLDataRSw = NaN(LdataLen,resampleRSw_p);
RL_TADataRSw = NaN(LdataLen,resampleRSw_p);

TrunkDataRSw = NaN(LdataLen,resampleRSw_p);
dTrunkDataRSw = NaN(LdataLen,resampleRSw_p);

% Preallocate memory for right muscle swing data

RHFLDataRSw = NaN(LdataLen,resampleRSw_p);
RGLUDataRSw = NaN(LdataLen,resampleRSw_p);
RHAMDataRSw = NaN(LdataLen,resampleRSw_p);
RVASDataRSw = NaN(LdataLen,resampleRSw_p);
RGASDataRSw = NaN(LdataLen,resampleRSw_p);
RSOLDataRSw = NaN(LdataLen,resampleRSw_p);
RTADataRSw = NaN(LdataLen,resampleRSw_p);

for i=1:LdataLen
    
    LLHFL_LSw = LL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angle
    LLHFL_LSwR = resample(LLHFL_LSw,resampleLSw_p,size(LLHFL_LSw,1)); % resample to max gait cycle length 
    
    LFGLU_LSw = LF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angular velocity
    LFGLU_LSwR = resample(LFGLU_LSw,resampleLSw_p,size(LFGLU_LSw,1));
    
    LLHAM_LSw = LL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LLHAM_LSwR = resample(LLHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LFHAM_LSw = LF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFHAM_LSwR = resample(LFHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LFVAS_LSw = LF_VAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFVAS_LSwR = resample(LFVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LFGAS_LSw = LF_GAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFGAS_LSwR = resample(LFGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LFSOL_LSw = LF_SOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LFSOL_LSwR = resample(LFSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
   
    LLTA_LSw = LL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LLTA_LSwR = resample(LLTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    RLHFL_LSw = RL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angle
    RLHFL_LSwR = resample(RLHFL_LSw,resampleLSw_p,size(LLHAM_LSw,1)); % resample to max gait cycle length 
    
    RFGLU_LSw = RF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angular velocity
    RFGLUSwR = resample(RFGLU_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    RLHAM_LSw = RL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RLHAM_LSwR = resample(RLHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    RFHAM_LSw = RF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFHAM_LSwR = resample(RFHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    RFVAS_LSw = RF_VAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFVAS_LSwR = resample(RFVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    RFGAS_LSw = RF_GAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFGAS_LSwR = resample(RFGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    RFSOL_LSw = RF_SOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RFSOL_LSwR = resample(RFSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
   
    RLTA_LSw = RL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    RLTA_LSwR = resample(RLTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
  
    Trunk_LSw = Torso.Data(LswingStart_ind(i):LgaitEnd_ind(i),1);
    Trunk_LSwR = resample(Trunk_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    dTrunk_LSw = Torso.Data(LswingStart_ind(i):LgaitEnd_ind(i),2);
    dTrunk_LSwR = resample(dTrunk_LSw,resampleLSw_p,size(LLHAM_LSw,1));
        
    LHFL_LSw = LStimHFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LHFL_LSwR = resample(LHFL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LGLU_LSw = LStimGLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LGLU_LSwR = resample(LGLU_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LHAM_LSw = LStimHAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LHAM_LSwR = resample(LHAM_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LVAS_LSw = LStimVAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LVAS_LSwR = resample(LVAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LGAS_LSw = LStimGAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LGAS_LSwR = resample(LGAS_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LSOL_LSw = LStimSOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LSOL_LSwR = resample(LSOL_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    LTA_LSw = LStimTA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);
    LTA_LSwR = resample(LTA_LSw,resampleLSw_p,size(LLHAM_LSw,1));
    
    
    LL_HFLDataLSw(i,:) = LLHFL_LSwR;
    LF_GLUDataLSw(i,:) = LFGLU_LSwR;
    LL_HAMDataLSw(i,:) = LLHAM_LSwR;
    LF_HAMDataLSw(i,:) = LFHAM_LSwR;
    LF_VASDataLSw(i,:) = LFVAS_LSwR;
    LF_GASDataLSw(i,:) = LFGAS_LSwR;
    LF_SOLDataLSw(i,:) = LFSOL_LSwR;
    LL_TADataLSw(i,:) = LLTA_LSwR;
    
    TrunkDataLSw(i,:) = Trunk_LSwR;
    
    RL_HFLDataLSw(i,:) = RLHFL_LSwR;
    RF_GLUDataLSw(i,:) = RFGLUSwR;
    RL_HAMDataLSw(i,:) = RLHAM_LSwR;
    RF_HAMDataLSw(i,:) = RFHAM_LSwR;
    RF_VASDataLSw(i,:) = RFVAS_LSwR;
    RF_GASDataLSw(i,:) = RFGAS_LSwR;
    RF_SOLDataLSw(i,:) = RFSOL_LSwR;
    RL_TADataLSw(i,:) = RLTA_LSwR;
    
    dTrunkDataLSw(i,:) = dTrunk_LSwR;
    
    LHFLDataLSw(i,:) = LHFL_LSwR;
    LGLUDataLSw(i,:) = LGLU_LSwR;
    LHAMDataLSw(i,:) = LHAM_LSwR;
    LVASDataLSw(i,:) = LVAS_LSwR;
    LGASDataLSw(i,:) = LGAS_LSwR;
    LSOLDataLSw(i,:) = LSOL_LSwR;
    LTADataLSw(i,:) = LTA_LSwR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    LLHFL_RSw = LL_HFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angle
    LLHFL_RSwR = resample(LLHFL_RSw,resampleRSw_p,size(LLHFL_RSw,1)); % resample to max gait cycle length 
    
    LFGLU_RSw = LF_GLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angular velocity
    LFGLU_RSwR = resample(LFGLU_RSw,resampleRSw_p,size(LFGLU_RSw,1));
    
    LLHAM_RSw = LL_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    LLHAM_RSwR = resample(LLHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    LFHAM_RSw = LF_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    LFHAM_RSwR = resample(LFHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    LFVAS_RSw = LF_VAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    LFVAS_RSwR = resample(LFVAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    LFGAS_RSw = LF_GAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    LFGAS_RSwR = resample(LFGAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    LFSOL_RSw = LF_SOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    LFSOL_RSwR = resample(LFSOL_RSw,resampleRSw_p,size(LLHAM_RSw,1));
   
    LLTA_RSw = LL_TA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    LLTA_RSwR = resample(LLTA_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RLHFL_RSw = RL_HFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angle
    RLHFL_RSwR = resample(RLHFL_RSw,resampleRSw_p,size(LLHAM_RSw,1)); % resample to max gait cycle length 
    
    RFGLU_RSw = RF_GLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angular velocity
    RFGLUSwR = resample(RFGLU_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RLHAM_RSw = RL_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RLHAM_RSwR = resample(RLHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RFHAM_RSw = RF_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RFHAM_RSwR = resample(RFHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RFVAS_RSw = RF_VAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RFVAS_RSwR = resample(RFVAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RFGAS_RSw = RF_GAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RFGAS_RSwR = resample(RFGAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RFSOL_RSw = RF_SOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RFSOL_RSwR = resample(RFSOL_RSw,resampleRSw_p,size(LLHAM_RSw,1));
   
    RLTA_RSw = RL_TA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RLTA_RSwR = resample(RLTA_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
  
    Trunk_RSw = Torso.Data(RswingStart_ind(i):RgaitEnd_ind(i),1);
    Trunk_RSwR = resample(Trunk_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    dTrunk_RSw = Torso.Data(RswingStart_ind(i):RgaitEnd_ind(i),2);
    dTrunk_RSwR = resample(dTrunk_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RHF_RSw = RStimHFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RHF_RSwR = resample(RHF_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RGLU_RSw = RStimGLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RGLU_RSwR = resample(RGLU_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RHAM_RSw = RStimHAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RHAM_RSwR = resample(RHAM_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RVAS_RSw = RStimVAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RVAS_RSwR = resample(RVAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RGAS_RSw = RStimGAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RGAS_RSwR = resample(RGAS_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RSOL_RSw = RStimSOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RSOL_RSwR = resample(RSOL_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    RTA_RSw = RStimTA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
    RTA_RSwR = resample(RTA_RSw,resampleRSw_p,size(LLHAM_RSw,1));
    
    LL_HFLDataRSw(i,:) = LLHFL_RSwR;
    LF_GLUDataRSw(i,:) = LFGLU_RSwR;
    LL_HAMDataRSw(i,:) = LLHAM_RSwR;
    LF_HAMDataRSw(i,:) = LFHAM_RSwR;
    LF_VASDataRSw(i,:) = LFVAS_RSwR;
    LF_GASDataRSw(i,:) = LFGAS_RSwR;
    LF_SOLDataRSw(i,:) = LFSOL_RSwR;
    LL_TADataRSw(i,:) = LLTA_RSwR;
    
    TrunkDataRSw(i,:) = Trunk_RSwR;
    
    RL_HFLDataRSw(i,:) = RLHFL_RSwR;
    RF_GLUDataRSw(i,:) = RFGLUSwR;
    RL_HAMDataRSw(i,:) = RLHAM_RSwR;
    RF_HAMDataRSw(i,:) = RFHAM_RSwR;
    RF_VASDataRSw(i,:) = RFVAS_RSwR;
    RF_GASDataRSw(i,:) = RFGAS_RSwR;
    RF_SOLDataRSw(i,:) = RFSOL_RSwR;
    RL_TADataRSw(i,:) = RLTA_RSwR;
    
    dTrunkDataRSw(i,:) = dTrunk_RSwR;
    
    RHFLDataRSw(i,:) = RHF_RSwR;
    RGLUDataRSw(i,:) = RGLU_RSwR;
    RHAMDataRSw(i,:) = RHAM_RSwR;
    RVASDataRSw(i,:) = RVAS_RSwR;
    RGASDataRSw(i,:) = RGAS_RSwR;
    RSOLDataRSw(i,:) = RSOL_RSwR;
    RTADataRSw(i,:) = RTA_RSwR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    KinInd = KinInd + 1; 
end

%% Hold out test data (stance)

testNum = 5; % number of test samples
testDataInd = randi(LdataLen,testNum,1);

% test data left stance
tLL_HFLDataLSt = LL_HFLDataLSt(testDataInd,:);
tLF_GLUDataLSt = LF_GLUDataLSt(testDataInd,:);
tLL_HAMDataLSt = LL_HAMDataLSt(testDataInd,:);
tLF_HAMDataLSt = LF_HAMDataLSt(testDataInd,:);
tLF_VASDataLSt = LF_VASDataLSt(testDataInd,:);
tLF_GASDataLSt = LF_GASDataLSt(testDataInd,:);
tLF_SOLDataLSt = LF_SOLDataLSt(testDataInd,:);
tLL_TADataLSt = LL_TADataLSt(testDataInd,:);

tTrunkDataLSt = TrunkDataLSt(testDataInd,:);

tRL_HFLDataLSt = RL_HFLDataLSt(testDataInd,:);
tRF_GLUDataLSt = RF_GLUDataLSt(testDataInd,:);
tRL_HAMDataLSt = RL_HAMDataLSt(testDataInd,:);
tRF_HAMDataLSt = RF_HAMDataLSt(testDataInd,:);
tRF_VASDataLSt = RF_VASDataLSt(testDataInd,:);
tRF_GASDataLSt = RF_GASDataLSt(testDataInd,:);
tRF_SOLDataLSt = RF_SOLDataLSt(testDataInd,:);
tRL_TADataLSt = RL_TADataLSt(testDataInd,:);

tdTrunkDataLSt = dTrunkDataLSt(testDataInd,:);

tLHFLDataLSt = LHFLDataLSt(testDataInd,:);
tLGLUDataLSt = LGLUDataLSt(testDataInd,:);
tLHAMDataLSt = LHAMDataLSt(testDataInd,:);
tLVASDataLSt = LVASDataLSt(testDataInd,:);
tLGASDataLSt = LGASDataLSt(testDataInd,:);
tLSOLDataLSt = LSOLDataLSt(testDataInd,:);
tLTADataLSt = LTADataLSt(testDataInd,:);

% Remove test data from all left stance data

LL_HFLDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_GLUDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LL_HAMDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_HAMDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_VASDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_GASDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LF_SOLDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
LL_TADataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);

TrunkDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);

RL_HFLDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RF_GLUDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RL_HAMDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RF_HAMDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RF_VASDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RF_GASDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RF_SOLDataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);
RL_TADataLSt(testDataInd,:)= zeros(length(testDataInd),resampleLSt_p);

dTrunkDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);

LHFLDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LGLUDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LHAMDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LVASDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LGASDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LSOLDataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);
LTADataLSt(testDataInd,:) = zeros(length(testDataInd),resampleLSt_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% test data right stance
tLL_HFLDataRSt = LL_HFLDataRSt(testDataInd,:);
tLF_GLUDataRSt = LF_GLUDataRSt(testDataInd,:);
tLL_HAMDataRSt = LL_HAMDataRSt(testDataInd,:);
tLF_HAMDataRSt = LF_HAMDataRSt(testDataInd,:);
tLF_VASDataRSt = LF_VASDataRSt(testDataInd,:);
tLF_GASDataRSt = LF_GASDataRSt(testDataInd,:);
tLF_SOLDataRSt = LF_SOLDataRSt(testDataInd,:);
tLL_TADataRSt = LL_TADataRSt(testDataInd,:);

tTrunkDataRSt = TrunkDataRSt(testDataInd,:);

tRL_HFLDataRSt = RL_HFLDataRSt(testDataInd,:);
tRF_GLUDataRSt = RF_GLUDataRSt(testDataInd,:);
tRL_HAMDataRSt = RL_HAMDataRSt(testDataInd,:);
tRF_HAMDataRSt = RF_HAMDataRSt(testDataInd,:);
tRF_VASDataRSt = RF_VASDataRSt(testDataInd,:);
tRF_GASDataRSt = RF_GASDataRSt(testDataInd,:);
tRF_SOLDataRSt = RF_SOLDataRSt(testDataInd,:);
tRL_TADataRSt = RL_TADataRSt(testDataInd,:);

tdTrunkDataRSt = dTrunkDataRSt(testDataInd,:);

tRHFLDataRSt = RHFLDataRSt(testDataInd,:);
tRGLUDataRSt = RGLUDataRSt(testDataInd,:);
tRHAMDataRSt = RHAMDataRSt(testDataInd,:);
tRVASDataRSt = RVASDataRSt(testDataInd,:);
tRGASDataRSt = RGASDataRSt(testDataInd,:);
tRSOLDataRSt = RSOLDataRSt(testDataInd,:);
tRTADataRSt = RTADataRSt(testDataInd,:);

% Remove test data from all right stance data

LL_HFLDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LF_GLUDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LL_HAMDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LF_HAMDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LF_VASDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LF_GASDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LF_SOLDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
LL_TADataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);

TrunkDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);

RL_HFLDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_GLUDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RL_HAMDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_HAMDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_VASDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_GASDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RF_SOLDataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);
RL_TADataRSt(testDataInd,:)= zeros(length(testDataInd),resampleRSt_p);

dTrunkDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);

RHFLDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RGLUDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RHAMDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RVASDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RGASDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RSOLDataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);
RTADataRSt(testDataInd,:) = zeros(length(testDataInd),resampleRSt_p);

%% Hold out test data (swing)

% test data left swing
tLL_HFLDataLSw = LL_HFLDataLSw(testDataInd,:);
tLF_GLUDataLSw = LF_GLUDataLSw(testDataInd,:);
tLL_HAMDataLSw = LL_HAMDataLSw(testDataInd,:);
tLF_HAMDataLSw = LF_HAMDataLSw(testDataInd,:);
tLF_VASDataLSw = LF_VASDataLSw(testDataInd,:);
tLF_GASDataLSw = LF_GASDataLSw(testDataInd,:);
tLF_SOLDataLSw = LF_SOLDataLSw(testDataInd,:);
tLL_TADataLSw = LL_TADataLSw(testDataInd,:);

tTrunkDataLSw = TrunkDataLSw(testDataInd,:);

tRL_HFLDataLSw = RL_HFLDataLSw(testDataInd,:);
tRF_GLUDataLSw = RF_GLUDataLSw(testDataInd,:);
tRL_HAMDataLSw = RL_HAMDataLSw(testDataInd,:);
tRF_HAMDataLSw = RF_HAMDataLSw(testDataInd,:);
tRF_VASDataLSw = RF_VASDataLSw(testDataInd,:);
tRF_GASDataLSw = RF_GASDataLSw(testDataInd,:);
tRF_SOLDataLSw = RF_SOLDataLSw(testDataInd,:);
tRL_TADataLSw = RL_TADataLSw(testDataInd,:);

tdTrunkDataLSw = dTrunkDataLSw(testDataInd,:);

tLHFLDataLSw = LHFLDataLSw(testDataInd,:);
tLGLUDataLSw = LGLUDataLSw(testDataInd,:);
tLHAMDataLSw = LHAMDataLSw(testDataInd,:);
tLVASDataLSw = LVASDataLSw(testDataInd,:);
tLGASDataLSw = LGASDataLSw(testDataInd,:);
tLSOLDataLSw = LSOLDataLSw(testDataInd,:);
tLTADataLSw = LTADataLSw(testDataInd,:);

% Remove test data from all left swing data

LL_HFLDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_GLUDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LL_HAMDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_HAMDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_VASDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_GASDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LF_SOLDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
LL_TADataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);

TrunkDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);

RL_HFLDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RF_GLUDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RL_HAMDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RF_HAMDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RF_VASDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RF_GASDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RF_SOLDataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);
RL_TADataLSw(testDataInd,:)= zeros(length(testDataInd),resampleLSw_p);

dTrunkDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);

LHFLDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LGLUDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LHAMDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LVASDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LGASDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LSOLDataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);
LTADataLSw(testDataInd,:) = zeros(length(testDataInd),resampleLSw_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% test data right swing
tLL_HFLDataRSw = LL_HFLDataRSw(testDataInd,:);
tLF_GLUDataRSw = LF_GLUDataRSw(testDataInd,:);
tLL_HAMDataRSw = LL_HAMDataRSw(testDataInd,:);
tLF_HAMDataRSw = LF_HAMDataRSw(testDataInd,:);
tLF_VASDataRSw = LF_VASDataRSw(testDataInd,:);
tLF_GASDataRSw = LF_GASDataRSw(testDataInd,:);
tLF_SOLDataRSw = LF_SOLDataRSw(testDataInd,:);
tLL_TADataRSw = LL_TADataRSw(testDataInd,:);

tTrunkDataRSw = TrunkDataRSw(testDataInd,:);

tRL_HFLDataRSw = RL_HFLDataRSw(testDataInd,:);
tRF_GLUDataRSw = RF_GLUDataRSw(testDataInd,:);
tRL_HAMDataRSw = RL_HAMDataRSw(testDataInd,:);
tRF_HAMDataRSw = RF_HAMDataRSw(testDataInd,:);
tRF_VASDataRSw = RF_VASDataRSw(testDataInd,:);
tRF_GASDataRSw = RF_GASDataRSw(testDataInd,:);
tRF_SOLDataRSw = RF_SOLDataRSw(testDataInd,:);
tRL_TADataRSw = RL_TADataRSw(testDataInd,:);

tdTrunkDataRSw = dTrunkDataRSw(testDataInd,:);

tRHFLDataRSw = RHFLDataRSw(testDataInd,:);
tRGLUDataRSw = RGLUDataRSw(testDataInd,:);
tRHAMDataRSw = RHAMDataRSw(testDataInd,:);
tRVASDataRSw = RVASDataRSw(testDataInd,:);
tRGASDataRSw = RGASDataRSw(testDataInd,:);
tRSOLDataRSw = RSOLDataRSw(testDataInd,:);
tRTADataRSw = RTADataRSw(testDataInd,:);

% Remove test data from all right swing data

LL_HFLDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LF_GLUDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LL_HAMDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LF_HAMDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LF_VASDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LF_GASDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LF_SOLDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
LL_TADataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);

TrunkDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);

RL_HFLDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_GLUDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RL_HAMDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_HAMDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_VASDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_GASDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RF_SOLDataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);
RL_TADataRSw(testDataInd,:)= zeros(length(testDataInd),resampleRSw_p);

dTrunkDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);

RHFLDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RGLUDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RHAMDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RVASDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RGASDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RSOLDataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
RTADataRSw(testDataInd,:) = zeros(length(testDataInd),resampleRSw_p);
%% Mean data (left stance)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataLSt = mean(LL_HFLDataLSt);
mLF_GLUDataLSt = mean(LF_GLUDataLSt);
mLL_HAMDataLSt = mean(LL_HAMDataLSt);
mLF_HAMDataLSt = mean(LF_HAMDataLSt);
mLF_VASDataLSt = mean(LF_VASDataLSt);
mLF_GASDataLSt = mean(LF_GASDataLSt);
mLF_SOLDataLSt = mean(LF_SOLDataLSt);
mLL_TADataLSt = mean(LL_TADataLSt);

mTrunkDataLSt = mean(TrunkDataLSt);

mRL_HFLDataLSt = mean(RL_HFLDataLSt);
mRF_GLUDataLSt = mean(RF_GLUDataLSt);
mRL_HAMDataLSt = mean(RL_HAMDataLSt);
mRF_HAMDataLSt = mean(RF_HAMDataLSt);
mRF_VASDataLSt = mean(RF_VASDataLSt);
mRF_GASDataLSt = mean(RF_GASDataLSt);
mRF_SOLDataLSt = mean(RF_SOLDataLSt);
mRL_TADataLSt = mean(RL_TADataLSt);

mdTrunkDataLSt = mean(dTrunkDataLSt);


% collect mean muscle features over gaits (only training)
mFeatDataLSt = [mLL_HFLDataLSt; mLF_GLUDataLSt; mLL_HAMDataLSt; mLF_HAMDataLSt; mLF_VASDataLSt; mLF_GASDataLSt; mLF_SOLDataLSt; mLL_TADataLSt; mTrunkDataLSt; ...
            mRL_HFLDataLSt; mRF_GLUDataLSt; mRL_HAMDataLSt; mRF_HAMDataLSt; mRF_VASDataLSt; mRF_GASDataLSt; mRF_SOLDataLSt; mRL_TADataLSt; mdTrunkDataLSt];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataLSt = mean(LHFLDataLSt);
mLGLUDataLSt = mean(LGLUDataLSt);
mLHAMDataLSt = mean(LHAMDataLSt);
mLVASDataLSt = mean(LVASDataLSt);
mLGASDataLSt = mean(LGASDataLSt);
mLSOLDataLSt = mean(LSOLDataLSt);
mLTADataLSt = mean(LTADataLSt); 

% collect mean muscles over gaits (only training)
mMusDataLSt = [mLHFLDataLSt; mLGLUDataLSt; mLHAMDataLSt; mLVASDataLSt; mLGASDataLSt; mLSOLDataLSt; mLTADataLSt];

%% Mean data (right stance)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataRSt = mean(LL_HFLDataRSt);
mLF_GLUDataRSt = mean(LF_GLUDataRSt);
mLL_HAMDataRSt = mean(LL_HAMDataRSt);
mLF_HAMDataRSt = mean(LF_HAMDataRSt);
mLF_VASDataRSt = mean(LF_VASDataRSt);
mLF_GASDataRSt = mean(LF_GASDataRSt);
mLF_SOLDataRSt = mean(LF_SOLDataRSt);
mLL_TADataRSt = mean(LL_TADataRSt);

mTrunkDataRSt = mean(TrunkDataRSt);

mRL_HFLDataRSt = mean(RL_HFLDataRSt);
mRF_GLUDataRSt = mean(RF_GLUDataRSt);
mRL_HAMDataRSt = mean(RL_HAMDataRSt);
mRF_HAMDataRSt = mean(RF_HAMDataRSt);
mRF_VASDataRSt = mean(RF_VASDataRSt);
mRF_GASDataRSt = mean(RF_GASDataRSt);
mRF_SOLDataRSt = mean(RF_SOLDataRSt);
mRL_TADataRSt = mean(RL_TADataRSt);

mdTrunkDataRSt = mean(dTrunkDataRSt);


% collect mean muscle features over gaits (only training)
mFeatDataRSt = [mLL_HFLDataRSt; mLF_GLUDataRSt; mLL_HAMDataRSt; mLF_HAMDataRSt; mLF_VASDataRSt; mLF_GASDataRSt; mLF_SOLDataRSt; mLL_TADataRSt; mTrunkDataRSt; ...
            mRL_HFLDataRSt; mRF_GLUDataRSt; mRL_HAMDataRSt; mRF_HAMDataRSt; mRF_VASDataRSt; mRF_GASDataRSt; mRF_SOLDataRSt; mRL_TADataRSt; mdTrunkDataRSt];


mRHFLDataRSt = mean(RHFLDataRSt);
mRGLUDataRSt = mean(RGLUDataRSt);
mRHAMDataRSt = mean(RHAMDataRSt);
mRVASDataRSt = mean(RVASDataRSt);
mRGASDataRSt = mean(RGASDataRSt);
mRSOLDataRSt = mean(RSOLDataRSt);
mRTADataRSt = mean(RTADataRSt); 

mMusDataRSt =  [mRHFLDataRSt; mRGLUDataRSt; mRHAMDataRSt; mRVASDataRSt; mRGASDataRSt; mRSOLDataRSt; mRTADataRSt];
           
        
%% Mean data (left swing)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataLSw = mean(LL_HFLDataLSw);
mLF_GLUDataLSw = mean(LF_GLUDataLSw);
mLL_HAMDataLSw = mean(LL_HAMDataLSw);
mLF_HAMDataLSw = mean(LF_HAMDataLSw);
mLF_VASDataLSw = mean(LF_VASDataLSw);
mLF_GASDataLSw = mean(LF_GASDataLSw);
mLF_SOLDataLSw = mean(LF_SOLDataLSw);
mLL_TADataLSw = mean(LL_TADataLSw);

mTrunkDataLSw = mean(TrunkDataLSw);

mRL_HFLDataLSw = mean(RL_HFLDataLSw);
mRF_GLUDataLSw = mean(RF_GLUDataLSw);
mRL_HAMDataLSw = mean(RL_HAMDataLSw);
mRF_HAMDataLSw = mean(RF_HAMDataLSw);
mRF_VASDataLSw = mean(RF_VASDataLSw);
mRF_GASDataLSw = mean(RF_GASDataLSw);
mRF_SOLDataLSw = mean(RF_SOLDataLSw);
mRL_TADataLSw = mean(RL_TADataLSw);

mdTrunkDataLSw = mean(dTrunkDataLSw);


% collect mean muscle features over gaits (only training)
mFeatDataLSw = [mLL_HFLDataLSw; mLF_GLUDataLSw; mLL_HAMDataLSw; mLF_HAMDataLSw; mLF_VASDataLSw; mLF_GASDataLSw; mLF_SOLDataLSw; mLL_TADataLSw; mTrunkDataLSw; ...
            mRL_HFLDataLSw; mRF_GLUDataLSw; mRL_HAMDataLSw; mRF_HAMDataLSw; mRF_VASDataLSw; mRF_GASDataLSw; mRF_SOLDataLSw; mRL_TADataLSw; mdTrunkDataLSw];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataLSw = mean(LHFLDataLSw);
mLGLUDataLSw = mean(LGLUDataLSw);
mLHAMDataLSw = mean(LHAMDataLSw);
mLVASDataLSw = mean(LVASDataLSw);
mLGASDataLSw = mean(LGASDataLSw);
mLSOLDataLSw = mean(LSOLDataLSw);
mLTADataLSw = mean(LTADataLSw); 

% collect mean muscles over gaits (only training)
mMusDataLSw = [mLHFLDataLSw; mLGLUDataLSw; mLHAMDataLSw; mLVASDataLSw; mLGASDataLSw; mLSOLDataLSw; mLTADataLSw];

%% Mean data (right swing)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataRSw = mean(LL_HFLDataRSw);
mLF_GLUDataRSw = mean(LF_GLUDataRSw);
mLL_HAMDataRSw = mean(LL_HAMDataRSw);
mLF_HAMDataRSw = mean(LF_HAMDataRSw);
mLF_VASDataRSw = mean(LF_VASDataRSw);
mLF_GASDataRSw = mean(LF_GASDataRSw);
mLF_SOLDataRSw = mean(LF_SOLDataRSw);
mLL_TADataRSw = mean(LL_TADataRSw);

mTrunkDataRSw = mean(TrunkDataRSw);

mRL_HFLDataRSw = mean(RL_HFLDataRSw);
mRF_GLUDataRSw = mean(RF_GLUDataRSw);
mRL_HAMDataRSw = mean(RL_HAMDataRSw);
mRF_HAMDataRSw = mean(RF_HAMDataRSw);
mRF_VASDataRSw = mean(RF_VASDataRSw);
mRF_GASDataRSw = mean(RF_GASDataRSw);
mRF_SOLDataRSw = mean(RF_SOLDataRSw);
mRL_TADataRSw = mean(RL_TADataRSw);

mdTrunkDataRSw = mean(dTrunkDataRSw);


% collect mean muscle features over gaits (only training)
mFeatDataRSw = [mLL_HFLDataRSw; mLF_GLUDataRSw; mLL_HAMDataRSw; mLF_HAMDataRSw; mLF_VASDataRSw; mLF_GASDataRSw; mLF_SOLDataRSw; mLL_TADataRSw; mTrunkDataRSw; ...
            mRL_HFLDataRSw; mRF_GLUDataRSw; mRL_HAMDataRSw; mRF_HAMDataRSw; mRF_VASDataRSw; mRF_GASDataRSw; mRF_SOLDataRSw; mRL_TADataRSw; mdTrunkDataRSw];
        

mRHFLDataRSw = mean(RHFLDataRSw);
mRGLUDataRSw = mean(RGLUDataRSw);
mRHAMDataRSw = mean(RHAMDataRSw);
mRVASDataRSw = mean(RVASDataRSw);
mRGASDataRSw = mean(RGASDataRSw);
mRSOLDataRSw = mean(RSOLDataRSw);
mRTADataRSw = mean(RTADataRSw); 

mMusDataRSw =[mRHFLDataRSw; mRGLUDataRSw; mRHAMDataRSw; mRVASDataRSw; mRGASDataRSw; mRSOLDataRSw; mRTADataRSw];