%clc;
%clear all;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

TD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle
TD_ind2 = TD_ind(2:end);

gaitStart_ind = TD_ind2(1:end-1);
gaitEnd_ind = TD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

swingStart_ind = LTO_ind(2:end-1);
swingEnd_ind = gaitEnd_ind;

% resampling length
resampleSt_p = max(swingStart_ind-gaitStart_ind)+1;
resampleSw_p = max(swingEnd_ind-swingStart_ind)+1;

dataLen = length(gaitStart_ind);

%% Store kinematics and muscle data from gait cycles (Stance)

KinInd = 0;

% Preallocate memory for muscle data

LL_HFLDataSt = NaN(dataLen,resampleSt_p);
LF_GLUDataSt = NaN(dataLen,resampleSt_p);
LL_HAMDataSt = NaN(dataLen,resampleSt_p);
LF_HAMDataSt = NaN(dataLen,resampleSt_p);
LF_VASDataSt = NaN(dataLen,resampleSt_p);
LF_GASDataSt = NaN(dataLen,resampleSt_p);
LF_SOLDataSt = NaN(dataLen,resampleSt_p);
LL_TADataSt = NaN(dataLen,resampleSt_p);

RL_HFLDataSt = NaN(dataLen,resampleSt_p);
RF_GLUDataSt = NaN(dataLen,resampleSt_p);
RL_HAMDataSt = NaN(dataLen,resampleSt_p);
RF_HAMDataSt = NaN(dataLen,resampleSt_p);
RF_VASDataSt = NaN(dataLen,resampleSt_p);
RF_GASDataSt = NaN(dataLen,resampleSt_p);
RF_SOLDataSt = NaN(dataLen,resampleSt_p);
RL_TADataSt = NaN(dataLen,resampleSt_p);

TrunkDataSt = NaN(dataLen,resampleSt_p);
dTrunkDataSt = NaN(dataLen,resampleSt_p);

% Preallocate memory for muscle data

LHFLDataSt = NaN(dataLen,resampleSt_p);
LGLUDataSt = NaN(dataLen,resampleSt_p);
LHAMDataSt = NaN(dataLen,resampleSt_p);
LVASDataSt = NaN(dataLen,resampleSt_p);
LGASDataSt = NaN(dataLen,resampleSt_p);
LSOLDataSt = NaN(dataLen,resampleSt_p);
LTADataSt = NaN(dataLen,resampleSt_p);

RHFLDataSt = NaN(dataLen,resampleSt_p);
RGLUDataSt = NaN(dataLen,resampleSt_p);
RHAMDataSt = NaN(dataLen,resampleSt_p);
RVASDataSt = NaN(dataLen,resampleSt_p);
RGASDataSt = NaN(dataLen,resampleSt_p);
RSOLDataSt = NaN(dataLen,resampleSt_p);
RTADataSt = NaN(dataLen,resampleSt_p);

for i=1:dataLen
    
    LLHFLSt = LL_HFL.Data(gaitStart_ind(i):swingStart_ind(i),:); % angle
    LLHFLStR = resample(LLHFLSt,resampleSt_p,size(LLHFLSt,1)); % resample to max gait cycle length 
    
    LFGLUSt = LF_GLU.Data(gaitStart_ind(i):swingStart_ind(i),:); % angular velocity
    LFGLUStR = resample(LFGLUSt,resampleSt_p,size(LFGLUSt,1));
    
    LLHAMSt = LL_HAM.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LLHAMStR = resample(LLHAMSt,resampleSt_p,size(LLHAMSt,1));
    
    LFHAMSt = LF_HAM.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LFHAMStR = resample(LFHAMSt,resampleSt_p,size(LLHAMSt,1));
    
    LFVASSt = LF_VAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LFVASStR = resample(LFVASSt,resampleSt_p,size(LLHAMSt,1));
    
    LFGASSt = LF_GAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LFGASStR = resample(LFGASSt,resampleSt_p,size(LLHAMSt,1));
    
    LFSOLSt = LF_SOL.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LFSOLStR = resample(LFSOLSt,resampleSt_p,size(LLHAMSt,1));
   
    LLTASt = LL_TA.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LLTAStR = resample(LLTASt,resampleSt_p,size(LLHAMSt,1));
    
    RLHFLSt = RL_HFL.Data(gaitStart_ind(i):swingStart_ind(i),:); % angle
    RLHFLStR = resample(RLHFLSt,resampleSt_p,size(LLHAMSt,1)); % resample to max gait cycle length 
    
    RFGLUSt = RF_GLU.Data(gaitStart_ind(i):swingStart_ind(i),:); % angular velocity
    RFGLUStR = resample(RFGLUSt,resampleSt_p,size(LLHAMSt,1));
    
    RLHAMSt = RL_HAM.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RLHAMStR = resample(RLHAMSt,resampleSt_p,size(LLHAMSt,1));
    
    RFHAMSt = RF_HAM.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RFHAMStR = resample(RFHAMSt,resampleSt_p,size(LLHAMSt,1));
    
    RFVASSt = RF_VAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RFVASStR = resample(RFVASSt,resampleSt_p,size(LLHAMSt,1));
    
    RFGASSt = RF_GAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RFGASStR = resample(RFGASSt,resampleSt_p,size(LLHAMSt,1));
    
    RFSOLSt = RF_SOL.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RFSOLStR = resample(RFSOLSt,resampleSt_p,size(LLHAMSt,1));
   
    RLTASt = RL_TA.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RLTAStR = resample(RLTASt,resampleSt_p,size(LLHAMSt,1));
    
  
    TrunkSt = Torso.Data(gaitStart_ind(i):swingStart_ind(i),1);
    TrunkStR = resample(TrunkSt,resampleSt_p,size(LLHAMSt,1));
    dTrunkSt = Torso.Data(gaitStart_ind(i):swingStart_ind(i),2);
    dTrunkStR = resample(dTrunkSt,resampleSt_p,size(LLHAMSt,1));
    
    %KinData(KinInd*resampleSt_p+1:KinInd*resampleSt_p+resampleSt_p,:) = [LAnkR LKneeR LHipR RAnkR RKneeR RHipR TrunkR];
    
    LHFLSt = LStimHFL.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LHFLStR = resample(LHFLSt,resampleSt_p,size(LLHAMSt,1));
    
    LGLUSt = LStimGLU.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LGLUStR = resample(LGLUSt,resampleSt_p,size(LLHAMSt,1));
    
    LHAMSt = LStimHAM.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LHAMStR = resample(LHAMSt,resampleSt_p,size(LLHAMSt,1));
    
    LVASSt = LStimVAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LVASStR = resample(LVASSt,resampleSt_p,size(LLHAMSt,1));
    
    LGASSt = LStimGAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LGASStR = resample(LGASSt,resampleSt_p,size(LLHAMSt,1));
    
    LSOLSt = LStimSOL.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LSOLStR = resample(LSOLSt,resampleSt_p,size(LLHAMSt,1));
    
    LTASt = LStimTA.Data(gaitStart_ind(i):swingStart_ind(i),:);
    LTAStR = resample(LTASt,resampleSt_p,size(LLHAMSt,1));
    
    RHFLSt = RStimHFL.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RHFLStR = resample(RHFLSt,resampleSt_p,size(LLHAMSt,1));
    
    RGLUSt = RStimGLU.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RGLUStR = resample(RGLUSt,resampleSt_p,size(LLHAMSt,1));
    
    RHAMSt = RStimHAM.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RHAMStR = resample(RHAMSt,resampleSt_p,size(LLHAMSt,1));
    
    RVASSt = RStimVAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RVASStR = resample(RVASSt,resampleSt_p,size(LLHAMSt,1));
    
    RGASSt = RStimGAS.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RGASStR = resample(RGASSt,resampleSt_p,size(LLHAMSt,1));
    
    RSOLSt = RStimSOL.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RSOLStR = resample(RSOLSt,resampleSt_p,size(LLHAMSt,1));
    
    RTASt = RStimTA.Data(gaitStart_ind(i):swingStart_ind(i),:);
    RTAStR = resample(RTASt,resampleSt_p,size(LLHAMSt,1));
    
    LL_HFLDataSt(i,:) = LLHFLStR;
    LF_GLUDataSt(i,:) = LFGLUStR;
    LL_HAMDataSt(i,:) = LLHAMStR;
    LF_HAMDataSt(i,:) = LFHAMStR;
    LF_VASDataSt(i,:) = LFVASStR;
    LF_GASDataSt(i,:) = LFGASStR;
    LF_SOLDataSt(i,:) = LFSOLStR;
    LL_TADataSt(i,:) = LLTAStR;
    
    TrunkDataSt(i,:) = TrunkStR;
    
    RL_HFLDataSt(i,:) = RLHFLStR;
    RF_GLUDataSt(i,:) = RFGLUStR;
    RL_HAMDataSt(i,:) = RLHAMStR;
    RF_HAMDataSt(i,:) = RFHAMStR;
    RF_VASDataSt(i,:) = RFVASStR;
    RF_GASDataSt(i,:) = RFGASStR;
    RF_SOLDataSt(i,:) = RFSOLStR;
    RL_TADataSt(i,:) = RLTAStR;
    
    dTrunkDataSt(i,:) = dTrunkStR;
    
    LHFLDataSt(i,:) = LHFLStR;
    LGLUDataSt(i,:) = LGLUStR;
    LHAMDataSt(i,:) = LHAMStR;
    LVASDataSt(i,:) = LVASStR;
    LGASDataSt(i,:) = LGASStR;
    LSOLDataSt(i,:) = LSOLStR;
    LTADataSt(i,:) = LTAStR;
    
    RHFLDataSt(i,:) = RHFLStR;
    RGLUDataSt(i,:) = RGLUStR;
    RHAMDataSt(i,:) = RHAMStR;
    RVASDataSt(i,:) = RVASStR;
    RGASDataSt(i,:) = RGASStR;
    RSOLDataSt(i,:) = RSOLStR;
    RTADataSt(i,:) = RTAStR;
    
    KinInd = KinInd + 1; 
    
    %plot(LAnkR(:,1));
end

%% Store kinematics and muscle data from gait cycles (Swing)

% Preallocate memory for muscle data

LL_HFLDataSw = NaN(dataLen,resampleSw_p);
LF_GLUDataSw = NaN(dataLen,resampleSw_p);
LL_HAMDataSw = NaN(dataLen,resampleSw_p);
LF_HAMDataSw = NaN(dataLen,resampleSw_p);
LF_VASDataSw = NaN(dataLen,resampleSw_p);
LF_GASDataSw = NaN(dataLen,resampleSw_p);
LF_SOLDataSw = NaN(dataLen,resampleSw_p);
LL_TADataSw = NaN(dataLen,resampleSw_p);

RL_HFLDataSw = NaN(dataLen,resampleSw_p);
RF_GLUDataSw = NaN(dataLen,resampleSw_p);
RL_HAMDataSw = NaN(dataLen,resampleSw_p);
RF_HAMDataSw = NaN(dataLen,resampleSw_p);
RF_VASDataSw = NaN(dataLen,resampleSw_p);
RF_GASDataSw = NaN(dataLen,resampleSw_p);
RF_SOLDataSw = NaN(dataLen,resampleSw_p);
RL_TADataSw = NaN(dataLen,resampleSw_p);

TrunkDataSw = NaN(dataLen,resampleSw_p);
dTrunkDataSw = NaN(dataLen,resampleSw_p);

% Preallocate memory for muscle data

LHFLDataSw = NaN(dataLen,resampleSw_p);
LGLUDataSw = NaN(dataLen,resampleSw_p);
LHAMDataSw = NaN(dataLen,resampleSw_p);
LVASDataSw = NaN(dataLen,resampleSw_p);
LGASDataSw = NaN(dataLen,resampleSw_p);
LSOLDataSw = NaN(dataLen,resampleSw_p);
LTADataSw = NaN(dataLen,resampleSw_p);

RHFLDataSw = NaN(dataLen,resampleSw_p);
RGLUDataSw = NaN(dataLen,resampleSw_p);
RHAMDataSw = NaN(dataLen,resampleSw_p);
RVASDataSw = NaN(dataLen,resampleSw_p);
RGASDataSw = NaN(dataLen,resampleSw_p);
RSOLDataSw = NaN(dataLen,resampleSw_p);
RTADataSw = NaN(dataLen,resampleSw_p);

for i=1:dataLen
    
    LLHFLSw = LL_HFL.Data(swingStart_ind(i):gaitEnd_ind(i),:); % angle
    LLHFLSwR = resample(LLHFLSw,resampleSw_p,size(LLHFLSw,1)); % resample to max gait cycle length 
    
    LFGLUSw = LF_GLU.Data(swingStart_ind(i):gaitEnd_ind(i),:); % angular velocity
    LFGLUSwR = resample(LFGLUSw,resampleSw_p,size(LFGLUSw,1));
    
    LLHAMSw = LL_HAM.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LLHAMSwR = resample(LLHAMSw,resampleSw_p,size(LLHAMSw,1));
    
    LFHAMSw = LF_HAM.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LFHAMSwR = resample(LFHAMSw,resampleSw_p,size(LLHAMSw,1));
    
    LFVASSw = LF_VAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LFVASSwR = resample(LFVASSw,resampleSw_p,size(LLHAMSw,1));
    
    LFGASSw = LF_GAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LFGASSwR = resample(LFGASSw,resampleSw_p,size(LLHAMSw,1));
    
    LFSOLSw = LF_SOL.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LFSOLSwR = resample(LFSOLSw,resampleSw_p,size(LLHAMSw,1));
   
    LLTASw = LL_TA.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LLTASwR = resample(LLTASw,resampleSw_p,size(LLHAMSw,1));
    
    RLHFLSw = RL_HFL.Data(swingStart_ind(i):gaitEnd_ind(i),:); % angle
    RLHFLSwR = resample(RLHFLSw,resampleSw_p,size(LLHAMSw,1)); % resample to max gait cycle length 
    
    RFGLUSw = RF_GLU.Data(swingStart_ind(i):gaitEnd_ind(i),:); % angular velocity
    RFGLUSwR = resample(RFGLUSw,resampleSw_p,size(LLHAMSw,1));
    
    RLHAMSw = RL_HAM.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RLHAMSwR = resample(RLHAMSw,resampleSw_p,size(LLHAMSw,1));
    
    RFHAMSw = RF_HAM.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RFHAMSwR = resample(RFHAMSw,resampleSw_p,size(LLHAMSw,1));
    
    RFVASSw = RF_VAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RFVASSwR = resample(RFVASSw,resampleSw_p,size(LLHAMSw,1));
    
    RFGASSw = RF_GAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RFGASSwR = resample(RFGASSw,resampleSw_p,size(LLHAMSw,1));
    
    RFSOLSw = RF_SOL.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RFSOLSwR = resample(RFSOLSw,resampleSw_p,size(LLHAMSw,1));
   
    RLTASw = RL_TA.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RLTASwR = resample(RLTASw,resampleSw_p,size(LLHAMSw,1));
    
  
    TrunkSw = Torso.Data(swingStart_ind(i):gaitEnd_ind(i),1);
    TrunkSwR = resample(TrunkSw,resampleSw_p,size(LLHAMSw,1));
    dTrunkSw = Torso.Data(swingStart_ind(i):gaitEnd_ind(i),2);
    dTrunkSwR = resample(dTrunkSw,resampleSw_p,size(LLHAMSw,1));
    
    %KinData(KinInd*resampleSw_p+1:KinInd*resampleSw_p+resampleSw_p,:) = [LAnkR LKneeR LHipR RAnkR RKneeR RHipR TrunkR];
    
    LHFLSw = LStimHFL.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LHFLSwR = resample(LHFLSw,resampleSw_p,size(LLHAMSw,1));
    
    LGLUSw = LStimGLU.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LGLUSwR = resample(LGLUSw,resampleSw_p,size(LLHAMSw,1));
    
    LHAMSw = LStimHAM.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LHAMSwR = resample(LHAMSw,resampleSw_p,size(LLHAMSw,1));
    
    LVASSw = LStimVAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LVASSwR = resample(LVASSw,resampleSw_p,size(LLHAMSw,1));
    
    LGASSw = LStimGAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LGASSwR = resample(LGASSw,resampleSw_p,size(LLHAMSw,1));
    
    LSOLSw = LStimSOL.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LSOLSwR = resample(LSOLSw,resampleSw_p,size(LLHAMSw,1));
    
    LTASw = LStimTA.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    LTASwR = resample(LTASw,resampleSw_p,size(LLHAMSw,1));
    
    RHFLSw = RStimHFL.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RHFLSwR = resample(RHFLSw,resampleSw_p,size(LLHAMSw,1));
    
    RGLUSw = RStimGLU.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RGLUSwR = resample(RGLUSw,resampleSw_p,size(LLHAMSw,1));
    
    RHAMSw = RStimHAM.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RHAMSwR = resample(RHAMSw,resampleSw_p,size(LLHAMSw,1));
    
    RVASSw = RStimVAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RVASSwR = resample(RVASSw,resampleSw_p,size(LLHAMSw,1));
    
    RGASSw = RStimGAS.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RGASSwR = resample(RGASSw,resampleSw_p,size(LLHAMSw,1));
    
    RSOLSw = RStimSOL.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RSOLSwR = resample(RSOLSw,resampleSw_p,size(LLHAMSw,1));
    
    RTASw = RStimTA.Data(swingStart_ind(i):gaitEnd_ind(i),:);
    RTASwR = resample(RTASw,resampleSw_p,size(LLHAMSw,1));
    
    LL_HFLDataSw(i,:) = LLHFLSwR;
    LF_GLUDataSw(i,:) = LFGLUSwR;
    LL_HAMDataSw(i,:) = LLHAMSwR;
    LF_HAMDataSw(i,:) = LFHAMSwR;
    LF_VASDataSw(i,:) = LFVASSwR;
    LF_GASDataSw(i,:) = LFGASSwR;
    LF_SOLDataSw(i,:) = LFSOLSwR;
    LL_TADataSw(i,:) = LLTASwR;
    
    TrunkDataSw(i,:) = TrunkSwR;
    
    RL_HFLDataSw(i,:) = RLHFLSwR;
    RF_GLUDataSw(i,:) = RFGLUSwR;
    RL_HAMDataSw(i,:) = RLHAMSwR;
    RF_HAMDataSw(i,:) = RFHAMSwR;
    RF_VASDataSw(i,:) = RFVASSwR;
    RF_GASDataSw(i,:) = RFGASSwR;
    RF_SOLDataSw(i,:) = RFSOLSwR;
    RL_TADataSw(i,:) = RLTASwR;
    
    dTrunkDataSw(i,:) = dTrunkSwR;
    
    LHFLDataSw(i,:) = LHFLSwR;
    LGLUDataSw(i,:) = LGLUSwR;
    LHAMDataSw(i,:) = LHAMSwR;
    LVASDataSw(i,:) = LVASSwR;
    LGASDataSw(i,:) = LGASSwR;
    LSOLDataSw(i,:) = LSOLSwR;
    LTADataSw(i,:) = LTASwR;
    
    RHFLDataSw(i,:) = RHFLSwR;
    RGLUDataSw(i,:) = RGLUSwR;
    RHAMDataSw(i,:) = RHAMSwR;
    RVASDataSw(i,:) = RVASSwR;
    RGASDataSw(i,:) = RGASSwR;
    RSOLDataSw(i,:) = RSOLSwR;
    RTADataSw(i,:) = RTASwR;
    
    KinInd = KinInd + 1; 
    
    %plot(LAnkR(:,1));
end

%% Hold out test data (stance)

testNum = 5; % number of test samples
testDataInd = randi(dataLen,testNum,1);

% test data 
tLL_HFLDataSt = LL_HFLDataSt(testDataInd,:);
tLF_GLUDataSt = LF_GLUDataSt(testDataInd,:);
tLL_HAMDataSt = LL_HAMDataSt(testDataInd,:);
tLF_HAMDataSt = LF_HAMDataSt(testDataInd,:);
tLF_VASDataSt = LF_VASDataSt(testDataInd,:);
tLF_GASDataSt = LF_GASDataSt(testDataInd,:);
tLF_SOLDataSt = LF_SOLDataSt(testDataInd,:);
tLL_TADataSt = LL_TADataSt(testDataInd,:);

tTrunkDataSt = TrunkDataSt(testDataInd,:);

tRL_HFLDataSt = RL_HFLDataSt(testDataInd,:);
tRF_GLUDataSt = RF_GLUDataSt(testDataInd,:);
tRL_HAMDataSt = RL_HAMDataSt(testDataInd,:);
tRF_HAMDataSt = RF_HAMDataSt(testDataInd,:);
tRF_VASDataSt = RF_VASDataSt(testDataInd,:);
tRF_GASDataSt = RF_GASDataSt(testDataInd,:);
tRF_SOLDataSt = RF_SOLDataSt(testDataInd,:);
tRL_TADataSt = RL_TADataSt(testDataInd,:);

tdTrunkDataSt = dTrunkDataSt(testDataInd,:);

tLHFLDataSt = LHFLDataSt(testDataInd,:);
tLGLUDataSt = LGLUDataSt(testDataInd,:);
tLHAMDataSt = LHAMDataSt(testDataInd,:);
tLVASDataSt = LVASDataSt(testDataInd,:);
tLGASDataSt = LGASDataSt(testDataInd,:);
tLSOLDataSt = LSOLDataSt(testDataInd,:);
tLTADataSt = LTADataSt(testDataInd,:);

tRHFLDataSt = RHFLDataSt(testDataInd,:);
tRGLUDataSt = RGLUDataSt(testDataInd,:);
tRHAMDataSt = RHAMDataSt(testDataInd,:);
tRVASDataSt = RVASDataSt(testDataInd,:);
tRGASDataSt = RGASDataSt(testDataInd,:);
tRSOLDataSt = RSOLDataSt(testDataInd,:);
tRTADataSt = RTADataSt(testDataInd,:);

% Remove test data from all data

LL_HFLDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LF_GLUDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LL_HAMDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LF_HAMDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LF_VASDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LF_GASDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LF_SOLDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
LL_TADataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);

TrunkDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);

RL_HFLDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RF_GLUDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RL_HAMDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RF_HAMDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RF_VASDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RF_GASDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RF_SOLDataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);
RL_TADataSt(testDataInd,:)= zeros(length(testDataInd),resampleSt_p);

dTrunkDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);

LHFLDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
LGLUDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
LHAMDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
LVASDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
LGASDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
LSOLDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
LTADataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);

RHFLDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
RGLUDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
RHAMDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
RVASDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
RGASDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
RSOLDataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);
RTADataSt(testDataInd,:) = zeros(length(testDataInd),resampleSt_p);

%% Hold out test data (swing)

% test data 
tLL_HFLDataSw = LL_HFLDataSw(testDataInd,:);
tLF_GLUDataSw = LF_GLUDataSw(testDataInd,:);
tLL_HAMDataSw = LL_HAMDataSw(testDataInd,:);
tLF_HAMDataSw = LF_HAMDataSw(testDataInd,:);
tLF_VASDataSw = LF_VASDataSw(testDataInd,:);
tLF_GASDataSw = LF_GASDataSw(testDataInd,:);
tLF_SOLDataSw = LF_SOLDataSw(testDataInd,:);
tLL_TADataSw = LL_TADataSw(testDataInd,:);

tTrunkDataSw = TrunkDataSw(testDataInd,:);

tRL_HFLDataSw = RL_HFLDataSw(testDataInd,:);
tRF_GLUDataSw = RF_GLUDataSw(testDataInd,:);
tRL_HAMDataSw = RL_HAMDataSw(testDataInd,:);
tRF_HAMDataSw = RF_HAMDataSw(testDataInd,:);
tRF_VASDataSw = RF_VASDataSw(testDataInd,:);
tRF_GASDataSw = RF_GASDataSw(testDataInd,:);
tRF_SOLDataSw = RF_SOLDataSw(testDataInd,:);
tRL_TADataSw = RL_TADataSw(testDataInd,:);

tdTrunkDataSw = dTrunkDataSw(testDataInd,:);

tLHFLDataSw = LHFLDataSw(testDataInd,:);
tLGLUDataSw = LGLUDataSw(testDataInd,:);
tLHAMDataSw = LHAMDataSw(testDataInd,:);
tLVASDataSw = LVASDataSw(testDataInd,:);
tLGASDataSw = LGASDataSw(testDataInd,:);
tLSOLDataSw = LSOLDataSw(testDataInd,:);
tLTADataSw = LTADataSw(testDataInd,:);

tRHFLDataSw = RHFLDataSw(testDataInd,:);
tRGLUDataSw = RGLUDataSw(testDataInd,:);
tRHAMDataSw = RHAMDataSw(testDataInd,:);
tRVASDataSw = RVASDataSw(testDataInd,:);
tRGASDataSw = RGASDataSw(testDataInd,:);
tRSOLDataSw = RSOLDataSw(testDataInd,:);
tRTADataSw = RTADataSw(testDataInd,:);

% Remove test data from all data

LL_HFLDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LF_GLUDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LL_HAMDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LF_HAMDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LF_VASDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LF_GASDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LF_SOLDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
LL_TADataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);

TrunkDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);

RL_HFLDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RF_GLUDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RL_HAMDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RF_HAMDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RF_VASDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RF_GASDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RF_SOLDataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);
RL_TADataSw(testDataInd,:)= zeros(length(testDataInd),resampleSw_p);

dTrunkDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);

LHFLDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
LGLUDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
LHAMDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
LVASDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
LGASDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
LSOLDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
LTADataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);

RHFLDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
RGLUDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
RHAMDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
RVASDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
RGASDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
RSOLDataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);
RTADataSw(testDataInd,:) = zeros(length(testDataInd),resampleSw_p);

%% Mean data (stance)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataSt = mean(LL_HFLDataSt);
mLF_GLUDataSt = mean(LF_GLUDataSt);
mLL_HAMDataSt = mean(LL_HAMDataSt);
mLF_HAMDataSt = mean(LF_HAMDataSt);
mLF_VASDataSt = mean(LF_VASDataSt);
mLF_GASDataSt = mean(LF_GASDataSt);
mLF_SOLDataSt = mean(LF_SOLDataSt);
mLL_TADataSt = mean(LL_TADataSt);

mTrunkDataSt = mean(TrunkDataSt);

mRL_HFLDataSt = mean(RL_HFLDataSt);
mRF_GLUDataSt = mean(RF_GLUDataSt);
mRL_HAMDataSt = mean(RL_HAMDataSt);
mRF_HAMDataSt = mean(RF_HAMDataSt);
mRF_VASDataSt = mean(RF_VASDataSt);
mRF_GASDataSt = mean(RF_GASDataSt);
mRF_SOLDataSt = mean(RF_SOLDataSt);
mRL_TADataSt = mean(RL_TADataSt);

mdTrunkDataSt = mean(dTrunkDataSt);


% collect mean muscle features over gaits (only training)
mFeatDataSt = [mLL_HFLDataSt; mLF_GLUDataSt; mLL_HAMDataSt; mLF_HAMDataSt; mLF_VASDataSt; mLF_GASDataSt; mLF_SOLDataSt; mLL_TADataSt; mTrunkDataSt; ...
            mRL_HFLDataSt; mRF_GLUDataSt; mRL_HAMDataSt; mRF_HAMDataSt; mRF_VASDataSt; mRF_GASDataSt; mRF_SOLDataSt; mRL_TADataSt; mdTrunkDataSt];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataSt = mean(LHFLDataSt);
mLGLUDataSt = mean(LGLUDataSt);
mLHAMDataSt = mean(LHAMDataSt);
mLVASDataSt = mean(LVASDataSt);
mLGASDataSt = mean(LGASDataSt);
mLSOLDataSt = mean(LSOLDataSt);
mLTADataSt = mean(LTADataSt); 
mRHFLDataSt = mean(RHFLDataSt);
mRGLUDataSt = mean(RGLUDataSt);
mRHAMDataSt = mean(RHAMDataSt);
mRVASDataSt = mean(RVASDataSt);
mRGASDataSt = mean(RGASDataSt);
mRSOLDataSt = mean(RSOLDataSt);
mRTADataSt = mean(RTADataSt); 

% collect mean muscles over gaits (only training)
mMusDataSt = [mLHFLDataSt; mLGLUDataSt; mLHAMDataSt; mLVASDataSt; mLGASDataSt; mLSOLDataSt; mLTADataSt; ...
            mRHFLDataSt; mRGLUDataSt; mRHAMDataSt; mRVASDataSt; mRGASDataSt; mRSOLDataSt; mRTADataSt];
        
%% Mean data (swing)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataSw = mean(LL_HFLDataSw);
mLF_GLUDataSw = mean(LF_GLUDataSw);
mLL_HAMDataSw = mean(LL_HAMDataSw);
mLF_HAMDataSw = mean(LF_HAMDataSw);
mLF_VASDataSw = mean(LF_VASDataSw);
mLF_GASDataSw = mean(LF_GASDataSw);
mLF_SOLDataSw = mean(LF_SOLDataSw);
mLL_TADataSw = mean(LL_TADataSw);

mTrunkDataSw = mean(TrunkDataSw);

mRL_HFLDataSw = mean(RL_HFLDataSw);
mRF_GLUDataSw = mean(RF_GLUDataSw);
mRL_HAMDataSw = mean(RL_HAMDataSw);
mRF_HAMDataSw = mean(RF_HAMDataSw);
mRF_VASDataSw = mean(RF_VASDataSw);
mRF_GASDataSw = mean(RF_GASDataSw);
mRF_SOLDataSw = mean(RF_SOLDataSw);
mRL_TADataSw = mean(RL_TADataSw);

mdTrunkDataSw = mean(dTrunkDataSw);


% collect mean muscle features over gaits (only training)
mFeatDataSw = [mLL_HFLDataSw; mLF_GLUDataSw; mLL_HAMDataSw; mLF_HAMDataSw; mLF_VASDataSw; mLF_GASDataSw; mLF_SOLDataSw; mLL_TADataSw; mTrunkDataSw; ...
            mRL_HFLDataSw; mRF_GLUDataSw; mRL_HAMDataSw; mRF_HAMDataSw; mRF_VASDataSw; mRF_GASDataSw; mRF_SOLDataSw; mRL_TADataSw; mdTrunkDataSw];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataSw = mean(LHFLDataSw);
mLGLUDataSw = mean(LGLUDataSw);
mLHAMDataSw = mean(LHAMDataSw);
mLVASDataSw = mean(LVASDataSw);
mLGASDataSw = mean(LGASDataSw);
mLSOLDataSw = mean(LSOLDataSw);
mLTADataSw = mean(LTADataSw); 
mRHFLDataSw = mean(RHFLDataSw);
mRGLUDataSw = mean(RGLUDataSw);
mRHAMDataSw = mean(RHAMDataSw);
mRVASDataSw = mean(RVASDataSw);
mRGASDataSw = mean(RGASDataSw);
mRSOLDataSw = mean(RSOLDataSw);
mRTADataSw = mean(RTADataSw); 

% collect mean muscles over gaits (only training)
mMusDataSw = [mLHFLDataSw; mLGLUDataSw; mLHAMDataSw; mLVASDataSw; mLGASDataSw; mLSOLDataSw; mLTADataSw; ...
            mRHFLDataSw; mRGLUDataSw; mRHAMDataSw; mRVASDataSw; mRGASDataSw; mRSOLDataSw; mRTADataSw];