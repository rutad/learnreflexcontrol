function [R2_adjst] = adjusted_rsquared(true,test,w)

R2 = rsquared(true,test);

n = size(true,2);
p = length(find(w~=0));

% reference: wikipedia: https://en.wikipedia.org/wiki/Coefficient_of_determination
% and http://web.maths.unsw.edu.au/~adelle/Garvan/Assays/GoodnessOfFit.html
R2_adjst = R2 - (1-R2)*((p)/(n-p-1));

end