% Relate kinematics data to muscle stimulation
clc;
close all;
clear all;

%load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSw_v2_RfeatFlip.mat;
load linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat;

%% Hold out data

holdFeatLSt = NaN(size(mFeatDataLSt,1),testNum*size(tLL_HAMDataLSt,2));
holdMusLSt = NaN(size(mMusDataLSt,1),testNum*size(tLHFLDataLSt,2));

holdFeatLSw = NaN(size(mFeatDataLSw,1),testNum*size(tLL_HAMDataLSw,2));
holdMusLSw = NaN(size(mMusDataLSw,1),testNum*size(tLHFLDataLSw,2));

tStartStL = 1;
tStartSwL = 1;

for t = 1:testNum; % index of test sample
    
    tEndStL = size(tLL_HAMDataLSt,2)+tStartStL-1;
    tEndSwL = size(tLL_HAMDataLSw,2)+tStartSwL-1;
    
    % left stance
    testFeatStL = [tLL_HFLDataLSt(t,:); tLF_GLUDataLSt(t,:); tLL_HAMDataLSt(t,:); tLF_HAMDataLSt(t,:); tLF_VASDataLSt(t,:); tLF_GASDataLSt(t,:); tLF_SOLDataLSt(t,:); tLL_TADataLSt(t,:); tTrunkDataLSt(t,:); ...
                tRL_HFLDataLSt(t,:); tRF_GLUDataLSt(t,:); tRL_HAMDataLSt(t,:); tRF_HAMDataLSt(t,:); tRF_VASDataLSt(t,:); tRF_GASDataLSt(t,:); tRF_SOLDataLSt(t,:); tRL_TADataLSt(t,:); tdTrunkDataLSt(t,:)];

    testMusStL = [tLHFLDataLSt(t,:); tLGLUDataLSt(t,:); tLHAMDataLSt(t,:); tLVASDataLSt(t,:); tLGASDataLSt(t,:); tLSOLDataLSt(t,:); tLTADataLSt(t,:)]; 
    
    % left swing
    testFeatSwL = [tLL_HFLDataLSw(t,:); tLF_GLUDataLSw(t,:); tLL_HAMDataLSw(t,:); tLF_HAMDataLSw(t,:); tLF_VASDataLSw(t,:); tLF_GASDataLSw(t,:); tLF_SOLDataLSw(t,:); tLL_TADataLSw(t,:); tTrunkDataLSw(t,:); ...
                tRL_HFLDataLSw(t,:); tRF_GLUDataLSw(t,:); tRL_HAMDataLSw(t,:); tRF_HAMDataLSw(t,:); tRF_VASDataLSw(t,:); tRF_GASDataLSw(t,:); tRF_SOLDataLSw(t,:); tRL_TADataLSw(t,:); tdTrunkDataLSw(t,:)];

    testMusSwL = [tLHFLDataLSw(t,:); tLGLUDataLSw(t,:); tLHAMDataLSw(t,:); tLVASDataLSw(t,:); tLGASDataLSw(t,:); tLSOLDataLSw(t,:); tLTADataLSw(t,:)]; 
    
    
    holdFeatLSt(:,tStartStL:tEndStL) = testFeatStL;
    holdMusLSt(:,tStartStL:tEndStL) = testMusStL;
    holdFeatLSw(:,tStartSwL:tEndSwL) = testFeatSwL;
    holdMusLSw(:,tStartSwL:tEndSwL) = testMusSwL;
    
    tStartStL = tEndStL + 1;
    tStartSwL = tEndSwL + 1;
    
end

holdFeatRSt = NaN(size(mFeatDataRSt,1),testNum*size(tRL_HAMDataRSt,2));
holdMusRSt = NaN(size(mMusDataRSt,1),testNum*size(tRHFLDataRSt,2));

holdFeatRSw = NaN(size(mFeatDataRSw,1),testNum*size(tRL_HAMDataRSw,2));
holdMusRSw = NaN(size(mMusDataRSw,1),testNum*size(tRHFLDataRSw,2));

tStartStR = 1;
tStartSwR = 1;

for t = 1:testNum; % index of test sample
    
    tEndStR = size(tRL_HAMDataRSt,2)+tStartStR-1;
    tEndSwR = size(tRL_HAMDataRSw,2)+tStartSwR-1;
    
    % right stance
    testFeatStR = [tRL_HFLDataRSt(t,:); tRF_GLUDataRSt(t,:); tRL_HAMDataRSt(t,:); tRF_HAMDataRSt(t,:); tRF_VASDataRSt(t,:); tRF_GASDataRSt(t,:); tRF_SOLDataRSt(t,:); tRL_TADataRSt(t,:); tTrunkDataRSt(t,:);...
        tLL_HFLDataRSt(t,:); tLF_GLUDataRSt(t,:); tLL_HAMDataRSt(t,:); tLF_HAMDataRSt(t,:); tLF_VASDataRSt(t,:); tLF_GASDataRSt(t,:); tLF_SOLDataRSt(t,:); tLL_TADataRSt(t,:); tdTrunkDataRSt(t,:)];
       
    testMusStR = [tRHFLDataRSt(t,:); tRGLUDataRSt(t,:); tRHAMDataRSt(t,:); tRVASDataRSt(t,:); tRGASDataRSt(t,:); tRSOLDataRSt(t,:); tRTADataRSt(t,:)];  
    
    % right swing
    testFeatSwR = [tRL_HFLDataRSw(t,:); tRF_GLUDataRSw(t,:); tRL_HAMDataRSw(t,:); tRF_HAMDataRSw(t,:); tRF_VASDataRSw(t,:); tRF_GASDataRSw(t,:); tRF_SOLDataRSw(t,:); tRL_TADataRSw(t,:); tTrunkDataRSw(t,:);...
        tLL_HFLDataRSw(t,:); tLF_GLUDataRSw(t,:); tLL_HAMDataRSw(t,:); tLF_HAMDataRSw(t,:); tLF_VASDataRSw(t,:); tLF_GASDataRSw(t,:); tLF_SOLDataRSw(t,:); tLL_TADataRSw(t,:); tdTrunkDataRSw(t,:)];
       
    testMusSwR = [tRHFLDataRSw(t,:); tRGLUDataRSw(t,:); tRHAMDataRSw(t,:); tRVASDataRSw(t,:); tRGASDataRSw(t,:); tRSOLDataRSw(t,:); tRTADataRSw(t,:)]; 
    
    
    holdFeatRSt(:,tStartStR:tEndStR) = testFeatStR;
    holdMusRSt(:,tStartStR:tEndStR) = testMusStR;
    holdFeatRSw(:,tStartSwR:tEndSwR) = testFeatSwR;
    holdMusRSw(:,tStartSwR:tEndSwR) = testMusSwR;
    
    tStartStR = tEndStR + 1;
    tStartSwR = tEndSwR + 1;
    
end
%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

%%%%% STANCE %%%%%%%%

% only least square solution using pinv
wp_LSt = mMusDataLSt*pinv(mFeatDataLSt); % wp is 14X14
wp_RSt = mMusDataRSt*pinv(mFeatDataRSt); % wp is 14X14

% l1-least square
wRSt = NaN(size(wp_RSt));
wLSt = NaN(size(wp_LSt));

ALSt = mFeatDataLSt';
ARSt = mFeatDataRSt';

err_tol_min = 1e-4;
MSE_errThSt = 0.001;
MSE_errThSw = 0.00001;%0.000001;

RMSE_errThSt = 0.001;%0.01;
RMSE_errThSw = 0.00001;%0.01;

lambdaLSt = NaN(size(mMusDataLSt,1),1);
lambdaLSw = NaN(size(mMusDataLSt,1),1);
lambdaRSt = NaN(size(mMusDataLSt,1),1);
lambdaRSw = NaN(size(mMusDataLSt,1),1);

relerrHoldLSt = NaN(size(mMusDataLSt,1),1);
relerrHoldLSw = NaN(size(mMusDataLSt,1),1);
relerrHoldRSt = NaN(size(mMusDataLSt,1),1);
relerrHoldRSw = NaN(size(mMusDataLSt,1),1);

for i = 1:7

    relerrLSt = 0;
    relerrRSt = 0;
    
    errLSt = 0;
    errRSt = 0;
    
    multThLSt = err_tol_min;
    multThRSt = err_tol_min;
    
    BLSt = mMusDataLSt(i,:)';
    
    if (max(BLSt)>0.05)
        [lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
        lambdaSt = lambda_max*multThLSt;
        [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt,[],'quiet');
        errLSt = mean((holdMusLSt(i,:)-xLSt'*holdFeatLSt).^2);
        relerrLSt = errLSt/max(holdMusLSt(i,:));

        while (relerrLSt<RMSE_errThSt && errLSt<MSE_errThSt && lambdaSt<=lambda_max)
            
            multThLSt = multThLSt*2;
            lambdaSt = lambda_max*multThLSt; 
            [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt,[],'quiet');        
            errLSt = mean((holdMusLSt(i,:)-xLSt'*holdFeatLSt).^2);
            relerrLSt = errLSt/max(holdMusLSt(i,:));

        end     
        
        lambdaSt = lambda_max*(multThLSt/2);
        [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt,[],'quiet');
        errLSt = mean((holdMusLSt(i,:)-xLSt'*holdFeatLSt).^2);  
        relerrLSt = errLSt/max(holdMusLSt(i,:));
        
        relerrHoldLSt(i) = relerrLSt;
        lambdaLSt(i) = lambdaSt;
        wLSt(i,:) = xLSt';
        
    else
        wLSt(i,:) = zeros(size(wLSt,2),1);
    end

    BRSt = mMusDataRSt(i,:)';      
    
    if (max(BRSt)>0.05)
        [lambda_max] = find_lambdamax_l1_ls(ARSt',BRSt);
        lambdaSt = lambda_max*multThRSt;
        [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt,[],'quiet');
        errRSt = mean((holdMusRSt(i,:)-xRSt'*holdFeatRSt).^2);
        relerrRSt = errRSt/max(holdMusRSt(i,:));

        while (relerrRSt<RMSE_errThSt && errRSt<MSE_errThSt && lambdaSt<=lambda_max)
            
            multThRSt = multThRSt*2;
            
            lambdaSt = lambda_max*multThRSt;      
            [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt,[],'quiet');        
            errRSt = mean((holdMusRSt(i,:)-xRSt'*holdFeatRSt).^2);
            relerrRSt = errRSt/max(holdMusRSt(i,:));

        end
        
        lambdaSt = lambda_max*(multThRSt/2);
        [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt,[],'quiet');
        errRSt = mean((holdMusRSt(i,:)-xRSt'*holdFeatRSt).^2);
        relerrRSt = errRSt/max(holdMusRSt(i,:));
        
        relerrHoldRSt(i) = relerrRSt;
        lambdaRSt(i) = lambdaSt;
        wRSt(i,:) = xRSt';
        
    else
        wRSt(i,:) = zeros(size(wRSt,2),1);
    end
       
end

%%
%%%%%%%%% SWING %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% only least square solution using pinv
wp_LSw = mMusDataLSw*pinv(mFeatDataLSw); % wp is 14X14
wp_RSw = mMusDataRSw*pinv(mFeatDataRSw); % wp is 14X14

% l1-least square
wLSw = NaN(size(wp_LSw));
wRSw = NaN(size(wp_RSw));

ALSw = mFeatDataLSw';
ARSw = mFeatDataRSw';

for i = 1:7
    
    relerrLSw = 0;
    relerrRSw = 0;
    
    errLSw = 0;
    errRSw = 0;
    
    multThLSw = err_tol_min;
    multThRSw = err_tol_min;
    
    BLSw = mMusDataLSw(i,:)';
    
    if (max(BLSw)-min(BLSw)>0.02)
        [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
        lambdaSw = lambda_max*multThLSw;
        [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw,[],'quiet');
        errLSw = mean((holdMusLSw(i,:)-xLSw'*holdFeatLSw).^2);
        relerrLSw = errLSw/max(holdMusLSw(i,:));
        while (relerrLSw<RMSE_errThSw && errLSw<MSE_errThSw && lambdaSw<=lambda_max)
            
            multThLSw = multThLSw*2;
            
            lambdaSw = lambda_max*multThLSw;      
            [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw,[],'quiet');        
            errLSw = mean((holdMusLSw(i,:)-xLSw'*holdFeatLSw).^2);
            relerrLSw = errLSw/max(holdMusLSw(i,:));

        end
        
        lambdaSw = lambda_max*(multThLSw/2);
        [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw,[],'quiet');
        errLSw = mean((holdMusLSw(i,:)-xLSw'*holdFeatLSw).^2);        
        relerrLSw = errLSw/max(holdMusLSw(i,:));
        
        relerrHoldLSw(i) = relerrLSw;
        lambdaLSw(i) = lambdaSw;
        wLSw(i,:) = xLSw';
    else
        wLSw(i,:) = zeros(size(wLSw,2),1);
    end

    BRSw = mMusDataRSw(i,:)';      
    
    if (max(BRSw)-min(BRSw)>0.02)
        [lambda_max] = find_lambdamax_l1_ls(ARSw',BRSw);
        lambdaSw = lambda_max*multThRSw;
        [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw,[],'quiet');
        errRSw = mean((holdMusRSw(i,:)-xRSw'*holdFeatRSw).^2);
        relerrRSw = errRSw/max(holdMusRSw(i,:));
        while (relerrRSw<RMSE_errThSw && errRSw<MSE_errThSw && lambdaSw<=lambda_max)
            
            multThRSw = multThRSw*2;
            
            lambdaSw = lambda_max*multThRSw;      
            [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw,[],'quiet');        
            errRSw = mean((holdMusRSw(i,:)-xRSw'*holdFeatRSw).^2);
            relerrRSw = errRSw/max(holdMusRSw(i,:));

        end
        
        lambdaSw = lambda_max*(multThRSw/2);
        [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw,[],'quiet');
        errRSw = mean((holdMusRSw(i,:)-xRSw'*holdFeatRSw).^2);        
        relerrRSw = errRSw/max(holdMusRSw(i,:));
        
        relerrHoldRSw(i) = relerrRSw;
        lambdaRSw(i) = lambdaSw;
        wRSw(i,:) = xRSw';
    else
        wRSw(i,:) = zeros(size(wRSw,2),1);
    end

end

%% Test weight w on a particular sample gait left leg     

t = randi(testNum,1,1); % index of test sample

% left stance
testFeatLSt = [tLL_HFLDataLSt(t,:); tLF_GLUDataLSt(t,:); tLL_HAMDataLSt(t,:); tLF_HAMDataLSt(t,:); tLF_VASDataLSt(t,:); tLF_GASDataLSt(t,:); tLF_SOLDataLSt(t,:); tLL_TADataLSt(t,:); tTrunkDataLSt(t,:); ...
            tRL_HFLDataLSt(t,:); tRF_GLUDataLSt(t,:); tRL_HAMDataLSt(t,:); tRF_HAMDataLSt(t,:); tRF_VASDataLSt(t,:); tRF_GASDataLSt(t,:); tRF_SOLDataLSt(t,:); tRL_TADataLSt(t,:); tdTrunkDataLSt(t,:)];
       
trueMusLSt = [tLHFLDataLSt(t,:); tLGLUDataLSt(t,:); tLHAMDataLSt(t,:); tLVASDataLSt(t,:); tLGASDataLSt(t,:); tLSOLDataLSt(t,:); tLTADataLSt(t,:)]; 
 
testMusLSt = wLSt*testFeatLSt;        
testMusp_LSt = wp_LSt*testFeatLSt;

% left swing
testFeatLSw = [tLL_HFLDataLSw(t,:); tLF_GLUDataLSw(t,:); tLL_HAMDataLSw(t,:); tLF_HAMDataLSw(t,:); tLF_VASDataLSw(t,:); tLF_GASDataLSw(t,:); tLF_SOLDataLSw(t,:); tLL_TADataLSw(t,:); tTrunkDataLSw(t,:); ...
            tRL_HFLDataLSw(t,:); tRF_GLUDataLSw(t,:); tRL_HAMDataLSw(t,:); tRF_HAMDataLSw(t,:); tRF_VASDataLSw(t,:); tRF_GASDataLSw(t,:); tRF_SOLDataLSw(t,:); tRL_TADataLSw(t,:); tdTrunkDataLSw(t,:)];
       
trueMusLSw = [tLHFLDataLSw(t,:); tLGLUDataLSw(t,:); tLHAMDataLSw(t,:); tLVASDataLSw(t,:); tLGASDataLSw(t,:); tLSOLDataLSw(t,:); tLTADataLSw(t,:)];
            
 
testMusLSw = wLSw*testFeatLSw;        
testMusp_LSw = wp_LSw*testFeatLSw;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % right stance
testFeatRSt = [tRL_HFLDataRSt(t,:); tRF_GLUDataRSt(t,:); tRL_HAMDataRSt(t,:); tRF_HAMDataRSt(t,:); tRF_VASDataRSt(t,:); tRF_GASDataRSt(t,:); tRF_SOLDataRSt(t,:); tRL_TADataRSt(t,:); tTrunkDataRSt(t,:);...
    tLL_HFLDataRSt(t,:); tLF_GLUDataRSt(t,:); tLL_HAMDataRSt(t,:); tLF_HAMDataRSt(t,:); tLF_VASDataRSt(t,:); tLF_GASDataRSt(t,:); tLF_SOLDataRSt(t,:); tLL_TADataRSt(t,:); tdTrunkDataRSt(t,:)];
       
trueMusRSt = [tRHFLDataRSt(t,:); tRGLUDataRSt(t,:); tRHAMDataRSt(t,:); tRVASDataRSt(t,:); tRGASDataRSt(t,:); tRSOLDataRSt(t,:); tRTADataRSt(t,:)]; 
 
testMusRSt = wRSt*testFeatRSt;        
testMusp_RSt = wp_RSt*testFeatRSt;

% right swing
testFeatRSw = [tRL_HFLDataRSw(t,:); tRF_GLUDataRSw(t,:); tRL_HAMDataRSw(t,:); tRF_HAMDataRSw(t,:); tRF_VASDataRSw(t,:); tRF_GASDataRSw(t,:); tRF_SOLDataRSw(t,:); tRL_TADataRSw(t,:); tTrunkDataRSw(t,:);...
    tLL_HFLDataRSw(t,:); tLF_GLUDataRSw(t,:); tLL_HAMDataRSw(t,:); tLF_HAMDataRSw(t,:); tLF_VASDataRSw(t,:); tLF_GASDataRSw(t,:); tLF_SOLDataRSw(t,:); tLL_TADataRSw(t,:); tdTrunkDataRSw(t,:)];
       
trueMusRSw = [tRHFLDataRSw(t,:); tRGLUDataRSw(t,:); tRHAMDataRSw(t,:); tRVASDataRSw(t,:); tRGASDataRSw(t,:); tRSOLDataRSw(t,:); tRTADataRSw(t,:)]; 
            
 
testMusRSw = wRSw*testFeatRSw;        
testMusp_RSw = wp_RSw*testFeatRSw;

% test error 
errTestRSt = NaN(size(testMusRSt,1),1);
errTestRSw = NaN(size(testMusRSw,1),1);
errTestLSt = NaN(size(testMusLSt,1),1);
errTestLSw = NaN(size(testMusLSw,1),1);

relerrTestRSt = NaN(size(testMusRSt,1),1);
relerrTestRSw = NaN(size(testMusRSw,1),1);
relerrTestLSt = NaN(size(testMusLSt,1),1);
relerrTestLSw = NaN(size(testMusLSw,1),1);

for m = 1:size(testMusRSt,1)
    
    errTestRSt(m) = mean((testMusRSt(m,:)-trueMusRSt(m,:)).^2);
    errTestRSw(m) = mean((testMusRSw(m,:)-trueMusRSw(m,:)).^2);
    errTestLSt(m) = mean((testMusLSt(m,:)-trueMusLSt(m,:)).^2);
    errTestLSw(m) = mean((testMusLSw(m,:)-trueMusLSw(m,:)).^2);
    
    relerrTestRSt(m) = mean((testMusRSt(m,:)-trueMusRSt(m,:)).^2)/max(trueMusRSt(m,:));
    relerrTestRSw(m) = mean((testMusRSw(m,:)-trueMusRSw(m,:)).^2)/max(trueMusRSw(m,:));
    relerrTestLSt(m) = mean((testMusLSt(m,:)-trueMusLSt(m,:)).^2)/max(trueMusLSt(m,:));
    relerrTestLSw(m) = mean((testMusLSw(m,:)-trueMusLSw(m,:)).^2)/max(trueMusLSw(m,:));

end

%% visualize stance
%cd linRel_Results/

figure;
subplot(7,1,1); plot(testMusLSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_LSt(1,:)');
subplot(7,1,1); plot(trueMusLSt(1,:)');
title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_LSt(2,:)');
subplot(7,1,2); plot(trueMusLSt(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_LSt(3,:)');
subplot(7,1,3); plot(trueMusLSt(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_LSt(4,:)');
subplot(7,1,4); plot(trueMusLSt(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_LSt(5,:)');
subplot(7,1,5); plot(trueMusLSt(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_LSt(6,:)');
subplot(7,1,6); plot(trueMusLSt(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_LSt(7,:)');
subplot(7,1,7); plot(trueMusLSt(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_MusFeatStSw_noDelL_LSt.pdf

%%
figure;
subplot(7,1,1); plot(testMusRSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_RSt(1,:)');
subplot(7,1,1); plot(trueMusRSt(1,:)');
title('Right muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_RSt(2,:)');
subplot(7,1,2); plot(trueMusRSt(2,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_RSt(3,:)');
subplot(7,1,3); plot(trueMusRSt(3,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_RSt(4,:)');
subplot(7,1,4); plot(trueMusRSt(4,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_RSt(5,:)');
subplot(7,1,5); plot(trueMusRSt(5,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_RSt(6,:)');
subplot(7,1,6); plot(trueMusRSt(6,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_RSt(7,:)');
subplot(7,1,7); plot(trueMusRSt(7,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_MusFeatStSw_noDelL_RSt.pdf

%% visualize swing 

figure;
subplot(7,1,1); plot(testMusLSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_LSw(1,:)');
subplot(7,1,1); plot(trueMusLSw(1,:)');
title('Left muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_LSw(2,:)');
subplot(7,1,2); plot(trueMusLSw(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_LSw(3,:)');
subplot(7,1,3); plot(trueMusLSw(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_LSw(4,:)');
subplot(7,1,4); plot(trueMusLSw(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_LSw(5,:)');
subplot(7,1,5); plot(trueMusLSw(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_LSw(6,:)');
subplot(7,1,6); plot(trueMusLSw(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_LSw(7,:)');
subplot(7,1,7); plot(trueMusLSw(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_MusFeatStSw_noDelL_LSw.pdf

figure;
subplot(7,1,1); plot(testMusRSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_RSw(1,:)');
subplot(7,1,1); plot(trueMusRSw(1,:)');
title('Right muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_RSw(2,:)');
subplot(7,1,2); plot(trueMusRSw(2,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_RSw(3,:)');
subplot(7,1,3); plot(trueMusRSw(3,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_RSw(4,:)');
subplot(7,1,4); plot(trueMusRSw(4,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_RSw(5,:)');
subplot(7,1,5); plot(trueMusRSw(5,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_RSw(6,:)');
subplot(7,1,6); plot(trueMusRSw(6,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_RSw(7,:)');
subplot(7,1,7); plot(trueMusRSw(7,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_MusFeatStSw_noDelL_RSw.pdf

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

%% left stance w
figure;imagesc(abs(wp_LSt)/(max(max(abs(wp_LSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wLSt)/(max(max(abs(wLSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% left swing w
figure;imagesc(abs(wp_LSw)/(max(max(abs(wp_LSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wLSw)/(max(max(abs(wLSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% Visualize w RIGHT

labels_ry = {'RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_rx = {'RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','Trunk','LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','dTrunk'};

%% right stance w
figure;imagesc(abs(wp_RSt)/(max(max(abs(wp_RSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wRSt)/(max(max(abs(wRSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

%% right swing w
figure;imagesc(abs(wp_RSw)/(max(max(abs(wp_RSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wRSw)/(max(max(abs(wRSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

%cd ..