%clc;
%clear all;

numMusLFFeatures = 16; %(+2 trunk Kin features)
numMusFeatures = 14;
KinInd = 0;

%% finding gait cycles starting from left leg touch down (TD) to its TD again.
% further breaking gait cycle into swing and stance

% Left leg
LTD_ind = find (L_TD.Data==1);

% as model starts with left stance neglect the first cycle`
LTD_ind2 = LTD_ind(2:end);

LgaitStart_ind = LTD_ind2(1:end-1);
LgaitEnd_ind = LTD_ind2(2:end);

LTO_ind = find(L_TO.Data==1);

LswingStart_ind = LTO_ind(2:end-1);%LTO_ind(2:end);
LswingEnd_ind = LgaitEnd_ind;

% Right leg
RTD_ind2 = find (R_TD.Data==1);

RgaitStart_ind = RTD_ind2(2:end-1);
RgaitEnd_ind = RTD_ind2(3:end);

RTO_ind = find(R_TO.Data==1);

RswingStart_ind = RTO_ind(3:end);%RTO_ind(3:end-1);
RswingEnd_ind = RgaitEnd_ind;

% double support left
LDsStart_ind = LgaitStart_ind(2:end);
LDsEnd_ind = RswingStart_ind(1:end-1);

% double support right
RDsStart_ind = RgaitStart_ind;
RDsEnd_ind = LswingStart_ind;


% resampling length left
resampleLDs_p = max(LDsEnd_ind-LDsStart_ind)+1;

LdataLen = length(LDsStart_ind);

% resample length right
resampleRDs_p = max(RDsEnd_ind-RDsStart_ind)+1;

RdataLen = length(RDsStart_ind);

if LdataLen~=RdataLen
    LdataLen = min(LdataLen,RdataLen);
end

%% Store kinematics and muscle data from gait cycles (DS)

% Preallocate memory for left DS muscle feat data

LL_HFLDataLDs = NaN(LdataLen,resampleLDs_p);
LF_GLUDataLDs = NaN(LdataLen,resampleLDs_p);
LL_HAMDataLDs = NaN(LdataLen,resampleLDs_p);
LF_HAMDataLDs = NaN(LdataLen,resampleLDs_p);
LF_VASDataLDs = NaN(LdataLen,resampleLDs_p);
LF_GASDataLDs = NaN(LdataLen,resampleLDs_p);
LF_SOLDataLDs = NaN(LdataLen,resampleLDs_p);
LL_TADataLDs = NaN(LdataLen,resampleLDs_p);

RL_HFLDataLDs = NaN(LdataLen,resampleLDs_p);
RF_GLUDataLDs = NaN(LdataLen,resampleLDs_p);
RL_HAMDataLDs = NaN(LdataLen,resampleLDs_p);
RF_HAMDataLDs = NaN(LdataLen,resampleLDs_p);
RF_VASDataLDs = NaN(LdataLen,resampleLDs_p);
RF_GASDataLDs = NaN(LdataLen,resampleLDs_p);
RF_SOLDataLDs = NaN(LdataLen,resampleLDs_p);
RL_TADataLDs = NaN(LdataLen,resampleLDs_p);

TrunkDataLDs = NaN(LdataLen,resampleLDs_p);
dTrunkDataLDs = NaN(LdataLen,resampleLDs_p);

% Preallocate memory for left DS muscle data

LHFLDataLDs = NaN(LdataLen,resampleLDs_p);
LGLUDataLDs = NaN(LdataLen,resampleLDs_p);
LHAMDataLDs = NaN(LdataLen,resampleLDs_p);
LVASDataLDs = NaN(LdataLen,resampleLDs_p);
LGASDataLDs = NaN(LdataLen,resampleLDs_p);
LSOLDataLDs = NaN(LdataLen,resampleLDs_p);
LTADataLDs = NaN(LdataLen,resampleLDs_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right DS muscle feat data

LL_HFLDataRDs = NaN(LdataLen,resampleRDs_p);
LF_GLUDataRDs = NaN(LdataLen,resampleRDs_p);
LL_HAMDataRDs = NaN(LdataLen,resampleRDs_p);
LF_HAMDataRDs = NaN(LdataLen,resampleRDs_p);
LF_VASDataRDs = NaN(LdataLen,resampleRDs_p);
LF_GASDataRDs = NaN(LdataLen,resampleRDs_p);
LF_SOLDataRDs = NaN(LdataLen,resampleRDs_p);
LL_TADataRDs = NaN(LdataLen,resampleRDs_p);

RL_HFLDataRDs = NaN(LdataLen,resampleRDs_p);
RF_GLUDataRDs = NaN(LdataLen,resampleRDs_p);
RL_HAMDataRDs = NaN(LdataLen,resampleRDs_p);
RF_HAMDataRDs = NaN(LdataLen,resampleRDs_p);
RF_VASDataRDs = NaN(LdataLen,resampleRDs_p);
RF_GASDataRDs = NaN(LdataLen,resampleRDs_p);
RF_SOLDataRDs = NaN(LdataLen,resampleRDs_p);
RL_TADataRDs = NaN(LdataLen,resampleRDs_p);

TrunkDataRDs = NaN(LdataLen,resampleRDs_p);
dTrunkDataRDs = NaN(LdataLen,resampleRDs_p);

% Preallocate memory for right muscle DS data

RHFLDataRDs = NaN(LdataLen,resampleRDs_p);
RGLUDataRDs = NaN(LdataLen,resampleRDs_p);
RHAMDataRDs = NaN(LdataLen,resampleRDs_p);
RVASDataRDs = NaN(LdataLen,resampleRDs_p);
RGASDataRDs = NaN(LdataLen,resampleRDs_p);
RSOLDataRDs = NaN(LdataLen,resampleRDs_p);
RTADataRDs = NaN(LdataLen,resampleRDs_p);

for i=1:LdataLen
    
    LLHFL_LDs = LL_HFL.Data(LDsStart_ind(i):LDsEnd_ind(i),:); % angle
    LLHFL_LDsR = resample(LLHFL_LDs,resampleLDs_p,size(LLHFL_LDs,1)); % resample to max gait cycle length 
    
    LFGLU_LDs = LF_GLU.Data(LDsStart_ind(i):LDsEnd_ind(i),:); % angular velocity
    LFGLU_LDsR = resample(LFGLU_LDs,resampleLDs_p,size(LFGLU_LDs,1));
    
    LLHAM_LDs = LL_HAM.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LLHAM_LDsR = resample(LLHAM_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LFHAM_LDs = LF_HAM.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LFHAM_LDsR = resample(LFHAM_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LFVAS_LDs = LF_VAS.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LFVAS_LDsR = resample(LFVAS_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LFGAS_LDs = LF_GAS.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LFGAS_LDsR = resample(LFGAS_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LFSOL_LDs = LF_SOL.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LFSOL_LDsR = resample(LFSOL_LDs,resampleLDs_p,size(LLHAM_LDs,1));
   
    LLTA_LDs = LL_TA.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LLTA_LDsR = resample(LLTA_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    RLHFL_LDs = RL_HFL.Data(LDsStart_ind(i):LDsEnd_ind(i),:); % angle
    RLHFL_LDsR = resample(RLHFL_LDs,resampleLDs_p,size(LLHAM_LDs,1)); % resample to max gait cycle length 
    
    RFGLU_LDs = RF_GLU.Data(LDsStart_ind(i):LDsEnd_ind(i),:); % angular velocity
    RFGLU_LDsR = resample(RFGLU_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    RLHAM_LDs = RL_HAM.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    RLHAM_LDsR = resample(RLHAM_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    RFHAM_LDs = RF_HAM.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    RFHAM_LDsR = resample(RFHAM_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    RFVAS_LDs = RF_VAS.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    RFVAS_LDsR = resample(RFVAS_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    RFGAS_LDs = RF_GAS.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    RFGAS_LDsR = resample(RFGAS_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    RFSOL_LDs = RF_SOL.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    RFSOL_LDsR = resample(RFSOL_LDs,resampleLDs_p,size(LLHAM_LDs,1));
   
    RLTA_LDs = RL_TA.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    RLTA_LDsR = resample(RLTA_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
  
    Trunk_LDs = Torso.Data(LDsStart_ind(i):LDsEnd_ind(i),1);
    Trunk_LDsR = resample(Trunk_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    dTrunk_LDs = Torso.Data(LDsStart_ind(i):LDsEnd_ind(i),2);
    dTrunk_LDsR = resample(dTrunk_LDs,resampleLDs_p,size(LLHAM_LDs,1));
        
    LHFL_LDs = LStimHFL.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LHFL_LDsR = resample(LHFL_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LGLU_LDs = LStimGLU.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LGLU_LDsR = resample(LGLU_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LHAM_LDs = LStimHAM.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LHAM_LDsR = resample(LHAM_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LVAS_LDs = LStimVAS.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LVAS_LDsR = resample(LVAS_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LGAS_LDs = LStimGAS.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LGAS_LDsR = resample(LGAS_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LSOL_LDs = LStimSOL.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LSOL_LDsR = resample(LSOL_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    LTA_LDs = LStimTA.Data(LDsStart_ind(i):LDsEnd_ind(i),:);
    LTA_LDsR = resample(LTA_LDs,resampleLDs_p,size(LLHAM_LDs,1));
    
    
    LL_HFLDataLDs(i,:) = LLHFL_LDsR;
    LF_GLUDataLDs(i,:) = LFGLU_LDsR;
    LL_HAMDataLDs(i,:) = LLHAM_LDsR;
    LF_HAMDataLDs(i,:) = LFHAM_LDsR;
    LF_VASDataLDs(i,:) = LFVAS_LDsR;
    LF_GASDataLDs(i,:) = LFGAS_LDsR;
    LF_SOLDataLDs(i,:) = LFSOL_LDsR;
    LL_TADataLDs(i,:) = LLTA_LDsR;
    
    TrunkDataLDs(i,:) = Trunk_LDsR;
    
    RL_HFLDataLDs(i,:) = RLHFL_LDsR;
    RF_GLUDataLDs(i,:) = RFGLU_LDsR;
    RL_HAMDataLDs(i,:) = RLHAM_LDsR;
    RF_HAMDataLDs(i,:) = RFHAM_LDsR;
    RF_VASDataLDs(i,:) = RFVAS_LDsR;
    RF_GASDataLDs(i,:) = RFGAS_LDsR;
    RF_SOLDataLDs(i,:) = RFSOL_LDsR;
    RL_TADataLDs(i,:) = RLTA_LDsR;
    
    dTrunkDataLDs(i,:) = dTrunk_LDsR;
    
    LHFLDataLDs(i,:) = LHFL_LDsR;
    LGLUDataLDs(i,:) = LGLU_LDsR;
    LHAMDataLDs(i,:) = LHAM_LDsR;
    LVASDataLDs(i,:) = LVAS_LDsR;
    LGASDataLDs(i,:) = LGAS_LDsR;
    LSOLDataLDs(i,:) = LSOL_LDsR;
    LTADataLDs(i,:) = LTA_LDsR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    LLHFL_RDs = LL_HFL.Data(RDsStart_ind(i):RDsEnd_ind(i),:); % angle
    LLHFL_RDsR = resample(LLHFL_RDs,resampleRDs_p,size(LLHFL_RDs,1)); % resample to max gait cycle length 
    
    LFGLU_RDs = LF_GLU.Data(RDsStart_ind(i):RDsEnd_ind(i),:); % angular velocity
    LFGLU_RDsR = resample(LFGLU_RDs,resampleRDs_p,size(LFGLU_RDs,1));
    
    LLHAM_RDs = LL_HAM.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    LLHAM_RDsR = resample(LLHAM_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    LFHAM_RDs = LF_HAM.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    LFHAM_RDsR = resample(LFHAM_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    LFVAS_RDs = LF_VAS.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    LFVAS_RDsR = resample(LFVAS_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    LFGAS_RDs = LF_GAS.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    LFGAS_RDsR = resample(LFGAS_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    LFSOL_RDs = LF_SOL.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    LFSOL_RDsR = resample(LFSOL_RDs,resampleRDs_p,size(LLHAM_RDs,1));
   
    LLTA_RDs = LL_TA.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    LLTA_RDsR = resample(LLTA_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RLHFL_RDs = RL_HFL.Data(RDsStart_ind(i):RDsEnd_ind(i),:); % angle
    RLHFL_RDsR = resample(RLHFL_RDs,resampleRDs_p,size(LLHAM_RDs,1)); % resample to max gait cycle length 
    
    RFGLU_RDs = RF_GLU.Data(RDsStart_ind(i):RDsEnd_ind(i),:); % angular velocity
    RFGLU_RDsR = resample(RFGLU_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RLHAM_RDs = RL_HAM.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RLHAM_RDsR = resample(RLHAM_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RFHAM_RDs = RF_HAM.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RFHAM_RDsR = resample(RFHAM_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RFVAS_RDs = RF_VAS.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RFVAS_RDsR = resample(RFVAS_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RFGAS_RDs = RF_GAS.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RFGAS_RDsR = resample(RFGAS_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RFSOL_RDs = RF_SOL.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RFSOL_RDsR = resample(RFSOL_RDs,resampleRDs_p,size(LLHAM_RDs,1));
   
    RLTA_RDs = RL_TA.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RLTA_RDsR = resample(RLTA_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
  
    Trunk_RDs = Torso.Data(RDsStart_ind(i):RDsEnd_ind(i),1);
    Trunk_RDsR = resample(Trunk_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    dTrunk_RDs = Torso.Data(RDsStart_ind(i):RDsEnd_ind(i),2);
    dTrunk_RDsR = resample(dTrunk_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RHF_RDs = RStimHFL.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RHF_RDsR = resample(RHF_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RGLU_RDs = RStimGLU.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RGLU_RDsR = resample(RGLU_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RHAM_RDs = RStimHAM.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RHAM_RDsR = resample(RHAM_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RVAS_RDs = RStimVAS.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RVAS_RDsR = resample(RVAS_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RGAS_RDs = RStimGAS.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RGAS_RDsR = resample(RGAS_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RSOL_RDs = RStimSOL.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RSOL_RDsR = resample(RSOL_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    RTA_RDs = RStimTA.Data(RDsStart_ind(i):RDsEnd_ind(i),:);
    RTA_RDsR = resample(RTA_RDs,resampleRDs_p,size(LLHAM_RDs,1));
    
    LL_HFLDataRDs(i,:) = LLHFL_RDsR;
    LF_GLUDataRDs(i,:) = LFGLU_RDsR;
    LL_HAMDataRDs(i,:) = LLHAM_RDsR;
    LF_HAMDataRDs(i,:) = LFHAM_RDsR;
    LF_VASDataRDs(i,:) = LFVAS_RDsR;
    LF_GASDataRDs(i,:) = LFGAS_RDsR;
    LF_SOLDataRDs(i,:) = LFSOL_RDsR;
    LL_TADataRDs(i,:) = LLTA_RDsR;
    
    TrunkDataRDs(i,:) = Trunk_RDsR;
    
    RL_HFLDataRDs(i,:) = RLHFL_RDsR;
    RF_GLUDataRDs(i,:) = RFGLU_RDsR;
    RL_HAMDataRDs(i,:) = RLHAM_RDsR;
    RF_HAMDataRDs(i,:) = RFHAM_RDsR;
    RF_VASDataRDs(i,:) = RFVAS_RDsR;
    RF_GASDataRDs(i,:) = RFGAS_RDsR;
    RF_SOLDataRDs(i,:) = RFSOL_RDsR;
    RL_TADataRDs(i,:) = RLTA_RDsR;
    
    dTrunkDataRDs(i,:) = dTrunk_RDsR;
    
    RHFLDataRDs(i,:) = RHF_RDsR;
    RGLUDataRDs(i,:) = RGLU_RDsR;
    RHAMDataRDs(i,:) = RHAM_RDsR;
    RVASDataRDs(i,:) = RVAS_RDsR;
    RGASDataRDs(i,:) = RGAS_RDsR;
    RSOLDataRDs(i,:) = RSOL_RDsR;
    RTADataRDs(i,:) = RTA_RDsR;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    KinInd = KinInd + 1; 
end

%% Hold out test data (stance)

% test data left stance
tLL_HFLDataLDs = LL_HFLDataLDs(testDataInd,:);
tLF_GLUDataLDs = LF_GLUDataLDs(testDataInd,:);
tLL_HAMDataLDs = LL_HAMDataLDs(testDataInd,:);
tLF_HAMDataLDs = LF_HAMDataLDs(testDataInd,:);
tLF_VASDataLDs = LF_VASDataLDs(testDataInd,:);
tLF_GASDataLDs = LF_GASDataLDs(testDataInd,:);
tLF_SOLDataLDs = LF_SOLDataLDs(testDataInd,:);
tLL_TADataLDs = LL_TADataLDs(testDataInd,:);

tTrunkDataLDs = TrunkDataLDs(testDataInd,:);

tRL_HFLDataLDs = RL_HFLDataLDs(testDataInd,:);
tRF_GLUDataLDs = RF_GLUDataLDs(testDataInd,:);
tRL_HAMDataLDs = RL_HAMDataLDs(testDataInd,:);
tRF_HAMDataLDs = RF_HAMDataLDs(testDataInd,:);
tRF_VASDataLDs = RF_VASDataLDs(testDataInd,:);
tRF_GASDataLDs = RF_GASDataLDs(testDataInd,:);
tRF_SOLDataLDs = RF_SOLDataLDs(testDataInd,:);
tRL_TADataLDs = RL_TADataLDs(testDataInd,:);

tdTrunkDataLDs = dTrunkDataLDs(testDataInd,:);

tLHFLDataLDs = LHFLDataLDs(testDataInd,:);
tLGLUDataLDs = LGLUDataLDs(testDataInd,:);
tLHAMDataLDs = LHAMDataLDs(testDataInd,:);
tLVASDataLDs = LVASDataLDs(testDataInd,:);
tLGASDataLDs = LGASDataLDs(testDataInd,:);
tLSOLDataLDs = LSOLDataLDs(testDataInd,:);
tLTADataLDs = LTADataLDs(testDataInd,:);

% Remove test data from all left stance data

LL_HFLDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LF_GLUDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LL_HAMDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LF_HAMDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LF_VASDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LF_GASDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LF_SOLDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
LL_TADataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);

TrunkDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);

RL_HFLDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RF_GLUDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RL_HAMDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RF_HAMDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RF_VASDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RF_GASDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RF_SOLDataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);
RL_TADataLDs(testDataInd,:)= zeros(length(testDataInd),resampleLDs_p);

dTrunkDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);

LHFLDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);
LGLUDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);
LHAMDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);
LVASDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);
LGASDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);
LSOLDataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);
LTADataLDs(testDataInd,:) = zeros(length(testDataInd),resampleLDs_p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% test data right stance
tLL_HFLDataRDs = LL_HFLDataRDs(testDataInd,:);
tLF_GLUDataRDs = LF_GLUDataRDs(testDataInd,:);
tLL_HAMDataRDs = LL_HAMDataRDs(testDataInd,:);
tLF_HAMDataRDs = LF_HAMDataRDs(testDataInd,:);
tLF_VASDataRDs = LF_VASDataRDs(testDataInd,:);
tLF_GASDataRDs = LF_GASDataRDs(testDataInd,:);
tLF_SOLDataRDs = LF_SOLDataRDs(testDataInd,:);
tLL_TADataRDs = LL_TADataRDs(testDataInd,:);

tTrunkDataRDs = TrunkDataRDs(testDataInd,:);

tRL_HFLDataRDs = RL_HFLDataRDs(testDataInd,:);
tRF_GLUDataRDs = RF_GLUDataRDs(testDataInd,:);
tRL_HAMDataRDs = RL_HAMDataRDs(testDataInd,:);
tRF_HAMDataRDs = RF_HAMDataRDs(testDataInd,:);
tRF_VASDataRDs = RF_VASDataRDs(testDataInd,:);
tRF_GASDataRDs = RF_GASDataRDs(testDataInd,:);
tRF_SOLDataRDs = RF_SOLDataRDs(testDataInd,:);
tRL_TADataRDs = RL_TADataRDs(testDataInd,:);

tdTrunkDataRDs = dTrunkDataRDs(testDataInd,:);

tRHFLDataRDs = RHFLDataRDs(testDataInd,:);
tRGLUDataRDs = RGLUDataRDs(testDataInd,:);
tRHAMDataRDs = RHAMDataRDs(testDataInd,:);
tRVASDataRDs = RVASDataRDs(testDataInd,:);
tRGASDataRDs = RGASDataRDs(testDataInd,:);
tRSOLDataRDs = RSOLDataRDs(testDataInd,:);
tRTADataRDs = RTADataRDs(testDataInd,:);

% Remove test data from all right stance data

LL_HFLDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LF_GLUDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LL_HAMDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LF_HAMDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LF_VASDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LF_GASDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LF_SOLDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
LL_TADataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);

TrunkDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);

RL_HFLDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RF_GLUDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RL_HAMDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RF_HAMDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RF_VASDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RF_GASDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RF_SOLDataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);
RL_TADataRDs(testDataInd,:)= zeros(length(testDataInd),resampleRDs_p);

dTrunkDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);

RHFLDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);
RGLUDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);
RHAMDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);
RVASDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);
RGASDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);
RSOLDataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);
RTADataRDs(testDataInd,:) = zeros(length(testDataInd),resampleRDs_p);

%% Mean data (left DS)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataLDs = mean(LL_HFLDataLDs);
mLF_GLUDataLDs = mean(LF_GLUDataLDs);
mLL_HAMDataLDs = mean(LL_HAMDataLDs);
mLF_HAMDataLDs = mean(LF_HAMDataLDs);
mLF_VASDataLDs = mean(LF_VASDataLDs);
mLF_GASDataLDs = mean(LF_GASDataLDs);
mLF_SOLDataLDs = mean(LF_SOLDataLDs);
mLL_TADataLDs = mean(LL_TADataLDs);

mTrunkDataLDs = mean(TrunkDataLDs);

mRL_HFLDataLDs = mean(RL_HFLDataLDs);
mRF_GLUDataLDs = mean(RF_GLUDataLDs);
mRL_HAMDataLDs = mean(RL_HAMDataLDs);
mRF_HAMDataLDs = mean(RF_HAMDataLDs);
mRF_VASDataLDs = mean(RF_VASDataLDs);
mRF_GASDataLDs = mean(RF_GASDataLDs);
mRF_SOLDataLDs = mean(RF_SOLDataLDs);
mRL_TADataLDs = mean(RL_TADataLDs);

mdTrunkDataLDs = mean(dTrunkDataLDs);


% collect mean muscle features over gaits (only training)
mFeatDataLDs = [mLL_HFLDataLDs; mLF_GLUDataLDs; mLL_HAMDataLDs; mLF_HAMDataLDs; mLF_VASDataLDs; mLF_GASDataLDs; mLF_SOLDataLDs; mLL_TADataLDs; mTrunkDataLDs; ...
            mRL_HFLDataLDs; mRF_GLUDataLDs; mRL_HAMDataLDs; mRF_HAMDataLDs; mRF_VASDataLDs; mRF_GASDataLDs; mRF_SOLDataLDs; mRL_TADataLDs; mdTrunkDataLDs];
        
% mean muscle stimulations over all gait cycles (only training)
mLHFLDataLDs = mean(LHFLDataLDs);
mLGLUDataLDs = mean(LGLUDataLDs);
mLHAMDataLDs = mean(LHAMDataLDs);
mLVASDataLDs = mean(LVASDataLDs);
mLGASDataLDs = mean(LGASDataLDs);
mLSOLDataLDs = mean(LSOLDataLDs);
mLTADataLDs = mean(LTADataLDs); 

% collect mean muscles over gaits (only training)
mMusDataLDs = [mLHFLDataLDs; mLGLUDataLDs; mLHAMDataLDs; mLVASDataLDs; mLGASDataLDs; mLSOLDataLDs; mLTADataLDs];

%% Mean data (right DS)

% mean kinematics over all gait cycles (only training)
mLL_HFLDataRDs = mean(LL_HFLDataRDs);
mLF_GLUDataRDs = mean(LF_GLUDataRDs);
mLL_HAMDataRDs = mean(LL_HAMDataRDs);
mLF_HAMDataRDs = mean(LF_HAMDataRDs);
mLF_VASDataRDs = mean(LF_VASDataRDs);
mLF_GASDataRDs = mean(LF_GASDataRDs);
mLF_SOLDataRDs = mean(LF_SOLDataRDs);
mLL_TADataRDs = mean(LL_TADataRDs);

mTrunkDataRDs = mean(TrunkDataRDs);

mRL_HFLDataRDs = mean(RL_HFLDataRDs);
mRF_GLUDataRDs = mean(RF_GLUDataRDs);
mRL_HAMDataRDs = mean(RL_HAMDataRDs);
mRF_HAMDataRDs = mean(RF_HAMDataRDs);
mRF_VASDataRDs = mean(RF_VASDataRDs);
mRF_GASDataRDs = mean(RF_GASDataRDs);
mRF_SOLDataRDs = mean(RF_SOLDataRDs);
mRL_TADataRDs = mean(RL_TADataRDs);

mdTrunkDataRDs = mean(dTrunkDataRDs);


% collect mean muscle features over gaits (only training)
mFeatDataRDs = [mRL_HFLDataRDs; mRF_GLUDataRDs; mRL_HAMDataRDs; mRF_HAMDataRDs; mRF_VASDataRDs; mRF_GASDataRDs; mRF_SOLDataRDs; mRL_TADataRDs; mTrunkDataRDs;...
    mLL_HFLDataRDs; mLF_GLUDataRDs; mLL_HAMDataRDs; mLF_HAMDataRDs; mLF_VASDataRDs; mLF_GASDataRDs; mLF_SOLDataRDs; mLL_TADataRDs; mdTrunkDataRDs];


mRHFLDataRDs = mean(RHFLDataRDs);
mRGLUDataRDs = mean(RGLUDataRDs);
mRHAMDataRDs = mean(RHAMDataRDs);
mRVASDataRDs = mean(RVASDataRDs);
mRGASDataRDs = mean(RGASDataRDs);
mRSOLDataRDs = mean(RSOLDataRDs);
mRTADataRDs = mean(RTADataRDs); 

mMusDataRDs =  [mRHFLDataRDs; mRGLUDataRDs; mRHAMDataRDs; mRVASDataRDs; mRGASDataRDs; mRSOLDataRDs; mRTADataRDs];