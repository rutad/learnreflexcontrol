%% Parsing learned weights into NMS gains

%load('w_learnGains_CtrlTimeDelayedFeat.mat','w*');
load('w_learnGains_CtrlFeat.mat','w*');

%% Stance

PreStimTASt = wLSt_TA(1);
GainTASt = wLSt_TA(2);
GainSOLTASt = wLSt_TA(3);

PreStimSOLSt = wLSt_SOL(1);
GainSOLSt = wLSt_SOL(2);

PreStimGASSt = wLSt_GAS(1);
GainGASSt = wLSt_GAS(2);

PreStimVASSt = wLSt_VAS(1);
GainVASSt = wLSt_VAS(2);
GainKneOverExtSt = wLSt_VAS(3);

PreStimGGSt = wLSt_HAM(1);
PosGainGGSt = wLSt_HAM(2);
SpeedGainGGSt = wLSt_HAM(3);

%% Swing

PreStimTASw = wLSw_TA(1);
GainTASw = wLSw_TA(2);

PreStimHAMSw = wLSw_HAM(1);
GainHAMSw = wLSw_HAM(2);

PreStimGLUSw = wLSw_GLU(1);
GainGLUSw = wLSw_GLU(2);

PreStimHFLSw = wLSw_HFL(1);
GainHFLSw = wLSw_HFL(2);
GainHAMHFLSw = wLSw_HFL(3);
GainDeltaThetaSw = wLSw_HFL(4);



