% Relate kinematics data to muscle stimulation

function [wDistLSw, delw_LSw] =  wSparseDistSw (distData, steadyData)

mFeatDataDistLSw = distData{1};
mMusDataDistLSw = distData{2};

mFeatDataLSw = steadyData{1};
mMusDataLSw = steadyData{2};

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSw_r=resample(mFeatDataLSw',size(mFeatDataDistLSw,2),size(mFeatDataLSw,2))';
mMusDataLSw_r=resample(mMusDataLSw',size(mMusDataDistLSw,2),size(mMusDataLSw,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSw = mMusDataDistLSw - mMusDataLSw_r;
del_mFeatDataLSw = mFeatDataDistLSw - mFeatDataLSw_r;

%%%%% DISTURBED DATA and DISTURBED - STEADY DATA %%%%%%%%

% l1-least square
wDistLSw = NaN(size(mMusDataLSw,1),size(mFeatDataLSw,1));
delw_LSw = NaN(size(wDistLSw));

ADistLSw = mFeatDataDistLSw';
del_ALSw = del_mFeatDataLSw';

err_tol_Sw = 1e-4;

%lambdaSw = 0.1; % l1 cost weight

for i = 1:7
        
    BDistLSw = mMusDataDistLSw(i,:)';
    del_BLSw = del_mMusDataLSw(i,:)';
    
    [lambda_max] = find_lambdamax_l1_ls(ADistLSw',BDistLSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [lambdaDel_max] = find_lambdamax_l1_ls(del_ALSw',del_BLSw);
    lambdaDelSw = lambdaDel_max*err_tol_Sw*50;
    
    [xDistLSw,~] = l1_ls(ADistLSw,BDistLSw,lambdaSw,[],'quiet');
    [del_xLSw,~] = l1_ls(del_ALSw,del_BLSw,lambdaDelSw,[],'quiet');
    
    wDistLSw(i,:) = xDistLSw';
    delw_LSw(i,:) = del_xLSw';
    
end

end