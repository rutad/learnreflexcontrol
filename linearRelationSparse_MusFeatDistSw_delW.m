% Relate kinematics data to muscle stimulation
clc;
close all;
%clear all;

%load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSw_v2_RfeatFlip.mat;
%load linRel_13ms_NMwalkingDisturbWait_LFtSwEnd_40sTrainData_musFeatStSw.mat;

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

% resample undisturbed data to match length of disturbed data
mFeatDataLSw_r=resample(mFeatDataLSw',size(mFeatDataDistLSw,2),size(mFeatDataLSw,2))';
mMusDataLSw_r=resample(mMusDataLSw',size(mMusDataDistLSw,2),size(mMusDataLSw,2))';

% subtracting steady state motion from the disturbed motion
del_mMusDataLSw = mMusDataDistLSw - mMusDataLSw_r;
del_mFeatDataLSw = mFeatDataDistLSw - mFeatDataLSw_r;

%%%%% DISTURBED DATA and DISTURBED - STEADY DATA %%%%%%%%

% only least square solution using pinv
wp_DistLSw = mMusDataDistLSw*pinv(mFeatDataDistLSw); % wp is 14X14
wp_LSw = mMusDataLSw*pinv(mFeatDataLSw); % wp is 14X14
del_wp_LSw = del_mMusDataLSw*pinv(del_mFeatDataLSw);

% l1-least square
wDistLSw = NaN(size(wp_DistLSw));
delw_LSw = NaN(size(del_wp_LSw));
wLSw = NaN(size(wp_LSw));

ADistLSw = mFeatDataDistLSw';
ALSw = mFeatDataLSw';
del_ALSw = del_mFeatDataLSw';

err_tol_Sw = 1e-3;

%lambdaSw = 0.1; % l1 cost weight

for i = 1:7
        
    BLSw = mMusDataLSw(i,:)';
    BDistLSw = mMusDataDistLSw(i,:)';
    del_BLSw = del_mMusDataLSw(i,:)';
    
    [lambda_maxDist] = find_lambdamax_l1_ls(ADistLSw',BDistLSw);
    lambdaSwDist = lambda_maxDist*err_tol_Sw;
    
    [lambda_maxDel] = find_lambdamax_l1_ls(del_ALSw',del_BLSw);
    lambdaSwDel = lambda_maxDel*err_tol_Sw;
    
    [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw,[],'quiet');
    [xDistLSw,~] = l1_ls(ADistLSw,BDistLSw,lambdaSwDist,[],'quiet');
    [del_xLSw,~] = l1_ls(del_ALSw,del_BLSw,lambdaSwDel,[],'quiet');
    
    wDistLSw(i,:) = xDistLSw';
    delw_LSw(i,:) = del_xLSw';
    wLSw(i,:) = xLSw';
end

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

%% undisturbed left swing w
% figure;imagesc(abs(wp_LSw)/(max(max(abs(wp_LSw)))));colorbar;title('Leg weights UNDISTURBED LEFT SWING (2-leg Mus Feat Data)-leastSq');
% set(gca(),'YTick',1:7) 
% set(gca(),'YTickLabel',labels_ly);
% set(gca(),'XTick',1:18) 
% set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wLSw)/(max(max(abs(wLSw)))));colorbar;title('Leg weights UNDISTURBED LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);


%% disturbed left swing w
% figure;imagesc(abs(wp_DistLSw)/(max(max(abs(wp_DistLSw)))));colorbar;title('Leg weights DISTURBED LEFT SWING (2-leg Mus Feat Data)-leastSq');
% set(gca(),'YTick',1:7) 
% set(gca(),'YTickLabel',labels_ly);
% set(gca(),'XTick',1:18) 
% set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wDistLSw)/(max(max(abs(wDistLSw)))));colorbar;title('Leg weights DISTURBED LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% delta left swing w (disturbed - steady)
% figure;imagesc(abs(del_wp_LSw)/(max(max(abs(del_wp_LSw)))));colorbar;title('Leg weights DEL LEFT SWING (2-leg Mus Feat Data)-leastSq');
% set(gca(),'YTick',1:7) 
% set(gca(),'YTickLabel',labels_ly);
% set(gca(),'XTick',1:18) 
% set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(delw_LSw)/(max(max(abs(delw_LSw)))));colorbar;title('Leg weights DEL LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);
