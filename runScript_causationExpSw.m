%% run causation experiment script

clc;clear all;close all;

all_fDisturb = [5,10,15];

for do = 1:length(all_fDisturb)
    fDisturb = all_fDisturb(do);
    saveName = strcat('causationExp_meanDisturbWaitLSw_musFeatNormTrk_F',num2str(fDisturb),'.mat');
    causationDisturbExp_musFeatSw;
    
    save (saveName, 'WStore', 'mean_delWLSw', 'mean_wDistLSw', 'tDisturb','fDisturb')
    
    
end