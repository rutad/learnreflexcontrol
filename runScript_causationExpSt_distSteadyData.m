%% run causation experiment script

clc;clear all;close all;

all_fDisturb = [5,10,15];

for do = 1:length(all_fDisturb)
    fDisturb = all_fDisturb(do);
    saveName = strcat('causExpDistWaitSt_musFeatNormTrk_distSteadyData_F',num2str(fDisturb),'.mat');
    causationDisturbExp_musFeatSt_distSteadyData;
    
    save (saveName, 'WStore','musDistDataStore','featDistDataStore', 'trunkNormStore','mean_wDelSteadyLSt', ...
        'mean_wDistSteadyLSt', 'tDisturb','fDisturb','meanTrunkNormMax');
    
    
end