% Relate kinematics data to muscle stimulation
clc;
close all;
%clear all;

%load linRel_13ms_NMwalking_100sTrainTestData_ALLmusFeatNORMStSw_v2_RfeatFlip.mat;
%load linRel_13ms_NMwalking_100sTrainTestData_ALLmusFeatStSw_v2_RfeatFlip.mat;
%load linRel_13ms_NMwalking_100sTrainTestData_ALLmusFeatWDelLStSw_v2_RfeatFlip.mat;

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

%%%%% LEFT %%%%%%%%

% only least square solution using pinv
wp_LSt = mMusDataLSt*pinv(mFeatDataLSt); % wp is 14X14
wp_LSw = mMusDataLSw*pinv(mFeatDataLSw); % wp is 14X14

% l1-least square
wLSt = NaN(size(wp_LSt));
wLSw = NaN(size(wp_LSw));

ALSt = mFeatDataLSt';
ALSw = mFeatDataLSw';

err_tol_Sw = 1e-3;

lambdaSt = 0.7; % l1 cost weight
%lambdaSw = 0.08; % 0.1 % l1 cost weight

for i = 1:7
    BLSt = mMusDataLSt(i,:)';
    
    %[lambda_max] = find_lambdamax_l1_ls(ALSt',BLSt);
    %lambdaSt = lambda_max*1e-3
    
    [xLSt,~] = l1_ls(ALSt,BLSt,lambdaSt);
    wLSt(i,:) = xLSt';
        
    BLSw = mMusDataLSw(i,:)';
    
    [lambda_max] = find_lambdamax_l1_ls(ALSw',BLSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [xLSw,~] = l1_ls(ALSw,BLSw,lambdaSw);
    wLSw(i,:) = xLSw';
end

%%%%%%%%% RIGHT %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% only least square solution using pinv
wp_RSt = mMusDataRSt*pinv(mFeatDataRSt); % wp is 14X14
wp_RSw = mMusDataRSw*pinv(mFeatDataRSw); % wp is 14X14

% l1-least square
wRSt = NaN(size(wp_RSt));
wRSw = NaN(size(wp_RSw));

ARSt = mFeatDataRSt';
ARSw = mFeatDataRSw';

for i = 1:7
    BRSt = mMusDataRSt(i,:)';
       
    %[lambda_max] = find_lambdamax_l1_ls(ARSt',BRSt);
    %lambdaSt = lambda_max*1e-3
    
    [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt);
    wRSt(i,:) = xRSt';
        
    BRSw = mMusDataRSw(i,:)';
    
    [lambda_max] = find_lambdamax_l1_ls(ARSw',BRSw);
    lambdaSw = lambda_max*err_tol_Sw;
    
    [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw);
    wRSw(i,:) = xRSw';
end

%% Test weight w on a particular sample gait left leg     

t = randi(testNum,1,1); % index of test sample

% left stance
testFeatLSt = [tLL_HFLDataLSt(t,:); tLF_HFLDataLSt(t,:);  tLL_GLUDataLSt(t,:); tLF_GLUDataLSt(t,:); tLL_HAMDataLSt(t,:); tLF_HAMDataLSt(t,:); ...
              tLL_VASDataLSt(t,:);  tLF_VASDataLSt(t,:);  tLL_GASDataLSt(t,:); tLF_GASDataLSt(t,:); tLL_SOLDataLSt(t,:); tLF_SOLDataLSt(t,:); tLL_TADataLSt(t,:); tLF_TADataLSt(t,:); tTrunkDataLSt(t,:); ...
              tRL_HFLDataLSt(t,:);  tRF_HFLDataLSt(t,:);  tRL_GLUDataLSt(t,:); tRF_GLUDataLSt(t,:); tRL_HAMDataLSt(t,:); tRF_HAMDataLSt(t,:); ...
              tRL_VASDataLSt(t,:);  tRF_VASDataLSt(t,:);  tRL_GASDataLSt(t,:); tRF_GASDataLSt(t,:); tRL_SOLDataLSt(t,:); tRF_SOLDataLSt(t,:); tRL_TADataLSt(t,:); tRF_TADataLSt(t,:); tdTrunkDataLSt(t,:)];
       
trueMusLSt = [tLHFLDataLSt(t,:); tLGLUDataLSt(t,:); tLHAMDataLSt(t,:); tLVASDataLSt(t,:); tLGASDataLSt(t,:); tLSOLDataLSt(t,:); tLTADataLSt(t,:)]; 
 
testMusLSt = wLSt*testFeatLSt;        
testMusp_LSt = wp_LSt*testFeatLSt;

% left swing
testFeatLSw = [tLL_HFLDataLSw(t,:); tLF_HFLDataLSw(t,:);  tLL_GLUDataLSw(t,:); tLF_GLUDataLSw(t,:); tLL_HAMDataLSw(t,:); tLF_HAMDataLSw(t,:); ...
              tLL_VASDataLSw(t,:);  tLF_VASDataLSw(t,:);  tLL_GASDataLSw(t,:); tLF_GASDataLSw(t,:); tLL_SOLDataLSw(t,:); tLF_SOLDataLSw(t,:); tLL_TADataLSw(t,:); tLF_TADataLSw(t,:); tTrunkDataLSw(t,:); ...
              tRL_HFLDataLSw(t,:);  tRF_HFLDataLSw(t,:);  tRL_GLUDataLSw(t,:); tRF_GLUDataLSw(t,:); tRL_HAMDataLSw(t,:); tRF_HAMDataLSw(t,:); ...
              tRL_VASDataLSw(t,:);  tRF_VASDataLSw(t,:);  tRL_GASDataLSw(t,:); tRF_GASDataLSw(t,:); tRL_SOLDataLSw(t,:); tRF_SOLDataLSw(t,:); tRL_TADataLSw(t,:); tRF_TADataLSw(t,:); tdTrunkDataLSw(t,:)];
       
trueMusLSw = [tLHFLDataLSw(t,:); tLGLUDataLSw(t,:); tLHAMDataLSw(t,:); tLVASDataLSw(t,:); tLGASDataLSw(t,:); tLSOLDataLSw(t,:); tLTADataLSw(t,:)];
            
 
testMusLSw = wLSw*testFeatLSw;        
testMusp_LSw = wp_LSw*testFeatLSw;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % right stance
testFeatRSt = [tRL_HFLDataRSt(t,:);  tRF_HFLDataRSt(t,:);  tRL_GLUDataRSt(t,:); tRF_GLUDataRSt(t,:); tRL_HAMDataRSt(t,:); tRF_HAMDataRSt(t,:); ...
               tRL_VASDataRSt(t,:);  tRF_VASDataRSt(t,:);  tRL_GASDataRSt(t,:); tRF_GASDataRSt(t,:); tRL_SOLDataRSt(t,:); tRF_SOLDataRSt(t,:); tRL_TADataRSt(t,:); tRF_TADataRSt(t,:);tTrunkDataRSt(t,:);...
               tLL_HFLDataRSt(t,:); tLF_HFLDataRSt(t,:);  tLL_GLUDataRSt(t,:); tLF_GLUDataRSt(t,:); tLL_HAMDataRSt(t,:); tLF_HAMDataRSt(t,:); ...
               tLL_VASDataRSt(t,:);  tLF_VASDataRSt(t,:);  tLL_GASDataRSt(t,:); tLF_GASDataRSt(t,:); tLL_SOLDataRSt(t,:); tLF_SOLDataRSt(t,:); tLL_TADataRSt(t,:); tLF_TADataRSt(t,:);tdTrunkDataRSt(t,:)];
       
trueMusRSt = [tRHFLDataRSt(t,:); tRGLUDataRSt(t,:); tRHAMDataRSt(t,:); tRVASDataRSt(t,:); tRGASDataRSt(t,:); tRSOLDataRSt(t,:); tRTADataRSt(t,:)]; 
 
testMusRSt = wRSt*testFeatRSt;        
testMusp_RSt = wp_RSt*testFeatRSt;

% right swing
testFeatRSw = [tRL_HFLDataRSw(t,:);  tRF_HFLDataRSw(t,:);  tRL_GLUDataRSw(t,:); tRF_GLUDataRSw(t,:); tRL_HAMDataRSw(t,:); tRF_HAMDataRSw(t,:); ...
               tRL_VASDataRSw(t,:);  tRF_VASDataRSw(t,:);  tRL_GASDataRSw(t,:); tRF_GASDataRSw(t,:); tRL_SOLDataRSw(t,:); tRF_SOLDataRSw(t,:); tRL_TADataRSw(t,:); tRF_TADataRSw(t,:);tTrunkDataRSw(t,:);...
               tLL_HFLDataRSw(t,:); tLF_HFLDataRSw(t,:);  tLL_GLUDataRSw(t,:); tLF_GLUDataRSw(t,:); tLL_HAMDataRSw(t,:); tLF_HAMDataRSw(t,:); ...
               tLL_VASDataRSw(t,:);  tLF_VASDataRSw(t,:);  tLL_GASDataRSw(t,:); tLF_GASDataRSw(t,:); tLL_SOLDataRSw(t,:); tLF_SOLDataRSw(t,:); tLL_TADataRSw(t,:); tLF_TADataRSw(t,:);tdTrunkDataRSw(t,:)];
       
trueMusRSw = [tRHFLDataRSw(t,:); tRGLUDataRSw(t,:); tRHAMDataRSw(t,:); tRVASDataRSw(t,:); tRGASDataRSw(t,:); tRSOLDataRSw(t,:); tRTADataRSw(t,:)]; 
            
 
testMusRSw = wRSw*testFeatRSw;        
testMusp_RSw = wp_RSw*testFeatRSw;

%% visualize stance
%cd linRel_Results/

figure;
subplot(7,1,1); plot(testMusLSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_LSt(1,:)');
subplot(7,1,1); plot(trueMusLSt(1,:)');
title('Left muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_LSt(2,:)');
subplot(7,1,2); plot(trueMusLSt(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_LSt(3,:)');
subplot(7,1,3); plot(trueMusLSt(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_LSt(4,:)');
subplot(7,1,4); plot(trueMusLSt(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_LSt(5,:)');
subplot(7,1,5); plot(trueMusLSt(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_LSt(6,:)');
subplot(7,1,6); plot(trueMusLSt(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_LSt(7,:)');
subplot(7,1,7); plot(trueMusLSt(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_ALLMusFeatLSt_v2.pdf

figure;
subplot(7,1,1); plot(testMusRSt(1,:)');hold on
subplot(7,1,1); plot(testMusp_RSt(1,:)');
subplot(7,1,1); plot(trueMusRSt(1,:)');
title('Right muscles STANCE reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRSt(2,:)');hold on
subplot(7,1,2); plot(testMusp_RSt(2,:)');
subplot(7,1,2); plot(trueMusRSt(2,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRSt(3,:)');hold on
subplot(7,1,3); plot(testMusp_RSt(3,:)');
subplot(7,1,3); plot(trueMusRSt(3,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRSt(4,:)');hold on
subplot(7,1,4); plot(testMusp_RSt(4,:)');
subplot(7,1,4); plot(trueMusRSt(4,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRSt(5,:)');hold on
subplot(7,1,5); plot(testMusp_RSt(5,:)');
subplot(7,1,5); plot(trueMusRSt(5,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRSt(6,:)');hold on
subplot(7,1,6); plot(testMusp_RSt(6,:)');
subplot(7,1,6); plot(trueMusRSt(6,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRSt(7,:)');hold on
subplot(7,1,7); plot(testMusp_RSt(7,:)');
subplot(7,1,7); plot(trueMusRSt(7,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_ALLMusFeatRSt_v2.pdf

%% visualize swing 

figure;
subplot(7,1,1); plot(testMusLSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_LSw(1,:)');
subplot(7,1,1); plot(trueMusLSw(1,:)');
title('Left muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_LSw(2,:)');
subplot(7,1,2); plot(trueMusLSw(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_LSw(3,:)');
subplot(7,1,3); plot(trueMusLSw(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_LSw(4,:)');
subplot(7,1,4); plot(trueMusLSw(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_LSw(5,:)');
subplot(7,1,5); plot(trueMusLSw(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_LSw(6,:)');
subplot(7,1,6); plot(trueMusLSw(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_LSw(7,:)');
subplot(7,1,7); plot(trueMusLSw(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_ALLMusFeatLSw_v2.pdf

figure;
subplot(7,1,1); plot(testMusRSw(1,:)');hold on
subplot(7,1,1); plot(testMusp_RSw(1,:)');
subplot(7,1,1); plot(trueMusRSw(1,:)');
title('Right muscles SWING reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRSw(2,:)');hold on
subplot(7,1,2); plot(testMusp_RSw(2,:)');
subplot(7,1,2); plot(trueMusRSw(2,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRSw(3,:)');hold on
subplot(7,1,3); plot(testMusp_RSw(3,:)');
subplot(7,1,3); plot(trueMusRSw(3,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRSw(4,:)');hold on
subplot(7,1,4); plot(testMusp_RSw(4,:)');
subplot(7,1,4); plot(trueMusRSw(4,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRSw(5,:)');hold on
subplot(7,1,5); plot(testMusp_RSw(5,:)');
subplot(7,1,5); plot(trueMusRSw(5,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRSw(6,:)');hold on
subplot(7,1,6); plot(testMusp_RSw(6,:)');
subplot(7,1,6); plot(trueMusRSw(6,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRSw(7,:)');hold on
subplot(7,1,7); plot(testMusp_RSw(7,:)');
subplot(7,1,7); plot(trueMusRSw(7,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_ALLMusFeatRSw_v2.pdf

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-HFL','LL-GLU','LF-GLU','LL-HAM','LF-HAM','LL-VAS','LF-VAS','LL-GAS','LF-GAS','LL-SOL','LF-SOL',...
    'LL-TA','LF-TA','Trunk','RL-HFL','RF-HFL','RL-GLU','RF-GLU','RL-HAM','RF-HAM','RL-VAS','RF-VAS','RL-GAS','RF-GAS','RL-SOL','RF-SOL','RL-TA','RF-TA','dTrunk'};

%% left stance w
figure;imagesc(abs(wp_LSt)/(max(max(abs(wp_LSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wLSt)/(max(max(abs(wLSt)))));colorbar;title('Leg weights LEFT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% left swing w
figure;imagesc(abs(wp_LSw)/(max(max(abs(wp_LSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wLSw)/(max(max(abs(wLSw)))));colorbar;title('Leg weights LEFT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% Visualize w RIGHT

labels_ry = {'RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_rx = {'RL-HFL','RF-HFL','RL-GLU','RF-GLU','RL-HAM','RF-HAM','RL-VAS','RF-VAS','RL-GAS','RF-GAS','RL-SOL','RF-SOL','RL-TA','RF-TA','Trunk',...
             'LL-HFL','LF-HFL','LL-GLU','LF-GLU','LL-HAM','LF-HAM','LL-VAS','LF-VAS','LL-GAS','LF-GAS','LL-SOL','LF-SOL',...
             'LL-TA','LF-TA','dTrunk'};

%% right stance w
figure;imagesc(abs(wp_RSt)/(max(max(abs(wp_RSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wRSt)/(max(max(abs(wRSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

%% right swing w
figure;imagesc(abs(wp_RSw)/(max(max(abs(wp_RSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wRSw)/(max(max(abs(wRSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:30) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

%cd ..