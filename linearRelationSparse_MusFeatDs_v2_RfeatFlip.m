% Relate kinematics data to muscle stimulation
clc;
close all;
%clear all;

%load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSwDs_v2_RfeatFlip.mat;

%% Direct linear relationship (Sparse weights using l1 regularized least square)
% using l1_ls package from stanford group

%%%%% LEFT %%%%%%%%

% only least square solution using pinv
wp_LDs = mMusDataLDs*pinv(mFeatDataLDs); % wp is 14X14

% l1-least square
wLDs = NaN(size(wp_LDs));

ALDs = mFeatDataLDs';

err_tol_Ds = 1e-4;

lambdaDs = 0.7; % l1 cost weight

for i = 1:7
    BLDs = mMusDataLDs(i,:)';
    
    [lambda_max] = find_lambdamax_l1_ls(ALDs',BLDs);
    lambdaDs = lambda_max*err_tol_Ds;
    
    [xLDs,~] = l1_ls(ALDs,BLDs,lambdaDs);
    wLDs(i,:) = xLDs';
end

%%%%%%%%% RIGHT %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% only least square solution using pinv
wp_RDs = mMusDataRDs*pinv(mFeatDataRDs); % wp is 14X14

% l1-least square
wRDs = NaN(size(wp_RDs));

ARDs = mFeatDataRDs';

for i = 1:7
    BRDs = mMusDataRDs(i,:)';
       
    [lambda_max] = find_lambdamax_l1_ls(ARDs',BRDs);
    lambdaDs = lambda_max*err_tol_Ds;
    
    [xRDs,~] = l1_ls(ARDs,BRDs,lambdaDs);
    wRDs(i,:) = xRDs';
        
end

%% Test weight w on a particular sample gait left leg     

t = randi(testNum,1,1); % index of test sample

% left Ds
testFeatLDs = [tLL_HFLDataLDs(t,:); tLF_GLUDataLDs(t,:); tLL_HAMDataLDs(t,:); tLF_HAMDataLDs(t,:); tLF_VASDataLDs(t,:); tLF_GASDataLDs(t,:); tLF_SOLDataLDs(t,:); tLL_TADataLDs(t,:); tTrunkDataLDs(t,:); ...
            tRL_HFLDataLDs(t,:); tRF_GLUDataLDs(t,:); tRL_HAMDataLDs(t,:); tRF_HAMDataLDs(t,:); tRF_VASDataLDs(t,:); tRF_GASDataLDs(t,:); tRF_SOLDataLDs(t,:); tRL_TADataLDs(t,:); tdTrunkDataLDs(t,:)];
       
trueMusLDs = [tLHFLDataLDs(t,:); tLGLUDataLDs(t,:); tLHAMDataLDs(t,:); tLVASDataLDs(t,:); tLGASDataLDs(t,:); tLSOLDataLDs(t,:); tLTADataLDs(t,:)]; 
 
testMusLDs = wLDs*testFeatLDs;        
testMusp_LDs = wp_LDs*testFeatLDs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % right Ds
testFeatRDs = [tRL_HFLDataRDs(t,:); tRF_GLUDataRDs(t,:); tRL_HAMDataRDs(t,:); tRF_HAMDataRDs(t,:); tRF_VASDataRDs(t,:); tRF_GASDataRDs(t,:); tRF_SOLDataRDs(t,:); tRL_TADataRDs(t,:); tTrunkDataRDs(t,:);...
    tLL_HFLDataRDs(t,:); tLF_GLUDataRDs(t,:); tLL_HAMDataRDs(t,:); tLF_HAMDataRDs(t,:); tLF_VASDataRDs(t,:); tLF_GASDataRDs(t,:); tLF_SOLDataRDs(t,:); tLL_TADataRDs(t,:); tdTrunkDataRDs(t,:)];
       
trueMusRDs = [tRHFLDataRDs(t,:); tRGLUDataRDs(t,:); tRHAMDataRDs(t,:); tRVASDataRDs(t,:); tRGASDataRDs(t,:); tRSOLDataRDs(t,:); tRTADataRDs(t,:)]; 
 
testMusRDs = wRDs*testFeatRDs;        
testMusp_RDs = wp_RDs*testFeatRDs;

%% visualize ds
%cd linRel_Results/

figure;
subplot(7,1,1); plot(testMusLDs(1,:)');hold on
subplot(7,1,1); plot(testMusp_LDs(1,:)');
subplot(7,1,1); plot(trueMusLDs(1,:)');
title('Left muscles DS reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('LHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusLDs(2,:)');hold on
subplot(7,1,2); plot(testMusp_LDs(2,:)');
subplot(7,1,2); plot(trueMusLDs(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMusLDs(3,:)');hold on
subplot(7,1,3); plot(testMusp_LDs(3,:)');
subplot(7,1,3); plot(trueMusLDs(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMusLDs(4,:)');hold on
subplot(7,1,4); plot(testMusp_LDs(4,:)');
subplot(7,1,4); plot(trueMusLDs(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMusLDs(5,:)');hold on
subplot(7,1,5); plot(testMusp_LDs(5,:)');
subplot(7,1,5); plot(trueMusLDs(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMusLDs(6,:)');hold on
subplot(7,1,6); plot(testMusp_LDs(6,:)');
subplot(7,1,6); plot(trueMusLDs(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMusLDs(7,:)');hold on
subplot(7,1,7); plot(testMusp_LDs(7,:)');
subplot(7,1,7); plot(trueMusLDs(7,:)');ylabel('LTA');

%export_fig -transparent reconstructSparse_MusFeatLDs.pdf

figure;
subplot(7,1,1); plot(testMusRDs(1,:)');hold on
subplot(7,1,1); plot(testMusp_RDs(1,:)');
subplot(7,1,1); plot(trueMusRDs(1,:)');
title('Right muscles DS reconstructed (using 2-leg Mus Feat)-comparing ls vs l1-ls'); ylabel('RHFL');
legend('Reconstructed-sparse','Reconstructed-leastSq','TrueData');

subplot(7,1,2); plot(testMusRDs(2,:)');hold on
subplot(7,1,2); plot(testMusp_RDs(2,:)');
subplot(7,1,2); plot(trueMusRDs(2,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMusRDs(3,:)');hold on
subplot(7,1,3); plot(testMusp_RDs(3,:)');
subplot(7,1,3); plot(trueMusRDs(3,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMusRDs(4,:)');hold on
subplot(7,1,4); plot(testMusp_RDs(4,:)');
subplot(7,1,4); plot(trueMusRDs(4,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMusRDs(5,:)');hold on
subplot(7,1,5); plot(testMusp_RDs(5,:)');
subplot(7,1,5); plot(trueMusRDs(5,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMusRDs(6,:)');hold on
subplot(7,1,6); plot(testMusp_RDs(6,:)');
subplot(7,1,6); plot(trueMusRDs(6,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMusRDs(7,:)');hold on
subplot(7,1,7); plot(testMusp_RDs(7,:)');
subplot(7,1,7); plot(trueMusRDs(7,:)');ylabel('RTA');

%export_fig -transparent reconstructSparse_MusFeatRDs.pdf

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};

%% left stance w
figure;imagesc(abs(wp_LDs)/(max(max(abs(wp_LDs)))));colorbar;title('Leg weights LEFT DS (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wLDs)/(max(max(abs(wLDs)))));colorbar;title('Leg weights LEFT DS (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

%% Visualize w RIGHT

labels_ry = {'RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_rx = {'RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','Trunk','LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','dTrunk'};

%% right stance w
figure;imagesc(abs(wp_RDs)/(max(max(abs(wp_RDs)))));colorbar;title('Leg weights RIGHT DS (2-leg Mus Feat Data)-leastSq');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

figure;imagesc(abs(wRDs)/(max(max(abs(wRDs)))));colorbar;title('Leg weights RIGHT DS (2-leg Mus Feat Data)-sparse');
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ry);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

%cd ..