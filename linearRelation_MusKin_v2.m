% Relate kinematics data to muscle stimulation
clc;
close all;

%load linRel_13ms_NMwalking_100sData.mat;
load linRel_13ms_NMwalking_100sTrainTestData.mat;

%% Direct linear relationship
% mMusData = W mKinData (min kinematic data relates to mean muscle stimualtion)

w1 = mMusData*pinv(mKinData); % w1 is 14X14

% Test weight w on a particular sample gait        

t = randi(testNum,1,1); % index of test sample

testKin = [tLAnkData(t,:); tLKneeData(t,:); tLHipData(t,:); tRAnkData(t,:); tRKneeData(t,:); tRHipData(t,:); tTrunkData(t,:); ...
            tdLAnkData(t,:); tdLKneeData(t,:); tdLHipData(t,:); tdRAnkData(t,:); tdRKneeData(t,:); tdRHipData(t,:); tdTrunkData(t,:)];
        
trueMus = [tLHFLData(t,:); tLGLUData(t,:); tLHAMData(t,:); tLVASData(t,:); tLGASData(t,:); tLSOLData(t,:); tLTAData(t,:); ...
            tRHFLData(t,:); tRGLUData(t,:); tRHAMData(t,:); tRVASData(t,:); tRGASData(t,:); tRSOLData(t,:); tRTAData(t,:)]; 
        
testMus1 = w1*testKin;

% visualize 

figure;
subplot(7,1,1); plot(testMus1(1,:)');hold on
subplot(7,1,1); plot(trueMus(1,:)');
title('Left muscles reconstructed-using 2 legs Kin data'); ylabel('LHFL');
legend('Reconstructed','TrueData');

subplot(7,1,2); plot(testMus1(2,:)');hold on
subplot(7,1,2); plot(trueMus(2,:)');ylabel('LGLU');

subplot(7,1,3); plot(testMus1(3,:)');hold on
subplot(7,1,3); plot(trueMus(3,:)');ylabel('LHAM');

subplot(7,1,4); plot(testMus1(4,:)');hold on
subplot(7,1,4); plot(trueMus(4,:)');ylabel('LVAS');

subplot(7,1,5); plot(testMus1(5,:)');hold on
subplot(7,1,5); plot(trueMus(5,:)');ylabel('LGAS');

subplot(7,1,6); plot(testMus1(6,:)');hold on
subplot(7,1,6); plot(trueMus(6,:)');ylabel('LSOL');

subplot(7,1,7); plot(testMus1(7,:)');hold on
subplot(7,1,7); plot(trueMus(7,:)');ylabel('LTA');

export_fig -transparent reconstruct_LMusKin.pdf

figure;
subplot(7,1,1); plot(testMus1(8,:)');hold on
subplot(7,1,1); plot(trueMus(8,:)');
title('Right muscles reconstructed-using 2 legs Kin data'); ylabel('RHFL');
legend('Reconstructed','TrueData');

subplot(7,1,2); plot(testMus1(9,:)');hold on
subplot(7,1,2); plot(trueMus(9,:)');ylabel('RGLU');

subplot(7,1,3); plot(testMus1(10,:)');hold on
subplot(7,1,3); plot(trueMus(10,:)');ylabel('RHAM');

subplot(7,1,4); plot(testMus1(11,:)');hold on
subplot(7,1,4); plot(trueMus(11,:)');ylabel('RVAS');

subplot(7,1,5); plot(testMus1(12,:)');hold on
subplot(7,1,5); plot(trueMus(12,:)');ylabel('RGAS');

subplot(7,1,6); plot(testMus1(13,:)');hold on
subplot(7,1,6); plot(trueMus(13,:)');ylabel('RSOL');

subplot(7,1,7); plot(testMus1(14,:)');hold on
subplot(7,1,7); plot(trueMus(14,:)');ylabel('RTA');

export_fig -transparent reconstruct_RMusKin.pdf

%% Visualize w

labels_y = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA','RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_x = {'LAnk','LKnee','LHip','RAnk','RKnee','RHip','Trunk',...
    'dLAnk','dLKnee','dLHip','dRAnk','dRKnee','dRHip','dTrunk'};

figure;imagesc(abs(w1));colorbar;title('Leg weights(2-leg Mus Kin Data)');
set(gca(),'YTick',1:14) 
set(gca(),'YTickLabel',labels_y);
set(gca(),'XTick',1:14) 
set(gca(),'XTickLabel',labels_x, 'XTickLabelRotation', 90);

