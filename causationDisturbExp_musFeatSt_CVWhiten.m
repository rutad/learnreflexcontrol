%% Disturbance experiments for establishing causation with Cross validation on whitened data
% Ruta Desai
% rutad@cs.cmu.edu
% 29th June 2015

%% Set up
clc; 
%close all; 
%clear all;

numFeat = 18; %(16 Mus+2 trunk Kin features)
numMus = 7;
count = 0; % count of disturbance scenarios

nms_model_MechInit;
nms_model_ControlInit;
modelName ='nms_saveMusFeatDataALLDisturbWaitSt_PCALinRel';

load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mMusDataLSt');
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mFeatDataLSt');

%% Disturbance times and forces in swing

% 10%,50% and 90% of swing -- early, mid and late swing disturbances
tDisturb = [0.1*350*1e-3;0.5*350*1e-3;0.9*350*1e-3];
%fDisturb = 25; %15; %[N]

motionDir = [-1,1]; %1 -> flex, -1 -> extend
numDisturbJnts = 4;

WStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
mean_wDistLSt = zeros(numMus, numFeat);
mean_delWLSt = zeros(size(mean_wDistLSt));

musDistDataStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
featDistDataStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);

tmusDistDataStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);
tfeatDistDataStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);

trunkNormStore = cell(length(motionDir),length(tDisturb),numDisturbJnts);

meanTrunkNormMax = [0, 0];

%% Disturb and collect mean stats over disturbed swings

for k = 1:length(motionDir)

    flex_extend = motionDir(k);
    
    for i = 1:length(tDisturb)
       tDisturbSt = tDisturb(i);

       for jntInd = 1:numDisturbJnts

           fd = {0, 0, 0, 0};
           fd(jntInd) = {fDisturb};
           [F_disturb_foot, F_disturb_shank, F_disturb_thigh,F_disturb_trunk]= fd{:};
           sim(modelName);

           sensors = {LL_HFL, LF_GLU, LL_HAM, LF_HAM, LF_VAS, LF_GAS, LF_SOL, LL_TA, ...
               RL_HFL, RF_GLU, RL_HAM, RF_HAM, RF_VAS, RF_GAS, RF_SOL, RL_TA, Torso};

           actuators = {LStimHFL,LStimGLU, LStimHAM, LStimVAS, LStimGAS, LStimSOL, LStimTA};

           fd_sig = {fd_lfoot, fd_lshank, fd_lthigh, fd_trunk};
           
           %disp([k,i,jntInd])
           %keyboard

           % obtain average sensor and actuator over disturbed swings
           [mMusDataDistLSt, mFeatDataDistLSt,trunkNormMax,tMusDataDistLSt,tFeatDataDistLSt] =  collectGaitDataDistSt_wTestData(fd_sig{jntInd}, ...
                                                    sensors, actuators, L_TD, L_TO);

           if (isempty(tMusDataDistLSt))
               continue;
           else
               testMusDistData = [tMusDataDistLSt{1} tMusDataDistLSt{2}];
               testFeatDistData = [tFeatDataDistLSt{1} tFeatDataDistLSt{2}];

               % obtain sparse correlations/ weights for each disturbance scenario
               [wDistLSt, delWLSt] =  wSparseDistSt_wCVWhitenData({mFeatDataDistLSt,mMusDataDistLSt},...
                                    {testFeatDistData,testMusDistData},{mFeatDataLSt,mMusDataLSt});

                musDistDataStore{k,i,jntInd}= mMusDataDistLSt;
                featDistDataStore{k,i,jntInd}= mFeatDataDistLSt;
                tmusDistDataStore{k,i,jntInd}= testMusDistData;
                tfeatDistDataStore{k,i,jntInd}= testFeatDistData;
                trunkNormStore{k,i,jntInd}=trunkNormMax;
                WStore{k,i,jntInd} = [wDistLSt, delWLSt];

                mean_wDistLSt = mean_wDistLSt + wDistLSt;
                mean_delWLSt = mean_delWLSt + delWLSt;
                meanTrunkNormMax = meanTrunkNormMax + trunkNormMax;
           end
         
            count = count +1
                     
       end    


    end
    
end


mean_wDistLSt = mean_wDistLSt./count;
mean_delWLSt = mean_delWLSt./count;
meanTrunkNormMax = meanTrunkNormMax/count;

%% Visualize w LEFT

labels_ly = {'LHFL','LGLU','LHAM','LVAS','LGAS','LSOL','LTA'};
labels_lx = {'LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','Trunk','RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','dTrunk'};


%% disturbed left swing w
str_disturb = strcat('Fdisturb =', num2str(fDisturb));

figure;imagesc(abs(mean_wDistLSt)./repmat(max(abs(mean_wDistLSt),[],2), 1, size(mean_wDistLSt,2)));colorbar;title({'Leg weights DISTURBED LEFT STANCE (2-leg Mus Feat Data)-sparse',str_disturb});
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

savefig1 = strcat('wSparseMEANSt_musFeatDistWaitwNormTrk_CVWhiten_f',num2str(fDisturb),'.pdf');
saveas(gcf,savefig1);

%% delta left swing w (disturbed - steady)
figure;imagesc(abs(mean_delWLSt)./repmat(max(abs(mean_delWLSt),[],2), 1, size(mean_delWLSt,2)));colorbar;title({'Leg weights DEL LEFT STANCE (2-leg Mus Feat Data)-sparse',str_disturb});
set(gca(),'YTick',1:7) 
set(gca(),'YTickLabel',labels_ly);
set(gca(),'XTick',1:18) 
set(gca(),'XTickLabel',labels_lx, 'XTickLabelRotation', 90);

savefig2 = strcat('delWSparseMEANSt_musFeatDistWaitwNormTrk_CVWhiten_f',num2str(fDisturb),'.pdf');
saveas(gcf,savefig2);


