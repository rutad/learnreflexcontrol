function [FeatDataLSt, FeatDataRSt, FeatDataLSw, FeatDataRSw, MusDataLSt,MusDataRSt, MusDataLSw, MusDataRSw,trunkMax,dtrunkMax] = ...
    collectGaitDataMusFeat_CVRoughGnd_Train(trainDataInd,holdDataInd,LStim, RStim, LFeat, RFeat, Torso, ...
    LgaitStart_ind, LgaitEnd_ind, LswingStart_ind, LswingEnd_ind, ...
    RgaitStart_ind, RgaitEnd_ind, RswingStart_ind, RswingEnd_ind)

% total length left
DataLenLSt = sum(LswingStart_ind(trainDataInd)-LgaitStart_ind(trainDataInd))+length(LswingStart_ind(trainDataInd)-LgaitStart_ind(trainDataInd));
DataLenLSw = sum(LswingEnd_ind(trainDataInd)-LswingStart_ind(trainDataInd))+length(LswingEnd_ind(trainDataInd)-LswingStart_ind(trainDataInd));

% total length right
DataLenRSt = sum(RswingStart_ind(trainDataInd)-RgaitStart_ind(trainDataInd))+length(RswingStart_ind(trainDataInd)-RgaitStart_ind(trainDataInd));
DataLenRSw = sum(RswingEnd_ind(trainDataInd)-RswingStart_ind(trainDataInd))+length(RswingEnd_ind(trainDataInd)-RswingStart_ind(trainDataInd));

% DataLen
LdataLen = length(LgaitStart_ind);
RdataLen = length(RgaitStart_ind);

%% Parse

LStimHFL = LStim(1);
LStimGLU = LStim(2);
LStimHAM = LStim(3);
LStimVAS = LStim(4);
LStimGAS = LStim(5);
LStimSOL = LStim(6);
LStimTA = LStim(7);

RStimHFL = RStim(1);
RStimGLU = RStim(2);
RStimHAM = RStim(3);
RStimVAS = RStim(4);
RStimGAS = RStim(5);
RStimSOL = RStim(6);
RStimTA = RStim(7);

LL_HFL = LFeat(1);
LF_GLU = LFeat(2);
LL_HAM = LFeat(3);
LF_HAM = LFeat(4);
LF_VAS = LFeat(5);
LF_GAS = LFeat(6);
LF_SOL = LFeat(7);
LL_TA = LFeat(8);

RL_HFL = RFeat(1);
RF_GLU = RFeat(2);
RL_HAM = RFeat(3);
RF_HAM = RFeat(4);
RF_VAS = RFeat(5);
RF_GAS = RFeat(6);
RF_SOL = RFeat(7);
RL_TA = RFeat(8);

%% Store kinematics and muscle data from gait cycles (Stance)

% Preallocate memory for left stance muscle feat data

LL_HFLDataLSt = NaN(DataLenLSt,1);
LF_GLUDataLSt = NaN(DataLenLSt,1);
LL_HAMDataLSt = NaN(DataLenLSt,1);
LF_HAMDataLSt = NaN(DataLenLSt,1);
LF_VASDataLSt = NaN(DataLenLSt,1);
LF_GASDataLSt = NaN(DataLenLSt,1);
LF_SOLDataLSt = NaN(DataLenLSt,1);
LL_TADataLSt = NaN(DataLenLSt,1);

RL_HFLDataLSt = NaN(DataLenLSt,1);
RF_GLUDataLSt = NaN(DataLenLSt,1);
RL_HAMDataLSt = NaN(DataLenLSt,1);
RF_HAMDataLSt = NaN(DataLenLSt,1);
RF_VASDataLSt = NaN(DataLenLSt,1);
RF_GASDataLSt = NaN(DataLenLSt,1);
RF_SOLDataLSt = NaN(DataLenLSt,1);
RL_TADataLSt = NaN(DataLenLSt,1);

TrunkDataLSt = NaN(DataLenLSt,1);
dTrunkDataLSt = NaN(DataLenLSt,1);

% Preallocate memory for left stance muscle data

LHFLDataLSt = NaN(DataLenLSt,1);
LGLUDataLSt = NaN(DataLenLSt,1);
LHAMDataLSt = NaN(DataLenLSt,1);
LVASDataLSt = NaN(DataLenLSt,1);
LGASDataLSt = NaN(DataLenLSt,1);
LSOLDataLSt = NaN(DataLenLSt,1);
LTADataLSt = NaN(DataLenLSt,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right stance muscle feat data

LL_HFLDataRSt = NaN(DataLenRSt,1);
LF_GLUDataRSt = NaN(DataLenRSt,1);
LL_HAMDataRSt = NaN(DataLenRSt,1);
LF_HAMDataRSt = NaN(DataLenRSt,1);
LF_VASDataRSt = NaN(DataLenRSt,1);
LF_GASDataRSt = NaN(DataLenRSt,1);
LF_SOLDataRSt = NaN(DataLenRSt,1);
LL_TADataRSt = NaN(DataLenRSt,1);

RL_HFLDataRSt = NaN(DataLenRSt,1);
RF_GLUDataRSt = NaN(DataLenRSt,1);
RL_HAMDataRSt = NaN(DataLenRSt,1);
RF_HAMDataRSt = NaN(DataLenRSt,1);
RF_VASDataRSt = NaN(DataLenRSt,1);
RF_GASDataRSt = NaN(DataLenRSt,1);
RF_SOLDataRSt = NaN(DataLenRSt,1);
RL_TADataRSt = NaN(DataLenRSt,1);

TrunkDataRSt = NaN(DataLenRSt,1);
dTrunkDataRSt = NaN(DataLenRSt,1);

% Preallocate memory for right muscle stance data

RHFLDataRSt = NaN(DataLenRSt,1);
RGLUDataRSt = NaN(DataLenRSt,1);
RHAMDataRSt = NaN(DataLenRSt,1);
RVASDataRSt = NaN(DataLenRSt,1);
RGASDataRSt = NaN(DataLenRSt,1);
RSOLDataRSt = NaN(DataLenRSt,1);
RTADataRSt = NaN(DataLenRSt,1);

indStart_LSt = 1;
indStart_RSt = 1;

for i=1:LdataLen

    if ~isempty(find(i == holdDataInd))
        continue
    else
        indEnd_LSt = (LswingStart_ind(i)-LgaitStart_ind(i))+indStart_LSt;

        LLHFL_LSt = LL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); 

        LFGLU_LSt = LF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); 

        LLHAM_LSt = LL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LFHAM_LSt = LF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LFVAS_LSt = LF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LFGAS_LSt = LF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LFSOL_LSt = LF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LLTA_LSt = LL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        RLHFL_LSt = RL_HFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angle

        RFGLU_LSt = RF_GLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:); % angular velocity

        RLHAM_LSt = RL_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        RFHAM_LSt = RF_HAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        RFVAS_LSt = RF_VAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        RFGAS_LSt = RF_GAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        RFSOL_LSt = RF_SOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        RLTA_LSt = RL_TA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);   

        Trunk_LSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),1);
        dTrunk_LSt = Torso.Data(LgaitStart_ind(i):LswingStart_ind(i),2);

        LHFL_LSt = LStimHFL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LGLU_LSt = LStimGLU.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LHAM_LSt = LStimHAM.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LVAS_LSt = LStimVAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LGAS_LSt = LStimGAS.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LSOL_LSt = LStimSOL.Data(LgaitStart_ind(i):LswingStart_ind(i),:);

        LTA_LSt = LStimTA.Data(LgaitStart_ind(i):LswingStart_ind(i),:);    

        LL_HFLDataLSt(indStart_LSt:indEnd_LSt,:) = LLHFL_LSt;
        LF_GLUDataLSt(indStart_LSt:indEnd_LSt,:) = LFGLU_LSt;
        LL_HAMDataLSt(indStart_LSt:indEnd_LSt,:) = LLHAM_LSt;
        LF_HAMDataLSt(indStart_LSt:indEnd_LSt,:) = LFHAM_LSt;
        LF_VASDataLSt(indStart_LSt:indEnd_LSt,:) = LFVAS_LSt;
        LF_GASDataLSt(indStart_LSt:indEnd_LSt,:) = LFGAS_LSt;
        LF_SOLDataLSt(indStart_LSt:indEnd_LSt,:) = LFSOL_LSt;
        LL_TADataLSt(indStart_LSt:indEnd_LSt,:) = LLTA_LSt;

        TrunkDataLSt(indStart_LSt:indEnd_LSt,:) = Trunk_LSt;

        RL_HFLDataLSt(indStart_LSt:indEnd_LSt,:) = RLHFL_LSt;
        RF_GLUDataLSt(indStart_LSt:indEnd_LSt,:) = RFGLU_LSt;
        RL_HAMDataLSt(indStart_LSt:indEnd_LSt,:) = RLHAM_LSt;
        RF_HAMDataLSt(indStart_LSt:indEnd_LSt,:) = RFHAM_LSt;
        RF_VASDataLSt(indStart_LSt:indEnd_LSt,:) = RFVAS_LSt;
        RF_GASDataLSt(indStart_LSt:indEnd_LSt,:) = RFGAS_LSt;
        RF_SOLDataLSt(indStart_LSt:indEnd_LSt,:) = RFSOL_LSt;
        RL_TADataLSt(indStart_LSt:indEnd_LSt,:) = RLTA_LSt;

        dTrunkDataLSt(indStart_LSt:indEnd_LSt,:) = dTrunk_LSt;

        LHFLDataLSt(indStart_LSt:indEnd_LSt,:) = LHFL_LSt;
        LGLUDataLSt(indStart_LSt:indEnd_LSt,:) = LGLU_LSt;
        LHAMDataLSt(indStart_LSt:indEnd_LSt,:) = LHAM_LSt;
        LVASDataLSt(indStart_LSt:indEnd_LSt,:) = LVAS_LSt;
        LGASDataLSt(indStart_LSt:indEnd_LSt,:) = LGAS_LSt;
        LSOLDataLSt(indStart_LSt:indEnd_LSt,:) = LSOL_LSt;
        LTADataLSt(indStart_LSt:indEnd_LSt,:) = LTA_LSt;

        indStart_LSt = indEnd_LSt+1;
    end
end
        
for i=1:RdataLen

    if ~isempty(find(i == holdDataInd))
        continue
    else    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        indEnd_RSt = (RswingStart_ind(i)-RgaitStart_ind(i))+indStart_RSt;

        LLHFL_RSt = LL_HFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angle

        LFGLU_RSt = LF_GLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angular velocity

        LLHAM_RSt = LL_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        LFHAM_RSt = LF_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        LFVAS_RSt = LF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        LFGAS_RSt = LF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        LFSOL_RSt = LF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        LLTA_RSt = LL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RLHFL_RSt = RL_HFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angle

        RFGLU_RSt = RF_GLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:); % angular velocity

        RLHAM_RSt = RL_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RFHAM_RSt = RF_HAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RFVAS_RSt = RF_VAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RFGAS_RSt = RF_GAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RFSOL_RSt = RF_SOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RLTA_RSt = RL_TA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);   

        Trunk_RSt = Torso.Data(RgaitStart_ind(i):RswingStart_ind(i),1);
        dTrunk_RSt = Torso.Data(RgaitStart_ind(i):RswingStart_ind(i),2);

        RHF_RSt = RStimHFL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RGLU_RSt = RStimGLU.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RHAM_RSt = RStimHAM.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RVAS_RSt = RStimVAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RGAS_RSt = RStimGAS.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RSOL_RSt = RStimSOL.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        RTA_RSt = RStimTA.Data(RgaitStart_ind(i):RswingStart_ind(i),:);

        LL_HFLDataRSt(indStart_RSt:indEnd_RSt,:) = LLHFL_RSt;
        LF_GLUDataRSt(indStart_RSt:indEnd_RSt,:) = LFGLU_RSt;
        LL_HAMDataRSt(indStart_RSt:indEnd_RSt,:) = LLHAM_RSt;
        LF_HAMDataRSt(indStart_RSt:indEnd_RSt,:) = LFHAM_RSt;
        LF_VASDataRSt(indStart_RSt:indEnd_RSt,:) = LFVAS_RSt;
        LF_GASDataRSt(indStart_RSt:indEnd_RSt,:) = LFGAS_RSt;
        LF_SOLDataRSt(indStart_RSt:indEnd_RSt,:) = LFSOL_RSt;
        LL_TADataRSt(indStart_RSt:indEnd_RSt,:) = LLTA_RSt;

        TrunkDataRSt(indStart_RSt:indEnd_RSt,:) = Trunk_RSt;

        RL_HFLDataRSt(indStart_RSt:indEnd_RSt,:) = RLHFL_RSt;
        RF_GLUDataRSt(indStart_RSt:indEnd_RSt,:) = RFGLU_RSt;
        RL_HAMDataRSt(indStart_RSt:indEnd_RSt,:) = RLHAM_RSt;
        RF_HAMDataRSt(indStart_RSt:indEnd_RSt,:) = RFHAM_RSt;
        RF_VASDataRSt(indStart_RSt:indEnd_RSt,:) = RFVAS_RSt;
        RF_GASDataRSt(indStart_RSt:indEnd_RSt,:) = RFGAS_RSt;
        RF_SOLDataRSt(indStart_RSt:indEnd_RSt,:) = RFSOL_RSt;
        RL_TADataRSt(indStart_RSt:indEnd_RSt,:) = RLTA_RSt;

        dTrunkDataRSt(indStart_RSt:indEnd_RSt,:) = dTrunk_RSt;

        RHFLDataRSt(indStart_RSt:indEnd_RSt,:) = RHF_RSt;
        RGLUDataRSt(indStart_RSt:indEnd_RSt,:) = RGLU_RSt;
        RHAMDataRSt(indStart_RSt:indEnd_RSt,:) = RHAM_RSt;
        RVASDataRSt(indStart_RSt:indEnd_RSt,:) = RVAS_RSt;
        RGASDataRSt(indStart_RSt:indEnd_RSt,:) = RGAS_RSt;
        RSOLDataRSt(indStart_RSt:indEnd_RSt,:) = RSOL_RSt;
        RTADataRSt(indStart_RSt:indEnd_RSt,:) = RTA_RSt;

        indStart_RSt = indEnd_RSt+1;
    end
end

%% Store kinematics and muscle data from gait cycles (Swing)

% Preallocate memory for left swing muscle feat data

LL_HFLDataLSw = NaN(DataLenLSw,1);
LF_GLUDataLSw = NaN(DataLenLSw,1);
LL_HAMDataLSw = NaN(DataLenLSw,1);
LF_HAMDataLSw = NaN(DataLenLSw,1);
LF_VASDataLSw = NaN(DataLenLSw,1);
LF_GASDataLSw = NaN(DataLenLSw,1);
LF_SOLDataLSw = NaN(DataLenLSw,1);
LL_TADataLSw = NaN(DataLenLSw,1);

RL_HFLDataLSw = NaN(DataLenLSw,1);
RF_GLUDataLSw = NaN(DataLenLSw,1);
RL_HAMDataLSw = NaN(DataLenLSw,1);
RF_HAMDataLSw = NaN(DataLenLSw,1);
RF_VASDataLSw = NaN(DataLenLSw,1);
RF_GASDataLSw = NaN(DataLenLSw,1);
RF_SOLDataLSw = NaN(DataLenLSw,1);
RL_TADataLSw = NaN(DataLenLSw,1);

TrunkDataLSw = NaN(DataLenLSw,1);
dTrunkDataLSw = NaN(DataLenLSw,1);

% Preallocate memory for left swing muscle data

LHFLDataLSw = NaN(DataLenLSw,1);
LGLUDataLSw = NaN(DataLenLSw,1);
LHAMDataLSw = NaN(DataLenLSw,1);
LVASDataLSw = NaN(DataLenLSw,1);
LGASDataLSw = NaN(DataLenLSw,1);
LSOLDataLSw = NaN(DataLenLSw,1);
LTADataLSw = NaN(DataLenLSw,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preallocate memory for right swing muscle feat data

LL_HFLDataRSw = NaN(DataLenRSw,1);
LF_GLUDataRSw = NaN(DataLenRSw,1);
LL_HAMDataRSw = NaN(DataLenRSw,1);
LF_HAMDataRSw = NaN(DataLenRSw,1);
LF_VASDataRSw = NaN(DataLenRSw,1);
LF_GASDataRSw = NaN(DataLenRSw,1);
LF_SOLDataRSw = NaN(DataLenRSw,1);
LL_TADataRSw = NaN(DataLenRSw,1);

RL_HFLDataRSw = NaN(DataLenRSw,1);
RF_GLUDataRSw = NaN(DataLenRSw,1);
RL_HAMDataRSw = NaN(DataLenRSw,1);
RF_HAMDataRSw = NaN(DataLenRSw,1);
RF_VASDataRSw = NaN(DataLenRSw,1);
RF_GASDataRSw = NaN(DataLenRSw,1);
RF_SOLDataRSw = NaN(DataLenRSw,1);
RL_TADataRSw = NaN(DataLenRSw,1);

TrunkDataRSw = NaN(DataLenRSw,1);
dTrunkDataRSw = NaN(DataLenRSw,1);

% Preallocate memory for right muscle swing data

RHFLDataRSw = NaN(DataLenRSw,1);
RGLUDataRSw = NaN(DataLenRSw,1);
RHAMDataRSw = NaN(DataLenRSw,1);
RVASDataRSw = NaN(DataLenRSw,1);
RGASDataRSw = NaN(DataLenRSw,1);
RSOLDataRSw = NaN(DataLenRSw,1);
RTADataRSw = NaN(DataLenRSw,1);

indStart_LSw = 1;
indStart_RSw = 1;

for i=1:LdataLen
            
    if ~isempty(find(i == holdDataInd))
        continue
    else
        indEnd_LSw = (LgaitEnd_ind(i)-LswingStart_ind(i))+indStart_LSw;

        LLHFL_LSw = LL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LFGLU_LSw = LF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); 

        LLHAM_LSw = LL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LFHAM_LSw = LF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LFVAS_LSw = LF_VAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LFGAS_LSw = LF_GAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LFSOL_LSw = LF_SOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LLTA_LSw = LL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        RLHFL_LSw = RL_HFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angle

        RFGLU_LSw = RF_GLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:); % angular velocity

        RLHAM_LSw = RL_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        RFHAM_LSw = RF_HAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        RFVAS_LSw = RF_VAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        RFGAS_LSw = RF_GAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        RFSOL_LSw = RF_SOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        RLTA_LSw = RL_TA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        Trunk_LSw = Torso.Data(LswingStart_ind(i):LgaitEnd_ind(i),1);
        dTrunk_LSw = Torso.Data(LswingStart_ind(i):LgaitEnd_ind(i),2);

        LHFL_LSw = LStimHFL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LGLU_LSw = LStimGLU.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LHAM_LSw = LStimHAM.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LVAS_LSw = LStimVAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LGAS_LSw = LStimGAS.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LSOL_LSw = LStimSOL.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LTA_LSw = LStimTA.Data(LswingStart_ind(i):LgaitEnd_ind(i),:);

        LL_HFLDataLSw(indStart_LSw:indEnd_LSw,:) = LLHFL_LSw;
        LF_GLUDataLSw(indStart_LSw:indEnd_LSw,:) = LFGLU_LSw;
        LL_HAMDataLSw(indStart_LSw:indEnd_LSw,:) = LLHAM_LSw;
        LF_HAMDataLSw(indStart_LSw:indEnd_LSw,:) = LFHAM_LSw;
        LF_VASDataLSw(indStart_LSw:indEnd_LSw,:) = LFVAS_LSw;
        LF_GASDataLSw(indStart_LSw:indEnd_LSw,:) = LFGAS_LSw;
        LF_SOLDataLSw(indStart_LSw:indEnd_LSw,:) = LFSOL_LSw;
        LL_TADataLSw(indStart_LSw:indEnd_LSw,:) = LLTA_LSw;

        TrunkDataLSw(indStart_LSw:indEnd_LSw,:) = Trunk_LSw;

        RL_HFLDataLSw(indStart_LSw:indEnd_LSw,:) = RLHFL_LSw;
        RF_GLUDataLSw(indStart_LSw:indEnd_LSw,:) = RFGLU_LSw;
        RL_HAMDataLSw(indStart_LSw:indEnd_LSw,:) = RLHAM_LSw;
        RF_HAMDataLSw(indStart_LSw:indEnd_LSw,:) = RFHAM_LSw;
        RF_VASDataLSw(indStart_LSw:indEnd_LSw,:) = RFVAS_LSw;
        RF_GASDataLSw(indStart_LSw:indEnd_LSw,:) = RFGAS_LSw;
        RF_SOLDataLSw(indStart_LSw:indEnd_LSw,:) = RFSOL_LSw;
        RL_TADataLSw(indStart_LSw:indEnd_LSw,:) = RLTA_LSw;

        dTrunkDataLSw(indStart_LSw:indEnd_LSw,:) = dTrunk_LSw;

        LHFLDataLSw(indStart_LSw:indEnd_LSw,:) = LHFL_LSw;
        LGLUDataLSw(indStart_LSw:indEnd_LSw,:) = LGLU_LSw;
        LHAMDataLSw(indStart_LSw:indEnd_LSw,:) = LHAM_LSw;
        LVASDataLSw(indStart_LSw:indEnd_LSw,:) = LVAS_LSw;
        LGASDataLSw(indStart_LSw:indEnd_LSw,:) = LGAS_LSw;
        LSOLDataLSw(indStart_LSw:indEnd_LSw,:) = LSOL_LSw;
        LTADataLSw(indStart_LSw:indEnd_LSw,:) = LTA_LSw;

        indStart_LSw = indEnd_LSw+1;

    end
end

for i=1:RdataLen

    if ~isempty(find(i == holdDataInd))
        continue
    else
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        indEnd_RSw = (RgaitEnd_ind(i)-RswingStart_ind(i))+indStart_RSw;

        LLHFL_RSw = LL_HFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); 

        LFGLU_RSw = LF_GLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); 

        LLHAM_RSw = LL_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        LFHAM_RSw = LF_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        LFVAS_RSw = LF_VAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        LFGAS_RSw = LF_GAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        LFSOL_RSw = LF_SOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        LLTA_RSw = LL_TA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RLHFL_RSw = RL_HFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angle

        RFGLU_RSw = RF_GLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:); % angular velocity

        RLHAM_RSw = RL_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RFHAM_RSw = RF_HAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RFVAS_RSw = RF_VAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RFGAS_RSw = RF_GAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RFSOL_RSw = RF_SOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RLTA_RSw = RL_TA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);   

        Trunk_RSw = Torso.Data(RswingStart_ind(i):RgaitEnd_ind(i),1);
        dTrunk_RSw = Torso.Data(RswingStart_ind(i):RgaitEnd_ind(i),2);

        RHF_RSw = RStimHFL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RGLU_RSw = RStimGLU.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RHAM_RSw = RStimHAM.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);
        RHAM_RSwR = resample(RHAM_RSw,DataLenRSw,size(LLHAM_RSw,1));

        RVAS_RSw = RStimVAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RGAS_RSw = RStimGAS.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RSOL_RSw = RStimSOL.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        RTA_RSw = RStimTA.Data(RswingStart_ind(i):RgaitEnd_ind(i),:);

        LL_HFLDataRSw(indStart_RSw:indEnd_RSw,:) = LLHFL_RSw;
        LF_GLUDataRSw(indStart_RSw:indEnd_RSw,:) = LFGLU_RSw;
        LL_HAMDataRSw(indStart_RSw:indEnd_RSw,:) = LLHAM_RSw;
        LF_HAMDataRSw(indStart_RSw:indEnd_RSw,:) = LFHAM_RSw;
        LF_VASDataRSw(indStart_RSw:indEnd_RSw,:) = LFVAS_RSw;
        LF_GASDataRSw(indStart_RSw:indEnd_RSw,:) = LFGAS_RSw;
        LF_SOLDataRSw(indStart_RSw:indEnd_RSw,:) = LFSOL_RSw;
        LL_TADataRSw(indStart_RSw:indEnd_RSw,:) = LLTA_RSw;

        TrunkDataRSw(indStart_RSw:indEnd_RSw,:) = Trunk_RSw;

        RL_HFLDataRSw(indStart_RSw:indEnd_RSw,:) = RLHFL_RSw;
        RF_GLUDataRSw(indStart_RSw:indEnd_RSw,:) = RFGLU_RSw;
        RL_HAMDataRSw(indStart_RSw:indEnd_RSw,:) = RLHAM_RSw;
        RF_HAMDataRSw(indStart_RSw:indEnd_RSw,:) = RFHAM_RSw;
        RF_VASDataRSw(indStart_RSw:indEnd_RSw,:) = RFVAS_RSw;
        RF_GASDataRSw(indStart_RSw:indEnd_RSw,:) = RFGAS_RSw;
        RF_SOLDataRSw(indStart_RSw:indEnd_RSw,:) = RFSOL_RSw;
        RL_TADataRSw(indStart_RSw:indEnd_RSw,:) = RLTA_RSw;

        dTrunkDataRSw(indStart_RSw:indEnd_RSw,:) = dTrunk_RSw;

        RHFLDataRSw(indStart_RSw:indEnd_RSw,:) = RHF_RSw;
        RGLUDataRSw(indStart_RSw:indEnd_RSw,:) = RGLU_RSw;
        RHAMDataRSw(indStart_RSw:indEnd_RSw,:) = RHAM_RSw;
        RVASDataRSw(indStart_RSw:indEnd_RSw,:) = RVAS_RSw;
        RGASDataRSw(indStart_RSw:indEnd_RSw,:) = RGAS_RSw;
        RSOLDataRSw(indStart_RSw:indEnd_RSw,:) = RSOL_RSw;
        RTADataRSw(indStart_RSw:indEnd_RSw,:) = RTA_RSw;

        indStart_RSw = indEnd_RSw+1;
        
    end
end

%% Normalize trunk data
dtrunkMax = max([max(mean(dTrunkDataLSt)),max(mean(dTrunkDataRSt)),max(mean(dTrunkDataLSw)),max(mean(dTrunkDataRSw))]);
trunkMax = max([max(mean(TrunkDataLSt)),max(mean(TrunkDataRSt)),max(mean(TrunkDataLSw)),max(mean(TrunkDataRSw))]);

%% LSt Data
% collect mean muscle features over gaits (only training)
FeatDataLSt = [LL_HFLDataLSt LF_GLUDataLSt LL_HAMDataLSt LF_HAMDataLSt LF_VASDataLSt LF_GASDataLSt LF_SOLDataLSt LL_TADataLSt TrunkDataLSt./trunkMax ...
            RL_HFLDataLSt RF_GLUDataLSt RL_HAMDataLSt RF_HAMDataLSt RF_VASDataLSt RF_GASDataLSt RF_SOLDataLSt RL_TADataLSt dTrunkDataLSt./dtrunkMax];
        

% collect mean muscles over gaits (only training)
MusDataLSt = [LHFLDataLSt LGLUDataLSt LHAMDataLSt LVASDataLSt LGASDataLSt LSOLDataLSt LTADataLSt];

%% RSt Data
% collect mean muscle features over gaits (only training)
FeatDataRSt = [RL_HFLDataRSt RF_GLUDataRSt RL_HAMDataRSt RF_HAMDataRSt RF_VASDataRSt RF_GASDataRSt RF_SOLDataRSt RL_TADataRSt TrunkDataRSt./trunkMax...
    LL_HFLDataRSt LF_GLUDataRSt LL_HAMDataRSt LF_HAMDataRSt LF_VASDataRSt LF_GASDataRSt LF_SOLDataRSt LL_TADataRSt dTrunkDataRSt./dtrunkMax];


MusDataRSt =  [RHFLDataRSt RGLUDataRSt RHAMDataRSt RVASDataRSt RGASDataRSt RSOLDataRSt RTADataRSt];


%% LSw Data
% collect mean muscle features over gaits (only training)
FeatDataLSw = [LL_HFLDataLSw LF_GLUDataLSw LL_HAMDataLSw LF_HAMDataLSw LF_VASDataLSw LF_GASDataLSw LF_SOLDataLSw LL_TADataLSw TrunkDataLSw./trunkMax ...
            RL_HFLDataLSw RF_GLUDataLSw RL_HAMDataLSw RF_HAMDataLSw RF_VASDataLSw RF_GASDataLSw RF_SOLDataLSw RL_TADataLSw dTrunkDataLSw./dtrunkMax];
        
        
% collect mean muscles over gaits (only training)
MusDataLSw = [LHFLDataLSw LGLUDataLSw LHAMDataLSw LVASDataLSw LGASDataLSw LSOLDataLSw LTADataLSw];

%% RSw Data
% collect mean muscle features over gaits (only training)
FeatDataRSw = [RL_HFLDataRSw RF_GLUDataRSw RL_HAMDataRSw RF_HAMDataRSw RF_VASDataRSw RF_GASDataRSw RF_SOLDataRSw RL_TADataRSw TrunkDataRSw./trunkMax...
    LL_HFLDataRSw LF_GLUDataRSw LL_HAMDataRSw LF_HAMDataRSw LF_VASDataRSw LF_GASDataRSw LF_SOLDataRSw LL_TADataRSw dTrunkDataRSw./dtrunkMax];


MusDataRSw =[RHFLDataRSw RGLUDataRSw RHAMDataRSw RVASDataRSw RGASDataRSw RSOLDataRSw RTADataRSw];