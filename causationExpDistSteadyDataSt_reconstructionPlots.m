%% reconstruction for causation experiment data

close all;
clear all;
clc;

load('causExpDistWaitSt_musFeatNormTrk_distSteadyData_F5.mat')

disturb_ijk = size(WStore);

% steady data
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mMusDataLSt');
load ('linRel_13ms_NMwalking_100sTrainTestData_musFeatNORMStSw_v2_RfeatFlip.mat','mFeatDataLSt');

%% 

for i =1:disturb_ijk(1)

    for j = 1:disturb_ijk(2)
        
        for k = 1:disturb_ijk(3)
            
            trueMus = [musDistDataStore{i,j,k} mMusDataLSt];
            testMus = [WStore{i,j,k}(:,1:18)*featDistDataStore{i,j,k} WStore{i,j,k}(:,1:18)*mFeatDataLSt];
            
            % resample undisturbed data to match length of disturbed data
            mFeatDataLSt_r=resample(mFeatDataLSt',size(featDistDataStore{i,j,k},2),size(mFeatDataLSt,2))';
            mMusDataLSt_r=resample(mMusDataLSt',size(musDistDataStore{i,j,k},2),size(mMusDataLSt,2))';
            
            % subtracting steady state motion from the disturbed motion
            del_mMusDataLSt = musDistDataStore{i,j,k} - mMusDataLSt_r;
            del_mFeatDataLSt = featDistDataStore{i,j,k} - mFeatDataLSt_r;
            
            trueMus2 = [del_mMusDataLSt mMusDataLSt];
            testMus2 = [WStore{i,j,k}(:,19:36)*del_mFeatDataLSt WStore{i,j,k}(:,19:36)*mFeatDataLSt];
            
            figure; axis([0,length(testMus),0,0.4])
            subplot(7,1,1); plot(testMus(1,:)');hold on
            subplot(7,1,1); plot(trueMus(1,:)');
            title('Left muscles [Dist Steady] STANCE reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
            legend('Reconstructed-sparse','TrueData');
            
            subplot(7,1,2); plot(testMus(2,:)');hold on;axis([0,length(testMus),0,0.6])
            subplot(7,1,2); plot(trueMus(2,:)');ylabel('LGLU');
            
            subplot(7,1,3); plot(testMus(3,:)');hold on;axis([0,length(testMus),0,0.6])
            subplot(7,1,3); plot(trueMus(3,:)');ylabel('LHAM');
            
            subplot(7,1,4); plot(testMus(4,:)');hold on;axis([0,length(testMus),0,0.6])
            subplot(7,1,4); plot(trueMus(4,:)');ylabel('LVAS');
            
            subplot(7,1,5); plot(testMus(5,:)');hold on;axis([0,length(testMus),0,0.8])
            subplot(7,1,5); plot(trueMus(5,:)');ylabel('LGAS');
            
            subplot(7,1,6); plot(testMus(6,:)');hold on;axis([0,length(testMus),0,0.8])
            subplot(7,1,6); plot(trueMus(6,:)');ylabel('LSOL');
            
            subplot(7,1,7); plot(testMus(7,:)');hold on;axis([0,length(testMus),0,0.6])
            subplot(7,1,7); plot(trueMus(7,:)');ylabel('LTA');
            
            
            figure; %axis([0,length(testMus),0,0.4])
            subplot(7,1,1); plot(testMus2(1,:)');hold on
            subplot(7,1,1); plot(trueMus2(1,:)');
            title('Left muscles [Del Steady] STANCE reconstructed (using 2-leg Mus Feat)'); ylabel('LHFL');
            legend('Reconstructed-sparse','TrueData');
            
            subplot(7,1,2); plot(testMus2(2,:)');hold on;%axis([0,length(testMus2),0,0.2])
            subplot(7,1,2); plot(trueMus2(2,:)');ylabel('LGLU');
            
            subplot(7,1,3); plot(testMus2(3,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,3); plot(trueMus2(3,:)');ylabel('LHAM');
            
            subplot(7,1,4); plot(testMus2(4,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,4); plot(trueMus2(4,:)');ylabel('LVAS');
            
            subplot(7,1,5); plot(testMus2(5,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,5); plot(trueMus2(5,:)');ylabel('LGAS');
            
            subplot(7,1,6); plot(testMus2(6,:)');hold on;%axis([0,length(testMus2),0,0.1])
            subplot(7,1,6); plot(trueMus2(6,:)');ylabel('LSOL');
            
            subplot(7,1,7); plot(testMus2(7,:)');hold on;%axis([0,length(testMus2),0,0.4])
            subplot(7,1,7); plot(trueMus2(7,:)');ylabel('LTA');
            
            
            keyboard
        end
    end
end
    

