function [R2] = rsquared(true,test)

ybar = mean(true');

res_sos = sum((true-test).^2'); % residual sum of square
tot_sos = sum(bsxfun(@minus,true,ybar').^2'); % total sum of square
% same as tot_sos = (length(true)-1)*var(true);

R2 = ones(1,size(true,1))-res_sos./tot_sos;

end