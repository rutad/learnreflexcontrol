% Relate Mus feat data to muscle stimulation
clc;
close all;
clear all;

load linRel_13ms_NMwalking_100sTrainTestData_musFeatStSwWindows_RfeatFlip.mat;

labels_ry = {'RHFL','RGLU','RHAM','RVAS','RGAS','RSOL','RTA'};
labels_rx = {'RL-HFL','RF-GLU','RL-HAM','RF-HAM','RF-VAS','RF-GAS','RF-SOL','RL-TA','Trunk','LL-HFL','LF-GLU','LL-HAM','LF-HAM','LF-VAS','LF-GAS','LF-SOL',...
    'LL-TA','dTrunk'};

%%

for z = 1:window_num
    
    %%%%%%%%% RIGHT %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    mFeatDataRSt = mallDataRSt{z,1};
    mMusDataRSt = mallDataRSt{z,2};
    
    mFeatDataRSw = mallDataRSw{z,1};
    mMusDataRSw = mallDataRSw{z,2};
    
    % only least square solution using pinv
    wp_RSt = mMusDataRSt*pinv(mFeatDataRSt); % wp is 14X14
    wp_RSw = mMusDataRSw*pinv(mFeatDataRSw); % wp is 14X14
    
    % l1-least square
    wRSt = NaN(size(wp_RSt));
    wRSw = NaN(size(wp_RSw));
    
    ARSt = mFeatDataRSt';
    ARSw = mFeatDataRSw';
    lambdaSt = 0.7; % l1 cost weight
    %lambdaSw = 0.1; % l1 cost weight
    
    for i = 1:7
        BRSt = mMusDataRSt(i,:)';
        
        %[lambda_max] = find_lambdamax_l1_ls(ARSt',BRSt);
        %lambdaSt = lambda_max*1e-3
        
        [xRSt,~] = l1_ls(ARSt,BRSt,lambdaSt);
        wRSt(i,:) = xRSt';
        
        BRSw = mMusDataRSw(i,:)';
        
        [lambda_max] = find_lambdamax_l1_ls(ARSw',BRSw);
        lambdaSw = lambda_max*1e-3;
        
        [xRSw,~] = l1_ls(ARSw,BRSw,lambdaSw);
        wRSw(i,:) = xRSw';
    end
    
    %%%%%%%%%%% Visualize w %%%%%%%%%%%%
    %%% right stance w
%     figure;imagesc(abs(wp_RSt)/(max(max(abs(wp_RSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-leastSq');
%     set(gca(),'YTick',1:7)
%     set(gca(),'YTickLabel',labels_ry);
%     set(gca(),'XTick',1:18)
%     set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
    
    figure;imagesc(abs(wRSt)/(max(max(abs(wRSt)))));colorbar;title('Leg weights RIGHT STANCE (2-leg Mus Feat Data)-sparse');
    set(gca(),'YTick',1:7)
    set(gca(),'YTickLabel',labels_ry);
    set(gca(),'XTick',1:18)
    set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
    
    %%% right swing w
%     figure;imagesc(abs(wp_RSw)/(max(max(abs(wp_RSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-leastSq');
%     set(gca(),'YTick',1:7)
%     set(gca(),'YTickLabel',labels_ry);
%     set(gca(),'XTick',1:18)
%     set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);
    
    figure;imagesc(abs(wRSw)/(max(max(abs(wRSw)))));colorbar;title('Leg weights RIGHT SWING (2-leg Mus Feat Data)-sparse');
    set(gca(),'YTick',1:7)
    set(gca(),'YTickLabel',labels_ry);
    set(gca(),'XTick',1:18)
    set(gca(),'XTickLabel',labels_rx, 'XTickLabelRotation', 90);

    keyboard
    
end    